import bundleAnalyzer from '@next/bundle-analyzer'

const withBundleAnalyzer = bundleAnalyzer({
  enabled: process.env.ANALYZE_BUNDLE === 'true',
})

export default withBundleAnalyzer({
  basePath: '',
  rewrites: () => [
    { source: '/api/:path*', destination: '/api/:path*' },
    { source: '/backend/:path*', destination: '/backend/:path*' },
    { source: '/:path*', destination: '/frontend/:path*' },
  ],
  transpilePackages: ['@package/bug'],
  compiler: {
    styledComponents: true,
  },
  eslint: {
    // already executed during the pre-push git hook
    // and too memory expensive on server
    ignoreDuringBuilds: true,
  },
  // same as above:
  typescript: { ignoreBuildErrors: true },
  webpack(config) {
    // see https://github.com/Automattic/mongoose/issues/13402#issuecomment-1548826056
    Object.assign(config.resolve.alias, {
      '@mongodb-js/zstd': false,
      '@aws-sdk/credential-providers': false,
      snappy: false,
      aws4: false,
      'mongodb-client-encryption': false,
      kerberos: false,
      'supports-color': false,
    })
    return config
  },
})
