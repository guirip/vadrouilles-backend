#!/bin/sh

goaccess /var/log/apache2/www.vadrouilles.fr-access.log -o /var/www/html/reports/vadrouilles/$(date +"%Y_%m_%d").html --log-format=COMBINED --html-prefs='{"theme":"dark-purple","perPage":24,"layout":"vertical","showTables":true,"visitors":{"plot":{"chartType":"bar"}}}'

