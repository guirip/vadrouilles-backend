#!/bin/sh

DATE_LABEL=$(date +"%Y_%m_%d-%Hh%Mm%S")
SRC_DIR=/var/www/vadrouilles.fr/static-files
#SRC_DIR=/opt/vadrouilles-backend-production-static
DEST_DIR=/opt/BACKUPS/vadrouilles.fr
DUMP_NAME=$DATE_LABEL"_vadrouilles_prod_assets"
ARCHIVE_NAME=$DUMP_NAME.tar.gz

echo "Creating archive.... (can be long)"

tar czf $DEST_DIR/$ARCHIVE_NAME $SRC_DIR && \

echo
echo Archive available:
echo "${DEST_DIR}/${ARCHIVE_NAME}"

echo
echo Done


