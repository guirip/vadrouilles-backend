import {
  STANDARD_MARGIN,
  computeSize,
} from 'src/app/frontend/_components/video/sizeHook'

describe('frontend / components / video / sizeHook', function () {
  describe('computeSize', function () {
    describe('1080p', function () {
      it('should return 1920x1080 when screen width>1920 (✓) and height>1080 (✓)', function () {
        jest
          .spyOn(document.documentElement, 'clientWidth', 'get')
          .mockImplementationOnce(() => 1960) // ✓
        jest
          .spyOn(document.documentElement, 'clientHeight', 'get')
          .mockImplementationOnce(() => 1280) // ✓

        const { width, height } = computeSize()
        expect(width).toBe(1920 + STANDARD_MARGIN)
        expect(height).toBe(1080 + STANDARD_MARGIN)
      })
    })

    describe('720p', function () {
      it('should return 1280x720 when screen width>1920 (✓) and height<1080 (✕)', function () {
        jest
          .spyOn(document.documentElement, 'clientWidth', 'get')
          .mockImplementationOnce(() => 1960) // ✓
        jest
          .spyOn(document.documentElement, 'clientHeight', 'get')
          .mockImplementationOnce(() => 1060) // ✕

        const { width, height } = computeSize()
        expect(width).toBe(1280 + STANDARD_MARGIN)
        expect(height).toBe(720 + STANDARD_MARGIN)
      })

      it('should return 1280x720 when screen width<1920 (✕) and height>1080 (✓)', function () {
        jest
          .spyOn(document.documentElement, 'clientWidth', 'get')
          .mockImplementationOnce(() => 1900) // ✕
        jest
          .spyOn(document.documentElement, 'clientHeight', 'get')
          .mockImplementationOnce(() => 1280) // ✓

        const { width, height } = computeSize()
        expect(width).toBe(1280 + STANDARD_MARGIN)
        expect(height).toBe(720 + STANDARD_MARGIN)
      })

      it('should return 1280x720 when screen width<1920 (✕) and height<1080 (✕)', function () {
        jest
          .spyOn(document.documentElement, 'clientWidth', 'get')
          .mockImplementationOnce(() => 1900) // ✕
        jest
          .spyOn(document.documentElement, 'clientHeight', 'get')
          .mockImplementationOnce(() => 1280) // ✕

        const { width, height } = computeSize()
        expect(width).toBe(1280 + STANDARD_MARGIN)
        expect(height).toBe(720 + STANDARD_MARGIN)
      })
    })

    describe('854x480', function () {
      it('should return 854x480 when screen width<1280 (✕) and height>720 (✓)', function () {
        jest
          .spyOn(document.documentElement, 'clientWidth', 'get')
          .mockImplementationOnce(() => 1024) // ✕
        jest
          .spyOn(document.documentElement, 'clientHeight', 'get')
          .mockImplementationOnce(() => 900) // ✓

        const { width, height } = computeSize()
        expect(width).toBe(854 + STANDARD_MARGIN)
        expect(height).toBe(480 + STANDARD_MARGIN)
      })

      it('should return 854x480 when screen width>1280 (✓) and height<720 (✕)', function () {
        jest
          .spyOn(document.documentElement, 'clientWidth', 'get')
          .mockImplementationOnce(() => 1440) // ✓
        jest
          .spyOn(document.documentElement, 'clientHeight', 'get')
          .mockImplementationOnce(() => 700) // ✕

        const { width, height } = computeSize()
        expect(width).toBe(854 + STANDARD_MARGIN)
        expect(height).toBe(480 + STANDARD_MARGIN)
      })

      it('should return 854x480 when screen width<1280 (✕) and height<720 (✕)', function () {
        jest
          .spyOn(document.documentElement, 'clientWidth', 'get')
          .mockImplementationOnce(() => 1200) // ✕
        jest
          .spyOn(document.documentElement, 'clientHeight', 'get')
          .mockImplementationOnce(() => 700) // ✕

        const { width, height } = computeSize()
        expect(width).toBe(854 + STANDARD_MARGIN)
        expect(height).toBe(480 + STANDARD_MARGIN)
      })
    })

    describe('640x360', function () {
      it('should return 640x360 when screen width<854 (✕) and height>480 (✓)', function () {
        jest
          .spyOn(document.documentElement, 'clientWidth', 'get')
          .mockImplementationOnce(() => 850) // ✕
        jest
          .spyOn(document.documentElement, 'clientHeight', 'get')
          .mockImplementationOnce(() => 900) // ✓

        const { width, height } = computeSize()
        expect(width).toBe(640 + STANDARD_MARGIN)
        expect(height).toBe(360 + STANDARD_MARGIN)
      })

      it('should return 640x360 when screen width>854 (✓) and height<480 (✕)', function () {
        jest
          .spyOn(document.documentElement, 'clientWidth', 'get')
          .mockImplementationOnce(() => 960) // ✓
        jest
          .spyOn(document.documentElement, 'clientHeight', 'get')
          .mockImplementationOnce(() => 460) // ✕

        const { width, height } = computeSize()
        expect(width).toBe(640 + STANDARD_MARGIN)
        expect(height).toBe(360 + STANDARD_MARGIN)
      })

      it('should return 640x360 when screen width<854 (✕) and height<480 (✕)', function () {
        jest
          .spyOn(document.documentElement, 'clientWidth', 'get')
          .mockImplementationOnce(() => 850) // ✕
        jest
          .spyOn(document.documentElement, 'clientHeight', 'get')
          .mockImplementationOnce(() => 440) // ✕

        const { width, height } = computeSize()
        expect(width).toBe(640 + STANDARD_MARGIN)
        expect(height).toBe(360 + STANDARD_MARGIN)
      })
    })

    describe('426x240', function () {
      it('should return 426x240 when screen width<640 (✕) and height>360 (✓)', function () {
        jest
          .spyOn(document.documentElement, 'clientWidth', 'get')
          .mockImplementationOnce(() => 520) // ✕
        jest
          .spyOn(document.documentElement, 'clientHeight', 'get')
          .mockImplementationOnce(() => 1000) // ✓

        const { width, height } = computeSize()
        expect(width).toBe(426 + STANDARD_MARGIN)
        expect(height).toBe(240 + STANDARD_MARGIN)
      })

      it('should return 426x240 when screen width>640 (✓) and height<360 (✕)', function () {
        jest
          .spyOn(document.documentElement, 'clientWidth', 'get')
          .mockImplementationOnce(() => 1460) // ✓
        jest
          .spyOn(document.documentElement, 'clientHeight', 'get')
          .mockImplementationOnce(() => 350) // ✕

        const { width, height } = computeSize()
        expect(width).toBe(426 + STANDARD_MARGIN)
        expect(height).toBe(240 + STANDARD_MARGIN)
      })
    })
  })
})
