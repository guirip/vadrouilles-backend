import type { IFile } from './common/File.type'

export interface IUploadResponse {
  outputs: IFile[]
  errors?: string[]
}
