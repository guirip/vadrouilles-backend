import type { ICatalogItemCategoryFront } from './CatalogItemCategory.type'
import type { IDbStringId } from './common/DbItemLean'
import type { IFile } from './common/File.type'

export enum ItemStatus {
  Available = 'available',
  Booked = 'booked',
  Sold = 'sold',
}

export const ITEM_DEFAULT_STATUS = ItemStatus.Available
export const MAX_FILE_SIZE = 10000000

export interface IBaseCatalogItem {
  status: ItemStatus
  visibility: boolean
  price: number
  order: number
  title: string
  description?: string
  technique?: string
  dimensions?: string
  weight?: string
  createDate?: Date | null
  updateDate?: Date | null
  files?: IFile[]
}

export interface ICatalogItemFront extends IBaseCatalogItem, IDbStringId {
  category?: ICatalogItemCategoryFront['_id']
}

export interface IItemsPerCategoryFront {
  [key: ICatalogItemCategoryFront['_id']]: ICatalogItemFront[]
}
