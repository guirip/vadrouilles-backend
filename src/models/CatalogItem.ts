import type { Model } from 'mongoose'
import { Schema } from 'mongoose'
import type {
  ICatalogItemCategory,
  ICatalogItemCategoryLean,
} from './CatalogItemCategory'
import { CatalogItemCategoryModel } from './CatalogItemCategory'
import type { IDbItem } from './common/DbItem'
import type { IDbStringId } from './common/DbItemLean'
import { fileSchema } from './common/File'
import { getModel } from './common/util'
import type { IBaseCatalogItem } from './CatalogItem.type'
import { ItemStatus, ITEM_DEFAULT_STATUS } from './CatalogItem.type'

export interface ICatalogItem extends IBaseCatalogItem, IDbItem {
  category?: ICatalogItemCategory['_id']
}

export interface ICatalogItemPopulated extends IBaseCatalogItem, IDbItem {
  category?: ICatalogItemCategory
}

export interface ICatalogItemLean extends IBaseCatalogItem, IDbStringId {
  category?: ICatalogItemCategoryLean['_id']
}

export interface IItemsPerCategory {
  [key: ICatalogItemCategoryLean['_id']]: ICatalogItemLean[]
}

const schema = new Schema<ICatalogItem>({
  status: {
    type: String,
    enum: Object.values(ItemStatus),
    default: ITEM_DEFAULT_STATUS,
  },
  visibility: {
    type: Boolean,
    required: true,
  },
  price: {
    type: Number,
    required: true,
  },
  title: {
    type: String,
    required: true,
  },
  description: String,
  order: Number,
  technique: String,
  dimensions: String,
  weight: String,
  createDate: Date,
  updateDate: Date,
  files: [fileSchema],
  category: {
    type: Schema.Types.ObjectId,
    ref: CatalogItemCategoryModel,
  },
})

export const CatalogItemModel: Model<ICatalogItem> = getModel<ICatalogItem>(
  'CatalogItem',
  schema
)
