import type { Model } from 'mongoose'
import { Schema } from 'mongoose'
import type { IDbItem } from './common/DbItem'
import type { IDbStringId } from './common/DbItemLean'
import { i18nLabelSchema } from './common/I18nLabel'
import { getModel } from './common/util'
import type { IBaseCatalogItemCategory } from './CatalogItemCategory.type'

export interface ICatalogItemCategory
  extends IBaseCatalogItemCategory,
    IDbItem {}

export interface ICatalogItemCategoryLean
  extends IBaseCatalogItemCategory,
    IDbStringId {}

export const catalogItemCategorySchema = new Schema<ICatalogItemCategory>({
  name: {
    type: i18nLabelSchema,
    default: {
      en: '?',
      fr: '?',
    },
  },
  slug: {
    type: String,
    required: true,
  },
  order: {
    type: Number,
    default: 0,
  },
  visibility: {
    type: Boolean,
    default: false,
  },
  isDefault: {
    type: Boolean,
    default: false,
  },
})

export const CatalogItemCategoryModel: Model<ICatalogItemCategory> =
  getModel<ICatalogItemCategory>(
    'CatalogItemCategory',
    catalogItemCategorySchema
  )
