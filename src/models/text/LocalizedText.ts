import type { Model } from 'mongoose'
import { Schema } from 'mongoose'
import type { IDbItem } from '../common/DbItem'
import { SUPPORTED_LANG } from '../common/Lang'
import { getModel } from '../common/util'
import type {
  IBaseLocalizedText,
  IBaseLocalizedTextMinimal,
} from './LocalizedText.type'

export type ILocalizedText = IBaseLocalizedText & IDbItem

// for backend: minimal version (without content)
export type ILocalizedTextMinimal = IBaseLocalizedTextMinimal & IDbItem

const schemaDef = {
  title: {
    type: String,
    required: true,
  },
  lang: {
    type: String,
    enum: SUPPORTED_LANG,
    required: true,
  },
  description: {
    type: String,
  },
  content: {
    type: Schema.Types.Mixed,
    default: {},
  },
}

export const localizedTextSchema = new Schema<ILocalizedText>(schemaDef)

export const LocalizedTextModel: Model<ILocalizedText> =
  getModel<ILocalizedText>('LocalizedText', localizedTextSchema)
