import type { IDbStringId } from '../common/DbItemLean'
import type {
  ILocalizedTextMinimalFront,
  ILocalizedTextFront,
} from './LocalizedText.type'

export interface IBaseTextChapter {
  slug: string
  visible: boolean
  order: number
}

export type IBaseTextChapterFront = IBaseTextChapter & IDbStringId

export interface ITextChapterMinimalPopulatedFront
  extends IBaseTextChapterFront {
  localizedTexts: ILocalizedTextMinimalFront[]
}
export interface ITextChapterPopulatedFront extends IBaseTextChapterFront {
  localizedTexts: ILocalizedTextFront[]
}
