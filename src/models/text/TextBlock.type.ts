import type { Metadata } from 'next'
import type { IFile } from '../common/File.type'
import type { IVideo } from '../common/Video'

export enum ToolType {
  Marker = 'marker',
  Underline = 'underline',
}

export enum Tunes {
  TextVariant = 'textVariant',
  Alignment = 'alignmentTune',
}

export enum TextVariantTune {
  NONE = '',
  CALL_OUT = 'call-out',
  CITATION = 'citation',
  DETAILS = 'details',
}

export enum AlignmentTune {
  LEFT = 'left',
  CENTER = 'center',
  RIGHT = 'right',
}

export enum BlockType {
  Button = 'button',
  Delimiter = 'delimiter',
  ExistingImage = 'existing-image',
  Gallery = 'gallery',
  Header = 'header',
  Image = 'image',
  List = 'list',
  Paragraph = 'paragraph',
  Social = 'social',
  Spacer = 'spacer',
  Summary = 'summary',
  Table = 'table',
  TextReference = 'text-reference',
  Video = 'video',
}

export interface IBlock {
  id?: string
  type: BlockType
  data: unknown
}

const buttonThemes = ['primary', 'secondary', 'callout', 'muted'] as const
export type ButtonTheme = (typeof buttonThemes)[number]
export const getButtonThemes = () => buttonThemes.map((type) => String(type))

export interface IButtonBlock extends IBlock {
  type: BlockType.Button
  data: {
    type: 'link'
    target?: '_self' | '_blank'
    value?: string
    label: string
    icon: string
    style: {
      size: number
      mx: number
      my: number
      px: number
      py: number
      theme: ButtonTheme
    }
  }
}

export interface IDelimiterBlock extends IBlock {
  type: BlockType.Delimiter
}

export interface IGalleryBlock extends IBlock {
  type: BlockType.Gallery
  data: {
    showThumbnails?: boolean
    visuals?: (IImageBlock | IVideoBlock)[] // front-end only
  }
}

export interface IHeaderBlock extends IBlock {
  type: BlockType.Header
  data: {
    text: string
    level: number
  }
  tunes?: {
    alignmentTune?: { alignment: AlignmentTune }
  }
}

export interface ITableBlock extends IBlock {
  type: BlockType.Table
  data: {
    withHeadings: boolean
    content: string[][]
  }
}

export interface ISummaryBlock extends IBlock {
  type: BlockType.Summary
  data: {
    headers: IHeaderBlock[]
  }
}

export interface IImageBlock extends IBlock {
  type: BlockType.Image
  data: {
    caption: string
    file: IFile
    stretched: boolean
    withBackground: boolean
    withBorder: boolean
  }
}

export interface INestedListItem {
  content: string
  items: INestedListItem[]
}

export interface IListBlock extends IBlock {
  type: BlockType.List
  data: {
    items: INestedListItem[]
    style: 'ordered' | 'unordered'
  }
}

export interface IParagraphBlock extends IBlock {
  type: BlockType.Paragraph
  data: {
    text: string
  }
  tunes: {
    textVariant: TextVariantTune
    alignmentTune?: { alignment: AlignmentTune }
  }
}

export interface ITextReferenceBlock extends IBlock {
  type: BlockType.Spacer
  data: {
    textPageId: string
    chapterId: string | null
    showPageTitle?: boolean
    showPageSubtitle?: boolean
    coverPosition?: string
    metadata?: Metadata // front-end only
  }
}

const socialTypes = ['instagram'] as const
export type SocialType = (typeof socialTypes)[number]
export const getSocialTypes = () => socialTypes.map((type) => String(type))

export const isSocialType = (value: unknown): value is SocialType =>
  !!(
    typeof value === 'string' &&
    value &&
    socialTypes.find((v) => String(v) === value)
  )

export interface ISocialBlock extends IBlock {
  type: BlockType.Social
  data: {
    type: SocialType
    url: string
  }
}

export interface ISpacerBlock extends IBlock {
  type: BlockType.Spacer
  data: {
    value: number
  }
}

export interface IVideoBlock extends IBlock {
  type: BlockType.Video
  data: IVideo
}

export interface IExistingImageBlock extends IBlock {
  type: BlockType.ExistingImage
  data: null | IImageBlock
}

export const isButtonBlock = (block: IBlock): block is IButtonBlock =>
  block.type === BlockType.Button

export const isDelimiterBlock = (block: IBlock): block is IDelimiterBlock =>
  block.type === BlockType.Delimiter

export const isGalleryBlock = (block: IBlock): block is IGalleryBlock =>
  block.type === BlockType.Gallery

export const isHeaderBlock = (block: IBlock): block is IHeaderBlock =>
  block.type === BlockType.Header

export const isImageBlock = (block: IBlock): block is IImageBlock =>
  block.type === BlockType.Image

export const isListBlock = (block: IBlock): block is IListBlock =>
  block.type === BlockType.List

export const isParagraphBlock = (block: IBlock): block is IParagraphBlock =>
  block.type === BlockType.Paragraph

export const isSocialBlock = (block: IBlock): block is ISocialBlock =>
  block.type === BlockType.Social

export const isSpacerBlock = (block: IBlock): block is ISpacerBlock =>
  block.type === BlockType.Spacer

export const isSummaryBlock = (block: IBlock): block is ISummaryBlock =>
  block.type === BlockType.Summary

export const isTableBlock = (block: IBlock): block is ITableBlock =>
  block.type === BlockType.Table

export const isTextReferenceBlock = (
  block: IBlock
): block is ITextReferenceBlock => block.type === BlockType.TextReference

export const isVideoBlock = (block: IBlock): block is IVideoBlock =>
  block.type === BlockType.Video

export const isExistingImageBlock = (
  block: IBlock
): block is IExistingImageBlock => block.type === BlockType.ExistingImage
