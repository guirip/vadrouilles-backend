import type { Model, Types } from 'mongoose'
import { Schema } from 'mongoose'
import { ContentCategory } from '../common/ContentCategory'
import type { IDbItem } from '../common/DbItem'
import { i18nLabelSchema } from '../common/I18nLabel'
import { getModel } from '../common/util'
import type { ITextChapterPopulated } from './TextChapter'
import { TextChapterModel } from './TextChapter'
import type { IBaseTextPage } from './TextPage.type'
import { fileSchema } from '../common/File'

export interface ITextPage extends IBaseTextPage, IDbItem {
  chapters: Types.ObjectId[]
}

// for backend: iso DB + chapters populated
export type ITextPagePopulated = IBaseTextPage &
  IDbItem & {
    chapters: ITextChapterPopulated[]
  }

const schemaDef = {
  slug: {
    type: String,
    unique: true,
    required: true,
  },
  title: {
    type: i18nLabelSchema,
    required: true,
  },
  subtitle: {
    type: i18nLabelSchema,
    required: true,
  },
  category: {
    type: String,
    enum: Object.values(ContentCategory),
    required: true,
  },
  visible: {
    type: Boolean,
    default: false,
  },
  chapters: [
    {
      type: Schema.Types.ObjectId,
      ref: TextChapterModel,
    },
  ],
  description: {
    type: i18nLabelSchema,
  },
  longDescription: {
    type: i18nLabelSchema,
  },
  order: {
    type: Number,
    default: 0,
  },
  cover: {
    type: fileSchema,
  },
}

const schema = new Schema<ITextPage>(schemaDef)

async function checkDuplicateChapterSlug(chapters: ITextPage['chapters']) {
  if (!Array.isArray(chapters) || chapters.length < 2) {
    return true
  }
  const chapterSlugs = (
    await TextChapterModel.find(
      { _id: { $in: chapters.map(({ _id }) => _id) } },
      'slug',
      { lean: true }
    )
  ).map(({ slug }) => slug)

  for (let i = 0; i < chapterSlugs.length; i++) {
    const slug = chapterSlugs[i]
    if (chapterSlugs.includes(slug, i + 1)) {
      throw new Error(
        `TextPage already includes a chapter whose slug is '${slug}'`
      )
    }
  }
  return true
}

schema.pre('save', async function () {
  await checkDuplicateChapterSlug(this.chapters ?? [])
})

export const TextPageModel: Model<ITextPage> = getModel<ITextPage>(
  'TextPage',
  schema
)
