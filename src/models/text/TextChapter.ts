import type { HydratedDocument, Model, Types } from 'mongoose'
import { Schema } from 'mongoose'
import type { IDbItem } from '../common/DbItem'
import { getModel } from '../common/util'
import type { ILocalizedText, ILocalizedTextMinimal } from './LocalizedText'
import { LocalizedTextModel } from './LocalizedText'
import type { IBaseTextChapter } from './TextChapter.type'

export interface ITextChapter extends IBaseTextChapter, IDbItem {
  localizedTexts: Types.ObjectId[]
}

// for backend: chapter + populated (object ids)
export type ITextChapterPopulated = ITextChapter & {
  localizedTexts: HydratedDocument<ILocalizedText>[]
}

// for backend: chapter + populated (object ids) - lightweight (content removed)
export interface ITextChapterMinimalPopulated
  extends IBaseTextChapter,
    IDbItem {
  localizedTexts: HydratedDocument<ILocalizedTextMinimal>[]
}

const schemaDef = {
  slug: {
    type: String,
    required: true,
  },
  visible: {
    type: Boolean,
    default: false,
  },
  order: Number,
  localizedTexts: [
    {
      type: Schema.Types.ObjectId,
      ref: LocalizedTextModel,
    },
  ],
}

const schema = new Schema<ITextChapter>(schemaDef)

export const TextChapterModel: Model<ITextChapter> = getModel<ITextChapter>(
  'TextChapter',
  schema
)
