import { ContentCategory } from '../common/ContentCategory'
import type { IDbStringId } from '../common/DbItemLean'
import type { IFile } from '../common/File.type'
import type { II18nLabel } from '../common/I18nLabel'
import type { IImageBlock } from './TextBlock.type'
import type { ITextChapterMinimalPopulatedFront } from './TextChapter.type'

export const textCategories = Object.values(ContentCategory).map((c) =>
  c.toString()
)
export const isValidTextCategory = (cat: string): cat is ContentCategory =>
  textCategories.includes(cat)

export const DEFAULT_TEXT_CATEGORY = ContentCategory.NATURE

export interface IBaseTextPage {
  slug: string
  title: II18nLabel
  subtitle: II18nLabel
  visible: boolean
  description: II18nLabel
  longDescription: II18nLabel
  order: number
  category: ContentCategory
  cover?: IFile | null
}

export interface ITextPageFront extends IBaseTextPage, IDbStringId {
  chapters: IDbStringId['_id'][]
}

// for frontend: iso DB with minimally populated chapters
export type ITextPageMinimalPopulatedFront = IBaseTextPage &
  IDbStringId & {
    chapters: ITextChapterMinimalPopulatedFront[]
  }

export interface ImagesPerChapter {
  title: string
  slug: string
  order: number
  images: IImageBlock[]
}

export interface ImagesPerText {
  _id: IDbStringId['_id']
  title: {
    fr: string
    en: string
  }
  imagesByChapter: Record<IDbStringId['_id'], ImagesPerChapter>
}
