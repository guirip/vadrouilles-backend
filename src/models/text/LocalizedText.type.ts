import type { IDbStringId } from '../common/DbItemLean'
import type { Lang } from '../common/Lang'
import type { IBlock } from './TextBlock.type'

export interface ITextContent {
  time: number
  version: string
  blocks: IBlock[]
}

export const isTextContent = (value): value is ITextContent =>
  !!(value?.time && value?.version)

export interface IBaseLocalizedText {
  title: string
  lang: Lang
  description: string
  content: ITextContent
}

// for frontend: with content and string ids
export type ILocalizedTextFront = IBaseLocalizedText & IDbStringId

export type IBaseLocalizedTextMinimal = Omit<IBaseLocalizedText, 'content'> & {
  blocksCount: number
  content: Omit<IBaseLocalizedText['content'], 'blocks'>
}

// for frontend: minimal version (without content)
export type ILocalizedTextMinimalFront = IBaseLocalizedTextMinimal & IDbStringId

// for backend form
export type ILocalizedTextFormItem = Omit<ILocalizedTextMinimalFront, 'content'>
