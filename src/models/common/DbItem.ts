import type { Types } from 'mongoose'

export type StringOrObjectId = Types.ObjectId | string

export interface IDbItem {
  _id: Types.ObjectId
}
