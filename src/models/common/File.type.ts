export const FILE_FIELDS = ['orig', 'mid', 'low']

export interface IFile {
  orig: string
  mid: string
  low: string
  url?: string
  isMain?: boolean
}

export const isFile = (value: unknown): value is IFile =>
  typeof value === 'object' &&
  value !== null &&
  'mid' in value &&
  'low' in value
