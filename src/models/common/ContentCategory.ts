export enum ContentCategory {
  NATURE = 'nature',
  GRAFFITI = 'graffiti',
  ABOUT = 'about',
}

const contentCategoryValues = Object.values(ContentCategory).map((c) =>
  String(c)
)
export const isValidCategory = (value: string): value is ContentCategory =>
  contentCategoryValues.includes(value)
