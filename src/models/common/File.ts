import { Schema } from 'mongoose'
import type { IFile } from './File.type'

export const fileSchema = new Schema<IFile>(
  {
    orig: {
      type: String,
      required: true,
    },
    mid: {
      type: String,
      required: true,
    },
    low: {
      type: String,
      required: true,
    },
    isMain: {
      type: Boolean,
      default: false,
    },
  },
  { _id: false }
)
