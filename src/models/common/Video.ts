export enum VideoSource {
  Youtube = 'youtube',
  Vimeo = 'vimeo',
  Dailymotion = 'dailymotion',
}

export const VIDEO_DEFAULT_SOURCE = VideoSource.Youtube

export interface IVideo {
  value: string
  label?: string
  description: string
  source: VideoSource
  width: number | null
  height: number | null
}

export const isVideo = (item: unknown): item is IVideo =>
  !!(
    item &&
    typeof item === 'object' &&
    'value' in item &&
    'source' in item &&
    typeof item.value === 'string' &&
    typeof item.source === 'string' &&
    Object.values(VideoSource).includes(item.source as VideoSource)
  )
