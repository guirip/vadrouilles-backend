export interface IIdOrderMap {
  [key: string]: number
}
