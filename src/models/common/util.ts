import type { Schema } from 'mongoose'
import { model } from 'mongoose'

export function getModel<T>(name: string, schema: Schema) {
  try {
    // handle OverwriteModelError
    return model<T>(name)
  } catch (e) {
    return model<T>(name, schema)
  }
}

export const filterRequiredFields = (paths: Schema['paths']) =>
  Object.keys(paths).filter((key) => paths[key].isRequired)
