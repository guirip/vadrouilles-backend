export enum Lang {
  fr = 'fr',
  en = 'en',
}
export const SUPPORTED_LANG = Object.values(Lang)

export const isLang = (value: string): value is Lang =>
  SUPPORTED_LANG.map((l) => String(l)).includes(value)
