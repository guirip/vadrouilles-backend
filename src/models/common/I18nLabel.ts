import { Schema } from 'mongoose'

export interface II18nLabel {
  fr: string
  en: string
}

export const i18nLabelSchema = new Schema(
  {
    fr: String,
    en: String,
  },
  { _id: false }
)
