import config from 'src/config'
import type { IDbStringId } from './common/DbItemLean'

export enum CodifType {
  String = 'string',
  Boolean = 'boolean',
  Number = 'number',
}
export const CODIF_DEFAULT_TYPE = CodifType.String

export const NON_PUBLIC_CODIFS_NAME = [config.AUTH_CODIF_NAME]

export interface IBaseCodif {
  name: string
  type: CodifType
  value: string | number | boolean
}

export interface IStringCodif extends IBaseCodif {
  value: string
}
export interface INumberCodif extends IBaseCodif {
  value: number
}
export interface IBooleanCodif extends IBaseCodif {
  value: boolean
}

export type ICodifFront = IBaseCodif & IDbStringId

export const isStringCodif = (
  codif: IBaseCodif | null
): codif is IStringCodif => !!codif && codif.type === CodifType.String
