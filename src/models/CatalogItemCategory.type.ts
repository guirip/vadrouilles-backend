import type { II18nLabel } from './common/I18nLabel'
import type { ICatalogItemCategoryLean } from './CatalogItemCategory'

export interface IBaseCatalogItemCategory {
  name: II18nLabel
  slug: string
  order: number
  visibility: boolean
  isDefault: boolean
}

export type ICatalogItemCategoryFront = ICatalogItemCategoryLean
