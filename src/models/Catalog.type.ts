import type { IItemsPerCategoryFront } from './CatalogItem.type'
import type { ICatalogItemCategoryFront } from './CatalogItemCategory.type'

export interface ICatalogData {
  categories: ICatalogItemCategoryFront[]
  itemsPerCategory: IItemsPerCategoryFront
}
