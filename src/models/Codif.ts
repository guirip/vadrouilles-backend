import type { Model } from 'mongoose'
import { Schema } from 'mongoose'
import type { IDbItem } from './common/DbItem'
import type { IDbStringId } from './common/DbItemLean'
import { getModel } from './common/util'
import type { IBaseCodif } from './Codif.type'
import { CodifType, CODIF_DEFAULT_TYPE } from './Codif.type'

export interface ICodif extends IBaseCodif, IDbItem {}
export interface ICodifLean extends IBaseCodif, IDbStringId {}

const schema = new Schema<ICodif>({
  name: {
    type: String,
    required: true,
  },
  value: Schema.Types.Mixed,
  type: {
    type: String,
    enum: Object.values(CodifType),
    default: CODIF_DEFAULT_TYPE,
  },
})

export const CodifModel: Model<ICodif> = getModel<ICodif>('Codif', schema)
