/**
 * Simplest sort
 * @param  {*} valueA
 * @param  {*} valueB
 * @return {number} 1|-1|0
 */
export function simpleSortAsc(valueA, valueB) {
  if (valueA > valueB) {
    return 1
  }
  if (valueA < valueB) {
    return -1
  }
  return 0
}

export const simpleSortDsc = (valueA, valueB) => simpleSortAsc(valueB, valueA)
