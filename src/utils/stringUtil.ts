import { decode } from 'html-entities'
import slufifyLib from 'slugify'

const ONLY_NUMS_REGEXP = /[^0-9.]+/g

export function parseNumber(str) {
  if (typeof str === 'number') {
    return str
  }
  if (str !== 0 && !str) {
    return null
  }
  const onlyNums = str.replace(ONLY_NUMS_REGEXP, '')
  if (onlyNums === '') {
    return null
  }
  return parseInt(onlyNums, 10)
}

const SLUGIFY_OPTS = {
  lower: true,
  strict: true,
  trim: false,
}

export const slugify = (str) => slufifyLib(str, SLUGIFY_OPTS)

export const removeHtml = (str = '') =>
  decode(str.replaceAll(/(<([^>]+)>)/gi, ''))
