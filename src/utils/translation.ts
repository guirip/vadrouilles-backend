import { timer } from './util'

interface ITranslation {
  detected_source_language: string
  text: string
}

export interface IDeepLResponse {
  translations: ITranslation[]
}

export async function translate(
  text: string,
  from: 'fr' | 'en',
  to: 'fr' | 'en'
): Promise<IDeepLResponse> {
  if (!text || typeof text !== 'string') {
    return {
      translations: [],
    }
  }
  let res: Response | null = null
  let data: IDeepLResponse
  try {
    await timer(100)

    const url = `https://${process.env.DEEPL_API_DOMAIN}/translate`
    const Authorization = `DeepL-Auth-Key ${process.env.DEEPL_AUTH_KEY}`
    res = await fetch(url, {
      method: 'POST',
      headers: {
        Authorization,
      },
      body: new URLSearchParams({
        source_lang: from.toUpperCase(),
        target_lang: to.toUpperCase(),
        tag_handling: 'html',
        text,
      }),
    })
    const { headers } = res
    if (
      res.status > 399 ||
      headers.get('Content-type') !== 'application/json'
    ) {
      // e.g 429 Too Many Requests
      console.error(
        `Translate error: Status: ${res.status} ${res.statusText} - Content-type: ${headers.get('Content-type')}`
      )
    }
    data = await res.json()
  } catch (e) {
    console.error(
      `Translation failed (reponse status: ${res?.status} / content-type: ${res?.headers.get('Content-type')})`,
      e
    )
    throw e
  }
  return data
}
