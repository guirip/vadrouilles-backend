import { useCallback, useState } from 'react'

export function useBoolean(
  defaultValue: boolean
): [boolean, () => void, () => void] {
  const [booleanValue, setBooleanValue] = useState(defaultValue)

  const setTrue = useCallback(() => {
    setBooleanValue(true)
  }, [])

  const setFalse = useCallback(() => {
    setBooleanValue(false)
  }, [])

  return [booleanValue, setTrue, setFalse]
}
