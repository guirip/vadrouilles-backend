export class CustomError extends Error {
  httpErrorCode: number
}

export class BadRequestError extends CustomError {
  name = 'BadRequestError'
  message: string
  httpErrorCode = 400

  constructor(reason: string) {
    super()
    this.message = reason ?? 'bad request'
  }
}

export class NotFoundError extends CustomError {
  name = 'NotFoundError'
  message: string
  httpErrorCode = 404

  constructor(reason?: string) {
    super()
    this.message = reason ?? 'not found'
  }
}

export function getErrorMessage(e: unknown) {
  if (e instanceof CustomError) {
    return e.message
  }
  return e instanceof Error ? `${e.name} - ${e.message}` : ''
}

export const getHttpStatus = (e: unknown) =>
  e instanceof CustomError ? e.httpErrorCode : 400
