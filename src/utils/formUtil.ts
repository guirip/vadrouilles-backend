import type { ICatalogItemLean } from 'src/models/CatalogItem'
import type { ILocalizedTextMinimalFront } from 'src/models/text/LocalizedText.type'
import type { ITextChapterMinimalPopulatedFront } from 'src/models/text/TextChapter.type'
import type { ITextPageFront } from 'src/models/text/TextPage.type'

// Manage prop targeting a subpath, such as `name.fr`
export function updateNestedProperty(path: string, value: unknown, data) {
  const paths = path.split('.')
  if (paths.length === 0) {
    return data
  }
  if (paths.length === 1) {
    data[path] = value
    return data
  }

  const fieldName = paths.shift()
  if (!fieldName) {
    return data
  }
  let field = data[fieldName]
  for (const subPath of paths) {
    if (
      typeof field[subPath] === 'object' &&
      Array.isArray(field[subPath]) !== true
    ) {
      field = field[subPath]
    } else {
      field[subPath] = value
      return data
    }
  }
}

type FormData =
  | ICatalogItemLean
  | ITextChapterMinimalPopulatedFront
  | ILocalizedTextMinimalFront
  | ITextPageFront

export const getFieldsNotSet = (mandatoryFields: string[], data: FormData) =>
  mandatoryFields.filter((field) => {
    const value = data[field]
    if (typeof value !== 'number' && typeof value !== 'boolean' && !value) {
      return true
    }
    return false
  })
