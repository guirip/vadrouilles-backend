/*
// NB: `process.env.KEY` are interpolated at next build time,
// so using a dynamic call (such as getEnv() below) fails as process.env is not exposed on client side.
// e.g reading a config value is not possible because module src/config.ts uses process.env
*/

import type { NextApiRequest } from 'next'
import { BadRequestError } from './error'
import { isArray } from './tsUtil'
import { Lang } from 'src/models/common/Lang'
import type { IBaseTextPage } from 'src/models/text/TextPage.type'
import type { IBaseTextChapter } from 'src/models/text/TextChapter.type'
import type { ContentCategory } from 'src/models/common/ContentCategory'

export const getBackCompletePath = (path: string) => {
  const result = `${process.env.NEXT_PUBLIC_BACK_BASE_PATH ?? ''}${path}`
  return result
}

export const getFrontCompletePath = (path: string, lang = Lang.fr) => {
  const result = `/?r=${process.env.NEXT_PUBLIC_FRONT_BASE_PATH ?? ''}${path}&lang=${lang}`
  return result
}

/**
 * Old path system (cleaner and redirected)
 * @param category
 * @param textPage
 * @param chapter
 * @returns
 */
export const getRouteOld = (
  category: ContentCategory,
  textPage: IBaseTextPage['slug'],
  chapter: IBaseTextChapter['slug'] | null
) =>
  `${process.env.NEXT_PUBLIC_FRONT_BASE_PATH ?? ''}/${category}/${textPage}${chapter ? `/${chapter}` : ''}`

export const getQueryParameters = () =>
  Object.fromEntries(new URLSearchParams(window.location.search))

export function getQuerySlugParameterOrThrow(
  req: NextApiRequest,
  index: number,
  description: string
) {
  const value = (() => {
    if (Array.isArray(req.query.slug) && req.query.slug.length > index) {
      return req.query?.slug[index]
    }
  })()
  if (!value) {
    throw new BadRequestError(`Missing path parameter: ${description}`)
  }
  return value
}

export function getNestedProperty(path: string | string[], data: object) {
  const paths = isArray(path) ? path : path.split('.')
  if (paths.length === 0) {
    return data
  }
  if (paths.length === 1) {
    return data[paths[0]]
  }

  let field = data[paths.shift() ?? '']

  for (const subPath of paths) {
    if (!field) {
      return
    }
    if (
      typeof field[subPath] === 'object' &&
      Array.isArray(field[subPath]) !== true
    ) {
      field = field[subPath]
    } else {
      return field[subPath]
    }
  }
}

export const timer = (time: number) =>
  new Promise((resolve) => setTimeout(resolve, time))
