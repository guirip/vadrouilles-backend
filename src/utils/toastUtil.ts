import type { Toast } from 'primereact/toast'

export function showInfo(toastRef: Toast, summary: string) {
  if (toastRef) {
    toastRef.show({
      closable: false,
      severity: 'info',
      summary,
      life: 2000,
    })
  }
  console.info(summary)
}

export function showSuccess(toastRef: Toast, summary: string) {
  if (toastRef) {
    toastRef.show({
      closable: false,
      severity: 'success',
      summary,
      life: 2000,
    })
  }
  console.info(summary)
}

export function showWarning(toastRef: Toast, summary: string, detail = '') {
  if (toastRef) {
    toastRef.show({
      closable: false,
      severity: 'warn',
      summary,
      detail: detail || '',
      life: 4000,
    })
  }
  console.log(`[warn] ${summary} ${detail}`)
}

export function showError(toastRef: Toast, summary: string, detail = '') {
  if (toastRef) {
    toastRef.show({
      closable: false,
      severity: 'error',
      summary,
      detail: detail || '',
      life: 6000,
    })
  }
  console.error(`[error] ${summary} - ${detail}`)
}
