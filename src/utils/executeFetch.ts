import config from 'src/config'

/**
 * Simple wrapper to handle errors in a generic way
 * @param  {string} url
 * @param  {object} opts
 * @param  {number} timeout
 * @return {object} response
 */
const executeFetch = (
  url,
  opts: RequestInit = {},
  timeoutParam?: number
): Promise<Response> =>
  new Promise(function (resolve, reject) {
    let didTimeout = false,
      fetchTimeoutId

    const timeout =
      typeof timeoutParam === 'number' ? timeoutParam : config.FETCH_TIMEOUT

    if (typeof timeout !== 'number') {
      console.warn('No timeout value so request timeout is disabled')
    } else {
      const handleTimeout = () => {
        didTimeout = true
        reject(new Error('Délai dépassé'))
      }
      fetchTimeoutId = setTimeout(handleTimeout, timeout)
    }
    function clearFetchTimeout() {
      clearTimeout(fetchTimeoutId)
    }

    const options = {
      // mode: 'cors',
      headers: {
        'Content-Type': 'application/json',
        Accept: 'application/json',
      },
      // credentials: 'include',
      ...opts,
    }
    if (!options.method) {
      options.method = 'GET'
    }

    fetch(url, options)
      .then(async function (response) {
        clearFetchTimeout()

        if (response.ok !== true) {
          let text
          try {
            text = await response.text()
          } catch (e) {
            /* ignore */
          }
          reject(
            `${text ? text + ' - ' : ''}${response.status} ${
              response.statusText
            }`
          )
        } else {
          resolve(response)
        }
      })
      .catch(function (e) {
        if (didTimeout) return
        clearFetchTimeout()
        reject(e.message)
      })
  })

export default executeFetch
