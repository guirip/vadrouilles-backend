'use client'

import { Toast } from 'primereact/toast'
import type { PropsWithChildren } from 'react'
import { createContext, useCallback, useContext, useMemo, useRef } from 'react'
import * as toastUtil from 'src/utils/toastUtil'
import { createGlobalStyle } from 'styled-components'
import { useDeviceContext } from './DeviceContextProvider'

const GlobalToastStyle = createGlobalStyle`
.p-toast {
  width: auto;
}
.p-toast > div {
  transform: scale(0.7);
}
.p-toast-title {
  font-size: 1.2rem;
}
.p-toast-message {
  font-size: 1.2rem;
  padding-right: 0.6em;
}
.p-toast-message-content {
  align-items: center !important;
}
`

interface IToastContext {
  showInfo: (summary: string) => void
  showSuccess: (summary: string) => void
  showWarning: (summary: string, detail?: string) => void
  showError: (summary: string, detail?: string) => void
}

const ToastContext = createContext<IToastContext | null>(null)

export function useToastContext() {
  const context = useContext(ToastContext)
  if (!context) {
    throw new Error('Toast context is not initialized yet')
  }
  return context
}

export function ToastProvider({ children }: PropsWithChildren) {
  const { isMobile } = useDeviceContext()
  const toastRef = useRef<Toast>(null)

  const showInfo = useCallback(
    (summary: string) => {
      if (!toastRef.current) {
        return
      }
      toastUtil.showInfo(toastRef.current, summary)
    },
    [toastRef]
  )
  const showSuccess = useCallback(
    (summary: string) => {
      if (!toastRef.current) {
        return
      }
      toastUtil.showSuccess(toastRef.current, summary)
    },
    [toastRef]
  )
  const showWarning = useCallback(
    (summary: string, detail?: string) => {
      if (!toastRef.current) {
        return
      }
      toastUtil.showWarning(toastRef.current, summary, detail)
    },
    [toastRef]
  )
  const showError = useCallback(
    (summary: string, detail?: string) => {
      if (!toastRef.current) {
        return
      }
      toastUtil.showError(toastRef.current, summary, detail)
    },
    [toastRef]
  )

  const contextValue = useMemo(
    () => ({
      showInfo,
      showSuccess,
      showWarning,
      showError,
    }),
    [showInfo, showSuccess, showWarning, showError]
  )

  return (
    <>
      <GlobalToastStyle />
      <Toast ref={toastRef} position={isMobile ? 'top-center' : 'top-right'} />
      <ToastContext.Provider value={contextValue}>
        {children}
      </ToastContext.Provider>
    </>
  )
}
