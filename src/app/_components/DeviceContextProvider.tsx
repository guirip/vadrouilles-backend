'use client'

import type { PropsWithChildren } from 'react'
import { createContext, useContext, useEffect, useMemo, useState } from 'react'
import { isMobileDevice, isSafari } from '@/frontend/_utils/Utils'

interface IDeviceContext {
  isMobile: boolean
  isSafari: boolean
}

const DeviceContext = createContext<IDeviceContext | null>(null)

export const useDeviceContext = () => {
  const context = useContext(DeviceContext)
  if (!context) {
    throw new Error('Device context is not initialized yet')
  }
  return context
}

export function DeviceContextProvider({ children }: PropsWithChildren) {
  const [mobileDevice, setMobileDevice] = useState(false)
  const [safariBrowser, setSafariBrowser] = useState(false)

  useEffect(() => {
    setMobileDevice(isMobileDevice)
    setSafariBrowser(isSafari())
  }, [])

  const contextValue = useMemo(
    () => ({
      isMobile: mobileDevice,
      isSafari: safariBrowser,
    }),
    [mobileDevice, safariBrowser]
  )

  return (
    <DeviceContext.Provider value={contextValue}>
      {children}
    </DeviceContext.Provider>
  )
}
