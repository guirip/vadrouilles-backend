'use client'

import { faXmark } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import type { Metadata } from 'next'
import {
  Container,
  Image,
  Inner,
  MetadataDescription,
  MetadataImages,
  MetadataSubtitle,
  MetadataText,
  MetadataTitle,
  TextFader,
} from './style'
import {
  METADATA_CHAPTER_SEPARATOR,
  METADATA_SUBTITLE_SEPARATOR,
} from 'src/services/TextService.metadata'
import { useMemo } from 'react'

interface IProps {
  focus?: boolean
  metadata: Metadata
  onClick?: () => void
  showPageTitle?: boolean
  showPageSubtitle?: boolean
  coverPosition?: string
}

export const PagePreview = ({
  focus = false,
  metadata,
  onClick,
  showPageTitle = true,
  showPageSubtitle = true,
  coverPosition,
}: IProps) => {
  const [title, chapterTitle, subtitle] = useMemo(() => {
    if (typeof metadata.title === 'string') {
      const [_title, subtitle] = metadata.title.split(
        METADATA_SUBTITLE_SEPARATOR
      )
      if (_title) {
        const [pageTitle, chapterTitle] = _title.split(
          METADATA_CHAPTER_SEPARATOR
        )
        return [pageTitle, chapterTitle, subtitle]
      }
      return [_title, '', subtitle]
    }
    return []
  }, [metadata.title])

  const displayedTitle = useMemo(() => {
    let str = ''
    if (title && showPageTitle) {
      str += title

      if (chapterTitle) {
        str += METADATA_CHAPTER_SEPARATOR
      }
    }
    if (chapterTitle) {
      str += chapterTitle
    }
    return str
  }, [chapterTitle, showPageTitle, title])

  return (
    <Container
      $focus={focus}
      $clickable={typeof onClick === 'function'}
      onClick={onClick}
    >
      <Inner>
        <MetadataImages>
          {Array.isArray(metadata.openGraph?.images) &&
          metadata.openGraph?.images.length > 0 &&
          metadata.openGraph.images[0]['url'] ? (
            <Image
              $src={metadata.openGraph.images[0]['url']}
              title={metadata.openGraph.images[0]['caption']}
              $position={coverPosition}
            />
          ) : (
            <FontAwesomeIcon icon={faXmark} />
          )}
        </MetadataImages>

        <MetadataText>
          <MetadataTitle>{displayedTitle}</MetadataTitle>
          {subtitle && showPageSubtitle && (
            <MetadataSubtitle>{subtitle}</MetadataSubtitle>
          )}
          <MetadataDescription>
            <div>{metadata.description}</div>
          </MetadataDescription>
          <TextFader />
        </MetadataText>
      </Inner>
    </Container>
  )
}
