import { getModeStyle, Mode } from 'src/app/frontend/_style/mode.style'
import { FONT_SIZE, TEXT_COLOR } from 'src/app/_style/style.constants'
import styled, { css } from 'styled-components'

export const Container = styled.div<{ $focus: boolean; $clickable: boolean }>`
  max-width: 94vw;
  margin: 0.25rem 0;
  width: 760px;
  height: 200px;
  color: ${TEXT_COLOR};
  border: 3px solid
    ${({ $focus }) =>
      $focus ? getModeStyle(true, Mode.DARK).background : 'transparent'};

  ${({ $clickable }) =>
    $clickable &&
    css`
      cursor: pointer;
    `}
  background-color: #000000aa;
  overflow: hidden;

  &:active {
    filter: brightness(1.3);
  }
  &:hover {
    filter: brightness(1.15);
  }
`
export const Inner = styled.div`
  display: flex;
  flex-wrap: nowrap;
  align-items: center;
  height: 100%;
  width: 100%;
  padding: 4px;
  border: 1px solid hsl(240deg 80% 50% / 70%);
`

export const MetadataImages = styled.div`
  flex: 1 1 34%;
  display: flex;
  justify-content: center;
  align-items: center;
  height: 100%;
`
export const Image = styled.div<{ $src: string; $position?: string }>`
  display: flex;
  height: 100%;
  width: 100%;
  background-image: url('${({ $src }) => $src}');
  background-repeat: no-repeat;
  background-size: cover;
  background-position: ${({ $position }) =>
    $position ? $position : 'center center'};
  overflow: hidden;
`

export const MetadataText = styled.div`
  flex: 1 0 66%;
  position: relative;
  display: flex;
  flex-direction: column;
  justify-content: center;
  height: 100%;
  padding: 1.6rem 2.2rem 1.2rem;
  text-align: center;
  overflow: hidden;

  @media (max-width: 600px) {
    padding: 1.3rem 1.6rem 1rem;
  }
`
export const MetadataTitle = styled.div`
  font-weight: 500;
  font-size: ${FONT_SIZE.h5};
`
export const MetadataSubtitle = styled.div`
  font-style: italic;
  font-size: ${FONT_SIZE.sm};
  opacity: 0.8;
`
export const MetadataDescription = styled.div`
  margin-top: 1rem;
  min-height: 0px;
  overflow-y: hidden;
  font-size: ${FONT_SIZE.sm};
`
export const TextFader = styled.div`
  position: absolute;
  z-index: 1;
  bottom: 6px;
  height: 60px;
  width: 100%;
  background: linear-gradient(to top, black, transparent);
`
