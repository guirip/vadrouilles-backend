import Link from 'next/link'
import { Button } from 'primereact/button'
import { FontFamily } from 'src/app/_assets/fonts'
import { ACCENT, getModeStyle, Mode } from 'src/app/frontend/_style/mode.style'
import type { ButtonTheme, IButtonBlock } from 'src/models/text/TextBlock.type'
import styled, { css } from 'styled-components'

const StyledButton = styled(Button)<{ $size: number; $theme: ButtonTheme }>`
  font-size: ${({ $size }) => $size}rem;
  letter-spacing: 1px;
  font-family: ${FontFamily.LEAGUESPARTAN};
  ${({ $theme }) => css`
    ${getThemeStyle($theme)}
  `};

  &:hover {
    filter: brightness(1.2);
  }
  &:active {
    filter: brightness(1.4);
  }
`

function getThemeStyle(theme: ButtonTheme) {
  switch (theme) {
    case 'primary':
      return `
        background: linear-gradient(0deg, #0e0034, #0c0a39, #0b123f, #0c1744, #0d1d49, #0e224e, #0f2853, #112d58);
        border: 1px solid ${ACCENT};
        border-bottom-width: 2px;
        color: ${getModeStyle(true, Mode.DARK).color};
      `
    case 'secondary':
      return `
        background: linear-gradient(0deg, #02554a, #005d5b, #00656e, #006d82, #007497, #007aac, #007fbf, #3d82d0);
        border: 1px solid lightblue;
        border-bottom-width: 2px;
        color: ${getModeStyle(true, Mode.DARK).color};
        `
    case 'callout':
      return `
        background: linear-gradient(0deg, #340000, #390001, #3e0002, #430002, #490002, #4e0002, #530001, #580000);
        border: 1px solid #c84d00;
        border-bottom-width: 2px;
        color: ${getModeStyle(true, Mode.DARK).color};
      `
    case 'muted':
      return `
        background: linear-gradient(0deg, #555555, #575d68, #58647b, #586c8f, #5674a3, #527cb7, #4c85cc, #438de1);
        filter: saturation(0.5);
        color: ${getModeStyle(true, Mode.DARK).color};
        font-weight: 300;
      `
  }
}

const Inner = ({ data }: { data: IButtonBlock['data'] }) => (
  <StyledButton
    $theme={data.style.theme}
    $size={data.style.size}
    className={`mx-${data.style.mx} my-${data.style.my} px-${data.style.px} py-${data.style.py}`}
    icon={data.icon}
  >
    {data.label ?? ''}
  </StyledButton>
)

export const TextButton = ({ data }: { data: IButtonBlock['data'] }) => {
  return data.type === 'link' && data.value ? (
    <Link href={data.value} target={data.target}>
      <Inner data={data} />
    </Link>
  ) : (
    <Inner data={data} />
  )
}
