import { redirect } from 'next/navigation'
import { DEFAULT_PAGE } from './page-paths'

export default function () {
  redirect(DEFAULT_PAGE)
}
