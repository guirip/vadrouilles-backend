'use client'

import { faUnlockKeyhole } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { signIn } from 'next-auth/react'
import { Button } from 'primereact/button'
import styled from 'styled-components'

const Container = styled.div`
  display: flex;
  width: 100%;
  height: 100%;
  align-items: center;
  justify-content: center;
`
const StyledButton = styled(Button)`
  white-space: nowrap;
`

export const LogInButton = () => (
  <Container>
    <StyledButton
      icon={
        <FontAwesomeIcon className="mr-2" icon={faUnlockKeyhole} size="sm" />
      }
      className="p-button-lg"
      onClick={() => {
        signIn()
      }}
      label="Log in"
    />
  </Container>
)
