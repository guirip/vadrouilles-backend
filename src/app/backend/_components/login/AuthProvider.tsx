'use client'

import { SessionProvider, useSession } from 'next-auth/react'
import type { PropsWithChildren } from 'react'
import { LogInButton } from './LogInButton'

export const LoginScreen = ({ children }: PropsWithChildren) => {
  const { data: session } = useSession()
  if (!session) {
    return <LogInButton />
  }
  return <>{children}</>
}

export const AuthProvider = ({ children }: PropsWithChildren) => (
  <SessionProvider>
    <LoginScreen>{children}</LoginScreen>
  </SessionProvider>
)
