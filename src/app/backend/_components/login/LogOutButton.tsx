'use client'

import { faLock } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { signOut } from 'next-auth/react'
import { Button as PrimeReactButton } from 'primereact/button'
import styled from 'styled-components'

export const Button = styled(PrimeReactButton)`
  position: absolute;
  z-index: 2;
  top: 10px;
  right: 10px;
  transform: scale(0.9);
  background-color: black !important;
`

export const LogOutButton = () => (
  <Button
    icon={<FontAwesomeIcon icon={faLock} size="lg" />}
    className="p-button-sm p-button-warning p-button-rounded p-button-outlined"
    onClick={() => signOut()}
  />
)
