'use client'

import styled from 'styled-components'

export const PageContainer = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  height: 100vh;
  padding: 2.5vh 0.5rem 2vh;
  overflow-y: auto;
`
