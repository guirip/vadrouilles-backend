'use client'

import styled from 'styled-components'

const envColor = () => {
  const envType = process.env.NODE_ENV
  switch (envType) {
    case 'production':
      return '#ff5c5c'

    case 'development':
    default:
      return 'cyan'
  }
}

export const H1 = styled.h1`
  display: flex;
  align-items: center;
  color: ${envColor};
`

export const H2 = styled.h2`
  display: flex;
  align-items: center;
  margin-top: 0;
  font-size: 1.3rem;
  font-style: italic;
  opacity: 0.6;
  color: ${envColor};
`
