'use client'

import { InputNumber } from 'primereact/inputnumber'
import { InputSwitch } from 'primereact/inputswitch'
import { InputText } from 'primereact/inputtext'
import styled from 'styled-components'

export const Title = styled.h2`
  margin-bottom: 0.2rem;
  font-size: 1.5rem;
  text-align: center;
`
export const Container = styled.div`
  min-width: 320px;
  max-width: 85vw;
`
export const Field = styled.div<{
  className?: string
}>``
export const InlineField = styled(Field)`
  display: flex;
  align-items: center;
  justify-content: space-between;

  .p-inputtext {
    width: 130px;
  }
`
export const FieldTitle = styled.div`
  margin: 0.4rem 1rem 0.4rem 0;
  font-weight: bold;
  font-size: 1.1em;
`
export const StyledInputText = styled(InputText)`
  width: 100%;
`
export const StyledInputNumber = styled(InputNumber)<{ $width?: number }>`
  input {
    width: ${({ $width }) =>
      typeof $width === 'number' ? $width : 130}px !important;
    text-align: right;
  }
`

export const NeutralInputSwitch = styled(InputSwitch)`
  &.p-highlight .p-inputswitch-slider:before {
    background: cyan;
  }
  .p-inputswitch-slider:before {
    background: yellow;
  }
`
