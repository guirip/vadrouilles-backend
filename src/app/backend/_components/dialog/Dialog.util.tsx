'use client'

import { faCheckCircle } from '@fortawesome/free-regular-svg-icons'
import { faTimes } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { Button } from 'primereact/button'

export const renderFooter = (
  onSubmit: () => void,
  onHide: () => void,
  isSubmitEnabled = true,
  isCancelEnabled = true
) => (
  <div className="flex p-justify-center mt-1">
    <Button
      className="p-button-secondary p-button mx-auto"
      icon={<FontAwesomeIcon className="mr-2" icon={faTimes} size="xl" />}
      onClick={onHide}
      label="Close"
      disabled={!isCancelEnabled}
    />
    <Button
      className="p-button-primary text-xl mx-auto"
      icon={<FontAwesomeIcon className="mr-2" icon={faCheckCircle} size="lg" />}
      onClick={onSubmit}
      label="Save"
      disabled={!isSubmitEnabled}
    />
  </div>
)
