import { forwardRef, type PropsWithChildren } from 'react'
import styled, { type CSSProperties } from 'styled-components'

const ItemWrapper = styled.div`
  max-width: 86vw;

  & .text-page-select-dialog & {
    margin: 0.5rem;
  }
`

interface IProps {
  id: string
  style?: CSSProperties
}

export const Item = forwardRef<HTMLDivElement, PropsWithChildren<IProps>>(
  ({ id, ...props }, ref) => {
    return (
      <ItemWrapper {...props} ref={ref}>
        {props.children}
      </ItemWrapper>
    )
  }
)
