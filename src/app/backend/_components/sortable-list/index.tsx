'use client'

import {
  closestCenter,
  DndContext,
  DragOverlay,
  PointerSensor,
  useSensor,
  useSensors,
} from '@dnd-kit/core'
import {
  arrayMove,
  SortableContext,
  verticalListSortingStrategy,
} from '@dnd-kit/sortable'
import { useEffect, useMemo, useState } from 'react'
import type { IDbStringId } from 'src/models/common/DbItemLean'
import type { IIdOrderMap } from 'src/models/common/IdOrderMap'
import styled from 'styled-components'
import { Item } from './Item'
import { SortableItem } from './SortableItem'

const Container = styled.div`
  margin-right: 2rem;
  margin-bottom: 4rem;

  & > * {
    touch-action: none;
    background-color: rgba(255, 255, 255, 0.05);
  }
  & > :nth-child(2n + 1) {
    background-color: rgba(255, 255, 255, 0.1);
  }
`

interface IProps {
  content?: IDbStringId[]
  renderItem: (item: IDbStringId) => React.ReactElement
  onOrderUpdate: (orderMap: IIdOrderMap) => Promise<boolean>
}

const SortableList = ({ content = [], renderItem, onOrderUpdate }: IProps) => {
  const [activeId, setActiveId] = useState(null)
  const sensors = useSensors(
    useSensor(PointerSensor, {
      activationConstraint: { delay: 300, tolerance: 100 },
    })
  )

  const [items, setItems] = useState(content ?? [])
  const ids = useMemo(() => (items ?? []).map((item) => item._id), [items])

  useEffect(() => {
    setItems(content ?? [])
  }, [content])

  function handleDragStart(event) {
    const { active } = event

    setActiveId(active.id)
  }

  async function handleDragEnd(event) {
    const { active, over } = event

    if (active.id !== over.id) {
      const oldIndex = items.findIndex((item) => item._id === active.id)
      const newIndex = items.findIndex((item) => item._id === over.id)

      const newContent = arrayMove(items, oldIndex, newIndex)

      setItems(newContent)
      const idOrderMap = newContent.reduce(function (
        map,
        currentItem: IDbStringId,
        index
      ) {
        map[currentItem._id] = index
        return map
      }, {})

      const result = await onOrderUpdate(idOrderMap)
      if (!result) {
        setItems(items)
      }
    }

    setActiveId(null)
  }

  return (
    <Container>
      <DndContext
        sensors={sensors}
        collisionDetection={closestCenter}
        onDragStart={handleDragStart}
        onDragEnd={handleDragEnd}
      >
        <SortableContext items={ids} strategy={verticalListSortingStrategy}>
          {(items ?? []).map((item) => (
            <SortableItem key={item._id} id={item._id}>
              {renderItem(item)}
            </SortableItem>
          ))}
        </SortableContext>
        <DragOverlay>{activeId ? <Item id={activeId} /> : null}</DragOverlay>
      </DndContext>
    </Container>
  )
}

export default SortableList
