import React, { type PropsWithChildren } from 'react'
import { useSortable } from '@dnd-kit/sortable'
import { CSS } from '@dnd-kit/utilities'
import { Item } from './Item'
import type { CSSProperties } from 'styled-components'

export function SortableItem(props: PropsWithChildren<{ id: string }>) {
  const {
    attributes,
    listeners,
    setNodeRef,
    transform,
    transition,
    isDragging,
  } = useSortable({ id: props.id })

  const style: CSSProperties = {
    transform: CSS.Transform.toString(transform) ?? '',
    transition: transition ?? '',
  }
  if (isDragging) {
    style.backgroundColor = '#00e7ff1f'
  }

  return (
    <Item
      id={props.id}
      ref={setNodeRef}
      style={style}
      {...attributes}
      {...listeners}
    >
      {props.children}
    </Item>
  )
}
