'use client'

import { useCallback, useMemo, useRef } from 'react'
import { usePathname, useRouter } from 'next/navigation'
import styled from 'styled-components'
import { Menu as PrimeMenu } from 'primereact/menu'
import { Button } from 'primereact/button'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faBars, faChevronRight } from '@fortawesome/free-solid-svg-icons'

import {
  TEXT_PAGE_PATH,
  CATALOG_PAGE_PATH,
  CATALOG_ITEM_CATEGORY_PAGE_PATH,
  CODIF_PAGE_PATH,
} from '../page-paths'

const MenuContainer = styled.div`
  position: absolute;
  top: 10px;
  left: 10px;
`
const StyledButton = styled(Button)`
  min-width: auto;
`

const chevronIcon = (
  <FontAwesomeIcon className="mr-2" icon={faChevronRight} size="xs" />
)

export function Menu() {
  const menuRef = useRef<PrimeMenu>(null)

  const pathname = usePathname()
  const router = useRouter()

  const items = useMemo(
    () => [
      {
        label: 'Texts & chapters',
        icon: chevronIcon,
        command: () => {
          router.push(TEXT_PAGE_PATH)
        },
        disabled: pathname === TEXT_PAGE_PATH,
      },
      {
        label: 'Catalog items',
        icon: chevronIcon,
        command: () => {
          router.push(CATALOG_PAGE_PATH)
        },
        disabled: pathname === CATALOG_PAGE_PATH,
      },
      {
        label: 'Catalog item categories',
        icon: chevronIcon,
        command: () => {
          router.push(CATALOG_ITEM_CATEGORY_PAGE_PATH)
        },
        disabled: pathname === CATALOG_ITEM_CATEGORY_PAGE_PATH,
      },
      {
        label: 'Codifs',
        icon: chevronIcon,
        command: () => {
          router.push(CODIF_PAGE_PATH)
        },
        disabled: pathname === CODIF_PAGE_PATH,
      },
    ],
    [router, pathname]
  )

  const toggleMenu = useCallback(function (e) {
    if (!menuRef.current) {
      throw new Error('menuRef should be defined')
    }
    menuRef.current.toggle(e)
  }, [])

  return (
    <MenuContainer>
      <PrimeMenu id="popup_menu" ref={menuRef} model={items} popup />
      <StyledButton
        className="z-2"
        onClick={toggleMenu}
        aria-controls="popup_menu"
        aria-haspopup
      >
        <FontAwesomeIcon icon={faBars} size="lg" />
      </StyledButton>
    </MenuContainer>
  )
}
