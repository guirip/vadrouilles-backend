'use client'

import type { PropsWithChildren } from 'react'
import styled from 'styled-components'

const Wrapper = styled.div`
  margin: 1rem;
  padding: 1rem;
  font-weight: bold;
  color: white;
  background: #ad7113;
  letter-spacing: 1px;
`

export const Warning = ({ children }: PropsWithChildren<unknown>) => (
  <Wrapper>{children}</Wrapper>
)
