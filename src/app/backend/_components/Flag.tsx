'use client'

import styled from 'styled-components'

import frFlag from 'src/app/_assets/flag-fr.webp'
import enFlag from 'src/app/_assets/flag-en.webp'

import { Lang } from 'src/models/common/Lang'

const flags = {
  [Lang.fr]: frFlag,
  [Lang.en]: enFlag,
}

interface IProps {
  $lang: 'en' | 'fr'
  $width?: string
  $height?: string
}

const Flag = styled.div<IProps>`
  width: ${({ $width }) => $width || '26px'};
  height: ${({ $height }) => $height || '26px'};
  opacity: 1;
  background-image: url(${({ $lang }) => flags[$lang].src});
  background-position: 50% 50%;
  background-repeat: no-repeat;
  background-size: contain;
  -webkit-tap-highlight-color: transparent;
`

export default Flag
