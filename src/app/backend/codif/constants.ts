import type { ICodifLean } from 'src/models/Codif'
import { CodifType } from 'src/models/Codif.type'

export const codifTypes = Object.values(CodifType).map((status) => ({
  label: status,
  value: status,
}))

export const DEFAULT_VALUE = {
  [CodifType.Boolean]: true,
  [CodifType.Number]: 0,
  [CodifType.String]: '',
}

export const NEW_CODIF = {
  _id: 'tmp',
  name: '',
  value: '',
  type: CodifType.String,
}

export const isNewCodif = (codif: ICodifLean) => codif._id === NEW_CODIF._id
