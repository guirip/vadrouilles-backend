'use client'

import { faCheckCircle } from '@fortawesome/free-regular-svg-icons'
import { faTimes } from '@fortawesome/free-solid-svg-icons'
import { Button } from 'primereact/button'
import { InputText } from 'primereact/inputtext'
import type { BaseSyntheticEvent } from 'react'
import { useState } from 'react'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'

import type { ICodifLean } from 'src/models/Codif'
import { CODIF_DEFAULT_TYPE } from 'src/models/Codif.type'
import { slugify } from 'src/utils/stringUtil'
import { DEFAULT_VALUE, codifTypes, isNewCodif } from '../constants'
import { Container, Dropdown } from './Codif.style'
import { CodifValue } from './CodifValue'

interface IProps {
  codif: ICodifLean
  onSave: (codif: ICodifLean) => Promise<void>
  onDelete: (e: BaseSyntheticEvent, _id: string) => void
}

export function Codif(props: IProps) {
  const [codif, setCodif] = useState(props.codif)

  return (
    <Container className="m-3">
      <InputText
        className="text-lg"
        value={codif.name}
        onChange={({ target }) => {
          setCodif({
            ...codif,
            name: slugify(target.value),
          })
        }}
        disabled={!isNewCodif(codif)}
      />

      <CodifValue
        codif={codif}
        onChange={({ value }) => {
          setCodif({
            ...codif,
            value: value,
          })
        }}
      />

      <Dropdown
        value={codif.type || CODIF_DEFAULT_TYPE}
        options={codifTypes}
        onChange={(e) => {
          const newType = e.value
          setCodif({
            ...codif,
            type: newType,
            value: DEFAULT_VALUE[newType],
          })
        }}
        placeholder="Codif type"
      />

      <div className="flex flex-nowrap">
        <Button
          className={`${
            isNewCodif(codif) ? 'slow-blink' : ''
          } p-button-primary mx-2 text-lg`}
          icon={
            <FontAwesomeIcon className="mr-2" icon={faCheckCircle} size="lg" />
          }
          onClick={() => {
            props.onSave(codif)
          }}
          label="Save"
        />
        <Button
          className="p-button-warning mx-1"
          icon={<FontAwesomeIcon className="mr-2" icon={faTimes} size="lg" />}
          onClick={(e) => props.onDelete(e, codif._id)}
          label="Delete"
        />
      </div>
    </Container>
  )
}
