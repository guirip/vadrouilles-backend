'use client'

import { Dropdown as PrimeReactDropDown } from 'primereact/dropdown'
import styled from 'styled-components'

export const Container = styled.div`
  display: flex;
  align-items: center;
  padding: 1.6rem;

  & > * {
    margin: 0.3rem;
  }

  & input[disabled] {
    opacity: 1;
    border-color: black;
  }

  @media (max-width: 768px) {
    & {
      flex-direction: column;
      padding-bottom: 2rem;
    }
  }
`

export const Dropdown = styled(PrimeReactDropDown)`
  flex-grow: 1;
`
