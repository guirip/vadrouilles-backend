'use client'

import { InputNumber } from 'primereact/inputnumber'
import { InputSwitch } from 'primereact/inputswitch'
import { InputText } from 'primereact/inputtext'

import { SwitchContainer } from './CodifValue.style'

export function CodifValue({ codif, onChange }) {
  switch (codif.type) {
    case 'boolean':
      return (
        <SwitchContainer>
          <InputSwitch
            className="mx-5"
            checked={codif.value === true}
            onChange={onChange}
          />
        </SwitchContainer>
      )

    case 'number':
      return (
        <InputNumber
          className="text-lg"
          value={codif.value || 0}
          onValueChange={onChange}
          maxFractionDigits={0}
          useGrouping={false}
        />
      )

    default:
      return (
        <InputText
          className="text-lg"
          value={codif.value || ''}
          onChange={(e) => {
            onChange({ value: e.target.value })
          }}
        />
      )
  }
}
