'use client'

import styled from 'styled-components'

export const SwitchContainer = styled.div`
  flex-grow: 1;
  display: flex;
  justify-content: center;
`
