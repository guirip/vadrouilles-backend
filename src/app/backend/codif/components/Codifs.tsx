'use client'

import {
  faArrowRotateRight,
  faPlus,
  faWarning,
} from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { Button } from 'primereact/button'
import { ConfirmPopup, confirmPopup } from 'primereact/confirmpopup'
import type { BaseSyntheticEvent } from 'react'
import { useCallback, useState } from 'react'
import styled from 'styled-components'

import type { ICodifLean } from 'src/models/Codif'
import { NEW_CODIF, isNewCodif } from '../constants'
import { useCodifsContext } from '../providers/CodifsProvider'
import { Codif } from './Codif'

const CodifsContainer = styled.div`
  & > * {
    background-color: rgba(255, 255, 255, 0.05);
  }
  & > :nth-child(2n + 1) {
    background-color: rgba(255, 255, 255, 0.1);
  }
`

export const Codifs = () => {
  const { codifs, refreshCodifs, saveCodif, deleteCodif } = useCodifsContext()

  const [newCodif, setNewCodif] = useState<ICodifLean | null>(null)

  const onClickOnAdd = useCallback(() => {
    setNewCodif({ ...NEW_CODIF })
  }, [])

  const onClickOnSave = useCallback(
    async function (codif: ICodifLean) {
      const success = await saveCodif(codif)
      if (success && isNewCodif(codif)) {
        setNewCodif(null)
      }
    },
    [saveCodif]
  )

  const onClickOnDelete = useCallback(
    async function (event: BaseSyntheticEvent, _id: string) {
      confirmPopup({
        target: event.currentTarget,
        message: `Confirm codif deletion`,
        icon: <FontAwesomeIcon icon={faWarning} size="lg" />,
        accept: () => {
          if (_id === NEW_CODIF._id) {
            // simple local delete
            setNewCodif(null)
            return
          }
          deleteCodif(_id)
        },
      })
    },
    [deleteCodif]
  )

  return (
    <>
      <ConfirmPopup />

      <div className="flex my-4">
        <Button
          className="mr-3 text-lg"
          icon={<FontAwesomeIcon className="mr-2" icon={faPlus} />}
          onClick={onClickOnAdd}
          label="Add codif"
        />
        <Button
          className="p-button-plain p-button p-button-rounded p-button-outlined mx-auto"
          icon={<FontAwesomeIcon icon={faArrowRotateRight} size="lg" />}
          onClick={refreshCodifs}
        />
      </div>

      <CodifsContainer>
        {newCodif && (
          <Codif
            key={newCodif._id}
            codif={newCodif}
            onSave={onClickOnSave}
            onDelete={onClickOnDelete}
          />
        )}
        {codifs.map((codif) => (
          <Codif
            key={codif._id}
            codif={codif}
            onSave={onClickOnSave}
            onDelete={onClickOnDelete}
          />
        ))}
      </CodifsContainer>
    </>
  )
}
