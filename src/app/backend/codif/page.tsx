import type { Metadata } from 'next/types'
import { H1 } from '@/backend/_components/Titles'
import type { ICodif, ICodifLean } from 'src/models/Codif'
import { CodifModel } from 'src/models/Codif'
import { connect, toSerializable } from 'src/services/DbService'
import { Codifs } from './components/Codifs'
import { CodifsProvider } from './providers/CodifsProvider'

const title = 'Codifs'

export const metadata: Metadata = {
  title,
}

export default async function CodifPage() {
  await connect()
  const codifs = await CodifModel.find({}).sort({ name: 1 }).lean<ICodif[]>()
  const codifsLean = codifs.map((c) => toSerializable<ICodifLean>(c))

  return (
    <>
      <H1>{title}</H1>
      <CodifsProvider codifs={codifsLean}>
        <Codifs />
      </CodifsProvider>
    </>
  )
}
