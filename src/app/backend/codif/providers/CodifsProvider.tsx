'use client'

import type { PropsWithChildren } from 'react'
import {
  createContext,
  useCallback,
  useContext,
  useMemo,
  useState,
} from 'react'
import {
  apiCreateCodif,
  apiDeleteCodif,
  apiGetCodifs,
  apiUpdateCodif,
} from 'src/client-api/CodifApi'
import { useToastContext } from 'src/app/_components/ToastProvider'
import type { ICodifLean } from 'src/models/Codif'
import { isNewCodif } from '../constants'

export interface ICodifsProvider {
  codifs: ICodifLean[]
  refreshCodifs: () => Promise<void>
  saveCodif: (_id: ICodifLean) => Promise<boolean>
  deleteCodif: (_id: string) => Promise<void>
}

const CodifsContext = createContext<ICodifsProvider | null>(null)

export function useCodifsContext() {
  const context = useContext(CodifsContext)
  if (!context) {
    throw new Error('Codifs context is not initiliazed yet')
  }
  return context
}

interface IProps {
  codifs: ICodifLean[]
}

export const CodifsProvider = (props: PropsWithChildren<IProps>) => {
  const [codifs, setCodifs] = useState(props.codifs)
  const { showSuccess, showError } = useToastContext()

  const refreshCodifs = useCallback(
    async function () {
      try {
        setCodifs(await apiGetCodifs())
      } catch (e) {
        showError('Failed to retrieve codifs', e)
      }
    },
    [showError]
  )

  const saveCodif = useCallback(
    async function (codif: ICodifLean) {
      const isCreate = isNewCodif(codif)
      try {
        if (isCreate) {
          const { _id, ...codifData } = codif
          await apiCreateCodif(codifData)
        } else {
          await apiUpdateCodif(codif)
        }
        showSuccess(`Successfully ${isCreate ? 'added' : 'updated'} codif`)
        await refreshCodifs()
      } catch (e) {
        showError(`Failed to ${isCreate ? 'save new' : 'update'} codif`, e)
        return false
      }
      return true
    },
    [refreshCodifs, showError, showSuccess]
  )

  const deleteCodif = useCallback(
    async function (_id: string) {
      try {
        await apiDeleteCodif(_id)
        showSuccess(`Successfully deleted codif`)
        await refreshCodifs()
      } catch (e) {
        showError(`Failed to delete codif (_id: ${_id})`, e)
      }
    },
    [refreshCodifs, showError, showSuccess]
  )

  const contextValue = useMemo(
    () => ({
      codifs,
      refreshCodifs,
      saveCodif,
      deleteCodif,
    }),
    [codifs, refreshCodifs, saveCodif, deleteCodif]
  )

  return (
    <CodifsContext.Provider value={contextValue}>
      {props.children}
    </CodifsContext.Provider>
  )
}
