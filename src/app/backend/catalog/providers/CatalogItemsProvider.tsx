'use client'

import { useToastContext } from 'src/app/_components/ToastProvider'
import type { PropsWithChildren } from 'react'
import {
  createContext,
  useCallback,
  useContext,
  useMemo,
  useState,
} from 'react'

import {
  apiCreateCatalogItem,
  apiDeleteCatalogItem,
  apiRefreshItems,
  apiUpdateCatalogItem,
  apiUpdateOrder,
} from 'src/client-api/CatalogItemApi'
import { apiUpdateCodif } from 'src/client-api/CodifApi'
import type { ICatalogItemLean } from 'src/models/CatalogItem'
import type { ICatalogItemCategoryLean } from 'src/models/CatalogItemCategory'
import type { ICodifLean } from 'src/models/Codif'
import type { IIdOrderMap } from 'src/models/common/IdOrderMap'

interface ICatalogItemsContext {
  items: ICatalogItemLean[]
  categories: ICatalogItemCategoryLean[]
  featureEnabled: boolean
  staticUrl: string
  discount?: number
  toggleFeatureStatus: () => Promise<void>
  refreshItems: () => Promise<void>
  persistItem: (item: Partial<ICatalogItemLean>) => Promise<boolean>
  updateOrder: (idOrderMap: IIdOrderMap) => Promise<boolean>
  deleteItem: (itemId: string) => Promise<boolean>
}

const CatalogItemsContext = createContext<ICatalogItemsContext | null>(null)

export function useCatalogItemsContext() {
  const context = useContext(CatalogItemsContext)
  if (!context) {
    throw new Error('Catalog items context is not initialized yet')
  }
  return context
}

interface IProps {
  items: ICatalogItemLean[]
  categories: ICatalogItemCategoryLean[]
  featureStatusCodif: ICodifLean
  staticUrl: string
  discount?: number
}

export function CatalogItemsProvider(props: PropsWithChildren<IProps>) {
  const { showSuccess, showError } = useToastContext()

  const [featureEnabled, setFeatureEnabled] = useState(
    props.featureStatusCodif &&
      typeof props.featureStatusCodif.value === 'boolean'
      ? props.featureStatusCodif.value
      : false
  )

  const [items, setItems] = useState(
    Array.isArray(props.items) ? props.items : []
  )
  const [categories, setCategories] = useState(
    Array.isArray(props.categories) ? props.categories : []
  )

  const refreshItems = useCallback(
    async function () {
      try {
        const { items, categories } = await apiRefreshItems()
        setItems(items || [])
        setCategories(categories || [])
      } catch (e) {
        showError('Failed to fetch items', e)
      }
    },
    [showError]
  )

  const toggleFeatureStatus = useCallback(async () => {
    try {
      const updatedCodif = await apiUpdateCodif({
        ...props.featureStatusCodif,
        value: !featureEnabled,
      })
      setFeatureEnabled(updatedCodif.value === true)
      showSuccess('Toggle status success')
    } catch (e) {
      showError('Failed to update feature status', e)
    }
  }, [featureEnabled, props.featureStatusCodif, showError, showSuccess])

  const persistItem = useCallback(
    async (item: Partial<ICatalogItemLean>) => {
      const isUpdate = !!item._id

      try {
        if (!isUpdate) {
          await apiCreateCatalogItem(item)
        } else {
          await apiUpdateCatalogItem(item)
        }
        showSuccess(`${isUpdate ? 'Item update' : 'Item creation'} success`)
      } catch (e) {
        showError('Failed to save item', e)
        return false
      }
      await refreshItems()
      return true
    },
    [refreshItems, showError, showSuccess]
  )

  const updateOrder = useCallback(
    async function (idOrderMap: IIdOrderMap) {
      let success = false
      try {
        await apiUpdateOrder(idOrderMap)
        success = true
        showSuccess('Order')
        refreshItems()
      } catch (e) {
        showError('Failed to update items order', e)
      }
      return success
    },
    [refreshItems, showError, showSuccess]
  )

  const deleteItem = useCallback(
    async (itemId: string) => {
      try {
        await apiDeleteCatalogItem(itemId)
        showSuccess('Delete item success')
      } catch (e) {
        showError('Failed to delete item', e)
        return false
      }
      await refreshItems()
      return true
    },
    [refreshItems, showError, showSuccess]
  )

  const contextValue = useMemo(
    () => ({
      items,
      categories,
      featureEnabled,
      staticUrl: props.staticUrl,
      discount: props.discount,
      toggleFeatureStatus,
      refreshItems,
      persistItem,
      updateOrder,
      deleteItem,
    }),
    [
      items,
      categories,
      featureEnabled,
      props.staticUrl,
      props.discount,
      toggleFeatureStatus,
      refreshItems,
      persistItem,
      updateOrder,
      deleteItem,
    ]
  )

  return (
    <CatalogItemsContext.Provider value={contextValue}>
      {props.children}
    </CatalogItemsContext.Provider>
  )
}
