import type { ICatalogItemLean } from 'src/models/CatalogItem'
import { ItemStatus } from 'src/models/CatalogItem.type'

export const NEW_CATALOG_ITEM: ICatalogItemLean = {
  _id: '',
  status: ItemStatus.Available,
  visibility: false,
  price: 0,
  order: 0,
  title: '',
  description: '',
  technique: '',
  dimensions: '',
  weight: '',
  createDate: null,
  updateDate: null,
  files: [],
}
