import { type Metadata } from 'next'

import { H1 } from '@/backend/_components/Titles'
import { type ICatalogItemLean } from 'src/models/CatalogItem'
import { type ICatalogItemCategoryLean } from 'src/models/CatalogItemCategory'
import { CodifModel, type ICodif, type ICodifLean } from 'src/models/Codif'
import { getCategories } from 'src/services/CatalogItemCategoryService'
import { getItems } from 'src/services/CatalogItemService'
import { connect, toSerializable } from 'src/services/DbService'
import { CatalogItems } from './components/CatalogItems'
import { CatalogItemsProvider } from './providers/CatalogItemsProvider'

const title = 'Catalog'

export const metadata: Metadata = {
  title,
}

export default async function CatalogPage() {
  await connect()

  const [items, categories, staticUrlCodif, featureStatusCodif, discountCodif] =
    await Promise.all([
      getItems({}),
      getCategories({}),
      CodifModel.findOne({ name: 'static-url' }).lean<ICodif>(),
      CodifModel.findOne({ name: 'catalog-status' }).lean<ICodif>(),
      CodifModel.findOne({
        name: 'catalog-discount',
      }).lean<ICodif>(),
    ])

  const itemsLean = Array.isArray(items)
    ? items.map((i) => toSerializable<ICatalogItemLean>(i))
    : []
  const categoriesLean = Array.isArray(categories)
    ? categories.map((i) => toSerializable<ICatalogItemCategoryLean>(i))
    : []
  const featureStatusCodifLean = featureStatusCodif
    ? toSerializable<ICodifLean>(featureStatusCodif)
    : null

  return (
    <>
      <H1>{title}</H1>

      {!featureStatusCodif && <strong>Missing feature status codif</strong>}
      {!staticUrlCodif && <strong>Missing static url codif</strong>}

      {featureStatusCodifLean && staticUrlCodif && (
        <CatalogItemsProvider
          items={itemsLean}
          categories={categoriesLean}
          featureStatusCodif={featureStatusCodifLean}
          staticUrl={staticUrlCodif.value as string}
          discount={discountCodif ? (discountCodif.value as number) : undefined}
        >
          <CatalogItems />
        </CatalogItemsProvider>
      )}
    </>
  )
}
