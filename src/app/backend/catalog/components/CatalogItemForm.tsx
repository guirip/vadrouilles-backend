'use client'

import { faCheck, faTimes } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { Button } from 'primereact/button'
import { Dialog } from 'primereact/dialog'
import { Dropdown } from 'primereact/dropdown'
import type { FileUploadUploadEvent } from 'primereact/fileupload'
import { FileUpload } from 'primereact/fileupload'
import { InputSwitch } from 'primereact/inputswitch'
import { useCallback, useMemo, useRef, useState } from 'react'
import styled from 'styled-components'

import { useToastContext } from 'src/app/_components/ToastProvider'
import {
  Container,
  Field,
  FieldTitle,
  InlineField,
  StyledInputNumber,
  StyledInputText,
} from '@/backend/_components/dialog/Dialog.styles'
import { renderFooter } from '@/backend/_components/dialog/Dialog.util'
import {
  apiDeleteCatalogItemFile,
  apiSetMainPicture,
} from 'src/client-api/CatalogItemApi'
import type { ICatalogItemLean } from 'src/models/CatalogItem'
import {
  ITEM_DEFAULT_STATUS,
  ItemStatus,
  MAX_FILE_SIZE,
} from 'src/models/CatalogItem.type'
import type { IFile } from 'src/models/common/File.type'
import { getFieldsNotSet } from 'src/utils/formUtil'
import { useCatalogItemsContext } from '../providers/CatalogItemsProvider'
import { PICTURES_UPLOAD_API_URL } from 'src/client-api/constants'

const ThumbContainer = styled.a`
  display: flex;
  align-items: center;
  justify-content: center;
  position: relative;
  max-width: 120px;
  max-height: 120px;

  & img {
    min-width: 80px;
    max-width: 100px;
    min-height: 80px;
    max-height: 100px;
  }
`
const Thumb = styled.img`
  object-fit: contain;
`
const ThumbButton = styled(Button)`
  position: absolute;
  right: 0;
  transform: scale(0.9);
`
const StyleFileDeleteButton = styled(ThumbButton)`
  top: 8px;
`
const StyleSetMainButton = styled(ThumbButton)`
  bottom: 8px;
`
const StyledFileUpload = styled(FileUpload)`
  max-width: 82vw;

  & .p-fileupload-buttonbar .p-button {
    font-size: 0.8rem;
  }
  /* Hack to hide the 'upload' button */
  & .p-fileupload-buttonbar > :nth-child(2) {
    display: none;
  }
`

const DEFAULT_ITEM_STATE: ICatalogItemLean = {
  _id: '',
  title: '',
  status: ITEM_DEFAULT_STATUS,
  visibility: true,
  price: 0,
  description: '',
  technique: '',
  dimensions: '',
  weight: '',
  order: -1,
  createDate: null,
  updateDate: null,
  files: [],
}

const mandatoryFields = ['title']

function setUploadRequestHeader({ xhr }) {
  xhr.setRequestHeader('Accept', 'application/json')
}

interface IProps {
  visible: boolean
  item: ICatalogItemLean
  onSubmit: (item: ICatalogItemLean) => void
  onHide: () => void
}

export function CatalogItemForm(props: IProps) {
  const { staticUrl, categories, refreshItems } = useCatalogItemsContext()
  const { showSuccess, showWarning } = useToastContext()
  const { visible, onSubmit, onHide } = props

  const [item, updateEditedItem] = useState({
    ...DEFAULT_ITEM_STATE,
    ...props.item,
  })

  const categoryValues = useMemo(
    () =>
      categories.map((cat) => ({
        label: cat.name?.fr,
        value: cat._id,
      })),
    [categories]
  )

  const fileUploadRef = useRef<FileUpload>(null)

  const itemStatuses = useMemo(
    () =>
      Object.values(ItemStatus).map((status) => ({
        label: status,
        value: status,
      })),
    []
  )

  const setItemFiles = useCallback(
    function (files: IFile[]) {
      updateEditedItem((item) => ({ ...item, files }))
    },
    [updateEditedItem]
  )

  const onPicturesValidationFail = useCallback(
    function (file: File) {
      showWarning(
        'Image rejected (too big?)',
        `${file.size ? `(${Math.floor(file.size / 10000) / 100} mo)` : ''} ${
          file.name
        }`
      )
    },
    [showWarning]
  )

  const onPictureUploadError = useCallback(
    function ({ xhr }) {
      showWarning('Upload failed', `${xhr.status} ${xhr.statusText}`)
    },
    [showWarning]
  )

  const onClickOnSubmit = useCallback(
    function () {
      const fieldsNotSet = getFieldsNotSet(mandatoryFields, item)
      if (fieldsNotSet.length > 0) {
        showWarning('Empty field', `${fieldsNotSet.join(', ')} must be set`)
      } else if (!fileUploadRef.current) {
        throw new Error('fileUploadRef should be defined')
      } else {
        fileUploadRef.current.upload()
      }
    },
    [item, showWarning]
  )

  const afterPicturesUpload = useCallback(
    async function ({ xhr }: FileUploadUploadEvent) {
      if (xhr) {
        const response = await new Response(xhr.responseText).json()
        let { outputs } = response
        const { errors } = response

        if (Array.isArray(item.files)) {
          outputs = item.files.concat(outputs)
        }

        const proceed = () => {
          onSubmit({ ...item, files: outputs })
        }

        if (Array.isArray(errors) && errors.length > 0) {
          showWarning(`${errors.length} errors occured`, errors.join(' + '))
          window.setTimeout(proceed, 4000)
        } else {
          proceed()
        }
      } else {
        console.error(
          'unexpected case, this function should receive `xhr` as parameter'
        )
        onSubmit(item)
      }
    },
    [item, onSubmit, showWarning]
  )

  const deleteFile = useCallback(
    async function (file) {
      let success = false
      try {
        setItemFiles(await apiDeleteCatalogItemFile(item._id, file.low))
        success = true
      } catch (e) {
        showWarning('Failed to delete file', e)
      }
      if (success) {
        await refreshItems()
      }
    },
    [item._id, refreshItems, setItemFiles, showWarning]
  )

  const setMainFile = useCallback(
    async function (file: IFile) {
      let success = false
      try {
        const updatedItemFiles = await apiSetMainPicture(item._id, file.low)
        setItemFiles(updatedItemFiles)
        showSuccess('Main picture set')
        success = true
      } catch (e) {
        showWarning('Failed to set main file', e)
      }
      if (success) {
        await refreshItems()
      }
    },
    [item._id, refreshItems, setItemFiles, showSuccess, showWarning]
  )

  return (
    <Dialog
      visible={visible}
      showHeader={false}
      closable={false}
      maximizable
      blockScroll
      maskClassName="dialog-mask"
      onHide={onHide}
      footer={renderFooter(onClickOnSubmit, onHide)}
    >
      <Container className="flex flex-column">
        <Field>
          <FieldTitle>Title</FieldTitle>
          <StyledInputText
            value={item.title}
            onChange={(e) => {
              updateEditedItem((item) => ({ ...item, title: e.target.value }))
            }}
          />
        </Field>

        <InlineField>
          <FieldTitle>Category</FieldTitle>
          <Dropdown
            value={item.category}
            options={categoryValues}
            onChange={(e) => {
              updateEditedItem((item) => ({ ...item, category: e.value }))
            }}
            placeholder="Category"
          />
        </InlineField>

        <Field>
          <FieldTitle>Description</FieldTitle>
          <StyledInputText
            value={item.description}
            onChange={(e) => {
              updateEditedItem((item) => ({
                ...item,
                description: e.target.value,
              }))
            }}
          />
        </Field>

        <Field>
          <FieldTitle>Technique</FieldTitle>
          <StyledInputText
            value={item.technique}
            onChange={(e) => {
              updateEditedItem((item) => ({
                ...item,
                technique: e.target.value,
              }))
            }}
          />
        </Field>

        <InlineField>
          <FieldTitle>Status</FieldTitle>
          <Dropdown
            value={item.status}
            options={itemStatuses}
            onChange={(e) => {
              updateEditedItem((item) => ({ ...item, status: e.value }))
            }}
            placeholder="Status"
          />
        </InlineField>

        <InlineField>
          <FieldTitle>Visibility</FieldTitle>
          <InputSwitch
            checked={item.visibility}
            onChange={(e) => {
              updateEditedItem((item) => ({
                ...item,
                visibility: e.value === true,
              }))
            }}
          />
        </InlineField>

        <InlineField>
          <FieldTitle>Price (€)</FieldTitle>
          <StyledInputNumber
            value={item.price}
            onValueChange={(e) => {
              updateEditedItem((item) => ({ ...item, price: e.value ?? 0 }))
            }}
            maxFractionDigits={0}
            useGrouping={false}
          />
        </InlineField>

        <InlineField>
          <FieldTitle>Dimensions</FieldTitle>
          <StyledInputText
            value={item.dimensions}
            onChange={(e) => {
              updateEditedItem((item) => ({
                ...item,
                dimensions: e.target.value,
              }))
            }}
          />
        </InlineField>

        <InlineField>
          <FieldTitle>Weight (gr)</FieldTitle>
          <StyledInputText
            value={item.weight}
            onChange={(e) => {
              updateEditedItem((item) => ({ ...item, weight: e.target.value }))
            }}
          />
        </InlineField>

        <Field>
          <FieldTitle>Photos</FieldTitle>
          {Array.isArray(item.files) && item.files.length > 0 && (
            <div className="flex flex-wrap">
              {item.files.map((file) => (
                <ThumbContainer
                  className="p-d-relative"
                  key={file.low}
                  href={`${staticUrl}/${file.mid}`}
                  target="_blank"
                >
                  <Thumb className="m-2" src={`${staticUrl}/${file.low}`} />
                  <StyleFileDeleteButton
                    icon={<FontAwesomeIcon icon={faTimes} size="lg" />}
                    className="p-button-sm p-button-rounded"
                    severity="danger"
                    onClick={(e) => {
                      e.preventDefault()
                      deleteFile(file)
                    }}
                    title="Delete picture"
                  />
                  <StyleSetMainButton
                    icon={<FontAwesomeIcon icon={faCheck} size="lg" />}
                    severity={file.isMain ? 'info' : 'help'}
                    className="p-button-sm p-button-rounded"
                    onClick={(e) => {
                      e.preventDefault()
                      setMainFile(file)
                    }}
                    title="Set main picture"
                  />
                </ThumbContainer>
              ))}
            </div>
          )}
          <StyledFileUpload
            ref={fileUploadRef}
            name="itemPhotos"
            multiple
            accept="image/*"
            url={PICTURES_UPLOAD_API_URL}
            maxFileSize={MAX_FILE_SIZE}
            onUpload={afterPicturesUpload}
            onBeforeSend={setUploadRequestHeader}
            onValidationFail={onPicturesValidationFail}
            onError={onPictureUploadError}
            emptyTemplate={<span></span>}
          />
        </Field>
      </Container>
    </Dialog>
  )
}
