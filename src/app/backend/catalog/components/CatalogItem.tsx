'use client'

import { Button } from 'primereact/button'
import type { BaseSyntheticEvent } from 'react'
import styled, { css } from 'styled-components'

import { faEye, faEyeSlash } from '@fortawesome/free-regular-svg-icons'
import { faPen, faTimes } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import type { ICatalogItemLean } from 'src/models/CatalogItem'
import type { ICatalogItemCategoryLean } from 'src/models/CatalogItemCategory'
import { useCatalogItemsContext } from '../providers/CatalogItemsProvider'
import { FONT_SIZE } from 'src/app/_style/style.constants'

const ItemContainer = styled.div`
  display: flex;
  flex-wrap: wrap;
  align-items: center;
  justify-content: space-evenly;
  max-width: 96vw;
  font-size: ${FONT_SIZE.sm};
  border: 1px solid #2f2f2f;

  @media (max-width: 576px) {
    & {
      flex-direction: column;
    }
  }
`
const ThumbContainer = styled.div`
  flex: 0 0 100px;
  display: flex;
  align-items: center;
  height: auto;

  a {
    width: 100%;
  }

  /* related to ItemContainer media query setting
    'flex-direction: column' on small screens */
  @media (max-width: 576px) {
    & {
      max-width: 80vw;
    }
  }

  @media (min-width: 768px) {
    & {
      flex-basis: 240px;
    }
  }
  @media (min-width: 1280px) {
    & {
      flex-basis: 300px;
    }
  }
`
const Thumb = styled.img`
  width: 100%;
  max-width: -webkit-fill-available;

  /* related to ItemContainer media query setting
    'flex-direction: column' on small screens */
  @media (max-width: 576px) {
    & {
      /*width: -webkit-fill-available;*/
    }
  }
`
const InfoBlock = styled.div`
  flex-grow: 2;
`
const Field = styled.div`
  display: flex;
  flex-direction: column;
  margin: 0 0 0.7rem;
`
const InlineField = styled(Field)`
  flex-direction: row;
  align-items: center;
`
const FieldName = styled.div`
  margin: 0.1em 1em 0.1em 0;
  font-size: ${FONT_SIZE.xs};
  font-weight: bold;
  opacity: 0.5;
  letter-spacing: 0.5px;
`

interface IPriceProps {
  $hasDiscount?: boolean
  $isDiscount?: boolean
}

const Price = styled.div<IPriceProps>`
  ${(props) =>
    props.$hasDiscount &&
    css`
      text-decoration: line-through;
      font-size: 0.9rem;
    `}
  ${(props) =>
    props.$isDiscount &&
    css`
      margin: 0 0.4rem;
      color: lightgreen;
      font-weight: bold;
    `}
`
const Discount = styled.div`
  opacity: 0.8;
  letter-spacing: 0.5px;
`
const ActionBlock = styled.div`
  flex-grow: 1;
`
const StyledEmptyField = styled.div`
  font-style: italic;
  opacity: 0.6;
`

const EmptyField = () => <StyledEmptyField>None</StyledEmptyField>

const getDiscountPrice = (price: number, discount: number) =>
  Math.floor(price - (price * discount) / 100)

interface IProps {
  item: ICatalogItemLean
  onEdit: (e: BaseSyntheticEvent, itemId: ICatalogItemLean['_id']) => void
  onDelete: (e: BaseSyntheticEvent, itemId: ICatalogItemLean['_id']) => void
  category?: ICatalogItemCategoryLean
}

const CatalogItem = ({ item, onEdit, onDelete, category }: IProps) => {
  const { staticUrl, discount } = useCatalogItemsContext()

  let mainPicture
  if (Array.isArray(item.files) && item.files.length > 0) {
    mainPicture = item.files.find((file) => file.isMain)

    if (!mainPicture) {
      // Fallback to first picture
      mainPicture = item.files[0]
    }
  }

  const hasDiscount = typeof discount === 'number' && discount > 0

  return (
    <ItemContainer className="my-3 px-2 py-3">
      <ThumbContainer>
        {mainPicture && (
          <a href={`${staticUrl}/${mainPicture.mid}`} target="_blank">
            <Thumb src={`${staticUrl}/${mainPicture.low}`} />
          </a>
        )}
      </ThumbContainer>

      <InfoBlock className="item-info-block align-items-center m-3">
        <Field>
          <FieldName>Title</FieldName>
          <div style={{ fontSize: '1.2rem' }}>{item.title}</div>
        </Field>

        <Field>
          <FieldName>Category</FieldName>
          <div>
            {!category
              ? 'None'
              : `🇫🇷 ${category.name.fr} 🇬🇧 ${category.name.en}`}
          </div>
        </Field>

        <Field>
          <FieldName>Description</FieldName>
          <div>{item.description || <EmptyField />}</div>
        </Field>

        <Field>
          <FieldName>Technique</FieldName>
          <div>{item.technique || <EmptyField />}</div>
        </Field>

        <InlineField>
          <FieldName>Status</FieldName>
          <div>{item.status}</div>
        </InlineField>

        <InlineField>
          <FieldName>Visibility</FieldName>
          <div>
            <FontAwesomeIcon icon={item.visibility ? faEye : faEyeSlash} />
          </div>
        </InlineField>

        <InlineField>
          <FieldName>Price</FieldName>
          <Price $hasDiscount={hasDiscount}>{item.price} €</Price>
          {hasDiscount && (
            <>
              <Price $isDiscount>
                {getDiscountPrice(item.price ?? 0, discount)} €
              </Price>
              <Discount>(-{discount}%)</Discount>
            </>
          )}
        </InlineField>

        <InlineField>
          <FieldName>Dimensions</FieldName>
          <div>{item.dimensions || <EmptyField />}</div>
        </InlineField>

        <InlineField>
          <FieldName>Weight</FieldName>
          <div>{item.weight || <EmptyField />}</div>
        </InlineField>
      </InfoBlock>

      <ActionBlock className="item-action-block flex align-items-center justify-content-center mt-3 mb-0">
        <Button
          className="p-button-primary p-button-sm m-1 text-lg"
          icon={<FontAwesomeIcon className="mr-2" icon={faPen} />}
          onClick={(e) => onEdit(e, item._id)}
          label="Edit"
        />
        <Button
          className="p-button-danger p-button-sm m-1 ml-2"
          icon={<FontAwesomeIcon icon={faTimes} size="xl" />}
          onClick={(e) => onDelete(e, item._id)}
        />
      </ActionBlock>
    </ItemContainer>
  )
}

export default CatalogItem
