'use client'

import {
  faArrowRotateRight,
  faPlus,
  faWarning,
} from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { Button } from 'primereact/button'
import { ConfirmPopup, confirmPopup } from 'primereact/confirmpopup'
import { InputSwitch } from 'primereact/inputswitch'
import type { BaseSyntheticEvent } from 'react'
import { useCallback, useState } from 'react'
import styled from 'styled-components'

import SortableList from '@/backend/_components/sortable-list'
import type { ICatalogItemLean } from 'src/models/CatalogItem'
import { NEW_CATALOG_ITEM } from '../constants'
import { useCatalogItemsContext } from '../providers/CatalogItemsProvider'
import CatalogItem from './CatalogItem'
import { CatalogItemForm } from './CatalogItemForm'

const AddItemButton = styled(Button)`
  height: fit-content;
`
const ListWrapper = styled.div`
  max-width: 1024px;
`

export function CatalogItems() {
  const {
    items,
    featureEnabled,
    categories,
    toggleFeatureStatus,
    refreshItems,
    persistItem,
    updateOrder,
    deleteItem,
  } = useCatalogItemsContext()

  const [editedItem, setEditedItem] = useState<ICatalogItemLean | null>(null)

  const showForm = useCallback(function () {
    setEditedItem({ ...NEW_CATALOG_ITEM })
  }, [])

  const hideForm = useCallback(function () {
    setEditedItem(null)
  }, [])

  const onClickOnItemEdit = useCallback(
    function (_e, itemId: string) {
      const item = items.find((_item) => _item._id === itemId)
      if (!item) {
        console.error('Unable to locally find the item to edit')
        return
      }
      setEditedItem(item)
    },
    [items, setEditedItem]
  )

  const onClickOnSave = useCallback(
    async (catalogItem: ICatalogItemLean) => {
      const success = await persistItem(catalogItem)
      if (success) {
        hideForm()
      }
    },
    [hideForm, persistItem]
  )

  const onClickOnDeleteItem = useCallback(
    async (e: BaseSyntheticEvent, itemId: string) => {
      confirmPopup({
        target: e.currentTarget,
        message: 'Confirm catalog item deletion',
        icon: <FontAwesomeIcon icon={faWarning} size="lg" />,
        accept: () => deleteItem(itemId),
      })
    },
    [deleteItem]
  )

  const renderItem = useCallback(
    (item: ICatalogItemLean) => (
      <CatalogItem
        item={item}
        onEdit={onClickOnItemEdit}
        onDelete={onClickOnDeleteItem}
        category={
          !item.category
            ? undefined
            : categories.find((cat) => cat._id === item.category)
        }
      />
    ),
    [categories, onClickOnDeleteItem, onClickOnItemEdit]
  )

  return (
    <>
      <ConfirmPopup />

      <div
        style={{ maxWidth: 400 }}
        className="flex justify-content-evenly align-items-center"
      >
        <div className="flex flex-column align-items-center">
          <div className="mx-3 my-2 text-lg">
            Feature {featureEnabled ? 'en' : 'dis'}abled
          </div>
          <InputSwitch
            className="m-1"
            checked={featureEnabled}
            onChange={toggleFeatureStatus}
          />
        </div>
        <AddItemButton
          icon={<FontAwesomeIcon className="mr-2" icon={faPlus} />}
          className="text-lg ml-4 mr-3"
          onClick={showForm}
          label="Add item"
        />
        <Button
          className="p-button-plain p-button p-button-rounded p-button-outlined mx-3"
          icon={<FontAwesomeIcon icon={faArrowRotateRight} size="lg" />}
          onClick={refreshItems}
        />
      </div>

      {editedItem && (
        <CatalogItemForm
          visible={true}
          item={editedItem}
          onSubmit={onClickOnSave}
          onHide={hideForm}
        />
      )}

      <ListWrapper>
        {Array.isArray(items) !== true || items.length === 0 ? (
          <div className="m-6 p-text-italic">No items yet</div>
        ) : (
          <SortableList
            content={items}
            renderItem={renderItem}
            onOrderUpdate={updateOrder}
          />
        )}
      </ListWrapper>
    </>
  )
}
