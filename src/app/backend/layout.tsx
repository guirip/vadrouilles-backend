import type { PropsWithChildren } from 'react'

import 'primeflex/primeflex.css'
import 'primereact/resources/primereact.min.css'
import 'primereact/resources/themes/mdc-dark-indigo/theme.css'

import { PageContainer } from '@/backend/_components/PageContainer'
import { ToastProvider } from 'src/app/_components/ToastProvider'
import { AuthProvider } from '@/backend/_components/login/AuthProvider'
import { LogOutButton } from '@/backend/_components/login/LogOutButton'
import StyledComponentsRegistry from 'src/app/styled-components-registry'
import { Menu } from './_components/Menu'
import { DeviceContextProvider } from 'src/app/_components/DeviceContextProvider'
import GlobalStyle from './_style/GlobalStyle'

export default function RootLayout({ children }: PropsWithChildren) {
  return (
    <html>
      <body>
        <StyledComponentsRegistry>
          <GlobalStyle>
            <DeviceContextProvider>
              <ToastProvider>
                <PageContainer>
                  <AuthProvider>
                    <Menu />
                    <LogOutButton />
                    {children}
                  </AuthProvider>
                </PageContainer>
              </ToastProvider>
            </DeviceContextProvider>
          </GlobalStyle>
        </StyledComponentsRegistry>
      </body>
    </html>
  )
}
