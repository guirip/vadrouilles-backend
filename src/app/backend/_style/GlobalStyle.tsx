'use client'

import type { PropsWithChildren } from 'react'
import {
  BG_BY_CATEGORY,
  DEFAULT_ROOT_FONT_SIZE,
  getHtmlFontSizeStyle,
} from 'src/app/_style/style.constants'
import { createGlobalStyle } from 'styled-components'

const GlobalStyle = createGlobalStyle`
html,
body {
  height: 100%;
  width: 100%;
  padding: 0;
  margin: 0;
  font-family:
    -apple-system,
    BlinkMacSystemFont,
    Segoe UI,
    Roboto,
    Oxygen,
    Ubuntu,
    Cantarell,
    Fira Sans,
    Droid Sans,
    Helvetica Neue,
    sans-serif;
  color: white;
  background-image: url('${BG_BY_CATEGORY.nature.src}');
  background-size: cover;
  overflow: hidden;
}

${getHtmlFontSizeStyle(DEFAULT_ROOT_FONT_SIZE, false)}

a {
  color: inherit;
  text-decoration: none;
}

* {
  box-sizing: border-box;
}

.dialog-mask {
  background-color: rgba(0, 0, 0, 0.6) !important;
}

.slow-blink {
  animation: 0.7s linear 0s infinite alternate slow_blink;
}

@keyframes slow_blink {
  from {
    opacity: 1;
  }
  to {
    opacity: 0.4;
  }
}
`

export default function ({ children }: PropsWithChildren) {
  return (
    <>
      <GlobalStyle />
      {children}
    </>
  )
}
