'use client'

import type { PropsWithChildren } from 'react'
import {
  createContext,
  useCallback,
  useContext,
  useMemo,
  useState,
} from 'react'

import { useToastContext } from 'src/app/_components/ToastProvider'
import {
  apiCreateItemCategory,
  apiDeleteItemCategory,
  apiFetchItemCategories,
  apiUpdateItemCategory,
  apiUpdateOrder,
} from 'src/client-api/CatalogItemCategoryApi'
import type { ICatalogItemCategoryLean } from 'src/models/CatalogItemCategory'
import type { IIdOrderMap } from 'src/models/common/IdOrderMap'
import { isNewItemCategory } from '../constants'

interface ICatalogItemCategoriesContext {
  itemCategories: ICatalogItemCategoryLean[]
  defaultCategoriesCount: number
  refreshItemCategories: () => Promise<void>
  saveItemCategory: (itemCat: ICatalogItemCategoryLean) => Promise<boolean>
  updateOrder: (idOrderMap: IIdOrderMap) => Promise<boolean>
  deleteItemCategory: (_id: string) => Promise<void>
}

const CatalogItemCategoriesContext =
  createContext<ICatalogItemCategoriesContext | null>(null)

export function useCatalogItemCategoriesContext() {
  const context = useContext(CatalogItemCategoriesContext)
  if (!context) {
    throw new Error('Catalog item categories context is not initialized yet')
  }
  return context
}

interface IProps {
  itemCategories: ICatalogItemCategoryLean[]
}

export function CatalogItemCategoriesProvider(
  props: PropsWithChildren<IProps>
) {
  const { showSuccess, showError } = useToastContext()

  const [itemCategories, setItemCategories] = useState(
    props.itemCategories || []
  )

  const defaultCategoriesCount = useMemo(
    () => itemCategories.filter((cat) => cat.isDefault === true).length,
    [itemCategories]
  )

  const refreshItemCategories = useCallback(
    async function () {
      try {
        setItemCategories(await apiFetchItemCategories())
      } catch (e) {
        showError('Failed to fetch', e.message ?? e)
      }
    },
    [showError]
  )

  const saveItemCategory = useCallback(
    async function (itemCat: ICatalogItemCategoryLean) {
      const isCreate = isNewItemCategory(itemCat)
      try {
        if (isCreate) {
          const { _id, ...itemCategoryData } = itemCat
          await apiCreateItemCategory(itemCategoryData)
        } else {
          await apiUpdateItemCategory(itemCat)
        }
        showSuccess('Category saved')
        await refreshItemCategories()
      } catch (e) {
        showError('Failed to save itemCategory', e.message ?? e)
        return false
      }
      return true
    },
    [refreshItemCategories, showError, showSuccess]
  )

  const updateOrder = useCallback(
    async function (idOrderMap: IIdOrderMap) {
      let success = false
      try {
        await apiUpdateOrder(idOrderMap)
        success = true
        showSuccess('Order')
        refreshItemCategories()
      } catch (e) {
        showError('Failed to update item categories order', e)
      }
      return success
    },
    [refreshItemCategories, showError, showSuccess]
  )

  const deleteItemCategory = useCallback(
    async function (_id: string) {
      try {
        await apiDeleteItemCategory(_id)
        await refreshItemCategories()
      } catch (e) {
        showError('Failed to delete itemCategory', e.message ?? e)
      }
    },
    [refreshItemCategories, showError]
  )

  const contextValue = useMemo(
    () => ({
      itemCategories,
      defaultCategoriesCount,
      refreshItemCategories,
      saveItemCategory,
      updateOrder,
      deleteItemCategory,
    }),
    [
      itemCategories,
      defaultCategoriesCount,
      refreshItemCategories,
      saveItemCategory,
      updateOrder,
      deleteItemCategory,
    ]
  )

  return (
    <CatalogItemCategoriesContext.Provider value={contextValue}>
      {props.children}
    </CatalogItemCategoriesContext.Provider>
  )
}
