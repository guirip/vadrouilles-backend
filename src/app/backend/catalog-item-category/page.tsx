import { type Metadata } from 'next'

import { H1 } from '@/backend/_components/Titles'
import type {
  ICatalogItemCategory,
  ICatalogItemCategoryLean,
} from 'src/models/CatalogItemCategory'
import { CatalogItemCategoryModel } from 'src/models/CatalogItemCategory'
import { connect, toSerializable } from 'src/services/DbService'
import { CatalogItemCategories } from './components/CatalogItemCategories'
import { CatalogItemCategoriesProvider } from './providers/CatalogItemCategoriesProvider'

const title = 'Catalog item categories'

export const metadata: Metadata = {
  title,
}

export default async function CatalogItemCategoryPage() {
  await connect()

  const itemCategories = await CatalogItemCategoryModel.find({})
    .sort({ order: 1 })
    .lean<ICatalogItemCategory[]>()

  const itemCategoriesLean = itemCategories.map((v) =>
    toSerializable<ICatalogItemCategoryLean>(v)
  )

  return (
    <>
      <H1>{title}</H1>
      <CatalogItemCategoriesProvider itemCategories={itemCategoriesLean}>
        <CatalogItemCategories />
      </CatalogItemCategoriesProvider>
    </>
  )
}
