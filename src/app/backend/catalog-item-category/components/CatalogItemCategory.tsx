'use client'

import { faCheckCircle } from '@fortawesome/free-regular-svg-icons'
import { faTimes } from '@fortawesome/free-solid-svg-icons'
import { Button } from 'primereact/button'
import { InputSwitch } from 'primereact/inputswitch'
import { InputText } from 'primereact/inputtext'
import type { BaseSyntheticEvent } from 'react'
import { useState } from 'react'
import styled from 'styled-components'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'

import type { ICatalogItemCategoryLean } from 'src/models/CatalogItemCategory'
import { slugify } from 'src/utils/stringUtil'
import { isNewItemCategory } from '../constants'

export const NEW_CATEGORY_ID = 'tmp'

const StyledCatalogItemCategory = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: center;
  align-items: center;
  flex-wrap: wrap;
  margin: 0.5rem 0;
  padding: 1rem 0;

  & > * {
    margin: 0.3rem;
  }
`
const Switches = styled.div`
  display: flex;
  flex-direction: row;
`
const SwitchWrapper = styled.div``
const SwitchLabel = styled.div`
  font-size: 0.8rem;
`

interface IProps {
  itemCategory: ICatalogItemCategoryLean
  onSave: (itemCategory: ICatalogItemCategoryLean) => Promise<void>
  onDelete: (e: BaseSyntheticEvent, itemCategoryId: string) => Promise<void>
}

export function CatalogItemCategory(props: IProps) {
  const [itemCategory, setItemCategory] = useState(props.itemCategory)

  return (
    <StyledCatalogItemCategory>
      <div className="p-float-label">
        <InputText
          id={`input-text-label-fr-${itemCategory._id}`}
          className="mb-1 text-lg"
          value={itemCategory.name.fr}
          onChange={({ target }) => {
            setItemCategory({
              ...itemCategory,
              name: {
                ...itemCategory.name,
                fr: target.value,
              },
            })
          }}
        />
        <label htmlFor={`input-text-label-fr-${itemCategory._id}`}>
          Name FR 🇫🇷
        </label>
      </div>

      <div className="p-float-label">
        <InputText
          id={`input-text-label-en-${itemCategory._id}`}
          className="mb-1 text-lg"
          value={itemCategory.name.en}
          onChange={({ target }) => {
            setItemCategory({
              ...itemCategory,
              name: {
                ...itemCategory.name,
                en: target.value,
              },
            })
          }}
        />
        <label htmlFor={`input-text-label-en-${itemCategory._id}`}>
          Name EN 🇬🇧
        </label>
      </div>

      <div className="p-float-label">
        <InputText
          id={`input-text-slug-${itemCategory._id}`}
          className="mb-1 text-lg"
          value={itemCategory.slug}
          onChange={({ target }) => {
            setItemCategory({
              ...itemCategory,
              slug: slugify(target.value),
            })
          }}
        />
        <label htmlFor={`input-text-slug-${itemCategory._id}`}>Slug *</label>
      </div>

      <Switches>
        <SwitchWrapper className="flex-column justify-content-center align-items-center mx-2">
          <SwitchLabel className="p-text-center mb-2 text-lg">
            Default category
          </SwitchLabel>
          <InputSwitch
            className="mx-5"
            checked={itemCategory.isDefault === true}
            onChange={(e) => {
              setItemCategory({
                ...itemCategory,
                isDefault: e.value === true,
              })
            }}
          />
        </SwitchWrapper>

        <SwitchWrapper className="flex-column justify-content-center align-items-center ml-2">
          <SwitchLabel className="p-text-center mb-2 text-lg">
            Visibility
          </SwitchLabel>
          <InputSwitch
            className="mx-5"
            checked={itemCategory.visibility === true}
            onChange={(e) => {
              setItemCategory({
                ...itemCategory,
                visibility: e.value === true,
              })
            }}
          />
        </SwitchWrapper>
      </Switches>

      <div className="flex flex-nowrap">
        <Button
          className={`${
            isNewItemCategory(itemCategory) ? 'slow-blink' : ''
          } p-button-primary mx-2 text-lg`}
          icon={
            <FontAwesomeIcon className="mr-2" icon={faCheckCircle} size="lg" />
          }
          onClick={() => {
            props.onSave(itemCategory)
          }}
          label="Save"
        />
        <Button
          className="p-button-warning mx-1"
          icon={<FontAwesomeIcon className="mr-2" icon={faTimes} size="lg" />}
          onClick={(e) => {
            props.onDelete(e, itemCategory._id)
          }}
          label="Delete"
        />
      </div>
    </StyledCatalogItemCategory>
  )
}
