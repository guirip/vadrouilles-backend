'use client'

import {
  faArrowRotateRight,
  faPlus,
  faWarning,
} from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { Button } from 'primereact/button'
import { ConfirmPopup, confirmPopup } from 'primereact/confirmpopup'
import type { BaseSyntheticEvent } from 'react'
import { useCallback, useState } from 'react'

import SortableList from '@/backend/_components/sortable-list'
import { Warning } from '@/backend/_components/Warning'
import type { ICatalogItemCategoryLean } from 'src/models/CatalogItemCategory'
import { NEW_ITEM_CATEGORY, isNewItemCategory } from '../constants'
import { useCatalogItemCategoriesContext } from '../providers/CatalogItemCategoriesProvider'
import { CatalogItemCategory } from './CatalogItemCategory'

export const CatalogItemCategories = () => {
  const {
    itemCategories,
    defaultCategoriesCount,
    refreshItemCategories,
    saveItemCategory,
    updateOrder,
    deleteItemCategory,
  } = useCatalogItemCategoriesContext()

  const [newItemCategory, setNewItemCategory] =
    useState<ICatalogItemCategoryLean | null>(null)

  const addNewItemCategory = useCallback(function () {
    setNewItemCategory({ ...NEW_ITEM_CATEGORY })
  }, [])

  const onClickOnSave = useCallback(
    async function (itemCategory: ICatalogItemCategoryLean) {
      const success = await saveItemCategory(itemCategory)
      if (success && isNewItemCategory(itemCategory)) {
        setNewItemCategory(null)
      }
    },
    [saveItemCategory]
  )

  const onClickOnDelete = useCallback(
    async function (event: BaseSyntheticEvent, itemCatId: string) {
      confirmPopup({
        target: event.currentTarget,
        message: 'Confirm item category deletion',
        icon: <FontAwesomeIcon icon={faWarning} size="lg" />,
        accept: () => {
          if (itemCatId === NEW_ITEM_CATEGORY._id) {
            setNewItemCategory(null)
            return
          }
          deleteItemCategory(itemCatId)
        },
      })
    },
    [deleteItemCategory]
  )

  const renderItemCategory = useCallback(
    (itemCategory: ICatalogItemCategoryLean) => (
      <CatalogItemCategory
        itemCategory={itemCategory}
        onSave={onClickOnSave}
        onDelete={onClickOnDelete}
      />
    ),
    [onClickOnSave, onClickOnDelete]
  )

  return (
    <>
      <ConfirmPopup />

      {defaultCategoriesCount > 1 && (
        <Warning>
          ⚠ There are {defaultCategoriesCount} default categories.
        </Warning>
      )}

      <div className="flex my-4">
        <Button
          className="mr-3 text-lg"
          icon={<FontAwesomeIcon className="mr-1" icon={faPlus} />}
          onClick={addNewItemCategory}
          label="Add item category"
        />
        <Button
          className="p-button-plain p-button p-button-rounded p-button-outlined mx-auto"
          icon={<FontAwesomeIcon icon={faArrowRotateRight} size="lg" />}
          onClick={refreshItemCategories}
        />
      </div>

      <div className="flex align-items-center flex-column mb-6">
        {newItemCategory && renderItemCategory(newItemCategory)}
        <SortableList
          content={itemCategories}
          renderItem={renderItemCategory}
          onOrderUpdate={updateOrder}
        />
      </div>
    </>
  )
}
