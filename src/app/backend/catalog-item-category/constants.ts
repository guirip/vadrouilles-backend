import type { ICatalogItemCategoryLean } from 'src/models/CatalogItemCategory'

export const NEW_ITEM_CATEGORY: ICatalogItemCategoryLean = {
  _id: '',
  name: {
    fr: '',
    en: '',
  },
  slug: '',
  visibility: true,
  order: 0,
  isDefault: false,
}

export const isNewItemCategory = (itemCat: ICatalogItemCategoryLean) =>
  itemCat._id === NEW_ITEM_CATEGORY._id
