import { type Metadata } from 'next'
import { H1 } from '@/backend/_components/Titles'
import type { ITextPageFront } from 'src/models/text/TextPage.type'
import {
  DEFAULT_TEXT_CATEGORY,
  isValidTextCategory,
} from 'src/models/text/TextPage.type'
import { connect } from 'src/services/DbService'
import { getPages } from 'src/services/TextService.page'
import { TextPages } from './components/page/TextPages'
import { TextPagesProvider } from './providers/TextPagesProvider'

const title = 'Texts & Chapters'

export const metadata: Metadata = {
  title,
}

interface IProps {
  searchParams: Promise<{
    category?: string
    slug?: string
  }>
}

export default async function TextPage(props: IProps) {
  const searchParams = await props.searchParams
  const category = searchParams.category
  const slug = searchParams.slug

  await connect()

  const currentCategory =
    typeof category === 'string' && isValidTextCategory(category)
      ? category
      : DEFAULT_TEXT_CATEGORY

  const textPages = await getPages(
    {
      category: currentCategory,
    },
    false
  )

  let currentTextPageId: null | ITextPageFront['_id'] = null

  if (Array.isArray(textPages) && textPages.length) {
    if (slug) {
      currentTextPageId =
        textPages.find((page) => page.slug === slug)?._id ?? null
    }
    if (!currentTextPageId) {
      currentTextPageId = textPages[0]._id
    }
  }

  return (
    <>
      <H1>{title}</H1>

      <TextPagesProvider
        textPages={textPages}
        currentTextPageId={currentTextPageId}
        currentCategory={currentCategory}
      >
        <TextPages />
      </TextPagesProvider>
    </>
  )
}
