'use client'

import { faPlus } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { Button } from 'primereact/button'
import { useCallback, useState } from 'react'
import styled from 'styled-components'

import Flag from '@/backend/_components/Flag'
import { SUPPORTED_LANG } from 'src/models/common/Lang'
import type {
  ILocalizedTextFormItem,
  ILocalizedTextMinimalFront,
} from 'src/models/text/LocalizedText.type'
import { useBoolean } from 'src/utils/useBoolean'
import { useChaptersContext } from '../../providers/ChaptersProvider'
import { useLocalizedTextsContext } from '../../providers/LocalizedTextsProvider'
import { LocalizedText } from './LocalizedText'
import { LocalizedTextForm } from './LocalizedTextForm'

export const Wrapper = styled.div`
  margin-top: 0.5rem;
`
export const TextsWrapper = styled.div`
  padding: 0 1rem 0.05rem;

  & > * {
    background-color: rgba(255, 255, 255, 0.1);
  }
  /*
  & > * {
    background-color: rgba(255, 255, 255, 0.05);
  }
  & > :nth-child(2n + 1) {
    background-color: rgba(255, 255, 255, 0.1);
  }
  */
`
const ButtonsRow = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  padding-top: 0.5rem;

  &:empty {
    display: none;
  }
`
const TranslateButton = styled(Button)<{ $isEnabled: boolean }>`
  opacity: ${(props) => (props.$isEnabled ? 1 : 0.5)};
  cursor: ${(props) => (props.$isEnabled ? 'pointer' : 'default')};
  transition: opacity 0.3s;
`

export const LocalizedTexts = () => {
  const {
    localizedTexts,
    persistLocalizedText,
    existingLocalizations,
    frLtIdWithContent,
    hasEnLt,
    translateFromFrench,
  } = useLocalizedTextsContext()

  const { refreshChapters } = useChaptersContext()

  const [editedLocalizedText, setEditedLocalizedText] =
    useState<ILocalizedTextFormItem | null>(null)

  const [
    isTranslateButtonEnabled,
    enableTranslateButton,
    disableTranslateButton,
  ] = useBoolean(true)

  const addLocalizedText = useCallback(
    function () {
      setEditedLocalizedText({
        _id: '',
        title: '',
        lang:
          SUPPORTED_LANG.find(
            (lang) => !existingLocalizations.includes(lang)
          ) || SUPPORTED_LANG[0],
        description: '',
        blocksCount: 0,
      })
    },
    [existingLocalizations, setEditedLocalizedText]
  )

  const onValidateLocalizedTextForm = useCallback(
    async (localizedText: ILocalizedTextMinimalFront) => {
      const success = await persistLocalizedText(localizedText)
      if (success) {
        setEditedLocalizedText(null)
        await refreshChapters()
        // setFolded(false)
      }
    },
    [persistLocalizedText, refreshChapters]
  )

  function onClickOnEdit(localizedText: ILocalizedTextMinimalFront) {
    setEditedLocalizedText(localizedText)
  }

  function hideLocalizedTextForm() {
    setEditedLocalizedText(null)
  }

  const onClickOnLocalizeFromFrench = useCallback(
    async function () {
      disableTranslateButton()
      await translateFromFrench()
      await refreshChapters()
      enableTranslateButton()
    },
    [
      disableTranslateButton,
      translateFromFrench,
      refreshChapters,
      enableTranslateButton,
    ]
  )

  return (
    <Wrapper>
      {editedLocalizedText && (
        <LocalizedTextForm
          visible={true}
          item={editedLocalizedText}
          callback={onValidateLocalizedTextForm}
          onHide={hideLocalizedTextForm}
        />
      )}

      {Array.isArray(localizedTexts) && localizedTexts.length > 0 && (
        <TextsWrapper>
          {localizedTexts.map((lt) => (
            <LocalizedText
              key={lt._id}
              localizedText={lt}
              onEdit={onClickOnEdit}
            />
          ))}
        </TextsWrapper>
      )}

      <ButtonsRow>
        {existingLocalizations.length < SUPPORTED_LANG.length && (
          <Button
            className="mx-2 p-button-rounded text-lg"
            icon={<FontAwesomeIcon className="mr-2" icon={faPlus} />}
            onClick={addLocalizedText}
            label="Add lang"
            size="small"
          />
        )}
        {frLtIdWithContent && !hasEnLt && (
          <TranslateButton
            $isEnabled={isTranslateButtonEnabled}
            title="Create EN version from french"
            className="p-0 p-button-rounded p-button-outlined mx-1"
            icon={<Flag $lang="en" $width="77%" $height="77%" />}
            onClick={
              isTranslateButtonEnabled ? onClickOnLocalizeFromFrench : undefined
            }
          />
        )}
      </ButtonsRow>
    </Wrapper>
  )
}
