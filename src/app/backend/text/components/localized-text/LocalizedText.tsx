'use client'

import { faPen, faTimes, faWarning } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import Link from 'next/link'
import { Badge } from 'primereact/badge'
import { confirmPopup } from 'primereact/confirmpopup'
import type { BaseSyntheticEvent } from 'react'
import { useCallback } from 'react'
import styled from 'styled-components'

import Flag from '@/backend/_components/Flag'
import { EDITOR_PAGE_PATH } from '@/backend/page-paths'
import type { ILocalizedTextMinimalFront } from 'src/models/text/LocalizedText.type'
import { useChaptersContext } from '../../providers/ChaptersProvider'
import { useLocalizedTextsContext } from '../../providers/LocalizedTextsProvider'
import { SmallerButton } from '../SmallerButton'

const Wrapper = styled.div``
const LocalizedTextTitle = styled.div`
  flex-grow: 1;
  font-size: 1.1em;
  font-weight: bold;
  cursor: pointer;
`

interface IProps {
  localizedText: ILocalizedTextMinimalFront
  onEdit: (localizedText: ILocalizedTextMinimalFront) => void
}

export const LocalizedText = ({ localizedText, onEdit }: IProps) => {
  const { textPageId, refreshChapters } = useChaptersContext()
  const { deleteLocalizedText } = useLocalizedTextsContext()

  const onClickOnDelete = useCallback(
    function (event: BaseSyntheticEvent<MouseEvent>) {
      confirmPopup({
        target: event.currentTarget,
        message: `Confirm text deletion`,
        icon: <FontAwesomeIcon icon={faWarning} size="lg" />,
        accept: async () => {
          await deleteLocalizedText(localizedText._id)
          await refreshChapters()
        },
      })
    },
    [deleteLocalizedText, localizedText._id, refreshChapters]
  )

  return (
    <Wrapper className="flex align-items-center mt-1 px-2">
      <Flag $lang={localizedText.lang} $width="20px" $height="20px" />

      <Badge
        className="ml-2 mr-1 flex-shrink-0 px-2 white-space-nowrap text-lg"
        size="normal"
        severity="info"
        value={localizedText.blocksCount}
        title={`${localizedText.blocksCount} block${localizedText.blocksCount > 1 ? 's' : ''}`}
      />

      <LocalizedTextTitle className="m-3 text-center">
        <Link
          href={`${EDITOR_PAGE_PATH}/${localizedText._id}?textPageId=${textPageId}`}
          target={'_blank'}
        >
          {localizedText.title}
        </Link>
      </LocalizedTextTitle>

      <SmallerButton
        className="p-button-success p-button-rounded p-button-outlined mx-1"
        icon={<FontAwesomeIcon icon={faPen} />}
        onClick={() => onEdit(localizedText)}
      />

      <SmallerButton
        className="p-button-danger p-button-rounded p-button-outlined mx-1"
        icon={<FontAwesomeIcon icon={faTimes} size="lg" />}
        onClick={onClickOnDelete}
      />
    </Wrapper>
  )
}
