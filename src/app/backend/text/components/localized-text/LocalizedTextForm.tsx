'use client'

import {
  Container,
  Field,
  FieldTitle,
  InlineField,
  StyledInputText,
  Title,
} from '@/backend/_components/dialog/Dialog.styles'
import { renderFooter } from '@/backend/_components/dialog/Dialog.util'
import { Dialog } from 'primereact/dialog'
import { Dropdown } from 'primereact/dropdown'
import { InputTextarea } from 'primereact/inputtextarea'
import { useCallback, useEffect, useMemo, useState } from 'react'
import { SUPPORTED_LANG } from 'src/models/common/Lang'
import type { ILocalizedTextFormItem } from 'src/models/text/LocalizedText.type'
import styled from 'styled-components'

const StyledTextarea = styled(InputTextarea)`
  width: 100%;
`

const DEFAULT_ITEM_STATE: ILocalizedTextFormItem = {
  _id: '',
  title: '',
  lang: SUPPORTED_LANG[0],
  description: '',
  blocksCount: 0,
}

interface IProps {
  visible: boolean
  item: ILocalizedTextFormItem
  callback: (localizedText: ILocalizedTextFormItem) => void
  onHide: () => void
}

export const LocalizedTextForm = ({
  visible,
  item: _item,
  callback,
  onHide,
}: IProps) => {
  const [item, updateEditedItem] = useState(DEFAULT_ITEM_STATE)
  const availableLangs = useMemo(() => SUPPORTED_LANG, [])

  useEffect(() => {
    updateEditedItem({ ...DEFAULT_ITEM_STATE, ..._item })
  }, [_item])

  const onSubmit = useCallback(
    function () {
      callback(item)
    },
    [item, callback]
  )

  return (
    <Dialog
      visible={visible}
      showHeader={false}
      closable={false}
      maximizable
      blockScroll
      maskClassName="dialog-mask"
      onHide={onHide}
      footer={renderFooter(onSubmit, onHide)}
    >
      <Title>{item._id ? 'Edit' : 'Add'} localizedText</Title>

      <Container className="flex flex-column pt-2">
        <InlineField className="mt-3">
          <FieldTitle>Lang</FieldTitle>
          <Dropdown
            className="mr-2"
            value={item.lang}
            options={availableLangs}
            placeholder="Select lang"
            onChange={({ target: { value } }) => {
              updateEditedItem((item) => ({ ...item, lang: value }))
            }}
          />
        </InlineField>

        <Field className="mt-3">
          <FieldTitle>Localized title</FieldTitle>
          <StyledInputText
            value={item.title}
            onChange={({ target: { value } }) => {
              updateEditedItem((item) => ({ ...item, title: value }))
            }}
          />
        </Field>

        <Field className="mt-3">
          <FieldTitle>Localized description</FieldTitle>
          <StyledTextarea
            rows={2}
            autoResize
            value={item.description}
            onChange={({ target: { value } }) => {
              updateEditedItem((item) => ({ ...item, description: value }))
            }}
          />
        </Field>
      </Container>
    </Dialog>
  )
}
