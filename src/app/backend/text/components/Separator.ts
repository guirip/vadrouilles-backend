'use client'

import styled from 'styled-components'

export const Separator = styled.hr`
  height: 5px;
  width: 200px;
  border: none;
  border-top: 1px double #333;
`
