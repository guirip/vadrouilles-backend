import { useState } from 'react'
import type { ImagesPerChapter } from 'src/models/text/TextPage.type'
import { useExistingImageContext } from './ExistingImageProvider'
import {
  Chapter,
  ChapterImage,
  ChapterImagesContent,
  ChapterTitle,
  CiField,
  EditorExistingImageForm,
} from './styles'

interface IProps {
  staticUrl: string
  chapterId: string
  content: ImagesPerChapter
}

const ChapterImages = ({ staticUrl, chapterId, content }: IProps) => {
  const [folded, setFolded] = useState(true)
  const { selectedImage, selectImage } = useExistingImageContext()

  return (
    <Chapter $disabled={content.images.length === 0}>
      <ChapterTitle onClick={() => setFolded(!folded)}>
        <CiField>{content.title}</CiField>
        <CiField $count>
          {content.images.length} image{content.images.length > 1 ? 's' : ''}
        </CiField>
      </ChapterTitle>

      <ChapterImagesContent $hidden={folded}>
        {content.images.map((imageBlock) => (
          <ChapterImage
            key={imageBlock.id}
            $selected={
              !!(selectedImage?.id && selectedImage.id === imageBlock.id)
            }
            onClick={() => selectImage(chapterId, imageBlock)}
            id={imageBlock.id}
            data-chapter-id={chapterId}
            src={`${staticUrl}/${imageBlock.data.file.low}`}
            title={imageBlock.data.caption}
            alt={imageBlock.data.caption}
          />
        ))}
      </ChapterImagesContent>
    </Chapter>
  )
}

export const ImagePicker = () => {
  const {
    chapterId,
    selectedImage,
    staticUrl,
    imagesPerText: { title, imagesByChapter },
  } = useExistingImageContext()

  return !imagesByChapter || !staticUrl ? null : (
    <EditorExistingImageForm
      className="wrapper"
      data-chapter-id={chapterId}
      data-block-id={selectedImage?.id ?? null}
    >
      <div>
        <div className="text-xl font-medium my-2">
          Existing image from {title?.fr ?? title?.en ?? ''}
        </div>
      </div>

      {Object.keys(imagesByChapter)
        .sort((cId1: string, cId2: string) => {
          const order1 = imagesByChapter[cId1].order
          const order2 = imagesByChapter[cId2].order
          return order1 - order2
        })
        .map((chapterId) => (
          <ChapterImages
            key={chapterId}
            staticUrl={staticUrl}
            chapterId={chapterId}
            content={imagesByChapter[chapterId]}
          />
        ))}
    </EditorExistingImageForm>
  )
}
