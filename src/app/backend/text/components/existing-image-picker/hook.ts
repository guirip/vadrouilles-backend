import { useEffect, useState } from 'react'
import { getImagesPerText } from 'src/app/_server-actions/textpage.actions'
import type { ImagesPerText } from 'src/models/text/TextPage.type'

export const useImagesPerText = (textPageId?: string) => {
  const [initialized, setInitialized] = useState(false)
  const [imagesPerText, setImagesPerText] = useState<ImagesPerText>()

  useEffect(() => {
    if (!textPageId) {
      setInitialized(true)
      return
    }
    ;(async () => {
      setImagesPerText(await getImagesPerText(textPageId))
      setInitialized(true)
    })()
  }, [textPageId])

  return {
    imagesPerText,
    initialized,
  }
}
