import {
  createContext,
  type PropsWithChildren,
  useCallback,
  useContext,
  useMemo,
  useState,
} from 'react'
import { type IImageBlock } from 'src/models/text/TextBlock.type'
import type { ImagesPerText } from 'src/models/text/TextPage.type'

interface IExistingImageContext {
  textPageId: string
  imagesPerText: ImagesPerText
  staticUrl: string
  chapterId: string | null
  selectedImage: IImageBlock | null
  selectImage: (chapterId: string, block: IImageBlock) => void
}

const ExistingImageContext = createContext<IExistingImageContext | null>(null)

export const useExistingImageContext = () => {
  const context = useContext(ExistingImageContext)
  if (!context) {
    throw new Error('ExistingImageContext context is not initialiazed yet')
  }
  return context
}

interface IProps {
  textPageId: string
  imagesPerText: ImagesPerText
  staticUrl: string
  onSelect?: (file: IImageBlock) => void
}

export const ExistingImageProvider = ({
  textPageId,
  imagesPerText,
  staticUrl,
  onSelect,
  children,
}: PropsWithChildren<IProps>) => {
  const [chapterId, setChapterId] =
    useState<IExistingImageContext['chapterId']>(null)

  const [selectedImage, setSelectedImage] =
    useState<IExistingImageContext['selectedImage']>(null)

  const selectImage = useCallback(
    (chapterId: string, block: IImageBlock) => {
      setChapterId(chapterId)
      setSelectedImage(block)
      onSelect?.(block)
    },
    [onSelect]
  )

  const context = useMemo(
    () => ({
      textPageId,
      imagesPerText,
      staticUrl,
      chapterId,
      selectedImage,
      selectImage,
    }),
    [
      chapterId,
      imagesPerText,
      selectImage,
      selectedImage,
      staticUrl,
      textPageId,
    ]
  )

  return (
    <ExistingImageContext.Provider value={context}>
      {children}
    </ExistingImageContext.Provider>
  )
}
