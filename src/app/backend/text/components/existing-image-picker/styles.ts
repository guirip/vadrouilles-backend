import { FONT_SIZE } from 'src/app/_style/style.constants'
import styled, { css } from 'styled-components'

export const EditorExistingImageForm = styled.div`
  max-width: 90vw;
`

export const ChapterTitle = styled.div`
  display: flex;
  align-items: center;
  margin: 4px 0 4px 2px;
  padding: 5px;
  font-size: ${FONT_SIZE.h6};
  background: rgba(255, 255, 255, 0.2);
  cursor: pointer;
  opacity: 0.8;
  -webkit-tap-highlight-color: transparent;

  * {
    pointer-events: none;
  }
  &:hover {
    filter: contrast(1.7);
  }
`

export const CiField = styled.span<{ $count?: boolean }>`
  flex: 1 1 auto;
  overflow: hidden;
  white-space: nowrap;
  text-overflow: ellipsis;
  margin: 2px 0.2rem 2px 0.2rem;
  padding: 6px 7px 4px;
  font-size: ${FONT_SIZE.sm};
  font-weight: normal;
  letter-spacing: 0.5px;
  background-color: #a6ff8685;
  color: black;
  border-radius: 2px;
  pointer-events: none;

  ${({ $count }) =>
    $count === true &&
    css`
      flex: 0 0 90px;
      text-align: center;
      filter: invert(1);
      border: 1px solid #979797;
      font-size: ${FONT_SIZE.xs};
      letter-spacing: 0;
    `}
`

export const Chapter = styled.div<{ $disabled?: boolean }>`
  margin: 0;
  pointer-events: ${({ $disabled }) => ($disabled ? 'none' : 'auto')};
  opacity: ${({ $disabled }) => ($disabled ? 0.4 : 1)};
`

export const ChapterImagesContent = styled.div<{ $hidden?: boolean }>`
  display: flex;
  flex-wrap: wrap;
  justify-content: center;
  align-items: center;

  ${({ $hidden }) =>
    $hidden === true &&
    css`
      display: none;
    `}
`

export const ChapterImage = styled.img<{ $selected?: boolean }>`
  max-width: 250px;
  max-height: 250px;
  margin: 10px;
  padding: 3px;
  border: 3px solid transparent;
  cursor: pointer;
  border-color: ${({ $selected }) => ($selected ? 'white' : 'transparent')};

  &:hover {
    filter: brightness(1.15);
  }
`
