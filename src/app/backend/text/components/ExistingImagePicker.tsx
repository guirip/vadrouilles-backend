import { Button } from 'primereact/button'
import { ExistingImageProvider } from './existing-image-picker/ExistingImageProvider'
import { useImagesPerText } from './existing-image-picker/hook'
import { ImagePicker } from './existing-image-picker/ImagePicker'
import { Loader } from '../../_components/Loader'
import type { IImageBlock } from 'src/models/text/TextBlock.type'
import { faClose } from '@fortawesome/free-solid-svg-icons'
import styled from 'styled-components'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'

interface IPickerProps {
  textPageId: string
  staticUrl: string
  onSelect: (image: IImageBlock) => void
}

function ExistingImagePicker({
  textPageId,
  staticUrl,
  onSelect,
}: IPickerProps) {
  const { initialized, imagesPerText } = useImagesPerText(textPageId)

  if (!initialized) {
    return <Loader />
  }
  if (!imagesPerText) {
    return <div>No image</div>
  }
  return (
    <ExistingImageProvider
      textPageId={textPageId}
      staticUrl={staticUrl}
      imagesPerText={imagesPerText}
      onSelect={onSelect}
    >
      <div className="my-3">
        <ImagePicker />
      </div>
    </ExistingImageProvider>
  )
}

interface IProps extends IPickerProps {
  enabled: boolean
  onClick: (enabled: boolean) => void
  onClose?: () => void
}

export function ExistingImagePickerToggle({
  textPageId,
  staticUrl,
  enabled,
  onClick,
  onSelect,
  onClose,
}: IProps) {
  return !enabled ? (
    <div className="w-full flex justify-content-center m-2">
      <Button label="Pick existing image" onClick={() => onClick(!enabled)} />
    </div>
  ) : (
    <>
      {typeof onClose === 'function' && (
        <div className="relative">
          <CloseButton onClick={onClose} />
        </div>
      )}
      <ExistingImagePicker
        textPageId={textPageId}
        staticUrl={staticUrl}
        onSelect={onSelect}
      />
    </>
  )
}

const StyledCloseButton = styled(Button)`
  position: absolute;
  top: 8px;
  right: 0;
`
const CloseButton = ({ onClick }: { onClick: () => void }) => (
  <StyledCloseButton
    className="p-button-rounded p-button-outlined"
    icon={<FontAwesomeIcon icon={faClose} size="lg" />}
    onClick={onClick}
    text
  />
)
