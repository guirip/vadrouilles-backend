'use client'

import styled, { css } from 'styled-components'

interface IProps {
  $outlined?: boolean
}

export const Box = styled.div<IProps>`
  display: flex;
  align-items: center;
  background: rgba(255, 255, 255, 0.1);
  border-radius: 6px;
  border: 2px solid transparent;

  ${({ $outlined }) =>
    $outlined &&
    css`
      border-color: #424242;
    `}
`
