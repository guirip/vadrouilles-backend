import { faEye, faEyeSlash } from '@fortawesome/free-regular-svg-icons'
import {
  FontAwesomeIcon,
  type FontAwesomeIconProps,
} from '@fortawesome/react-fontawesome'

interface IProps extends Omit<FontAwesomeIconProps, 'icon'> {
  visible: boolean
}

export const VisibleIcon = ({ visible, ...props }: IProps) => (
  <span>
    <FontAwesomeIcon icon={visible ? faEye : faEyeSlash} {...props} />
  </span>
)
