'use client'

import {
  faArrowUpRightFromSquare,
  faPen,
  faTimes,
  faWarning,
} from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { Button } from 'primereact/button'
import { confirmPopup } from 'primereact/confirmpopup'
import type { BaseSyntheticEvent } from 'react'
import { useCallback, useMemo } from 'react'
import styled from 'styled-components'

import type { ITextPageFront } from 'src/models/text/TextPage.type'
import { getFrontCompletePath } from 'src/utils/util'
import { ChaptersProvider } from '../../providers/ChaptersProvider'
import { useTextPagesContext } from '../../providers/TextPagesProvider'
import { getTextPage } from '../../util'
import { Separator } from '../Separator'
import { Chapters } from '../chapter/Chapters'

const Container = styled.div`
  display: flex;
  flex-direction: column;
`
const ButtonsRow = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: center;
  align-items: center;
  margin: 1rem 1rem 1.6rem;
`

interface IProps {
  showEditDialog: (textPage: ITextPageFront) => void
}

export const TextPage = ({ showEditDialog }: IProps) => {
  const { currentTextPageId, pages, refreshTextPages, deleteTextPage } =
    useTextPagesContext()

  const currentTextPage = useMemo(
    () => (!currentTextPageId ? null : getTextPage(currentTextPageId, pages)),
    [currentTextPageId, pages]
  )

  const onClickOnDelete = useCallback(
    function (event: BaseSyntheticEvent) {
      if (!currentTextPageId) {
        return
      }
      confirmPopup({
        target: event.currentTarget as HTMLElement,
        message: `Confirm page deletion`,
        icon: <FontAwesomeIcon icon={faWarning} size="lg" />,
        accept: () => deleteTextPage(currentTextPageId),
      })
    },
    [deleteTextPage, currentTextPageId]
  )

  const onClickOnEdit = useCallback(() => {
    if (!currentTextPage) {
      return
    }
    showEditDialog(currentTextPage)
  }, [currentTextPage, showEditDialog])

  if (!currentTextPage) {
    return null
  }

  return (
    <Container>
      <ButtonsRow>
        <Button
          className="p-button-success p-button-rounded p-button-outlined mx-1 text-lg"
          icon={<FontAwesomeIcon className="mr-2" icon={faPen} />}
          label="Edit page"
          onClick={onClickOnEdit}
        />
        <Button
          className="p-button-danger p-button-rounded p-button-outlined mr-1 ml-2 text-lg"
          icon={<FontAwesomeIcon className="mr-2" icon={faTimes} size="lg" />}
          label="Delete page"
          onClick={onClickOnDelete}
        />
        <a
          href={getFrontCompletePath(`/text-preview/${currentTextPage.slug}`)}
          target="_blank"
        >
          <Button
            className="p-button-rounded p-button-outlined mx-2 text-lg"
            icon={
              <FontAwesomeIcon
                className="mr-2"
                icon={faArrowUpRightFromSquare}
              />
            }
            label="Preview page"
          />
        </a>
      </ButtonsRow>

      <Separator />

      <ChaptersProvider
        textPageId={currentTextPage._id}
        textPageSlug={currentTextPage.slug}
        refreshTextPages={refreshTextPages}
      >
        <Chapters />
      </ChaptersProvider>
    </Container>
  )
}
