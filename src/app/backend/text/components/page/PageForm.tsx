'use client'

import { useCallback, useEffect, useState } from 'react'

import { Dialog } from 'primereact/dialog'
import { InputSwitch } from 'primereact/inputswitch'
import { InputTextarea } from 'primereact/inputtextarea'

import {
  Container,
  Field,
  FieldTitle,
  InlineField,
  StyledInputText,
  Title,
} from '@/backend/_components/dialog/Dialog.styles'
import { renderFooter } from '@/backend/_components/dialog/Dialog.util'
import { FileUpload } from 'primereact/fileupload'
import { getStaticUrl } from 'src/app/_server-actions/codif.actions'
import { useImageUpload } from 'src/app/backend/_hooks/useImageUpload'
import { PICTURES_UPLOAD_API_URL } from 'src/client-api/constants'
import { MAX_FILE_SIZE } from 'src/models/CatalogItem.type'
import type { ContentCategory } from 'src/models/common/ContentCategory'
import type { IFile } from 'src/models/common/File.type'
import type { ITextPageFront } from 'src/models/text/TextPage.type'
import { slugify } from 'src/utils/stringUtil'
import styled from 'styled-components'
import { useTextPagesContext } from '../../providers/TextPagesProvider'
import { CategorySelect } from './CategorySelect'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faTimes } from '@fortawesome/free-solid-svg-icons'
import { Button } from 'primereact/button'
import { ExistingImagePickerToggle } from '../ExistingImagePicker'

const CoverWrapper = styled.a`
  display: flex;
  align-items: center;
  justify-content: center;
  position: relative;
  max-width: 120px;
  max-height: 120px;

  & img {
    min-width: 80px;
    max-width: 120px;
    min-height: 80px;
    max-height: 120px;
    object-fit: contain;
  }
`

const getDefaultItemState = (category: ContentCategory): ITextPageFront => ({
  _id: '',
  slug: '',
  title: {
    fr: '',
    en: '',
  },
  subtitle: {
    fr: '',
    en: '',
  },
  category,
  visible: false,
  description: { fr: '', en: '' },
  longDescription: { fr: '', en: '' },
  chapters: [],
  order: 0,
  cover: undefined,
})

interface IProps {
  visible: boolean
  item: Partial<ITextPageFront>
  callback: (textPage: ITextPageFront) => void
  onHide: () => void
}

export function TextPageForm({
  visible,
  item: _item,
  callback,
  onHide,
}: IProps) {
  const { currentCat: category } = useTextPagesContext()
  const [item, updateEditedItem] = useState(getDefaultItemState(category))
  const [staticUrl, setStaticUrl] = useState('')
  const [imagePickerEnabled, setImagePickerEnabled] = useState(false)

  const {
    afterPicturesUpload,
    fileUploadRef,
    onPictureUploadError,
    onPicturesValidationFail,
    setUploadRequestHeader,
  } = useImageUpload((files: IFile[]) => {
    updateEditedItem((item) => ({
      ...item,
      cover: Array.isArray(files) && files.length > 0 ? files[0] : undefined,
    }))
  })

  useEffect(() => {
    ;(async () => {
      setStaticUrl(await getStaticUrl())
    })()
  }, [])

  useEffect(() => {
    updateEditedItem({ ...getDefaultItemState(category), ..._item })
  }, [_item, category])

  const updateI18nField = useCallback(
    function (
      field: 'title' | 'subtitle' | 'description' | 'longDescription',
      lang: string,
      value: string
    ) {
      const __item = { ...item }
      __item[field] = { ...__item[field] }
      __item[field][lang] = value
      updateEditedItem(__item)
    },
    [item, updateEditedItem]
  )

  const onSubmit = useCallback(
    function () {
      callback(item)
    },
    [item, callback]
  )

  return (
    <Dialog
      visible={visible}
      showHeader={false}
      closable={false}
      maximizable
      blockScroll
      maskClassName="dialog-mask"
      onHide={onHide}
      footer={renderFooter(onSubmit, onHide)}
    >
      <Title>
        {item._id ? 'Edit' : 'Add'} {category} page
      </Title>

      <Container className="flex flex-column pt-5">
        <InlineField>
          <FieldTitle>Category</FieldTitle>
          <CategorySelect
            currentCategory={item.category}
            onChange={(selectedCategory) => {
              updateEditedItem((item) => ({
                ...item,
                category: selectedCategory,
              }))
            }}
          />
        </InlineField>

        <div className="flex mt-1">
          <Field>
            <FieldTitle>Slug</FieldTitle>
            <StyledInputText
              value={item.slug}
              onChange={({ target: { value } }) => {
                updateEditedItem((item) => ({ ...item, slug: slugify(value) }))
              }}
            />
          </Field>

          <Field className="flex-auto flex flex-column justify-content-evenly align-items-center">
            <FieldTitle className="mr-0">Visible</FieldTitle>
            <InputSwitch
              checked={item.visible}
              onChange={(e) => {
                updateEditedItem((item) => ({
                  ...item,
                  visible: e.value === true,
                }))
              }}
            />
          </Field>
        </div>

        <Field className="mt-4">
          <FieldTitle>Title FR 🇫🇷</FieldTitle>
          <StyledInputText
            value={item.title.fr || ''}
            onChange={({ target: { value } }) => {
              updateI18nField('title', 'fr', value)
            }}
          />
        </Field>
        <Field className="mt-1">
          <FieldTitle>Subtitle FR 🇫🇷</FieldTitle>
          <StyledInputText
            value={item.subtitle.fr || ''}
            onChange={({ target: { value } }) => {
              updateI18nField('subtitle', 'fr', value)
            }}
          />
        </Field>
        <Field className="mt-1">
          <FieldTitle>Short description FR 🇫🇷</FieldTitle>
          <InputTextarea
            rows={1}
            value={item.description.fr}
            className="w-full"
            onChange={({ target: { value } }) => {
              updateI18nField('description', 'fr', value)
            }}
          />
        </Field>
        <Field className="mt-1">
          <FieldTitle>Long description FR 🇫🇷</FieldTitle>
          <InputTextarea
            rows={3}
            value={item.longDescription.fr}
            className="w-full"
            onChange={({ target: { value } }) => {
              updateI18nField('longDescription', 'fr', value)
            }}
          />
        </Field>

        <Field className="mt-4">
          <FieldTitle>Title EN 🇬🇧</FieldTitle>
          <StyledInputText
            value={item.title.en || ''}
            onChange={({ target: { value } }) => {
              updateI18nField('title', 'en', value)
            }}
          />
        </Field>
        <Field className="mt-1">
          <FieldTitle>Subtitle EN 🇬🇧</FieldTitle>
          <StyledInputText
            value={item.subtitle.en || ''}
            onChange={({ target: { value } }) => {
              updateI18nField('subtitle', 'en', value)
            }}
          />
        </Field>
        <Field className="mt-1">
          <FieldTitle>Short description EN 🇬🇧</FieldTitle>
          <InputTextarea
            rows={1}
            value={item.description.en}
            className="w-full"
            onChange={({ target: { value } }) => {
              updateI18nField('description', 'en', value)
            }}
          />
        </Field>
        <Field className="mt-1">
          <FieldTitle>Long description EN 🇬🇧</FieldTitle>
          <InputTextarea
            rows={3}
            value={item.longDescription.en}
            className="w-full"
            onChange={({ target: { value } }) => {
              updateI18nField('longDescription', 'en', value)
            }}
          />
        </Field>

        <Field className="mt-4">
          <FieldTitle>Cover</FieldTitle>
          {item.cover && (
            <div className="flex justify-content-center align-items-center">
              <CoverWrapper>
                {/* eslint-disable-next-line @next/next/no-img-element */}
                <img src={`${staticUrl}/${item.cover.low}`} />
              </CoverWrapper>
              <Button
                icon={<FontAwesomeIcon icon={faTimes} size="lg" />}
                className="m-3 p-button-sm p-button-rounded"
                severity="danger"
                onClick={(e) => {
                  e.preventDefault()
                  updateEditedItem((item) => ({ ...item, cover: null }))
                }}
                title="Clear"
              />
            </div>
          )}
          {!item.cover && item._id && (
            <div className="flex flex-column">
              <ExistingImagePickerToggle
                textPageId={item._id}
                staticUrl={staticUrl}
                enabled={imagePickerEnabled}
                onClick={(value) => {
                  setImagePickerEnabled(value)
                }}
                onClose={() => setImagePickerEnabled(false)}
                onSelect={(image) => {
                  updateEditedItem((item) => ({
                    ...item,
                    cover: !image?.data.file ? null : image.data.file,
                  }))
                }}
              />
              {!imagePickerEnabled && (
                <div className="text-center mb-2">or</div>
              )}
            </div>
          )}

          {!item.cover && !imagePickerEnabled && (
            <FileUpload
              ref={fileUploadRef}
              name="itemPhotos"
              accept="image/*"
              url={PICTURES_UPLOAD_API_URL}
              maxFileSize={MAX_FILE_SIZE}
              onUpload={afterPicturesUpload}
              onBeforeSend={setUploadRequestHeader}
              onValidationFail={onPicturesValidationFail}
              onError={onPictureUploadError}
              emptyTemplate={<span></span>}
            />
          )}
        </Field>
      </Container>
    </Dialog>
  )
}
