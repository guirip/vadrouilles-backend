'use client'

import { Dropdown } from 'primereact/dropdown'
import { textCategories } from 'src/models/text/TextPage.type'
import type { ContentCategory } from 'src/models/common/ContentCategory'

interface IProps {
  currentCategory: ContentCategory
  onChange: (selectedCategory: ContentCategory) => void
}

export const CategorySelect = ({ currentCategory, onChange }: IProps) => (
  <Dropdown
    value={currentCategory}
    options={textCategories}
    onChange={(e) => {
      onChange(e.value as ContentCategory)
    }}
    placeholder="Category"
  />
)
