'use client'

import styled from 'styled-components'

import type { ITextPageFront } from 'src/models/text/TextPage.type'
import { VisibleIcon } from '../VisibleIcon'
import { FONT_SIZE } from 'src/app/_style/style.constants'

const getPageTitle = (page: ITextPageFront): string =>
  page?.title?.fr || '(not set)'

const getPageSubtitle = (page: ITextPageFront): string =>
  page?.subtitle?.fr || ''

const Titles = styled.span`
  display: flex;
  justify-content: center;
  flex-direction: column;
  flex-grow: 1;
  padding-right: 2rem;
`
const PageTitle = styled.span`
  font-size: ${FONT_SIZE.md};
  font-weight: bold;
`
const PageSubtitle = styled.span`
  font-size: ${FONT_SIZE.xs};
  opacity: 0.9;

  &:empty {
    display: none;
  }
`
const Badge = styled.span`
  padding: 0.2rem 0.4rem;
  font-size: ${FONT_SIZE.xs};
  color: #d6d6d6;
  background-color: #292929;
  border-radius: 6px;
  border: 1px solid #4e4e4e;
`
const CategoryCount = styled(Badge)`
  display: none;
`

interface IWrapperProps {
  $isSelected: boolean
}

const Wrapper = styled.div<IWrapperProps>`
  display: flex;
  align-items: center;
  margin: 0.3rem;
  padding: 0.4rem;
  font-size: 0.8rem;
  border-radius: 8px;
  cursor: pointer;
  user-select: none;

  & > * {
    margin: 0.2rem;
  }

  .text-page-select-dialog & {
    margin-right: 0;
    margin-left: 0;
    border-radius: 0;
    border: 2px solid
      ${({ $isSelected }) => ($isSelected ? '#a3a3a3' : '#4f4f4f')};

    ${CategoryCount} {
      display: block;
    }
  }
`

interface IProps {
  textPage: ITextPageFront
  onClick: (textPageId: string) => void
  selected?: boolean
}

export const TextPageButton = ({ textPage, onClick, selected }: IProps) => (
  <Wrapper
    $isSelected={selected === true}
    onClick={() => onClick(textPage._id)}
  >
    <Titles>
      <PageTitle>{getPageTitle(textPage)}</PageTitle>
      <PageSubtitle>{getPageSubtitle(textPage)}</PageSubtitle>
    </Titles>

    <Badge>{textPage.slug}</Badge>

    <CategoryCount>{textPage.chapters.length}</CategoryCount>

    <VisibleIcon visible={textPage.visible} size="lg" />
  </Wrapper>
)
