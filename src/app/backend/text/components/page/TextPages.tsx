'use client'

import { faPlus } from '@fortawesome/free-solid-svg-icons'
import { Button } from 'primereact/button'
import { ConfirmPopup } from 'primereact/confirmpopup'
import { useCallback, useMemo, useState } from 'react'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'

import type { ITextPageFront } from 'src/models/text/TextPage.type'
import { useBoolean } from 'src/utils/useBoolean'
import { useTextPagesContext } from '../../providers/TextPagesProvider'
import { Box } from '../Box'
import { Separator } from '../Separator'
import { CategorySelect } from './CategorySelect'
import { TextPageForm } from './PageForm'
import { TextPage } from './TextPage'
import { TextPageButton } from './TextPageButton'
import { TextPageSelect } from './TextPageSelect'

export const TextPages = () => {
  const {
    pages,
    currentTextPageId,
    setCurrentTextPageId,
    currentCat,
    setCurrentCat,
    persistTextPage,
  } = useTextPagesContext()

  const currentTextPage = useMemo(
    () =>
      !currentTextPageId
        ? null
        : pages.find((p) => p._id === currentTextPageId),
    [pages, currentTextPageId]
  )

  const [editedTextPage, setEditedTextPage] = useState<Partial<ITextPageFront>>(
    {}
  )
  const [textPageDialogVisible, setTextPageDialogVisible] = useState(false)

  const [pageSelectorVisible, showPageSelector, hidePageSelector] =
    useBoolean(false)

  function addNewTextPage() {
    setEditedTextPage({})
    setTextPageDialogVisible(true)
  }

  function onTextPageDialogHide() {
    setEditedTextPage({})
    setTextPageDialogVisible(false)
  }

  function showTextPageEditDialog(textPage: ITextPageFront) {
    setEditedTextPage(textPage)
    setTextPageDialogVisible(true)
  }

  const savePage = useCallback(
    async (textPage: ITextPageFront) => {
      const success = await persistTextPage(textPage)
      if (success) {
        setTextPageDialogVisible(false)
      }
    },
    [persistTextPage]
  )

  return (
    <>
      <ConfirmPopup />
      <TextPageForm
        visible={textPageDialogVisible}
        item={editedTextPage}
        callback={savePage}
        onHide={onTextPageDialogHide}
      />

      <Box className="mb-4 px-4 py-3">
        <span className="mr-3">
          <b>CATEGORY</b>
        </span>
        <CategorySelect currentCategory={currentCat} onChange={setCurrentCat} />
      </Box>

      <Separator />

      <div className="flex align-items-center flex-column mt-3 mb-6 text-lg">
        <div className="flex align-items-center px-2 py-1">
          {!currentTextPage ? (
            <strong>There is no page yet</strong>
          ) : (
            <Box $outlined>
              <TextPageButton
                textPage={currentTextPage}
                onClick={showPageSelector}
              />
            </Box>
          )}
          {pageSelectorVisible && (
            <TextPageSelect
              currentPageId={currentTextPageId}
              onChange={(selectedPageId) => {
                setCurrentTextPageId(selectedPageId)
                hidePageSelector()
              }}
              onHide={hidePageSelector}
            />
          )}

          <Button
            className="mx-3 p-button-rounded"
            icon={<FontAwesomeIcon className="mr-2" icon={faPlus} />}
            onClick={addNewTextPage}
            label="New page"
            severity="help"
          />
        </div>

        <TextPage showEditDialog={showTextPageEditDialog} />
      </div>
    </>
  )
}
