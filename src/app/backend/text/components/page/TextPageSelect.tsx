'use client'

import { Dialog } from 'primereact/dialog'
import { useCallback } from 'react'
import SortableList from '@/backend/_components/sortable-list'
import type { ITextPageFront } from 'src/models/text/TextPage.type'
import { useTextPagesContext } from '../../providers/TextPagesProvider'
import { TextPageButton } from './TextPageButton'

interface IProps {
  currentPageId: ITextPageFront['_id'] | null
  onChange: (pageId: ITextPageFront['_id']) => void
  onHide: () => void
}

export const TextPageSelect = ({ onChange, currentPageId, onHide }: IProps) => {
  const { pages, updatePagesOrder } = useTextPagesContext()

  const renderPage = useCallback(
    (textPage: ITextPageFront) => (
      <TextPageButton
        textPage={textPage}
        selected={currentPageId === textPage._id}
        onClick={onChange}
      />
    ),
    [currentPageId, onChange]
  )

  return (
    <Dialog
      visible={true}
      maximizable
      dismissableMask
      blockScroll
      onHide={onHide}
      className="mx-2"
      contentClassName="text-page-select-dialog"
    >
      <SortableList
        content={pages}
        renderItem={renderPage}
        onOrderUpdate={updatePagesOrder}
      />
    </Dialog>
  )
}
