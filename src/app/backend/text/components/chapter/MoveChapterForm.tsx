'use client'

import { Dialog } from 'primereact/dialog'
import { useCallback, useMemo, useState } from 'react'

import { Container, Title } from '@/backend/_components/dialog/Dialog.styles'
import { renderFooter } from '@/backend/_components/dialog/Dialog.util'
import { useBoolean } from 'src/utils/useBoolean'
import { useChaptersContext } from '../../providers/ChaptersProvider'
import { useTextPagesContext } from '../../providers/TextPagesProvider'
import { Box } from '../Box'
import { TextPageButton } from '../page/TextPageButton'
import { TextPageSelect } from '../page/TextPageSelect'

interface IProps {
  visible: boolean
  callback: (newTextPageId: string) => void
  onHide: () => void
}

export function MoveChapterForm({ visible, callback, onHide }: IProps) {
  const { pages } = useTextPagesContext()
  const { textPageId } = useChaptersContext()

  const [selectedPageId, setSelectedPageId] = useState(textPageId)

  const [pageSelectorVisible, showPageSelector, hidePageSelector] =
    useBoolean(false)

  const currentTextPage = useMemo(
    () =>
      !selectedPageId ? null : pages.find((p) => p._id === selectedPageId),
    [pages, selectedPageId]
  )

  const onSubmit = useCallback(() => {
    callback(selectedPageId)
  }, [callback, selectedPageId])

  const footer = useMemo(
    () => renderFooter(onSubmit, onHide, textPageId !== selectedPageId),
    [onHide, onSubmit, selectedPageId, textPageId]
  )

  if (!currentTextPage) {
    return null
  }

  return (
    <Dialog
      visible={visible}
      showHeader={false}
      onHide={onHide}
      footer={() => footer}
    >
      <Title>Move chapter to page</Title>

      <Container className="flex justify-content-center mt-4">
        <Box $outlined>
          <TextPageButton
            textPage={currentTextPage}
            onClick={showPageSelector}
          />
        </Box>
      </Container>

      {pageSelectorVisible && (
        <TextPageSelect
          currentPageId={selectedPageId}
          onChange={(pageId: string) => {
            setSelectedPageId(pageId)
            hidePageSelector()
          }}
          onHide={hidePageSelector}
        />
      )}
    </Dialog>
  )
}
