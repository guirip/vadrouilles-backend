'use client'

import { Dialog } from 'primereact/dialog'
import { InputSwitch } from 'primereact/inputswitch'
import { useCallback, useEffect, useState } from 'react'

import {
  Container,
  FieldTitle,
  InlineField,
  NeutralInputSwitch,
  StyledInputText,
  Title,
} from '@/backend/_components/dialog/Dialog.styles'
import { renderFooter } from '@/backend/_components/dialog/Dialog.util'
import type { ITextChapterMinimalPopulatedFront } from 'src/models/text/TextChapter.type'
import { slugify } from 'src/utils/stringUtil'

const DEFAULT_ITEM_STATE: ITextChapterMinimalPopulatedFront = {
  _id: '',
  slug: '',
  visible: false,
  order: -1,
  localizedTexts: [],
}

interface IProps {
  visible: boolean
  item: Partial<ITextChapterMinimalPopulatedFront>
  callback: (textChapter: ITextChapterMinimalPopulatedFront) => void
  onHide: () => void
}

export function ChapterForm({
  visible,
  item: _item,
  callback,
  onHide,
}: IProps) {
  const [item, updateEditedItem] = useState(DEFAULT_ITEM_STATE)
  const [positionLast, setPositionLast] = useState(true)

  useEffect(() => {
    updateEditedItem({ ...DEFAULT_ITEM_STATE, ..._item })
  }, [_item])

  const onSubmit = useCallback(
    function () {
      if (typeof item.order !== 'number' || item.order === -1) {
        item.order = positionLast ? -1 : 0
      }
      callback(item)
    },
    [item, callback, positionLast]
  )

  return (
    <Dialog
      visible={visible}
      showHeader={false}
      closable={false}
      maximizable
      blockScroll
      maskClassName="dialog-mask"
      onHide={onHide}
      footer={renderFooter(onSubmit, onHide)}
    >
      <Title>{item._id ? 'Edit' : 'Add'} chapter</Title>

      <Container className="pt-4 flex flex-column">
        {!item._id && (
          <div className="flex justify-content-between align-items-center">
            <FieldTitle>Position</FieldTitle>

            <div className="flex align-items-center">
              <span className="mx-3">First</span>
              <NeutralInputSwitch
                checked={positionLast}
                onChange={(e) => setPositionLast(e.value === true)}
              />
              <span className="mx-3">Last</span>
            </div>
          </div>
        )}

        <InlineField className="mt-3">
          <FieldTitle>Slug</FieldTitle>
          <StyledInputText
            value={item.slug}
            onChange={({ target: { value } }) => {
              updateEditedItem((item) => ({ ...item, slug: slugify(value) }))
            }}
          />
        </InlineField>

        <InlineField className="mt-3">
          <FieldTitle>Visible</FieldTitle>
          <InputSwitch
            checked={item.visible}
            onChange={(e) => {
              updateEditedItem((item) => ({
                ...item,
                visible: e.value === true,
              }))
            }}
          />
        </InlineField>

        {!!item._id && (
          <InlineField className="mt-3">
            <FieldTitle>Localizations</FieldTitle>
            {!Array.isArray(item.localizedTexts) ||
            item.localizedTexts.length === 0 ? (
              <div className="flex flex-wrap">
                <i>None yet</i>
              </div>
            ) : (
              <div className="flex flex-wrap">
                {item.localizedTexts.length} localization
                {item.localizedTexts.length > 1 ? 's' : ''}
              </div>
            )}
          </InlineField>
        )}
      </Container>
    </Dialog>
  )
}
