'use client'

import { faArrowRotateRight, faPlus } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { Button } from 'primereact/button'
import { ConfirmPopup } from 'primereact/confirmpopup'
import { useCallback, useState } from 'react'

import SortableList from '@/backend/_components/sortable-list'
import { useToastContext } from 'src/app/_components/ToastProvider'
import type { ITextChapterMinimalPopulatedFront } from 'src/models/text/TextChapter.type'
import { useChaptersContext } from '../../providers/ChaptersProvider'
import { Chapter } from './Chapter'
import { ChapterForm } from './ChapterForm'
import { MoveChapterForm } from './MoveChapterForm'

export const Chapters = () => {
  const {
    chapters,
    refreshChapters,
    persistChapter,
    moveChapter,
    updateChaptersOrder,
  } = useChaptersContext()

  const { showError } = useToastContext()

  const [editedChapter, setEditedChapter] = useState<
    Partial<ITextChapterMinimalPopulatedFront>
  >({})

  const [chapterFormVisible, setChapterFormVisible] = useState(false)

  const [chapterToMove, setChapterToMove] =
    useState<null | ITextChapterMinimalPopulatedFront>(null)

  const [moveChapterDialogVisible, setMoveChapterDialogVisible] =
    useState(false)

  function addChapter() {
    setEditedChapter({})
    setChapterFormVisible(true)
  }

  function showChapterForm(chapter: ITextChapterMinimalPopulatedFront) {
    setEditedChapter(chapter)
    setChapterFormVisible(true)
  }

  function hideChapterDorm() {
    setEditedChapter({})
    setChapterFormVisible(false)
  }

  function showMoveChapterDialog(chapter: ITextChapterMinimalPopulatedFront) {
    setChapterToMove(chapter)
    setMoveChapterDialogVisible(true)
  }

  function hideMoveChapterDialog() {
    setChapterToMove(null)
    setMoveChapterDialogVisible(false)
  }

  const onValidateChapterForm = useCallback(
    async (chapter: ITextChapterMinimalPopulatedFront) => {
      const success = await persistChapter(chapter)
      if (success) {
        setChapterFormVisible(false)
      }
    },
    [persistChapter]
  )

  const onValidateMoveChapter = useCallback(
    async (newTextPageId: string) => {
      if (!chapterToMove) {
        showError('Chapter to move state variable should be defined')
        return
      }
      const success = await moveChapter(newTextPageId, chapterToMove)
      if (success) {
        setMoveChapterDialogVisible(false)
      }
    },
    [chapterToMove, moveChapter, showError]
  )

  const renderChapter = useCallback(
    (chapter: ITextChapterMinimalPopulatedFront) => (
      <Chapter
        chapter={chapter}
        editChapter={showChapterForm}
        moveChapter={showMoveChapterDialog}
      />
    ),
    []
  )

  return (
    <>
      <ConfirmPopup />

      <ChapterForm
        visible={chapterFormVisible}
        item={editedChapter}
        callback={onValidateChapterForm}
        onHide={hideChapterDorm}
      />

      <MoveChapterForm
        visible={moveChapterDialogVisible}
        callback={onValidateMoveChapter}
        onHide={hideMoveChapterDialog}
      />

      <div className="flex mt-4 mb-2 justify-content-center">
        <Button
          className="mr-3 text-lg"
          icon={<FontAwesomeIcon className="mr-2" icon={faPlus} />}
          label="Add chapter"
          onClick={addChapter}
        />
        <Button
          className="p-button-plain p-button p-button-rounded p-button-outlined"
          icon={<FontAwesomeIcon icon={faArrowRotateRight} />}
          onClick={refreshChapters}
          severity="help"
        />
      </div>

      {Array.isArray(chapters) !== true || chapters.length === 0 ? (
        <div className="flex justify-content-center mt-5">
          <i>No chapters yet</i>
        </div>
      ) : (
        <SortableList
          content={chapters}
          renderItem={renderChapter}
          onOrderUpdate={updateChaptersOrder}
        />
      )}
    </>
  )
}
