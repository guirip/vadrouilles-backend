'use client'

import { faCopy } from '@fortawesome/free-regular-svg-icons'
import {
  faArrowRightToBracket,
  faArrowUpRightFromSquare,
  faPen,
  faTimes,
  faWarning,
} from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { Button } from 'primereact/button'
import { ConfirmPopup, confirmPopup } from 'primereact/confirmpopup'
import type { MouseEventHandler } from 'react'
import { useCallback, useEffect, useState } from 'react'
import styled from 'styled-components'

import type { Lang } from 'src/models/common/Lang'
import type { ITextChapterMinimalPopulatedFront } from 'src/models/text/TextChapter.type'
import { getFrontCompletePath } from 'src/utils/util'
import { useChaptersContext } from '../../providers/ChaptersProvider'
import { LocalizedTextsProvider } from '../../providers/LocalizedTextsProvider'
import { SmallerButton } from '../SmallerButton'
import { VisibleIcon } from '../VisibleIcon'
import { LocalizedTexts } from '../localized-text/LocalizedTexts'
import { FONT_SIZE } from 'src/app/_style/style.constants'

export const Wrapper = styled.div`
  margin-top: 1rem;
  padding: 0.8rem 0.4rem 0.8rem 0.6rem;
  border: 1px solid #333333;
`

export const ChapterRow = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: center;
  align-items: center;
  flex-wrap: wrap;

  & > * {
    margin: 0.3rem 0.4rem;
  }
`
export const TitleWrapper = styled.div`
  cursor: pointer;
`
export const ChapterTitle = styled.div`
  font-size: ${FONT_SIZE.md};
  font-weight: bold;
`
export const LocalizationsCount = styled.div`
  font-size: ${FONT_SIZE.sm};
  font-style: italic;
  font-weight: bold;
  opacity: 0.6;
`

interface IProps {
  chapter: ITextChapterMinimalPopulatedFront
  editChapter: (chapter: ITextChapterMinimalPopulatedFront) => void
  moveChapter: (chapter: ITextChapterMinimalPopulatedFront) => void
}

export function Chapter({ chapter, editChapter, moveChapter }: IProps) {
  const { textPageSlug, duplicateChapter, deleteChapter } = useChaptersContext()

  const [folded, setFolded] = useState(true)

  const [existingLocalizations, setExistingLocalizations] = useState<Lang[]>([])

  useEffect(() => {
    if (!Array.isArray(chapter.localizedTexts)) {
      setExistingLocalizations([])
    } else {
      setExistingLocalizations(chapter.localizedTexts.map(({ lang }) => lang))
    }
  }, [chapter, setExistingLocalizations])

  const toggle = useCallback(
    function () {
      setFolded(!folded)
    },
    [folded, setFolded]
  )

  const onClickOnDeleteChapter: MouseEventHandler<HTMLButtonElement> =
    useCallback(
      function (event) {
        confirmPopup({
          target: event.currentTarget as HTMLElement,
          message: `Confirm chapter deletion`,
          icon: <FontAwesomeIcon icon={faWarning} size="lg" />,
          accept: () => deleteChapter(chapter._id),
        })
      },
      [chapter._id, deleteChapter]
    )

  return (
    <Wrapper>
      <ConfirmPopup />

      <ChapterRow>
        <TitleWrapper
          className="mx-auto flex flex-column align-items-center p-text-center"
          onClick={toggle}
        >
          <ChapterTitle>{chapter.slug}</ChapterTitle>

          <div className="flex flex-nowrap align-items-center pt-1">
            <LocalizationsCount className="mr-1">
              {existingLocalizations.length} localization
              {existingLocalizations.length > 1 ? 's' : ''}
            </LocalizationsCount>

            <div className="p-float-label mx-2">
              <VisibleIcon visible={chapter.visible} color="lightgrey" />
            </div>
          </div>
        </TitleWrapper>

        <div className="flex flex-nowrap">
          <SmallerButton
            className="p-button-success p-button-rounded p-button-outlined mr-1"
            icon={<FontAwesomeIcon icon={faPen} />}
            title="Edit properties"
            onClick={() => {
              editChapter(chapter)
            }}
          />
          <SmallerButton
            className="p-button-success p-button-rounded p-button-outlined mr-1"
            icon={<FontAwesomeIcon icon={faCopy} />}
            title="Duplicate chapter"
            onClick={() => {
              duplicateChapter(chapter._id)
            }}
          />
          <SmallerButton
            className="p-button-success p-button-rounded p-button-outlined mr-1"
            icon={<FontAwesomeIcon icon={faArrowRightToBracket} />}
            title="Move chapter to another page"
            onClick={() => {
              moveChapter(chapter)
            }}
          />
          <SmallerButton
            className="p-button-danger p-button-rounded p-button-outlined mr-2"
            icon={<FontAwesomeIcon icon={faTimes} size="lg" />}
            title="Delete chapter"
            onClick={onClickOnDeleteChapter}
          />
          <a
            href={getFrontCompletePath(
              `/text-preview/${textPageSlug}/${chapter.slug}`
            )}
            target="_blank"
          >
            <Button
              className="p-button-rounded p-button-outlined mx-1"
              icon={<FontAwesomeIcon icon={faArrowUpRightFromSquare} />}
              title="Preview chapter"
              size="large"
            />
          </a>
        </div>
      </ChapterRow>

      {!folded && (
        <LocalizedTextsProvider
          chapterId={chapter._id}
          existingLocalizations={existingLocalizations}
          localizedTexts={chapter.localizedTexts}
        >
          <LocalizedTexts />
        </LocalizedTextsProvider>
      )}
    </Wrapper>
  )
}
