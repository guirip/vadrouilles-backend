'use client'

import { Button } from 'primereact/button'
import styled from 'styled-components'

export const SmallerButton = styled(Button)`
  margin: 0 0.3rem;
  opacity: 80%;
  transform: scale(92%);
`
