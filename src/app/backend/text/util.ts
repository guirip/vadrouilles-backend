import type { ITextPageFront } from 'src/models/text/TextPage.type'

export const getTextPage = (
  textPageId: ITextPageFront['_id'],
  pages: ITextPageFront[]
) => pages.find((p) => p._id === textPageId) ?? null

export function determineNewTextPageId(
  currentTextPageId: ITextPageFront['_id'] | null,
  pages: ITextPageFront[]
) {
  let newTextPageId = currentTextPageId
  if (currentTextPageId) {
    const match = (pages || []).find(({ _id }) => _id === currentTextPageId)
    if (!match) {
      newTextPageId = null
    }
  }
  if (!newTextPageId && Array.isArray(pages) && pages.length > 0) {
    newTextPageId = pages[0]._id
  }
  return newTextPageId
}
