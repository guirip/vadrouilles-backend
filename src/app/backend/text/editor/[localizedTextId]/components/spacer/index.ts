import { faArrowsUpDown } from '@fortawesome/free-solid-svg-icons'
import type { ISpacerBlock } from 'src/models/text/TextBlock.type'
import { wrapSvgIconPath } from '../service'

const DEFAULT_VALUE = 1.5

const getRealHeightLabel = (value: number) => `Height: ${value}rem`

export class EditorSpacer {
  static get toolbox() {
    return {
      title: 'Spacer',
      icon: wrapSvgIconPath(faArrowsUpDown.icon[4]),
    }
  }

  data: ISpacerBlock['data']
  inputEl: HTMLInputElement | null
  valueDisplayEl: HTMLSpanElement | null

  constructor({ data }: { data: ISpacerBlock['data'] }) {
    this.data = data
  }

  onValueChange = function () {
    if (this.inputEl && this.valueDisplayEl) {
      const newValue = parseFloat(this.inputEl.value)
      if (!isNaN(newValue)) {
        this.valueDisplayEl.textContent = getRealHeightLabel(newValue)
      }
    }
  }

  render() {
    if (this.inputEl) {
      this.inputEl.removeEventListener('input', this.onValueChange)
    }

    const currentValue = this.data.value ?? DEFAULT_VALUE

    const container = document.createElement('div')
    container.innerHTML = `
      <style>
        .spacer-block {
          display: flex;
          align-items: center;
          opacity: 0.6;
        }
        .spacer-block b {
          font-size: 1.2rem;
        }
        .spacer-form {
          display: flex;
          align-items: center;
        }
        .spacer-form input {
          margin: 0 1.2rem;
        }
        .spacer-form span {
        }
      </style>
      <div class="spacer-block">
        <div><b>Spacer</b></div>
        <span class="spacer-form">
          <input type="range" id="spacer-range" name="spacer-value" min="0.5" max="3" step="0.1" value="${currentValue}" />
          <span>${getRealHeightLabel(currentValue)}</span>
        </span>
      </div>`

    this.inputEl = container.querySelector('input')
    this.valueDisplayEl = container.querySelector('.spacer-form span')
    if (this.inputEl) {
      this.inputEl.addEventListener('input', this.onValueChange.bind(this))
    }
    return container
  }

  save(container) {
    const inputEl = container.querySelector('input')
    return {
      value: inputEl.value,
    }
  }
}
