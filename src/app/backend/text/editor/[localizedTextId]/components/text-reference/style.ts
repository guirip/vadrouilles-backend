import { FONT_SIZE } from 'src/app/_style/style.constants'
import styled, { css } from 'styled-components'

export const Wrapper = styled.div`
  max-width: 90vw;

  & > div {
    margin: 1rem 0;
  }
`

export const CategoryTitle = styled.div`
  display: flex;
  align-items: center;
  margin: 5px 0 5px 5px;
  padding: 8px 10px 4px;
  font-size: ${FONT_SIZE.h5};
  font-weight: 500;
  letter-spacing: 1px;
  background: rgba(93, 93, 93, 0.7);
  cursor: pointer;
  opacity: 0.8;
  -webkit-tap-highlight-color: transparent;

  & * {
    pointer-events: none;
  }
`

export const Content = styled.div<{ $visible: boolean }>`
  ${({ $visible }) =>
    !$visible &&
    css`
      display: none;
    `}
  margin-left: 18px;
`

export const TextPage = styled.div<{ $visible: boolean }>`
  opacity: ${({ $visible }) => ($visible ? 1 : 0.7)};
`

export const PageTitle = styled(CategoryTitle)`
  padding: 3px;
  margin: 8px 0;
  letter-spacing: 0.5px;
  font-weight: 400;
`
export const PageContent = styled.div``

export const Field = styled.div`
  flex: 1 1 auto;
  display: flex;
  justify-content: space-between;
  align-items: center;
  overflow: hidden;
  white-space: nowrap;
  font-size: ${FONT_SIZE.md};
  text-overflow: ellipsis;
  margin: 2px 0.2rem 2px 0.2rem;
  padding: 6px 7px 4px;
  background-color: rgba(175, 175, 255, 0.7);
  color: black;
  border-radius: 2px;
  pointer-events: none;
`

export const FieldCount = styled.div`
  flex: 0 0 90px;
  text-align: center;
  font-size: ${FONT_SIZE.sm};
  color: white;
  padding: 5px 8px 3px;
  background-color: rgba(255, 255, 255, 0.15);
`

export const ChapterWrapper = styled.div<{ $visible: boolean }>`
  display: flex;
  flex-wrap: no-wrap;
  opacity: ${({ $visible }) => ($visible ? 1 : 0.7)};
`

export const EditButton = styled.div`
  margin: 8px;
  padding: 8px;
  font-size: 1.1em;
  background-color: rgb(53, 53, 98);
  border-radius: 50%;
  cursor: pointer;

  &:hover {
    filter: brightness(1.3);
  }
`
