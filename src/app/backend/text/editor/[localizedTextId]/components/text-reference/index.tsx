import { createRoot } from 'react-dom/client'
import { DeviceContextProvider } from 'src/app/_components/DeviceContextProvider'
import { ToastProvider } from 'src/app/_components/ToastProvider'
import { TextRef } from './TextRef'
import { TextRefProvider } from './TextRefProvider'
import type { ITextReferenceBlock } from 'src/models/text/TextBlock.type'
import { wrapSvgIconPath } from '../service'
import { faCube } from '@fortawesome/free-solid-svg-icons'
import StyledComponentsRegistry from 'src/app/styled-components-registry'
import { Lang } from 'src/models/common/Lang'

export class EditorTextReference {
  static get toolbox() {
    return {
      title: 'Text reference',
      icon: wrapSvgIconPath(faCube.icon[4]),
    }
  }

  lang: Lang
  data: ITextReferenceBlock['data'] | null

  constructor({
    config,
    data,
  }: {
    config?: { lang: Lang }
    data: ITextReferenceBlock['data']
  }) {
    this.lang = config?.lang || Lang.fr
    this.data = data
  }

  render() {
    const container = document.createElement('div')
    const reactRoot = createRoot(container)
    reactRoot.render(
      <StyledComponentsRegistry>
        <DeviceContextProvider>
          <ToastProvider>
            <TextRefProvider
              textPageId={this.data?.textPageId ?? null}
              chapterId={this.data?.chapterId ?? null}
              lang={this.lang}
              showPageTitle={this.data?.showPageTitle ?? true}
              showPageSubtitle={this.data?.showPageSubtitle ?? true}
              coverPosition={this.data?.coverPosition ?? ''}
              onUpdate={(data) => (this.data = data)}
            >
              <TextRef />
            </TextRefProvider>
          </ToastProvider>
        </DeviceContextProvider>
      </StyledComponentsRegistry>
    )
    return container
  }

  save() {
    const data = this.data
    return data
  }
}
