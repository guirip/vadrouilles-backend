import { useEffect, useMemo, useState } from 'react'
import { PagePreview } from 'src/app/_components/text/PagePreview'
import { Loader } from 'src/app/backend/_components/Loader'
import type { ITextPageMetadata } from 'src/services/TextService.metadata'
import { useTextRefContext } from './TextRefProvider'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faPen } from '@fortawesome/free-solid-svg-icons'
import {
  CategoryTitle,
  ChapterWrapper,
  Content,
  EditButton,
  Field,
  FieldCount,
  PageContent,
  PageTitle,
  TextPage,
  Wrapper,
} from './style'
import { faEyeSlash } from '@fortawesome/free-regular-svg-icons'
import { InputSwitch } from 'primereact/inputswitch'
import {
  FieldTitle,
  InlineField,
} from 'src/app/backend/_components/dialog/Dialog.styles'
import { Badge } from 'primereact/badge'
import { InputText } from 'primereact/inputtext'

const SelectedTextRef = () => {
  const {
    getSelectedMetadata,
    selectedChapterId,
    select,
    showPageTitle,
    setShowPageTitle,
    showPageSubtitle,
    setShowPageSubtitle,
    coverPosition,
    setCoverPosition,
  } = useTextRefContext()
  const metadata = useMemo(() => getSelectedMetadata(), [getSelectedMetadata])

  return !metadata ? (
    'Failed to find selected item'
  ) : (
    <div className="">
      <div className="flex justify-content-center">
        <PagePreview
          focus={true}
          metadata={metadata}
          showPageTitle={showPageTitle}
          showPageSubtitle={showPageSubtitle}
          coverPosition={coverPosition}
        />

        <div className="flex flex-column align-items-center justify-content-center align-items-center m-3">
          <Badge
            value={selectedChapterId ? 'chapter' : 'page'}
            size="large"
            severity="contrast"
            className="font-normal"
          />
          <EditButton
            onClick={() => {
              select(null, null)
            }}
          >
            <FontAwesomeIcon icon={faPen} size="lg" />
          </EditButton>
        </div>
      </div>
      <div className="flex justify-content-center">
        <InlineField className="mt-3 mx-4">
          <FieldTitle>Show page title</FieldTitle>
          <InputSwitch
            className="m-1"
            checked={showPageTitle}
            onChange={(e) => {
              setShowPageTitle(e.value)
            }}
          />
        </InlineField>

        <InlineField className="mt-3 mx-4">
          <FieldTitle>Show page subtitle</FieldTitle>
          <InputSwitch
            className="m-1"
            checked={showPageSubtitle}
            onChange={(e) => {
              setShowPageSubtitle(e.value)
            }}
          />
        </InlineField>

        <InlineField className="mt-3 mx-4">
          <FieldTitle>Cover position</FieldTitle>
          <InputText
            value={coverPosition}
            onChange={({ target: { value } }) => {
              setCoverPosition(value)
            }}
          />
        </InlineField>
      </div>
    </div>
  )
}

const Chapter = ({
  textPageId,
  chapterId,
}: {
  textPageId: string
  chapterId: string
}) => {
  const context = useTextRefContext()
  const chapter = context.findChapter(textPageId, chapterId)

  return !chapter ? (
    'Failed to find chapter'
  ) : (
    <ChapterWrapper $visible={chapter.visible}>
      <div className="flex justify-content-center flex-grow-1">
        <PagePreview
          metadata={chapter.metadata}
          onClick={() => {
            context.select(textPageId, chapter.chapterId)
          }}
        />
      </div>
    </ChapterWrapper>
  )
}

const Page = ({ pageMetadata }: { pageMetadata: ITextPageMetadata }) => {
  const [hidden, setHidden] = useState(false)
  const context = useTextRefContext()

  return (
    <TextPage $visible={pageMetadata.visible}>
      <PageTitle onClick={() => setHidden(!hidden)}>
        <Field>
          <div>{pageMetadata.title}</div>
          {!pageMetadata.visible && (
            <FontAwesomeIcon icon={faEyeSlash} color="#3b3b73" size="sm" />
          )}
        </Field>
        <FieldCount>
          {pageMetadata.chapters.length} chapter
          {pageMetadata.chapters.length > 1 ? 's' : ''}
        </FieldCount>
      </PageTitle>
      <PageContent
        className={`column align-items-center justify-content-center pb-5 ${hidden ? '' : 'hidden'}`}
      >
        <div className="font-medium text-center text-xl mt-4 mb-2">PAGE</div>
        <div className="flex justify-content-center">
          <PagePreview
            metadata={pageMetadata.metadata}
            onClick={() => {
              context.select(pageMetadata.textPageId, null)
            }}
          />
        </div>

        <div className="font-medium text-center text-xl mt-4 mb-2">
          CHAPTERS
        </div>
        {pageMetadata.chapters.map((chapterMetadata) => (
          <Chapter
            key={chapterMetadata.chapterId}
            textPageId={pageMetadata.textPageId}
            chapterId={chapterMetadata.chapterId}
          />
        ))}
      </PageContent>
    </TextPage>
  )
}

interface ICategoryProps {
  name: string
  pagesMetadata: ITextPageMetadata[]
}

const Category = ({ name, pagesMetadata }: ICategoryProps) => {
  const [visible, setVisible] = useState(false)
  return (
    <div>
      <CategoryTitle onClick={() => setVisible(!visible)}>
        {`${name.charAt(0).toUpperCase()}${name.slice(1)}`}
      </CategoryTitle>
      <Content $visible={visible}>
        {pagesMetadata.map((pageMetadata) => (
          <Page key={pageMetadata.textPageId} pageMetadata={pageMetadata} />
        ))}
      </Content>
    </div>
  )
}

const TextRefSelect = () => {
  const context = useTextRefContext()

  return (
    <div>
      {context.allMetadata &&
        Object.keys(context.allMetadata).map((category) =>
          !context.allMetadata ? null : (
            <Category
              key={category}
              name={category}
              pagesMetadata={context.allMetadata[category]}
            />
          )
        )}
    </div>
  )
}

export const TextRef = () => {
  const context = useTextRefContext()

  useEffect(() => {
    if (!context.loaded && !context.loading) {
      context.load()
    }
  })

  return (
    <Wrapper id="text-ref-wrapper">
      {context.loading && <Loader />}
      {context.loaded &&
        (context.selectedTextPageId ? <SelectedTextRef /> : <TextRefSelect />)}
    </Wrapper>
  )
}
