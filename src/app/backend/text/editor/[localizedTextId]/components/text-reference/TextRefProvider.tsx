import type { Metadata } from 'next'
import type { PropsWithChildren } from 'react'
import {
  createContext,
  useCallback,
  useContext,
  useEffect,
  useMemo,
  useState,
} from 'react'
import { useToastContext } from 'src/app/_components/ToastProvider'
import { getAllMetadata } from 'src/app/_server-actions/metadata.actions'
import type { Lang } from 'src/models/common/Lang'
import type { ITextReferenceBlock } from 'src/models/text/TextBlock.type'
import {
  type AllTextMetadata,
  type IChapterMetadata,
} from 'src/services/TextService.metadata'

function getPageMetadata(
  allMetadata: AllTextMetadata | null,
  textPageId: string | null
) {
  if (allMetadata && textPageId) {
    for (const catTextPages of Object.values(allMetadata)) {
      const match = catTextPages.find(
        (pageMetadata) => pageMetadata.textPageId === textPageId
      )
      if (match) {
        return match
      }
    }
  }
  return null
}

interface ITextRefContext {
  selectedTextPageId: string | null
  selectedChapterId: string | null
  select: (textPageId: string | null, chapterId: string | null) => void

  showPageTitle: boolean
  setShowPageTitle: (value: boolean) => void

  showPageSubtitle: boolean
  setShowPageSubtitle: (value: boolean) => void

  coverPosition: string
  setCoverPosition: (value: string) => void

  allMetadata: AllTextMetadata | null
  loaded: boolean
  loading: boolean
  load: () => Promise<void>
  findChapter: (
    textPageId: string,
    chapterId: string
  ) => IChapterMetadata | null
  getSelectedMetadata: () => Metadata | null
}

const TextRefContext = createContext<ITextRefContext | null>(null)

export const useTextRefContext = () => {
  const context = useContext(TextRefContext)
  if (!context) {
    throw new Error('TextRef context is not initialiazed yet')
  }
  return context
}

interface IProps {
  textPageId: string | null
  chapterId: string | null
  lang: Lang
  showPageTitle: ITextReferenceBlock['data']['showPageTitle']
  showPageSubtitle: ITextReferenceBlock['data']['showPageSubtitle']
  coverPosition: ITextReferenceBlock['data']['coverPosition']
  onUpdate: (data: null | ITextReferenceBlock['data']) => void
}

export const TextRefProvider = (props: PropsWithChildren<IProps>) => {
  const { showError } = useToastContext()

  const [selectedTextPageId, setSelectedTextPageId] = useState<
    ITextRefContext['selectedTextPageId']
  >(props.textPageId)

  const [selectedChapterId, setSelectedChapterId] = useState<
    ITextRefContext['selectedChapterId']
  >(props.chapterId ?? null)

  const [showPageTitle, setShowPageTitle] = useState(
    props.showPageTitle ?? true
  )
  const [showPageSubtitle, setShowPageSubtitle] = useState(
    props.showPageSubtitle ?? true
  )
  const [coverPosition, setCoverPosition] = useState(props.coverPosition ?? '')

  const [allMetadata, setMetadata] =
    useState<ITextRefContext['allMetadata']>(null)
  const [loaded, setLoaded] = useState<ITextRefContext['loaded']>(false)
  const [loading, setLoading] = useState<ITextRefContext['loading']>(false)

  const select = useCallback(
    (textPageId: string | null, chapterId: string | null) => {
      setSelectedTextPageId(textPageId)
      setSelectedChapterId(chapterId)
    },
    []
  )

  useEffect(() => {
    props.onUpdate(
      !selectedTextPageId
        ? null
        : {
            textPageId: selectedTextPageId,
            chapterId: selectedChapterId,
            showPageTitle,
            showPageSubtitle,
            coverPosition,
          }
    )
  }, [
    coverPosition,
    props,
    selectedChapterId,
    selectedTextPageId,
    showPageSubtitle,
    showPageTitle,
  ])

  const load = useCallback(async () => {
    setLoading(true)
    try {
      const allMetadata = await getAllMetadata(props.lang)
      setMetadata(allMetadata)
    } catch (e) {
      showError('Failed to fetch pages metadata', e)
    }
    setLoaded(true)
    setLoading(false)
  }, [props.lang, showError])

  const findChapter = useCallback(
    (textPageId: string, chapterId: string) => {
      if (allMetadata) {
        const textPage = getPageMetadata(allMetadata, textPageId)
        if (textPage) {
          return (
            textPage.chapters.find(
              (chapterMetadata) => chapterMetadata.chapterId === chapterId
            ) ?? null
          )
        }
      }
      return null
    },
    [allMetadata]
  )

  const getSelectedMetadata = useCallback(() => {
    if (!selectedTextPageId) {
      return null
    }
    if (selectedChapterId) {
      return (
        findChapter(selectedTextPageId, selectedChapterId)?.metadata ?? null
      )
    }
    return getPageMetadata(allMetadata, selectedTextPageId)?.metadata ?? null
  }, [allMetadata, findChapter, selectedChapterId, selectedTextPageId])

  const context = useMemo(
    () => ({
      selectedTextPageId,
      selectedChapterId,
      select,
      allMetadata,
      loaded,
      loading,
      load,
      findChapter,
      getSelectedMetadata,
      showPageTitle,
      setShowPageTitle,
      showPageSubtitle,
      setShowPageSubtitle,
      coverPosition,
      setCoverPosition,
    }),
    [
      selectedTextPageId,
      selectedChapterId,
      select,
      allMetadata,
      loaded,
      loading,
      load,
      findChapter,
      getSelectedMetadata,
      showPageTitle,
      showPageSubtitle,
      coverPosition,
    ]
  )

  return (
    <TextRefContext.Provider value={context}>
      {props.children}
    </TextRefContext.Provider>
  )
}
