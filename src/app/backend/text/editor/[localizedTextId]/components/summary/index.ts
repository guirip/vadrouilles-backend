import { faBookBookmark } from '@fortawesome/free-solid-svg-icons'
import { wrapSvgIconPath } from '../service'
import { FONT_SIZE } from 'src/app/_style/style.constants'

export class EditorSummary {
  static get toolbox() {
    return {
      title: 'Table of contents',
      icon: wrapSvgIconPath(faBookBookmark.icon[4]),
    }
  }

  render() {
    const container = document.createElement('div')
    container.innerHTML = `
      <style>
        .toc-block {
          display: flex;
          justify-content: center;
          user-select: none;
          font-size: ${FONT_SIZE.md};
        }
        .toc-block i {
          opacity: 0.9;
        }
      </style>
      <div class="toc-block">
        <i>[ Table of contents ]</i>
      </div>`
    return container
  }

  save() {
    // Data is processed server-side when saving
    return {}
  }
}
