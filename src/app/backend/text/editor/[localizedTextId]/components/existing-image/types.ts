import type { ImagesPerText } from 'src/models/text/TextPage.type'

export interface IConfig {
  textPageId: string
  imagesPerText: ImagesPerText | undefined
  staticUrl: string
}
