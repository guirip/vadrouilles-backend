import { createRoot } from 'react-dom/client'
import type { IConfig } from './types'
import { wrapSvgIconPath } from '../service'
import { faImages } from '@fortawesome/free-regular-svg-icons'
import { ExistingImageProvider } from 'src/app/backend/text/components/existing-image-picker/ExistingImageProvider'
import { ImagePicker } from 'src/app/backend/text/components/existing-image-picker/ImagePicker'

const errorDiv = (message: string) => {
  const container = document.createElement('div')
  container.textContent = message
  return container
}

export class EditorExistingImage {
  static get toolbox() {
    return {
      title: 'Existing image',
      icon: wrapSvgIconPath(faImages.icon[4]),
    }
  }

  config: IConfig | null

  constructor({ config }: { config?: IConfig }) {
    this.config = config ?? null
    this.getSelectedImageBlock.bind(this)
  }

  getSelectedImageBlock(container: HTMLDivElement) {
    if (!this.config) {
      return null
    }
    const wrapperEl = container.querySelector('.wrapper')
    if (!(wrapperEl instanceof HTMLDivElement)) {
      console.error('Failed to find .wrapper div element')
      return null
    }

    const { chapterId, blockId } = wrapperEl.dataset
    if (!chapterId || !blockId) {
      console.info('it seems no image is selected')
      return null
    }

    const imageBlock =
      this.config.imagesPerText &&
      this.config.imagesPerText.imagesByChapter[chapterId].images.find(
        ({ id }) => id === blockId
      )
    if (!imageBlock) {
      alert(
        `Failed to match selected existing image (chapter id ${chapterId} and image block id ${blockId})`
      )
    }
    return imageBlock
  }

  render() {
    if (!this.config?.textPageId) {
      return errorDiv('Missing text page id')
    }
    if (!this.config?.imagesPerText) {
      return errorDiv('Images list not loaded yet')
    }
    const { textPageId, staticUrl, imagesPerText } = this.config

    const container = document.createElement('div')
    const reactRoot = createRoot(container)
    reactRoot.render(
      <ExistingImageProvider
        textPageId={textPageId}
        staticUrl={staticUrl}
        imagesPerText={imagesPerText}
      >
        <ImagePicker />
      </ExistingImageProvider>
    )
    return container
  }

  save(container) {
    const imageBlock = this.getSelectedImageBlock(container)
    return imageBlock
  }
}
