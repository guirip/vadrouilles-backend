import { VIDEO_DEFAULT_SOURCE, VideoSource } from 'src/models/common/Video'
import type { IVideoBlock } from 'src/models/text/TextBlock.type'
import { wrapSvgIconPath } from '../service'
import { faFilm } from '@fortawesome/free-solid-svg-icons'

const Ids = {
  VIDEO_ID: 'value',
  LABEL: 'label',
  DESCRIPTION: 'description',
  PLATFORM: 'source',
}

const isPlatformSelected = (
  iValue: VideoSource,
  blockValue: IVideoBlock['data']['source'] | undefined
) => (blockValue ? iValue === blockValue : iValue === VIDEO_DEFAULT_SOURCE)

const renderForm = (data: Partial<IVideoBlock['data']>) => `
<style>
  .editor-video-form > div {
    margin: 1rem 0;
  }
  .editor-video-form input,
  .editor-video-form select {
    margin-left: 1rem;
    padding: .5rem
  }
  .editor-video-text {
    min-width: 360px;
  }
</style>
<div class="editor-video-form">
  <div><b>Video</b></div>

  <div>
    <label htmlFor="${Ids.VIDEO_ID}">Video ID</label>
    <input
      id="${Ids.VIDEO_ID}"
      type="text"
      value="${data?.value ?? ''}"
    />
  </div>

  <div>
    <label htmlFor="${Ids.LABEL}">Label (private)</label>
    <input
      id="${Ids.LABEL}"
      class="editor-video-text"
      type="text"
      value="${data?.label ?? ''}"
    />
  </div>

  <div>
    <label htmlFor="${Ids.DESCRIPTION}">Description (public)</label>
    <input
      id="${Ids.DESCRIPTION}"
      class="editor-video-text"
      type="text"
      value="${data?.description ?? ''}"
    />
  </div>

  <div>
    <label htmlFor="${Ids.PLATFORM}">Platform</label>
    <select id="${Ids.PLATFORM}">
      ${Object.keys(VideoSource).map(
        (videoKey) =>
          `<option value="${VideoSource[videoKey]}" ${
            isPlatformSelected(VideoSource[videoKey], data?.source)
              ? 'selected'
              : ''
          }>${videoKey}</option>`
      )}
    </select>
  </div>

</div>
`

export class EditorVideo {
  static get toolbox() {
    return {
      title: 'Video',
      icon: wrapSvgIconPath(faFilm.icon[4]),
    }
  }

  data: Partial<IVideoBlock['data']> = {}

  constructor({ data }) {
    this.data = data
  }

  render() {
    const container = document.createElement('div')
    container.innerHTML = renderForm(this.data)
    return container
  }

  save(containerEl: HTMLDivElement) {
    const data = Object.values(Ids).reduce((acc, id) => {
      acc[id] = (
        containerEl.querySelector(`#${id}`) as
          | HTMLInputElement
          | HTMLSelectElement
      ).value
      return acc
    }, {})

    return data
  }
}
