const renderPath = (path: string) => `<path d="${path}" />`

export const wrapSvgIconPath = (path: string | string[]) =>
  `<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" width="256" height="256" viewBox="0 0 256 256" xml:space="preserve"><g transform="scale(0.4 0.4)">${Array.isArray(path) ? path.map((p) => renderPath(p)).join('') : renderPath(path)}</g></svg>`
