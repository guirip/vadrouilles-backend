import { Button } from 'primereact/button'
import { FontFamily } from 'src/app/_assets/fonts'
import { FONT_SIZE } from 'src/app/_style/style.constants'
import { headerStyle } from 'src/app/_style/text/header'
import { listStyle, liStyle } from 'src/app/_style/text/list'
import {
  callOutStyle,
  citationStyle,
  detailsStyle,
  paragraphStyle,
} from 'src/app/_style/text/paragraph'
import { getTableStyle } from 'src/app/_style/text/table'
import { getModeStyle, Mode } from 'src/app/frontend/_style/mode.style'
import styled from 'styled-components'

const EDITOR_TOOLS_COLOR = '#4f4f4f'

export const Holder = styled.div`
  margin-top: 1rem;
  width: 1280px;
  max-width: 90vw;

  & .ce-block {
    // mimic frontend block style
    margin: 0.3rem 0.3rem 14px;
    padding: 0.5rem 0.75rem;
    background-color: rgba(0, 0, 0, 0.3);

    & > div {
      overflow-x: auto;
      overflow-y: hidden; // hide scrollbars when dragging to reorder
    }
  }
  & .ce-block__content,
  & .ce-toolbar__content {
    min-width: 220px;
    max-width: 90vw;
  }

  & .ce-block__content {
    font-family: ${FontFamily.LEAGUESPARTAN}, sans-serif;

    ${headerStyle}

    & .cdx-nested-list {
      ${listStyle}
      & li {
        ${liStyle}
      }
    }

    & .tc-wrap:has(> .tc-table) {
      /* reduce table component width to avoid overflow */
      width: 95%;
    }
    & .tc-table {
      ${getTableStyle(
        '.tc-row',
        '.tc-cell[heading]',
        '.tc-cell:not([heading])'
      )}
    }
    & .tc-row--selected,
    & .tc-cell--selected,
    & .tc-add-row:hover,
    & .tc-add-column:hover {
      background-color: ${getModeStyle(true, Mode.DARK).background};
    }
    & .tc-add-row:hover:before,
    & .tc-row--selected:after {
      background: transparent;
    }

    & .ce-paragraph {
      font-size: ${FONT_SIZE.md};
      ${paragraphStyle}
    }
  }
  & .ce-block {
    .cdx-text-variant--call-out .ce-block__content {
      border: 0;
      box-shadow: none;
      margin: 0;
      padding: 0;
    }
    .cdx-text-variant--call-out .ce-paragraph {
      ${callOutStyle}
    }
    .cdx-text-variant--citation .ce-paragraph {
      ${citationStyle}
    }
    .cdx-text-variant--details .ce-paragraph {
      ${detailsStyle}
    }
  }

  & .ce-inline-toolbar__dropdown {
    color: ${EDITOR_TOOLS_COLOR};
  }

  /* Conversion tool */
  & .ce-conversion-tool {
    color: ${EDITOR_TOOLS_COLOR};
  }
  & .ce-inline-tool {
    min-width: 2rem;
    color: ${EDITOR_TOOLS_COLOR};
  }

  /* texts in the toolbox list are not visible */
  & .ce-toolbox,
  & .tc-popover {
    color: ${EDITOR_TOOLS_COLOR};
  }
  & .ce-popover__item-icon svg {
    scale: 0.7;
  }

  /* icons on the right are barely visible on a dark background */
  & .ce-toolbar__plus,
  & .ce-toolbar__settings-btn {
    background-color: #898787;
  }
  & .ce-toolbar__plus:hover,
  & .ce-toolbar__settings-btn:hover {
    background-color: white;
  }

  /* filter field, not working */
  & .cdx-search-field {
    display: none;
  }
  & .ce-popover--opened {
    max-height: 80vh;
  }
  /* move up, delete, move down */
  & .ce-popover__items {
    color: ${EDITOR_TOOLS_COLOR};
  }
  /* text variants icons */
  & .ce-popover__custom-content {
    fill: ${EDITOR_TOOLS_COLOR};
  }

  /* delimiter */
  & .ce-delimiter {
    overflow: hidden;
  }

  /* image */
  & .image-tool__image-picture {
    margin: auto;
  }

  /* table */
  & .tc-wrap {
    margin-left: calc(var(--toolbox-icon-size) * 2);
  }
  & .tc-row {
    border-left: 1px solid var(--color-border);
    border-right: 1px solid var(--color-border);
  }
  & .tc-row:after,
  & .tc-table--heading .tc-row:first-child:after,
  & .tc-add-column {
    border: 0;
  }

  & .codex-editor__redactor {
    padding-bottom: 100px !important;
  }
`
export const SaveButton = styled(Button)`
  position: absolute;
  z-index: 2;
  top: 10px;
  right: 60px;
  font-size: 1.2rem;
`
