import type { IGalleryBlock } from 'src/models/text/TextBlock.type'
import icon from './icon'
import { FONT_SIZE } from 'src/app/_style/style.constants'

type IData = Pick<IGalleryBlock['data'], 'showThumbnails'>

export class EditorGallery {
  static get toolbox() {
    return {
      title: 'Gallery',
      icon,
    }
  }

  data: IData

  constructor({ data }: { data: IData }) {
    this.data = data
  }

  render() {
    const container = document.createElement('div')
    container.innerHTML = `
      <style>
        .gallery-block {
          display: flex;
          align-items: center;
          font-size: ${FONT_SIZE.md};
        }
        .gallery-block-title {
          display: flex;
          align-items: baseline;
        }
        .gallery-form {
          margin-left: 2rem;
        }
      </style>
      <div class="gallery-block">
        <div class="gallery-block-title">⬇ <h6>Gallery</h6> ⬇</div>
        <span class="gallery-form pt-1">
          <label for="gallery-show-thumbnail">Show thumbnails?</label>
          <input type="checkbox" name="gallery-show-thumbnail" ${
            this.data.showThumbnails === true ? 'checked' : ''
          } />
        </span>
      </div>`
    return container
  }

  save(container) {
    const inputEl = container.querySelector('input')
    return {
      showThumbnails: inputEl.checked,
    }
  }
}
