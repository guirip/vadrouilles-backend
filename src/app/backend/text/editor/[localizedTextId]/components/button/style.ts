import { Card } from 'primereact/card'
import { InputText } from 'primereact/inputtext'
import { FieldTitle } from 'src/app/backend/_components/dialog/Dialog.styles'
import styled, { css } from 'styled-components'

export const Container = styled.div`
  max-width: 85vw;
`

export const ButtonPreviewWrapper = styled.div`
  background: black;
`

export const StyledCard = styled(Card)`
  background: #00000044;
  border: 1px solid #434343;
`

export const StyledInputText = styled(InputText)`
  width: 16rem !important;
`

export const StyledFieldTitle = styled(FieldTitle)<{ $width?: number }>`
  ${({ $width }) =>
    $width &&
    css`
      width: ${$width}rem;
    `};
`
