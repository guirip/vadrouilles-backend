import { faHandPointer } from '@fortawesome/free-solid-svg-icons'
import { createRoot } from 'react-dom/client'
import StyledComponentsRegistry from 'src/app/styled-components-registry'
import type { IButtonBlock } from 'src/models/text/TextBlock.type'
import { wrapSvgIconPath } from '../service'
import { ButtonPreviewAndForm } from './ButtonPreviewAndForm'

const ButtonDataTemplate: IButtonBlock['data'] = {
  type: 'link',
  value: '',
  target: '_blank',
  label: '',
  icon: '',
  style: {
    size: 1.2,
    mx: 4,
    my: 4,
    px: 4,
    py: 3,
    theme: 'primary',
  },
}

export class EditorButton {
  static get toolbox() {
    return {
      title: 'Button',
      icon: wrapSvgIconPath(faHandPointer.icon[4]),
    }
  }

  data: IButtonBlock['data']

  constructor({ data }: { data: IButtonBlock['data'] }) {
    this.data =
      Object.keys(data).length > 0 ? data : structuredClone(ButtonDataTemplate)
  }

  render() {
    const container = document.createElement('div')
    const reactRoot = createRoot(container)
    reactRoot.render(
      <StyledComponentsRegistry>
        <ButtonPreviewAndForm
          data={this.data}
          callback={(data) => {
            this.data = data
          }}
        />
      </StyledComponentsRegistry>
    )
    return container
  }

  save() {
    return this.data
  }
}
