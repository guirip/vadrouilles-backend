import { Divider } from 'primereact/divider'
import { Dropdown } from 'primereact/dropdown'
import { useCallback, useEffect, useState } from 'react'
import { TextButton } from 'src/app/_components/text/TextButton'
import {
  InlineField,
  StyledInputNumber,
} from 'src/app/backend/_components/dialog/Dialog.styles'
import {
  getButtonThemes,
  type IButtonBlock,
} from 'src/models/text/TextBlock.type'
import {
  ButtonPreviewWrapper,
  Container,
  StyledCard,
  StyledFieldTitle,
  StyledInputText,
} from './style'

interface IProps {
  data: IButtonBlock['data']
  callback: (data: IButtonBlock['data']) => void
}

export const ButtonPreviewAndForm = (props: IProps) => {
  const [data, updateData] = useState(props.data)

  const updateStyle = useCallback((key, value) => {
    updateData(({ style, ...restData }) => ({
      ...restData,
      style: {
        ...style,
        [key]: value,
      },
    }))
  }, [])

  useEffect(() => {
    props.callback(data)
  }, [data, props])

  return (
    <Container
      id="button-wrapper"
      className="flex flex-column justify-content-evenly align-items-center"
    >
      <div className="flex-grow-1 mt-1 mb-2 mx-4 flex justify-content-center">
        <ButtonPreviewWrapper>
          <TextButton data={data} />
        </ButtonPreviewWrapper>
      </div>

      <div className="flex-grow-1 flex justify-content-center align-items-center flex-wrap">
        <StyledCard className="flex flex-column px-3 my-2 mx-2">
          <InlineField className="flex-grow-1">
            <StyledFieldTitle $width={3}>Lien</StyledFieldTitle>
            <StyledInputText
              className="flex-grow-1"
              value={data.value}
              onChange={({ target: { value } }) => {
                updateData((data) => ({ ...data, value }))
              }}
            />
          </InlineField>

          <InlineField className="flex-grow-1 mt-3">
            <StyledFieldTitle $width={3}>Label</StyledFieldTitle>
            <StyledInputText
              className="flex-grow-1"
              value={data.label}
              onChange={({ target: { value } }) => {
                updateData((data) => ({ ...data, label: value }))
              }}
            />
          </InlineField>

          <InlineField className="flex-grow-1 mt-3">
            <StyledFieldTitle $width={3}>Icon</StyledFieldTitle>
            <StyledInputText
              className="flex-grow-1"
              value={data.icon}
              onChange={({ target: { value } }) => {
                updateData((data) => ({ ...data, icon: value }))
              }}
            />
          </InlineField>

          <InlineField className="flex-grow-1 mt-3">
            <StyledFieldTitle $width={3}>Target</StyledFieldTitle>
            <Dropdown
              value={data.target}
              options={['_blank', '_self']}
              placeholder="Select target"
              onChange={({ target: { value } }) => {
                updateData((data) => ({ ...data, target: value }))
              }}
            />
          </InlineField>
        </StyledCard>

        <StyledCard className="mx-5 px-2 flex flex-wrap">
          <div className="flex justify-content-evenly mx-1 flex-wrap">
            <InlineField className="flex-grow-1 mb-2 mx-3">
              <StyledFieldTitle>Theme</StyledFieldTitle>
              <Dropdown
                value={data.style.theme}
                options={getButtonThemes()}
                placeholder="Select theme"
                onChange={({ target: { value } }) => {
                  updateStyle('theme', value)
                }}
              />
            </InlineField>

            <InlineField className="mx-3 mb-1">
              <StyledFieldTitle $width={8}>Size (rem)</StyledFieldTitle>
              <StyledInputNumber
                $width={60}
                value={data.style.size}
                onChange={({ value }) => {
                  updateStyle('size', value)
                }}
              />
            </InlineField>
          </div>
          <Divider />

          <div className="flex justify-content-evenly mx-1 flex-wrap">
            <InlineField className="mx-3 my-1">
              <StyledFieldTitle $width={8}>
                X margins <small>(0-8)</small>
              </StyledFieldTitle>
              <StyledInputNumber
                $width={60}
                value={data.style.mx}
                onChange={({ value }) => {
                  updateStyle('mx', value)
                }}
              />
            </InlineField>
            <InlineField className="mx-3 my-1">
              <StyledFieldTitle $width={8}>
                Y margins <small>(0-8)</small>
              </StyledFieldTitle>
              <StyledInputNumber
                $width={60}
                value={data.style.my}
                onChange={({ value }) => {
                  updateStyle('my', value)
                }}
              />
            </InlineField>
          </div>
          <Divider />

          <div className="flex justify-content-evenly mx-1 flex-wrap">
            <InlineField className="mx-3 my-1">
              <StyledFieldTitle $width={8}>
                X paddings <small>(0-8)</small>
              </StyledFieldTitle>
              <StyledInputNumber
                $width={60}
                value={data.style.px}
                onChange={({ value }) => {
                  updateStyle('px', value)
                }}
              />
            </InlineField>
            <InlineField className="mx-3 my-1">
              <StyledFieldTitle $width={8}>
                Y paddings <small>(0-8)</small>
              </StyledFieldTitle>
              <StyledInputNumber
                $width={60}
                maxLength={1}
                value={data.style.py}
                onChange={({ value }) => {
                  updateStyle('py', value)
                }}
              />
            </InlineField>
          </div>
        </StyledCard>
      </div>
    </Container>
  )
}
