'use client'

import Delimiter from '@editorjs/delimiter'
import type { OutputData, ToolConstructable } from '@editorjs/editorjs'
import EditorJS from '@editorjs/editorjs'
import Header from '@editorjs/header'
import Image from '@editorjs/image'
import Marker from '@editorjs/marker'
import NestedList from '@editorjs/nested-list'
import Table from '@editorjs/table'
import TextVariantTune from '@editorjs/text-variant-tune'
import Underline from '@editorjs/underline'
import { faCheckCircle } from '@fortawesome/free-regular-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import DragDrop from 'editorjs-drag-drop'
import { useCallback, useEffect, useState } from 'react'
import AlignmentBlockTune from 'editorjs-text-alignment-blocktune'

import { useImagesPerText } from '@/backend/text/components/existing-image-picker/hook'
import { useToastContext } from 'src/app/_components/ToastProvider'
import { apiUpdateLocalizedText } from 'src/client-api/LocalizedTextApi'
import { apiUploadFile } from 'src/client-api/UploadApi'
import type {
  ILocalizedTextFront,
  ITextContent,
} from 'src/models/text/LocalizedText.type'
import { BlockType, ToolType, Tunes } from 'src/models/text/TextBlock.type'
import { EditorButton } from './button'
import { EditorExistingImage } from './existing-image'
import { EditorGallery } from './gallery'
import { Social } from './social'
import { EditorSpacer } from './spacer'
import { Holder, SaveButton } from './style'
import { EditorSummary } from './summary'
import { EditorTextReference } from './text-reference'
import { EditorVideo } from './video'

// NB: known issue about importing `LogLevels`: https://github.com/codex-team/editor.js/issues/1576
export enum LogLevels {
  VERBOSE = 'VERBOSE',
  INFO = 'INFO',
  WARN = 'WARN',
  ERROR = 'ERROR',
}
const EDITOR_LOG_LEVEL = LogLevels.VERBOSE

const HOLDER_ID = 'editor'
interface IProps {
  staticUrl: string
  localizedText: ILocalizedTextFront
  textPageId: string
}

const EditorComponent = ({ localizedText, staticUrl, textPageId }: IProps) => {
  const [editorInitialized, setEditorInitialized] = useState(false)
  const [editor, setEditor] = useState<null | EditorJS>(null)

  const { imagesPerText, initialized: imagesInitialized } =
    useImagesPerText(textPageId)

  const { showError, showSuccess } = useToastContext()

  useEffect(() => {
    if (!window || !EditorJS || !imagesInitialized || editorInitialized) {
      return
    }
    setEditorInitialized(true)

    const _editor = new EditorJS({
      holder: HOLDER_ID,
      onReady: () => {
        new DragDrop(_editor)
      },
      tools: {
        [BlockType.Paragraph]: {
          tunes: [Tunes.TextVariant, Tunes.Alignment],
        },
        [BlockType.Button]: EditorButton,
        [BlockType.Summary]: EditorSummary,
        [BlockType.Header]: {
          class: Header,
          tunes: [Tunes.Alignment],
        },
        [BlockType.Gallery]: EditorGallery,
        [BlockType.Image]: {
          class: Image,
          config: {
            uploader: {
              uploadByFile(file: File) {
                return apiUploadFile(file)
              },
            },
          },
        },
        [BlockType.ExistingImage]: {
          class: EditorExistingImage,
          config: {
            textPageId,
            imagesPerText,
            staticUrl,
          },
        },
        [BlockType.TextReference]: {
          class: EditorTextReference,
          config: {
            lang: localizedText.lang,
          },
        },
        [BlockType.Video]: EditorVideo,
        [BlockType.List]: {
          class: NestedList as unknown as ToolConstructable,
          inlineToolbar: true,
        },
        [BlockType.Social]: Social,
        [BlockType.Table]: {
          class: Table as ToolConstructable,
          inlineToolbar: true,
          config: {
            row: 2,
            cols: 3,
          },
        },
        [BlockType.Delimiter]: Delimiter,
        [BlockType.Spacer]: EditorSpacer,
        // Tools:
        [ToolType.Marker]: Marker,
        [ToolType.Underline]: Underline,
        // Tunes:
        [Tunes.TextVariant]: TextVariantTune,
        [Tunes.Alignment]: AlignmentBlockTune,
      },
      data: (localizedText.content as OutputData) || {},
      autofocus: true,
      placeholder: 'Let`s write an awesome story!',
      logLevel: EDITOR_LOG_LEVEL,
      tunes: [], // applies to all blocks
    })
    setEditor(_editor)

    // unmount callback
    return () => {
      if (_editor && _editor.destroy) {
        _editor.destroy()
      }
    }
  }, [
    localizedText,
    staticUrl,
    setEditor,
    imagesInitialized,
    imagesPerText,
    textPageId,
    editorInitialized,
  ])

  const onClickOnSave = useCallback(
    async function save() {
      if (!editor) {
        throw new Error('editor should be defined')
      }
      const data = (await editor.save()) as ITextContent
      try {
        await apiUpdateLocalizedText({ _id: localizedText._id, content: data })
        showSuccess('Saved 😇')
      } catch (e) {
        showError('Failed to save localizedText', e)
      }
    },
    [editor, localizedText._id, showError, showSuccess]
  )

  return (
    <>
      <SaveButton
        severity="info"
        icon={<FontAwesomeIcon className="mr-2" icon={faCheckCircle} />}
        label="Save"
        onClick={onClickOnSave}
      />
      <Holder id={HOLDER_ID} />
    </>
  )
}

export default EditorComponent
