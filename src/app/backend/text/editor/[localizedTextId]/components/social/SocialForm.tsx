import { Dropdown } from 'primereact/dropdown'
import { InputText } from 'primereact/inputtext'
import { useMemo, useState } from 'react'
import {
  InlineField,
  FieldTitle,
} from 'src/app/backend/_components/dialog/Dialog.styles'
import {
  getSocialTypes,
  type ISocialBlock,
} from 'src/models/text/TextBlock.type'
import styled from 'styled-components'

const Wrapper = styled.div`
  max-width: 450px;
`

export const SocialForm = (props: ISocialBlock['data']) => {
  const [url, setUrl] = useState(props.url ?? '')
  const [type, setType] = useState(props.type ?? 'instagram')

  const types = useMemo(
    () =>
      getSocialTypes().map((type) => ({
        label: type,
        value: type,
      })),
    []
  )

  return (
    <Wrapper
      className="wrapper flex-row justify-content-start"
      data-type={type}
      data-url={url}
    >
      <div className="flex-column">
        <InlineField className="m-2">
          <FieldTitle>Social</FieldTitle>
          <Dropdown
            value={type}
            options={types}
            onChange={(e) => {
              setType(e.value)
            }}
            placeholder="Category"
            className=""
          />
        </InlineField>

        <InlineField className="m-2">
          <FieldTitle>Post url</FieldTitle>
          <InputText
            value={url}
            onChange={({ target }) => {
              setUrl(target.value)
            }}
            className="flex-grow-1"
          />
        </InlineField>
      </div>
    </Wrapper>
  )
}
