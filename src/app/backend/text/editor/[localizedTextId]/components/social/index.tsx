import { createRoot } from 'react-dom/client'
import { isSocialType, type ISocialBlock } from 'src/models/text/TextBlock.type'
import { SocialForm } from './SocialForm'
import { faInstagram } from '@fortawesome/free-brands-svg-icons'
import { wrapSvgIconPath } from '../service'

export class Social {
  static get toolbox() {
    return {
      title: 'Social',
      icon: wrapSvgIconPath(faInstagram.icon[4]),
    }
  }

  data: ISocialBlock['data']

  constructor({ data }: { data: ISocialBlock['data'] }) {
    this.data = data
    this.getFormData.bind(this)
  }

  getFormData(container: HTMLDivElement): ISocialBlock['data'] | null {
    const wrapperEl = container.querySelector('.wrapper')
    if (!(wrapperEl instanceof HTMLDivElement)) {
      console.error('Failed to find wrapper element')
      return null
    }

    const socialType = wrapperEl.dataset.type
    const socialUrl = wrapperEl.dataset.url

    if (!socialType || !socialUrl) {
      alert('Failed to retrieve social form data')
      return null
    }
    if (!isSocialType(socialType)) {
      alert(`Invalid social: ${socialType}`)
      return null
    }

    return {
      type: socialType,
      url: socialUrl,
    }
  }

  render() {
    const container = document.createElement('div')
    const reactRoot = createRoot(container)
    reactRoot.render(<SocialForm type={this.data.type} url={this.data.url} />)
    return container
  }

  save(container) {
    const data = this.getFormData(container)
    return data
  }
}
