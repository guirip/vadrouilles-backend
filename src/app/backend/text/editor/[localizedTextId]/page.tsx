import { H1 } from '@/backend/_components/Titles'
import { Warning } from '@/backend/_components/Warning'
import { isValidObjectId } from 'mongoose'
import dynamic from 'next/dynamic'
import type { ICodif } from 'src/models/Codif'
import { CodifModel } from 'src/models/Codif'
import type { ILocalizedText } from 'src/models/text/LocalizedText'
import { LocalizedTextModel } from 'src/models/text/LocalizedText'
import type {
  IBaseLocalizedText,
  ILocalizedTextFront,
} from 'src/models/text/LocalizedText.type'
import { toSerializable } from 'src/services/DbService'
import { addUrlToImageBlocks } from 'src/services/TextService.localizedText'

const getTitle = (lt: IBaseLocalizedText) =>
  `📝 ${lt.title || 'unknown title'} (${lt.lang || 'unknown lang'})`

const EditorComponent = dynamic(() => import('./components/Editor'))

interface IProps {
  params: Promise<{ localizedTextId: string }>
  searchParams: Promise<{ textPageId?: string }>
}

export default async function EditorPage(props: IProps) {
  const searchParams = await props.searchParams

  const { textPageId } = searchParams

  const params = await props.params

  const { localizedTextId } = params

  const [localizedText, staticUrlCodif] = await Promise.all([
    isValidObjectId(localizedTextId)
      ? LocalizedTextModel.findOne({
          _id: localizedTextId,
        }).lean<ILocalizedText>()
      : null,
    CodifModel.findOne({ name: 'static-url' }).lean<ICodif>(),
  ])

  const warnings: string[] = []
  if (!localizedText) {
    warnings.push('Localized text is not available.')
  }
  if (!textPageId) {
    warnings.push('Text page id parameter is missing.')
  }
  if (!staticUrlCodif || !staticUrlCodif.value) {
    warnings.push('Static url codif is missing.')
  }
  if (!EditorComponent) {
    warnings.push('Editor component is not available.')
  }
  if (
    warnings.length > 0 ||
    // for linter:
    !localizedText ||
    !textPageId ||
    !staticUrlCodif?.value ||
    !EditorComponent
  ) {
    return (
      <Warning>
        {warnings.map((warning, index) => (
          <div key={index}>{warning}</div>
        ))}
      </Warning>
    )
  }

  const staticUrl = String(staticUrlCodif.value)
  addUrlToImageBlocks(localizedText, staticUrl)

  const localizedTextLean = toSerializable<ILocalizedTextFront>(localizedText)

  const title = getTitle(localizedTextLean)

  return (
    <>
      <H1 className="mx-5">{title}</H1>

      <EditorComponent
        localizedText={localizedTextLean}
        textPageId={textPageId}
        staticUrl={staticUrl}
      />
    </>
  )
}
