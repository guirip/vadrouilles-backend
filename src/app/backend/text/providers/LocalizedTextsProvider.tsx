'use client'

import type { PropsWithChildren } from 'react'
import {
  createContext,
  useCallback,
  useContext,
  useEffect,
  useMemo,
  useState,
} from 'react'
import { useToastContext } from 'src/app/_components/ToastProvider'
import {
  apiCreateLocalizedText,
  apiDeleteLocalizedText,
  apiTranslateLocalizedText,
  apiUpdateLocalizedText,
} from 'src/client-api/LocalizedTextApi'
import type {
  ILocalizedTextFormItem,
  ILocalizedTextMinimalFront,
} from 'src/models/text/LocalizedText.type'
import { Lang } from 'src/models/common/Lang'
import type { ITextChapterMinimalPopulatedFront } from 'src/models/text/TextChapter.type'

interface ILocalizedTextsContext {
  chapterId: ITextChapterMinimalPopulatedFront['_id']
  localizedTexts: ILocalizedTextMinimalFront[]
  persistLocalizedText: (
    localizedText: ILocalizedTextMinimalFront
  ) => Promise<boolean>
  deleteLocalizedText: (_id: ILocalizedTextMinimalFront['_id']) => Promise<void>
  frLtIdWithContent: ILocalizedTextMinimalFront['_id'] | null
  hasEnLt: boolean
  existingLocalizations: Lang[]
  translateFromFrench: () => Promise<void>
}

const LocalizedTextsContext = createContext<ILocalizedTextsContext | null>(null)

export function useLocalizedTextsContext() {
  const context = useContext(LocalizedTextsContext)

  if (context === null) {
    throw new Error('Localized texts context is not initialized yet')
  }
  return context
}

interface IProps {
  chapterId: ITextChapterMinimalPopulatedFront['_id']
  localizedTexts: ILocalizedTextMinimalFront[]
  existingLocalizations: Lang[]
}

export const LocalizedTextsProvider = ({
  chapterId,
  localizedTexts,
  existingLocalizations,
  children,
}: PropsWithChildren<IProps>) => {
  const { showInfo, showError } = useToastContext()

  const [frLtIdWithContent, setFrLtIdWithContent] = useState<
    ILocalizedTextMinimalFront['_id'] | null
  >(null)
  const [hasEnLt, setHasEnLt] = useState(false)

  useEffect(() => {
    if (Array.isArray(localizedTexts)) {
      const frLt = localizedTexts.find(({ lang }) => lang === Lang.fr)
      setFrLtIdWithContent(frLt && frLt.blocksCount > 0 ? frLt._id : null)

      const enLt = localizedTexts.find(({ lang }) => lang === Lang.en)
      setHasEnLt(!!enLt)
    }
  }, [localizedTexts, existingLocalizations, setHasEnLt])

  const persistLocalizedText = useCallback(
    async function (localizedText: ILocalizedTextFormItem) {
      const { blocksCount, ...ltData } = localizedText
      try {
        if (!localizedText._id) {
          await apiCreateLocalizedText(chapterId, ltData)
        } else {
          await apiUpdateLocalizedText(ltData)
        }
      } catch (e) {
        showError('Failed to save text', e)
        return false
      }
      return true
    },
    [chapterId, showError]
  )

  const deleteLocalizedText = useCallback(
    async function (_id: ILocalizedTextMinimalFront['_id']) {
      try {
        await apiDeleteLocalizedText(_id)
      } catch (e) {
        showError('Failed to delete text', e)
      }
    },
    [showError]
  )

  const translateFromFrench = useCallback(
    async function () {
      if (!frLtIdWithContent) {
        return
      }
      showInfo(`Translation process ongoing...`)
      try {
        await apiTranslateLocalizedText(frLtIdWithContent, Lang.en)
      } catch (e) {
        showError('Failed to generate english version by translating french', e)
      }
    },
    [frLtIdWithContent, showError, showInfo]
  )

  const context = useMemo(
    () => ({
      chapterId,
      localizedTexts,
      persistLocalizedText,
      deleteLocalizedText,
      existingLocalizations,
      frLtIdWithContent,
      hasEnLt,
      translateFromFrench,
    }),
    [
      chapterId,
      localizedTexts,
      persistLocalizedText,
      deleteLocalizedText,
      existingLocalizations,
      frLtIdWithContent,
      hasEnLt,
      translateFromFrench,
    ]
  )

  return (
    <>
      <LocalizedTextsContext.Provider value={context}>
        {children}
      </LocalizedTextsContext.Provider>
    </>
  )
}
