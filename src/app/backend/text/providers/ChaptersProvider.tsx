'use client'

import type { PropsWithChildren } from 'react'
import {
  createContext,
  useCallback,
  useContext,
  useEffect,
  useMemo,
  useState,
} from 'react'
import { useToastContext } from 'src/app/_components/ToastProvider'
import {
  apiCreateChapter,
  apiDeleteChapter,
  apiDuplicateChapter,
  apiFetchChapters,
  apiMoveChapter,
  apiUpdateChapter,
  apiUpdateChaptersOrder,
} from 'src/client-api/ChapterApi'
import type { IIdOrderMap } from 'src/models/common/IdOrderMap'
import type { ITextChapterMinimalPopulatedFront } from 'src/models/text/TextChapter.type'
import type { ITextPageFront } from 'src/models/text/TextPage.type'

export interface IChaptersContext {
  textPageId: string
  textPageSlug: string
  chapters: ITextChapterMinimalPopulatedFront[]
  refreshChapters: () => Promise<void>
  persistChapter: (
    chapter: ITextChapterMinimalPopulatedFront
  ) => Promise<boolean>
  moveChapter: (
    targetTextPageId: string,
    chapter: ITextChapterMinimalPopulatedFront
  ) => Promise<boolean>
  duplicateChapter: (
    chapterId: ITextChapterMinimalPopulatedFront['_id']
  ) => Promise<void>
  deleteChapter: (
    chapterId: ITextChapterMinimalPopulatedFront['_id']
  ) => Promise<void>
  updateChaptersOrder: (idOrderMap: IIdOrderMap) => Promise<boolean>
}

const ChaptersContext = createContext<IChaptersContext | null>(null)

export const useChaptersContext = () => {
  const context = useContext(ChaptersContext)
  if (!context) {
    throw new Error('Text page context is not initialized yet')
  }
  return context
}

interface IProps {
  textPageId: ITextPageFront['_id']
  textPageSlug: ITextPageFront['slug']
  refreshTextPages: () => Promise<void>
}

export const ChaptersProvider = ({
  textPageId,
  textPageSlug,
  refreshTextPages,
  children,
}: PropsWithChildren<IProps>) => {
  const { showSuccess, showError } = useToastContext()
  const [chapters, setChapters] = useState<ITextChapterMinimalPopulatedFront[]>(
    []
  )

  const refreshChapters = useCallback(
    async function () {
      try {
        setChapters(await apiFetchChapters(textPageId))
      } catch (e) {
        showError('Failed to fetch chapters', e)
      }
    },
    [showError, textPageId]
  )

  useEffect(() => {
    refreshChapters()
  }, [refreshChapters, textPageId])

  const persistChapter = useCallback(
    async function ({
      localizedTexts,
      ...chapterData
    }: ITextChapterMinimalPopulatedFront) {
      let success = false
      try {
        if (!chapterData._id) {
          await apiCreateChapter(chapterData, textPageId)
        } else {
          await apiUpdateChapter(chapterData)
        }
        success = true
      } catch (e) {
        showError('Failed to save chapter', e)
      }
      if (success) {
        await refreshChapters()
        await refreshTextPages()
      }
      return success
    },
    [textPageId, showError, refreshChapters, refreshTextPages]
  )

  const updateChaptersOrder = useCallback(
    async function (idOrderMap: IIdOrderMap) {
      let success = false
      try {
        await apiUpdateChaptersOrder(idOrderMap)
        success = true
        showSuccess('Order')
        refreshChapters()
      } catch (e) {
        showError('Failed to update order', e)
      }
      return success
    },
    [refreshChapters, showError, showSuccess]
  )

  const duplicateChapter = useCallback(
    async function (chapterId: ITextChapterMinimalPopulatedFront['_id']) {
      try {
        await apiDuplicateChapter(textPageId, chapterId)
      } catch (e) {
        showError('Failed to duplicate chapter', e)
      }
      await refreshChapters()
      await refreshTextPages()
    },
    [refreshChapters, refreshTextPages, showError, textPageId]
  )

  const moveChapter = useCallback(
    async (
      targetTextPageId: string,
      chapter: ITextChapterMinimalPopulatedFront
    ) => {
      if (textPageId === targetTextPageId) {
        return false
      }
      try {
        await apiMoveChapter(targetTextPageId, chapter._id)
        showSuccess('Chapter move success')
        await refreshChapters()
        await refreshTextPages()
      } catch (e) {
        showError('Failed to move chapter', e)
        return false
      }
      return true
    },
    [refreshChapters, refreshTextPages, showError, showSuccess, textPageId]
  )

  const deleteChapter = useCallback(
    async function (_id: ITextChapterMinimalPopulatedFront['_id']) {
      try {
        await apiDeleteChapter(_id)
        showSuccess('Chapter delete success')
        await refreshChapters()
        await refreshTextPages()
      } catch (e) {
        showError('Failed to delete chapter', e)
      }
    },
    [refreshChapters, refreshTextPages, showError, showSuccess]
  )

  const context = useMemo(
    () => ({
      textPageId,
      textPageSlug,
      chapters,
      refreshChapters,
      persistChapter,
      moveChapter,
      duplicateChapter,
      deleteChapter,
      updateChaptersOrder,
    }),
    [
      textPageId,
      textPageSlug,
      chapters,
      refreshChapters,
      persistChapter,
      moveChapter,
      duplicateChapter,
      deleteChapter,
      updateChaptersOrder,
    ]
  )

  return (
    <>
      <ChaptersContext.Provider value={context}>
        {children}
      </ChaptersContext.Provider>
    </>
  )
}
