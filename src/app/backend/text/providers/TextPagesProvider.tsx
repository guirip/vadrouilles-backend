'use client'

import { useRouter } from 'next/navigation'
import type { PropsWithChildren } from 'react'
import {
  createContext,
  useCallback,
  useContext,
  useEffect,
  useMemo,
  useState,
} from 'react'
import type { ITextPageFront } from 'src/models/text/TextPage.type'
import { DEFAULT_TEXT_CATEGORY } from 'src/models/text/TextPage.type'
import type { ContentCategory } from 'src/models/common/ContentCategory'

import {
  apiCreatePage,
  apiDeletePage,
  apiFetchPages,
  apiUpdateOrder,
  apiUpdatePage,
} from 'src/client-api/TextPageApi'
import { useToastContext } from 'src/app/_components/ToastProvider'
import type { IIdOrderMap } from 'src/models/common/IdOrderMap'
import { TEXT_PAGE_PATH } from '@/backend/page-paths'
import { determineNewTextPageId, getTextPage } from '../util'

interface ITextPagesContext {
  pages: ITextPageFront[]
  refreshTextPages: () => Promise<void>

  currentTextPageId: ITextPageFront['_id'] | null
  setCurrentTextPageId: (textPageId: ITextPageFront['_id'] | null) => void
  persistTextPage: (textPage: ITextPageFront) => Promise<boolean>
  updatePagesOrder: (idOrderMap: IIdOrderMap) => Promise<boolean>
  deleteTextPage: (textPageId: ITextPageFront['_id']) => Promise<void>

  currentCat: ContentCategory
  setCurrentCat: (cat: ContentCategory) => void
}

const TextPagesContext = createContext<ITextPagesContext | null>(null)

export const useTextPagesContext = () => {
  const context = useContext(TextPagesContext)
  if (!context) {
    throw new Error('Text manager context not initialized yet')
  }
  return context
}

interface IProps {
  textPages: ITextPageFront[]
  currentTextPageId: ITextPageFront['_id'] | null
  currentCategory: ContentCategory
}

export const TextPagesProvider = (props: PropsWithChildren<IProps>) => {
  const router = useRouter()
  const { showSuccess, showError } = useToastContext()

  const [currentCat, _setCurrentCat] = useState(
    props.currentCategory || DEFAULT_TEXT_CATEGORY
  )

  const [pages, setPages] = useState(props.textPages)

  const [currentTextPageId, setCurrentTextPageId] = useState<
    ITextPageFront['_id'] | null
  >(props.currentTextPageId || (pages.length > 0 ? pages[0]._id : null))

  const [contextualPath, setContextualPath] = useState('')

  useEffect(() => {
    const textPage = currentTextPageId
      ? getTextPage(currentTextPageId, pages)
      : null
    const path = `${TEXT_PAGE_PATH}?category=${currentCat}${
      textPage?.slug ? `&slug=${textPage.slug}` : ''
    }`
    setContextualPath(path)
  }, [currentTextPageId, pages, currentCat])

  useEffect(() => {
    router.replace(contextualPath)
  }, [contextualPath, router])

  const refreshTextPages = useCallback(
    async function (
      cat: ContentCategory = currentCat,
      newPageId?: ITextPageFront['_id']
    ) {
      try {
        const pages = await apiFetchPages(cat)
        setPages(pages)

        const newTextPageId = determineNewTextPageId(
          newPageId ?? currentTextPageId,
          pages
        )
        if (newTextPageId !== currentTextPageId) {
          setCurrentTextPageId(newTextPageId)
        }
      } catch (e) {
        showError('Failed to fetch pages', e)
      }
    },
    [currentCat, currentTextPageId, showError]
  )

  const setCurrentCat = useCallback(
    function (newCat: ContentCategory) {
      _setCurrentCat(newCat)
      setPages([])
      setCurrentTextPageId(null)
      refreshTextPages(newCat)
    },
    [refreshTextPages, setCurrentTextPageId]
  )

  const persistTextPage = useCallback(
    async function ({ chapters, ...textPage }) {
      try {
        const createdPage = !textPage._id
          ? await apiCreatePage(textPage)
          : await apiUpdatePage(textPage)
        await refreshTextPages(currentCat, createdPage._id)
      } catch (e) {
        showError('Failed to save page', e)
        return false
      }
      return true
    },
    [currentCat, refreshTextPages, showError]
  )

  const updatePagesOrder = useCallback(
    async function (idOrderMap: IIdOrderMap) {
      let success = false
      try {
        await apiUpdateOrder(idOrderMap)
        success = true
        showSuccess('Order')
        refreshTextPages()
      } catch (e) {
        showError('Failed to update order', e)
      }
      return success
    },
    [refreshTextPages, showError, showSuccess]
  )

  const deleteTextPage = useCallback(
    async function (textPageId: ITextPageFront['_id']) {
      try {
        await apiDeletePage(textPageId)
        showSuccess('Page delete success')
        refreshTextPages()
      } catch (e) {
        showError('Failed to delete page', e)
      }
    },
    [refreshTextPages, showError, showSuccess]
  )

  const context = useMemo(
    () => ({
      pages,
      currentTextPageId,
      setCurrentTextPageId,
      refreshTextPages,
      persistTextPage,
      updatePagesOrder,
      deleteTextPage,
      currentCat,
      setCurrentCat,
    }),
    [
      pages,
      currentTextPageId,
      setCurrentTextPageId,
      refreshTextPages,
      persistTextPage,
      updatePagesOrder,
      deleteTextPage,
      currentCat,
      setCurrentCat,
    ]
  )

  return (
    <>
      <TextPagesContext.Provider value={context}>
        {props.children}
      </TextPagesContext.Provider>
    </>
  )
}
