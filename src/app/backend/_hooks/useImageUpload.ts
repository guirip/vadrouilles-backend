import type { FileUpload, FileUploadUploadEvent } from 'primereact/fileupload'
import { useCallback, useRef } from 'react'
import { useToastContext } from 'src/app/_components/ToastProvider'
import type { IUploadResponse } from 'src/models/Api'
import type { IFile } from 'src/models/common/File.type'

export function useImageUpload(onSuccess: (files: IFile[]) => void) {
  const fileUploadRef = useRef<FileUpload>(null)

  const { showWarning } = useToastContext()

  const afterPicturesUpload = useCallback(
    async function ({ xhr }: FileUploadUploadEvent) {
      if (xhr) {
        const response = (await new Response(
          xhr.responseText
        ).json()) as IUploadResponse
        const { outputs } = response
        const { errors } = response
        const proceed = () => {
          onSuccess(outputs)
        }

        if (Array.isArray(errors) && errors.length > 0) {
          showWarning(`${errors.length} errors occured`, errors.join('  -  '))
          window.setTimeout(proceed, 4000)
        } else {
          proceed()
        }
      } else {
        console.error(
          'unexpected case, this function should receive `xhr` as parameter'
        )
        onSuccess([])
      }
    },
    [onSuccess, showWarning]
  )

  function setUploadRequestHeader({ xhr }) {
    xhr.setRequestHeader('Accept', 'application/json')
  }

  const onPicturesValidationFail = useCallback(
    function (file: File) {
      showWarning(
        'Image rejected (too big?)',
        `${file.size ? `(${Math.floor(file.size / 10000) / 100} mo)` : ''} ${
          file.name
        }`
      )
    },
    [showWarning]
  )

  const onPictureUploadError = useCallback(
    function ({ xhr }) {
      showWarning('Upload failed', `${xhr.status} ${xhr.statusText}`)
    },
    [showWarning]
  )

  return {
    fileUploadRef,
    setUploadRequestHeader,
    onPicturesValidationFail,
    onPictureUploadError,
    afterPicturesUpload,
  }
}
