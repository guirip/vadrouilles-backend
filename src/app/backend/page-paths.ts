import { getBackCompletePath } from 'src/utils/util'

export const CODIF_PAGE_PATH = getBackCompletePath('/codif')

export const CATALOG_PAGE_PATH = getBackCompletePath('/catalog')
export const CATALOG_ITEM_CATEGORY_PAGE_PATH = getBackCompletePath(
  '/catalog-item-category'
)

export const TEXT_PAGE_PATH = getBackCompletePath('/text')
export const EDITOR_PAGE_PATH = getBackCompletePath('/text/editor')

export const DEFAULT_PAGE = TEXT_PAGE_PATH
