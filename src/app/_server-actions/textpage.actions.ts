'use server'

import { listImageBlocks } from 'src/services/TextService.page'

export async function getImagesPerText(textPageId: string) {
  return await listImageBlocks(textPageId)
}
