'use server'

import { getCodifStaticUrl } from 'src/services/CodifService'

export async function getStaticUrl() {
  return (await getCodifStaticUrl())?.value ?? ''
}
