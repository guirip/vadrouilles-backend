'use server'

import type { ITextReferenceBlock } from 'src/models/text/TextBlock.type'
import type { Lang } from 'src/models/common/Lang'
import {
  getFullMetadata,
  queryTextRefMetadata,
} from 'src/services/TextService.metadata'

export async function getTextRefMetadata(
  data: ITextReferenceBlock['data'],
  lang: Lang
) {
  return await queryTextRefMetadata(data, lang)
}

export async function getAllMetadata(lang: Lang) {
  return await getFullMetadata(lang)
}
