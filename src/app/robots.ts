import type { MetadataRoute } from 'next'

export default function robots(): MetadataRoute.Robots {
  return {
    rules: [
      { userAgent: 'Amazonbot', disallow: '/', },
      { userAgent: 'anthropic-ai', disallow: '/', },
      { userAgent: 'Applebot', disallow: '/', },
      { userAgent: 'Bytespider', disallow: '/', },
      { userAgent: 'CCBot', disallow: '/', },
      { userAgent: 'ChatGPT-User', disallow: '/', },
      { userAgent: 'Claude-Web', disallow: '/', },
      { userAgent: 'ClaudeBot', disallow: '/', },
      { userAgent: 'Diffbot', disallow: '/', },
      { userAgent: 'FacebookBot', disallow: '/', },
      { userAgent: 'Google-Extended', disallow: '/', },
      { userAgent: 'GPTBot', disallow: '/', },
      { userAgent: 'ImagesiftBot', disallow: '/', },
      { userAgent: 'Omgili', disallow: '/', },
      { userAgent: 'Omgilibot', disallow: '/', },
      { userAgent: 'PerplexityBot', disallow: '/', },
      { userAgent: 'YouBot', disallow: '/', },
      {
        userAgent: '*',
        allow: '/',
      }
    ],
    sitemap: `${process.env.PUBLIC_URL}/sitemap.xml`,
  }
}
