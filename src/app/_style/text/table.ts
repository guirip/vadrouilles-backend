import { FONT_SIZE } from '../style.constants'

const BORDER_STYLE = '1px solid #797979'
const TH_BG_COLOR = 'rgba(255, 255, 255, .1)'

export const getTableStyle = (
  rowSelector = 'tr',
  thSelector = 'th',
  tdSelector = 'td'
) => `
  font-size: ${FONT_SIZE.md};
  font-weight: 300;
  border-spacing: 0;
  border: 0;

  & ${thSelector} {
    padding: 0.85rem 1rem 0.6rem;
    text-align: center;
    font-weight: 500;
    letter-spacing: 0.5px;
    background-color: ${TH_BG_COLOR};
    border: 0;
  }
  & ${tdSelector} {
    padding: 0.6rem 0.9rem;
    max-width: 600px;
    border: ${BORDER_STYLE};
    border-right: 0;
    border-bottom: 0;
  }
  & ${tdSelector}:last-child {
    border-right: ${BORDER_STYLE};
  }
  & ${rowSelector} {
    border: 0;
  }
  & ${rowSelector}:last-child ${tdSelector} {
    border-bottom: ${BORDER_STYLE};
  }
`
