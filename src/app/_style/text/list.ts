import { FONT_SIZE } from '../style.constants'
import { BLOCK_MARGIN_BOTTOM, BLOCK_MARGIN_TOP, MAX_WIDTH } from './constants'

const MARGIN_X = 20

export const listStyle = `
  margin-top: ${BLOCK_MARGIN_TOP} ${MARGIN_X}px ${BLOCK_MARGIN_BOTTOM};
  max-width: ${MAX_WIDTH - MARGIN_X * 2}px;
  padding: 0.1rem 0.6rem 0.1rem 0;
  background-color: rgba(0, 0, 0, 0.25);
  font-size: ${FONT_SIZE.md};

  & ul,
  & ol {
  }
`
export const olStyle = ``

export const ulStyle = ``

export const liStyle = `
  margin: 0.1rem 0;
  padding: 0 0.2rem;

  & span {
    display: block;
  }
`
