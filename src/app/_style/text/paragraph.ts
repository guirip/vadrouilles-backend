import { getModeStyle } from '../../frontend/_style/mode.style'
import { FONT_SIZE } from '../style.constants'
import { MAX_WIDTH } from './constants'

export const paragraphStyle = `
  max-width: ${MAX_WIDTH}px;
  background-color: rgba(0, 0, 0, 0.3);
  padding: 0.5rem 0.75rem;
  line-height: 1.9rem;
  font-weight: 300;
`

export const callOutStyle = `
  padding: 0.3rem 0.5rem;
  text-align: center;
  background: ${getModeStyle(true).background};
  border-radius: 4px;
`
export const citationStyle = `
  padding: 0.6rem 1rem;
  font-style: italic;
  text-align: center;
  background: rgba(0, 0, 0, 0.6);
  border-radius: 4px;
`
export const detailsStyle = `
  display: flex;
  justify-content: center;
  align-items: center;
  padding: 0.4rem 0.7rem;
  font-size: ${FONT_SIZE.md};
  background: rgba(180, 180, 180, 0.2);
`
