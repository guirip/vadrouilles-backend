import { FONT_SIZE } from '../style.constants'

export const headerStyle = `
  h1 {
    font-size: ${FONT_SIZE.h1};
    margin: 48px 10px 20px;
  }
  h2 {
    font-size: ${FONT_SIZE.h2};
    margin: 43px 10px 18px;
  }
  h3 {
    font-size: ${FONT_SIZE.h3};
    margin: 38px 10px 15px;
  }
  h4 {
    font-size: ${FONT_SIZE.h4};
    margin: 33px 10px 14px;
  }
  h5 {
    font-size: ${FONT_SIZE.h5};
    margin: 20px 10px 10px;
  }
  h6 {
    font-size: ${FONT_SIZE.h6};
    margin: 15px 10px 8px;
  }

  &:first-child > h1,
  &:first-child > h2,
  &:first-child > h3,
  &:first-child > h4,
  &:first-child > h5,
  &:first-child > h6 {
    margin-top: 4px;
  }
`
