import { TEXT_COLOR } from '../style.constants'

export const delimiterStyle = `
  display: flex;
  justify-content: center;

  & div {
    margin: 2rem 0;
    width: 45vw;
    border-bottom: 1px solid ${TEXT_COLOR};
    opacity: 0.6;
  }
`
