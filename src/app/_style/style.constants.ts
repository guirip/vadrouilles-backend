import graffitiBgImage from 'src/app/_assets/background-graffiti.webp'
import natureBgImage from 'src/app/_assets/background-nature.webp'
import { ContentCategory } from 'src/models/common/ContentCategory'

export const BG_BY_CATEGORY = {
  [ContentCategory.NATURE]: natureBgImage,
  [ContentCategory.ABOUT]: natureBgImage,
  [ContentCategory.GRAFFITI]: graffitiBgImage,
}

export const TEXT_COLOR = '#ffffff'
export const BG_COLOR = '#000000'
export const SUBTITLE_COLOR = '#b8b8b8'
export const LINK_COLOR = '#56d5ff'

export const DEFAULT_ROOT_FONT_SIZE = 13
export const MOBILE_DELTA_FONT_SIZE = -2

export const FONT_SIZE = {
  h1: '2.3rem',
  h2: '2.1rem',
  h3: '2rem',
  h4: '1.8rem',
  h5: '1.6rem',
  h6: '1.5rem',
  md: '1.4rem',
  sm: '1.3rem',
  xs: '1.2rem',
  xxs: '1.1rem',
}

export const getHtmlFontSizeStyle = (
  rootFontSize: number,
  override: boolean
) => `
  html {
    font-size: ${rootFontSize}px${override ? ' !important' : ''};
  }
  @media screen and (max-width: 600px) {
    html {
      font-size: ${rootFontSize + MOBILE_DELTA_FONT_SIZE}px${override ? ' !important' : ''};
    }
  }
`
