'use client'

import styled, { css } from 'styled-components'

import { FONT_SIZE, LINK_COLOR } from 'src/app/_style/style.constants'
import { ItemStatus } from 'src/models/CatalogItem.type'

export const Field = styled.div`
  margin: 0.9rem 0;
  font-size: ${FONT_SIZE.md};
`

export const FieldName = styled.div`
  margin-right: 0.5rem;
  font-size: ${FONT_SIZE.sm};
  font-weight: bold;
  opacity: 0.6;
`

export const FieldNameCentered = styled(FieldName)`
  text-align: center;
`

export const InlineField = styled(Field)`
  display: flex;
  flex-direction: row;
  align-items: baseline;
`

interface IContainerProps {
  $status: ItemStatus
}

export const Container = styled.div<IContainerProps>`
  display: flex;
  flex-direction: column;
  align-items: center;
  margin: 1rem 0.5rem;
  padding: 1.3rem ${FONT_SIZE.md} 1rem;
  font-style: normal;
  background-color: rgba(0, 0, 0, 0.45);
  transition: background-color 0.4s;

  max-width: 90vw;
  & img {
    max-width: 86vw;
  }
  @media (min-width: 768px) {
    & {
      width: 360px;
    }
    & img {
      max-width: 340px;
    }
  }
  @media (min-width: 1024px) {
    & {
      max-width: 360px;
    }
    & img {
      max-width: 340px;
    }
  }

  ${({ $status }) =>
    $status !== ItemStatus.Available &&
    css`
      //opacity: .6;
    `}

  &:hover {
    background-color: rgba(0, 0, 0, 0.6);
  }
`

export const StatusPriceContainer = styled.div`
  width: 100%;
  max-width: 360px;
  margin: 0.4rem 0.2rem 0;
`

export const FieldStatus = styled(Field)`
  font-size: ${FONT_SIZE.md};
  font-weight: bold;
  text-align: center;
  letter-spacing: 1px;

  &.available {
    color: #62ff62;
    opacity: 0.7;
  }
  &.booked {
    color: #ffca00;
    font-size: ${FONT_SIZE.md};
  }
  &.sold {
    color: white;
    text-transform: uppercase;
  }
`

interface IPriceProps {
  $isOldPrice?: boolean
  $isNewPrice?: boolean
}

export const Price = styled.div<IPriceProps>`
  font-size: ${FONT_SIZE.md};

  ${(props) =>
    props.$isOldPrice &&
    css`
      text-decoration: line-through;
      font-size: ${FONT_SIZE.sm};
    `}
  ${(props) =>
    props.$isNewPrice &&
    css`
      margin: 0 0.4rem;
      color: lightgreen;
      font-weight: bold;
    `}
`

export const ItemTitle = styled.div`
  margin: 1rem 0 0rem;
  font-size: ${FONT_SIZE.md};
  text-align: center;
  text-transform: uppercase;
  font-style: italic;
  font-weight: bold;
`

export const CenteredText = styled.div`
  text-align: center;
  word-break: break-word;

  a {
    color: ${LINK_COLOR};
  }
`
