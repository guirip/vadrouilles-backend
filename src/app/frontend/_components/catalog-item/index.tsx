'use client'

import { useCodifContext } from '@/frontend/_providers/CodifContextProvider'
import { useLangContext } from '@/frontend/_providers/LangContextProvider'
import { useMemo } from 'react'
import type { ICatalogItemFront } from 'src/models/CatalogItem.type'
import type { IFile } from 'src/models/common/File.type'
import Gallery from '../gallery'
import { GallerySize, type IVisual } from '../gallery/types'
import {
  CenteredText,
  Container,
  Field,
  FieldName,
  FieldNameCentered,
  FieldStatus,
  InlineField,
  ItemTitle,
  Price,
  StatusPriceContainer,
} from './styles'

const getDiscountedPrice = (price: number, discount: number) =>
  Math.floor(price - (price * discount) / 100)

const fileToImageBlockData = (f: IFile): IVisual => ({
  file: f,
  caption: '',
  stretched: false,
  withBackground: false,
  withBorder: false,
})

interface IProps {
  item: ICatalogItemFront
}

function CatalogItem({ item }: IProps) {
  const { getLabel } = useLangContext()

  const { catalogDiscount } = useCodifContext()
  const hasDiscount = typeof catalogDiscount === 'number' && catalogDiscount > 0

  const visuals = useMemo(
    () => (item.files ?? []).map((f) => fileToImageBlockData(f)),
    [item]
  )

  return (
    <Container $status={item.status}>
      <Gallery
        visuals={visuals}
        thumbSize={GallerySize.BIG}
        showGalleryThumbnails
      />

      <ItemTitle>{item.title}</ItemTitle>

      <StatusPriceContainer className="flex flex-row justify-content-evenly align-items-baseline">
        <FieldStatus className={item.status}>
          {getLabel(`catalog.status.${item.status}`)}
        </FieldStatus>

        {item.price && item.status === 'available' && (
          <InlineField>
            <FieldName>{getLabel('catalog.price')}</FieldName>
            <Price $isOldPrice={hasDiscount}>{item.price} €</Price>
            {hasDiscount && (
              <Price $isNewPrice>
                {getDiscountedPrice(item.price, catalogDiscount)} €
              </Price>
            )}
          </InlineField>
        )}
      </StatusPriceContainer>

      <div>
        {item.description && (
          <Field>
            <CenteredText
              dangerouslySetInnerHTML={{ __html: item.description }}
            />
          </Field>
        )}

        {item.technique && (
          <Field>
            <FieldNameCentered>
              {getLabel('catalog.technique')}
            </FieldNameCentered>
            <CenteredText>{item.technique}</CenteredText>
          </Field>
        )}

        {item.dimensions && (
          <Field className="justify-content-center">
            <FieldNameCentered>
              {getLabel('catalog.dimensions')}
            </FieldNameCentered>
            <CenteredText>{item.dimensions}</CenteredText>
          </Field>
        )}
        {item.weight && (
          <InlineField className="justify-content-center">
            <FieldName>{getLabel('catalog.weight')}</FieldName>
            <CenteredText>{item.weight}</CenteredText>
          </InlineField>
        )}
      </div>
    </Container>
  )
}

export default CatalogItem
