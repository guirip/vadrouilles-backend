'use client'

import styled from 'styled-components'

import { FontFamily } from 'src/app/_assets/fonts'
import { useLangContext } from '@/frontend/_providers/LangContextProvider'
import { faClock } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'

export type LoaderSize = 'small' | 'medium' | 'big'

const sizeToScale: Record<LoaderSize, number> = {
  small: 0.3,
  medium: 0.6,
  big: 1,
}

interface IWrapperProps {
  $size: LoaderSize
}

const Wrapper = styled.div<IWrapperProps>`
  display: flex;
  width: 100%;
  height: 80%;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  transform: ${({ $size }) => `scale(${sizeToScale[$size]})`};
  opacity: 0.7;
`
const LoaderText = styled.div`
  margin: 1em;
  font-size: 2em;
  font-family: ${FontFamily.LEAGUESPARTAN}, sans-serif;
  color: lightgrey;
`

const Text = () => {
  const { getLabel } = useLangContext()
  return <LoaderText>{getLabel('loading')}</LoaderText>
}

interface IProps {
  size?: LoaderSize
  withLabel: boolean
}

export function Loader({ size = 'big', withLabel = true }: IProps) {
  return (
    <Wrapper
      $size={size}
      className="fadeout animation-duration-1000 animation-iteration-infinite"
    >
      <FontAwesomeIcon icon={faClock} color="lightgrey" size="4x" />
      {withLabel && <Text />}
    </Wrapper>
  )
}
