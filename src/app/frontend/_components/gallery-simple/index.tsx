'use client'

import { useLangContext } from '@/frontend/_providers/LangContextProvider'
import type { StaticImageData } from 'next/image'
import { Image } from './Image'
import { Item, ItemInfo, StyledGallery, Thumb } from './styles'

export interface IGalleryItem {
  title: string
  description: string
  descriptionClassName?: string
  date: string
  image: StaticImageData
}

interface IProps {
  data: IGalleryItem[]
  priorityAmount?: number
}

function Gallery({ data, priorityAmount = 0 }: IProps) {
  const { getLabel } = useLangContext()

  return (
    <StyledGallery>
      {data.map((item, index) => (
        <Item key={item.title}>
          <h4>{item.title}</h4>
          <ItemInfo>
            <div>{item.date}</div>
            <div
              className={item.descriptionClassName || undefined}
              dangerouslySetInnerHTML={{ __html: item.description }}
            />
          </ItemInfo>
          <Thumb>
            <a aria-label={getLabel('enlargePicture')} href={item.image.src}>
              <Image
                imageData={item.image}
                title={item.title}
                priority={index < priorityAmount}
              />
            </a>
          </Thumb>
        </Item>
      ))}
    </StyledGallery>
  )
}

export default Gallery
