'use client'

import { FONT_SIZE } from 'src/app/_style/style.constants'
import styled from 'styled-components'

export const StyledGallery = styled.div`
  display: flex;
  justify-content: space-evenly;
  flex-wrap: wrap;
`

export const Item = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  margin: 0.6em 0.4em;
  padding: 0 0.5em 0.5em;
  font-size: ${FONT_SIZE.md};
  background-color: rgba(0, 0, 0, 0.35);

  & h4 {
    margin-bottom: 0.5em;
  }
`

export const ItemInfo = styled.div`
  margin: 0.2em 0;
  text-align: center;

  & div {
    padding: 0.2em;
    font-size: ${FONT_SIZE.xs};
  }
`

export const Thumb = styled.div`
  flex-grow: 1;
  display: flex;
  align-items: center;
  margin: 0.5em;
`
