import debounce from 'debounce'
import type { StaticImageData } from 'next/image'
import NextImage from 'next/image'
import { useState, useEffect } from 'react'

const DEFAULT_THUMB_WIDTH = 340

function determineThumbWidth() {
  const { clientWidth } = document.documentElement
  if (clientWidth < 460) {
    return clientWidth - 60
  }
  if (clientWidth < 1024) {
    return clientWidth - clientWidth * 0.2
  }
  return 600
}

interface IProps {
  title: string
  imageData: StaticImageData
  priority?: boolean
}

export function Image({ title, imageData, priority }: IProps) {
  const [maxWidth, setMaxWidth] = useState(DEFAULT_THUMB_WIDTH)

  const computeMaxWidth = debounce(() => {
    setMaxWidth(determineThumbWidth())
  }, 500)

  useEffect(() => {
    computeMaxWidth()

    window.addEventListener('resize', computeMaxWidth)

    return function cleanup() {
      window.removeEventListener('resize', computeMaxWidth)
    }
  }, [computeMaxWidth])

  return (
    <NextImage
      alt={title}
      src={imageData}
      fill={false}
      width={maxWidth}
      priority={priority}
      quality={90}
    />
  )
}
