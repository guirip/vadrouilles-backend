'use client'

import { FONT_SIZE } from 'src/app/_style/style.constants'
import styled from 'styled-components'

const StyledErrorMessage = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  text-align: center;
  align-items: center;
  font-size: ${FONT_SIZE.md};
  font-style: normal;
  font-weight: regular;
  letter-spacing: 0.5px;

  & small {
    font-style: italic;
  }
`

interface IProps {
  text: string | React.ReactElement
  errMessage?: unknown
}

const ErrorMessage = ({ text, errMessage }: IProps) => (
  <StyledErrorMessage>
    <span className="m-5">{text || ''}</span>
    {errMessage ? (
      <small className="mb-3">({JSON.stringify(errMessage)})</small>
    ) : null}
  </StyledErrorMessage>
)

export default ErrorMessage
