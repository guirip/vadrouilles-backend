'use client'

import anime from 'animejs'
import {
  forwardRef,
  useCallback,
  useEffect,
  useImperativeHandle,
  useMemo,
  useRef,
  useState,
} from 'react'

import { useLangContext } from '@/frontend/_providers/LangContextProvider'
import { Mode } from 'src/app/frontend/_style/mode.style'
import { getStyleAsInteger } from '@/frontend/_utils/Utils'
import debounce from 'debounce'
import { TabContent } from './TabContent'
import type { Size } from './size.styles'
import { ScrollableTabsContainer, StyledTabs, Tab } from './styles'
import type { ITabData } from './tab.type'

const MINIMUM_MOVE = 15 // pixels

const DEBUG = false
const log = (...args: unknown[]) => DEBUG && console.log(...args)

const HOME_SLUGS = ['home', 'latest']
const isHome = (textPageSlug?: string) =>
  !!textPageSlug && HOME_SLUGS.includes(textPageSlug)

function animeScroll(target: Element, value: number) {
  anime({
    targets: target,
    scrollLeft: value,
    easing: 'easeInOutQuad',
    duration: 600,
  })
}

function computeXMaxThreshold(tabsWidth: number) {
  const windowWidth = document.documentElement.clientWidth

  let value = 0
  if (tabsWidth < windowWidth) {
    // No auto scroll when there is no x overflow
    value = 0
  } else {
    value = tabsWidth - windowWidth + 15 + windowWidth * 0.16 // page style margin: 8vw
  }
  log(`tabsWidth:${tabsWidth} / windowWidth:${windowWidth} / max X:${value}`)
  return value
}

export interface ITabsHandle {
  resetScroll: () => void
}

interface IProps {
  data: ITabData[]
  previewMode: boolean
  currentTabIndex?: number
  onTabIndexChange?: (index: number, key: string) => void
  size?: Size
  mode?: Mode
}

const Tabs = forwardRef(function (
  {
    data,
    previewMode,
    currentTabIndex,
    onTabIndexChange,
    size,
    mode = Mode.DARK,
  }: IProps,
  ref
) {
  // const { isMobile } = useDeviceContext()
  const { lang } = useLangContext()

  const containerRef = useRef<HTMLDivElement>(null)
  const tabsRef = useRef<HTMLDivElement>(null)

  const [scrollLeft, setScrollLeft] = useState(0)
  const [xMaxThreshold, setXMaxThreshold] = useState(0)

  const scrollTimeout = useRef<ReturnType<typeof setTimeout> | null>(null)

  // Auto scroll to center the tab el
  const computeScrollLeft = useMemo(
    () =>
      debounce(function (index: number) {
        if (typeof index !== 'number' || !tabsRef.current) return

        const tabEl = tabsRef.current.children[index] as HTMLSpanElement
        if (!tabEl || !containerRef) return

        const { marginLeft: tabMarginLeft } = getStyleAsInteger(tabEl, [
          'marginLeft',
        ])

        const halfScreen = Math.floor(document.documentElement.clientWidth / 2)
        const halfTab = Math.floor(tabEl.offsetWidth / 2 + tabMarginLeft)
        let newScrollLeft = tabEl.offsetLeft - halfScreen + halfTab

        const logs = [
          '[autoScroll]',
          '  Target tab index: ' + index,
          `  tabMarginLeft:${tabMarginLeft} / halfScreen:${halfScreen} / halfTab:${halfTab} / newScrollLeft:${newScrollLeft}`,
        ]

        // keep most right element along the edge
        if (newScrollLeft > xMaxThreshold) {
          newScrollLeft = xMaxThreshold
          logs.push(`  keep most RIGHT element along the edge`)
        }
        // keep most left element along the edge
        else if (newScrollLeft < 0) {
          newScrollLeft = 0
          logs.push(`  keep most LEFT element along the edge`)
        }

        // Scroll for tabs on the ends
        // or when delta is at least MINIMUM_MOVE px
        if (Math.abs(scrollLeft - newScrollLeft) > MINIMUM_MOVE) {
          setScrollLeft(newScrollLeft)
          logs.push(`  computed scrollLeft = ${newScrollLeft}`)
        }

        log(logs.join('\n'))
      }, 200),
    [scrollLeft, xMaxThreshold]
  )

  const scrollTo = useCallback((value: number) => {
    if (!containerRef.current) {
      return
    }
    animeScroll(containerRef.current, value)
  }, [])

  useImperativeHandle(
    ref,
    () =>
      ({
        resetScroll: () => {
          scrollTo(0)
        },
      }) as ITabsHandle,
    [scrollTo]
  )

  useEffect(() => {
    scrollTimeout.current = setTimeout(function () {
      if (!Array.isArray(data) || data.length === 0 || !tabsRef.current) {
        return
      }
      setXMaxThreshold(computeXMaxThreshold(tabsRef.current.offsetWidth))
    }, 500)

    return () => {
      if (scrollTimeout.current) {
        clearTimeout(scrollTimeout.current)
        scrollTimeout.current = null
      }
    }
  }, [data])

  useEffect(() => {
    if (typeof currentTabIndex === 'number') {
      computeScrollLeft(currentTabIndex)
    }
  }, [data, currentTabIndex, xMaxThreshold, computeScrollLeft])

  useEffect(() => {
    scrollTo(scrollLeft)
  }, [scrollLeft, scrollTo])

  return !Array.isArray(data) || data.length === 0 ? null : (
    <ScrollableTabsContainer ref={containerRef}>
      <StyledTabs ref={tabsRef}>
        {data.map(
          (
            { key, content, isClickable, ...remainingProps }: ITabData,
            index: number
          ) => {
            const homeTab = isHome(content?.navigate?.textPageSlug)
            return (
              <Tab
                key={key}
                $mode={mode}
                $isHome={homeTab}
                $size={size}
                $selected={index === currentTabIndex}
                $disabled={
                  typeof isClickable === 'function' ? !isClickable : false
                }
                data-index={key}
                onClick={() => {
                  onTabIndexChange?.(index, key)
                }}
                {...remainingProps}
              >
                <TabContent
                  content={content}
                  previewMode={previewMode}
                  lang={lang}
                />
              </Tab>
            )
          }
        )}
      </StyledTabs>
    </ScrollableTabsContainer>
  )
})

export default Tabs
