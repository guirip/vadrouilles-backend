import type { INavigationData } from '@/frontend/_types/navigation.types'

interface IBaseTabProperties {
  title: string
  subtitle?: string
}

interface INavigateTabProperties extends IBaseTabProperties {
  navigate?: INavigationData
}

/*
interface IClickHandlerTabProperties extends IBaseTabProperties {
  onClick?: () => void
}
*/

export interface ITabData {
  key: string
  content: /*JSX.Element | IClickHandlerTabProperties | */ INavigateTabProperties
  // isCurrentTab: boolean
  isClickable?: boolean
}

/*
export const isNavigateTabContent = (
  content: ITabData['content']
): content is INavigateTabProperties =>
  content['navigate'] &&
  // content['navigate']['category'] &&
  content['navigate']['textPage']

export const isClickHandlerTabContent = (
  content: ITabData['content']
): content is IClickHandlerTabProperties =>
  typeof content['onClick'] === 'function'

export const isJsxTabContent = (
  content: ITabData['content']
): content is JSX.Element =>
  !isNavigateTabContent(content) && !isClickHandlerTabContent(content)
*/
