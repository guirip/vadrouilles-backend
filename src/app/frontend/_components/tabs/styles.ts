'use client'

import styled, { css } from 'styled-components'

import ScrollbarsHidden from '@/frontend/_components/tweaks/ScrollbarsHidden'
import { FONT_SIZE, SUBTITLE_COLOR } from 'src/app/_style/style.constants'
import type { Mode } from 'src/app/frontend/_style/mode.style'
import { getModeStyle } from 'src/app/frontend/_style/mode.style'
import { getSizeStyle, Size } from './size.styles'

export const ScrollableTabsContainer = styled(ScrollbarsHidden)`
  flex-shrink: 0;
  max-width: 100vw;
  margin-bottom: 2px;
  padding-top: 0.2rem;
  overflow-x: auto;
  overflow-y: hidden;
  -webkit-overflow-scrolling: touch;
`

export const StyledTabs = styled.div`
  display: flex;
  flex-grow: 1;
  width: max-content;
  margin: auto;
  padding: 0 0.3rem;
`

export const TabContentSimple = styled.div``

export const Title = styled.div<{ $hasSubtitle: boolean }>`
  white-space: ${({ $hasSubtitle }) => ($hasSubtitle ? 'nowrap' : 'normal')};
`
export const Subtitle = styled.div`
  font-size: ${FONT_SIZE.xxs};
  color: ${SUBTITLE_COLOR};
`

interface ITabProps {
  $selected: boolean
  $mode?: Mode
  $size?: Size
  $disabled: boolean
  $isHome: boolean
}

export const Tab = styled.span<ITabProps>`
  display: flex;
  align-items: center;
  padding: 0.1rem;
  margin: 0 ${({ $size }) => getSizeStyle($size).container.marginX * 2}px 10px;
  ${({ $isHome }) =>
    $isHome &&
    css`
      margin-right: 0.6rem;
    `}

  @media (max-width: 600px) {
    padding: 0.1rem 0.3rem;
    margin: 0 ${({ $size }) => getSizeStyle($size).container.marginX}px 2px;
  }

  max-width: 240px;
  font-style: normal;
  text-align: center;
  opacity: ${({ $selected }) => ($selected ? 1 : 0.8)};
  -webkit-tap-highlight-color: transparent;
  user-select: none;

  &:hover {
    filter: brightness(
      ${({ $size }) => ($size === Size.NORMAL ? '1.7' : '1.2')}
    );
  }

  & > *:only-child {
    width: 100%;
    border-radius: 3%;
  }

  & ${TabContentSimple} > * {
    display: flex;
    flex-direction: column;
    justify-content: center;
    align-items: center;
    margin: 0;
    padding: ${({ $size }) => getSizeStyle($size).a.padding};
    height: ${({ $size }) => getSizeStyle($size).h2.height};
    font-size: ${({ $size }) => getSizeStyle($size).a.fontSize};
    font-weight: 400;
    text-decoration: none;
    color: ${({ $selected, $mode }) => getModeStyle($selected, $mode).color};
    letter-spacing: ${({ $size }) => getSizeStyle($size).a.letterSpacing};
    border: ${({ $selected, $mode, $isHome }) =>
      getModeStyle($isHome || $selected, $mode).border};
    box-shadow: ${({ $selected, $mode }) =>
      getModeStyle($selected, $mode).boxShadow};
    cursor: pointer;

    ${({ $selected }) =>
      $selected &&
      css`
        transition:
          background-color 0.6s,
          box-shadow 0.4s;
      `}

    background-color: ${({ $selected, $mode }) =>
      getModeStyle($selected, $mode).background};

    ${({ $isHome }) =>
      $isHome &&
      css`
        padding: 1.2rem 0.8rem 1.4rem 1rem;
        font-size: ${FONT_SIZE.h5};
        border-radius: 42%;
        filter: contrast(1.2) brightness(1.1);
      `}
  }

  & small {
    font-size: 0.9rem;
  }
`
