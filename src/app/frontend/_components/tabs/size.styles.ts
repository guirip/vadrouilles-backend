import { FONT_SIZE } from 'src/app/_style/style.constants'

export enum Size {
  NORMAL = 'normal',
  SMALLER = 'smaller',
}
const DEFAULT_SIZE = Size.NORMAL

const SIZE_STYLE = {
  [Size.NORMAL]: {
    container: {
      marginX: 2, // px
    },
    h2: {
      height: '44px',
    },
    a: {
      padding: '0 1.5rem',
      fontSize: FONT_SIZE.md,
      letterSpacing: '1px',
    },
  },
  [Size.SMALLER]: {
    container: {
      marginX: 1, // px
    },
    h2: {
      height: '36px',
    },
    a: {
      padding: '0 1rem',
      fontSize: FONT_SIZE.xs,
      letterSpacing: '0.5px',
    },
  },
}

export const getSizeStyle = (size?: Size) => SIZE_STYLE[size || DEFAULT_SIZE]
