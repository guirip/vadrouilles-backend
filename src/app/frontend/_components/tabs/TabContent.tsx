import Link from 'next/link'
import { getRoute } from '../../_services/navigation-service'
import { Subtitle, TabContentSimple, Title } from './styles'
import { type ITabData } from './tab.type'
import type { Lang } from 'src/models/common/Lang'
import { useMemo } from 'react'

interface IProps {
  content: ITabData['content']
  previewMode: boolean
  lang: Lang
}

export function TabContent({ content, previewMode, lang }: IProps) {
  const children = (
    <>
      <Title $hasSubtitle={!!content.subtitle}>{content.title}</Title>
      {content.subtitle && <Subtitle>{content.subtitle}</Subtitle>}
    </>
  )
  const route = useMemo(
    () =>
      !content.navigate
        ? null
        : getRoute(
            previewMode,
            content.navigate.category,
            content.navigate.textPageSlug,
            content.navigate.chapterSlug ?? null,
            lang
          ),
    [content.navigate, lang, previewMode]
  )

  return (
    <TabContentSimple>
      {route ? <Link href={route}>{children}</Link> : <div>{children}</div>}
    </TabContentSimple>
  )
}
