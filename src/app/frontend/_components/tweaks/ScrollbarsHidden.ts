'use client'

import styled from 'styled-components'

const ScrollbarsHidden = styled.div`
  /* Hide horizontal scrollbar */
  /* chrome & safari */
  &::-webkit-scrollbar,
  &::-webkit-scrollbar-thumb,
  &::-webkit-scrollbar-track {
    height: 0 !important;
    background-color: transparent;
  }
  /* ffox */
  scrollbar-width: none;
  /* ie */
  -ms-overflow-style: none;
`

export default ScrollbarsHidden
