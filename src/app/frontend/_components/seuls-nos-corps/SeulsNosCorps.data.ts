import type { IGalleryItem } from '@/frontend/_components/gallery-simple'

import oeuvre01 from '../../../../../public/seuls-nos-corps-sont-confines/full/oeuvre-01.jpg'
import oeuvre02 from '../../../../../public/seuls-nos-corps-sont-confines/full/oeuvre-02.jpg'
import oeuvre03 from '../../../../../public/seuls-nos-corps-sont-confines/full/oeuvre-03.jpg'
import oeuvre04 from '../../../../../public/seuls-nos-corps-sont-confines/full/oeuvre-04.jpg'
import oeuvre05 from '../../../../../public/seuls-nos-corps-sont-confines/full/oeuvre-05.jpg'
import oeuvre06 from '../../../../../public/seuls-nos-corps-sont-confines/full/oeuvre-06.jpg'
import oeuvre07 from '../../../../../public/seuls-nos-corps-sont-confines/full/oeuvre-07.jpg'
import oeuvre08 from '../../../../../public/seuls-nos-corps-sont-confines/full/oeuvre-08.jpg'
import oeuvre09 from '../../../../../public/seuls-nos-corps-sont-confines/full/oeuvre-09.jpg'
import oeuvre10 from '../../../../../public/seuls-nos-corps-sont-confines/full/oeuvre-10.jpg'
import oeuvre11 from '../../../../../public/seuls-nos-corps-sont-confines/full/oeuvre-11.jpg'
import oeuvre12 from '../../../../../public/seuls-nos-corps-sont-confines/full/oeuvre-12.jpg'
import oeuvre13 from '../../../../../public/seuls-nos-corps-sont-confines/full/oeuvre-13.jpg'
import oeuvre14 from '../../../../../public/seuls-nos-corps-sont-confines/full/oeuvre-14.jpg'
import oeuvre15 from '../../../../../public/seuls-nos-corps-sont-confines/full/oeuvre-15.jpg'
import oeuvre16 from '../../../../../public/seuls-nos-corps-sont-confines/full/oeuvre-16.jpg'

/*
    description: getLabel(['seulsNosCorps', 'status', 'available']),
    descriptionClassName: 'item-available',
*/
export const getData = (getLabel: (path: string[]) => string) => {
  const data: IGalleryItem[] = [
    {
      title: getLabel(['seulsNosCorps', 'artwork']) + ' 1',
      description: getLabel(['seulsNosCorps', 'status', 'unavailable']),
      date: getLabel(['month', 'apr']) + ' 2020',
      image: oeuvre01,
    },
    {
      title: getLabel(['seulsNosCorps', 'artwork']) + ' 2',
      description: `${getLabel([
        'seulsNosCorps',
        'status',
        'sold',
      ])} ・ <b>Thank you Bryan</b> <span class="emoji">✌</span>`,
      date: getLabel(['month', 'apr']) + ' 2020',
      image: oeuvre02,
    },
    {
      title: getLabel(['seulsNosCorps', 'artwork']) + ' 3',
      description: getLabel(['seulsNosCorps', 'status', 'soldForAPHP']),
      date: getLabel(['month', 'apr']) + ' 2020',
      image: oeuvre03,
    },
    {
      title: getLabel(['seulsNosCorps', 'artwork']) + ' 4',
      description: getLabel(['seulsNosCorps', 'status', 'soldForAPHP']),
      date: getLabel(['month', 'apr']) + ' 2020',
      image: oeuvre04,
    },
    {
      title: getLabel(['seulsNosCorps', 'artwork']) + ' 5',
      description: getLabel(['seulsNosCorps', 'status', 'soldForAPHP']),
      date: getLabel(['month', 'apr']) + ' 2020',
      image: oeuvre05,
    },
    {
      title: getLabel(['seulsNosCorps', 'artwork']) + ' 6',
      description: getLabel(['for']) + ' <b>Etienne</b>',
      date: getLabel(['month', 'apr']) + ' 2020',
      image: oeuvre06,
    },
    {
      title: getLabel(['seulsNosCorps', 'artwork']) + ' 7',
      description: getLabel(['seulsNosCorps', 'status', 'soldForAPHP']),
      date: getLabel(['month', 'apr']) + ' 2020',
      image: oeuvre07,
    },
    {
      title: getLabel(['seulsNosCorps', 'artwork']) + ' 8',
      description: getLabel(['seulsNosCorps', 'status', 'soldForAPHP']),
      date: getLabel(['month', 'may']) + ' 2020',
      image: oeuvre08,
    },
    {
      title: getLabel(['seulsNosCorps', 'artwork']) + ' 9',
      description:
        getLabel(['seulsNosCorps', 'status', 'sold']) +
        ' ・ <b>Merci Céline</b> <span class="emoji">😉</span>',
      date: getLabel(['month', 'may']) + ' 2020',
      image: oeuvre09,
    },
    {
      title: getLabel(['seulsNosCorps', 'artwork']) + ' 10',
      description:
        getLabel(['seulsNosCorps', 'status', 'command']) +
        ' ・ <b>Merci Aurélie</b> <span class="emoji">🙏</span>',
      date: getLabel(['month', 'may']) + ' 2020',
      image: oeuvre10,
    },
    {
      title: getLabel(['seulsNosCorps', 'artwork']) + ' 11',
      description: getLabel(['seulsNosCorps', 'status', 'available']),
      descriptionClassName: 'item-available',
      date: getLabel(['month', 'may']) + ' 2020',
      image: oeuvre11,
    },
    {
      title: getLabel(['seulsNosCorps', 'artwork']) + ' 12',
      description:
        getLabel(['seulsNosCorps', 'status', 'command']) +
        ' ・ <b>Merci Laurent</b> <span class="emoji">🙏</span> ' +
        getLabel(['seulsNosCorps', 'oldParisMapA4']),
      date: getLabel(['month', 'may']) + ' 2020',
      image: oeuvre12,
    },
    {
      title: getLabel(['seulsNosCorps', 'artwork']) + ' 13',
      description:
        getLabel(['seulsNosCorps', 'status', 'sold']) +
        ' ・ <b>Merci Florence</b> <span class="emoji">😊</span>',
      date: getLabel(['month', 'may']) + ' 2020',
      image: oeuvre13,
    },
    {
      title: getLabel(['seulsNosCorps', 'artwork']) + ' 14',
      description:
        getLabel(['seulsNosCorps', 'status', 'command']) +
        ' ・ <b>Merci Florence</b> <span class="emoji">🙏</span>' +
        getLabel(['seulsNosCorps', 'oldParisTarideMap1973']),
      date: `${getLabel(['month', 'may'])}/${getLabel(['month', 'jun'])} 2020`,
      image: oeuvre14,
    },
    {
      title: getLabel(['seulsNosCorps', 'artwork']) + ' 15',
      description:
        getLabel(['seulsNosCorps', 'status', 'command']) +
        ' ・ <b>Merci Céline</b> <span class="emoji">😉</span>' +
        getLabel(['seulsNosCorps', 'oldMichelinMap1958']),
      date: `${getLabel(['month', 'may'])}/${getLabel(['month', 'jun'])} 2020`,
      image: oeuvre15,
    },
    {
      title: getLabel(['seulsNosCorps', 'artwork']) + ' 16',
      description:
        getLabel(['seulsNosCorps', 'status', 'command']) +
        ' ・ <b>Merci Carine</b> <span class="emoji">✌</span>',
      date: `${getLabel(['month', 'aug'])} 2020`,
      image: oeuvre16,
    },
  ]
  return data
}
