'use client'

import { faInstagram } from '@fortawesome/free-brands-svg-icons'
import { faExternalLinkAlt } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { Helmet } from 'react-helmet-async'

import { CoreContent } from '@/frontend/_components/core-content'
import Gallery from '@/frontend/_components/gallery-simple'
import YouTubeVideo from '@/frontend/_components/video/YouTubeVideo'
import { useLangContext } from '@/frontend/_providers/LangContextProvider'
import { TextBlock } from '@/frontend/_style/style.main'
import { LINK_COLOR } from 'src/app/_style/style.constants'
import { getData } from './SeulsNosCorps.data'
import {
  Infos,
  IntroWrap,
  Order,
  OrderLine2,
  SubContainer,
  TechnicalDetails,
} from './SeulsNosCorps.style'

import type { ITextPageMinimalPopulatedFront } from 'src/models/text/TextPage.type'
import './style.css'

interface IProps {
  currentTextPage: ITextPageMinimalPopulatedFront
  previewMode: boolean
}

function SeulsNosCorps({ currentTextPage, previewMode }: IProps) {
  const { lang, getLabel, getAppTitle } = useLangContext()

  const orderLine2 = getLabel(['seulsNosCorps', 'orderPossibleLine2'])

  return (
    <SubContainer>
      <Helmet>
        <title>
          {`${currentTextPage.title[lang]}`} - $
          {getAppTitle(previewMode, currentTextPage.category)}
        </title>
        <meta
          name="description"
          content={getLabel(['seulsNosCorps', 'intro1'])}
        />
      </Helmet>

      <CoreContent>
        <IntroWrap>
          <TextBlock>
            <p
              dangerouslySetInnerHTML={{
                __html: getLabel(['seulsNosCorps', 'intro1']),
              }}
            />
            <p>{getLabel(['seulsNosCorps', 'intro2'])}</p>
            <p>{getLabel(['seulsNosCorps', 'intro3'])}</p>
          </TextBlock>
        </IntroWrap>
        <Order>
          <div
            dangerouslySetInnerHTML={{
              __html: getLabel(['seulsNosCorps', 'orderPossibleLine1']),
            }}
          />
          {orderLine2 && (
            <OrderLine2
              dangerouslySetInnerHTML={{
                __html: orderLine2,
              }}
            />
          )}
        </Order>
        <h3>{getLabel(['seulsNosCorps', 'galleryTitle'])}</h3>
        <Gallery data={getData(getLabel)} priorityAmount={1} />
        <Infos>
          <TechnicalDetails>
            <div>
              <b>{getLabel(['seulsNosCorps', 'supportLabel'])}</b>{' '}
              {getLabel(['seulsNosCorps', 'support'])}
            </div>
            <div>
              <b>{getLabel(['seulsNosCorps', 'sizeLabel'])}</b>{' '}
              {getLabel(['seulsNosCorps', 'size'])}
            </div>
            <div>
              <b>{getLabel(['seulsNosCorps', 'techniqueLabel'])}</b>{' '}
              {getLabel(['seulsNosCorps', 'technique'])}
            </div>
            <div>
              <b>{getLabel(['seulsNosCorps', 'saatoProject'])}</b>
              <ul>
                <li>
                  <FontAwesomeIcon
                    className="mr-2"
                    opacity={0.9}
                    color={LINK_COLOR}
                    icon={faInstagram}
                  />
                  <a
                    aria-label={getLabel([
                      'seulsNosCorps',
                      'notoriousInstagramLinkAria',
                    ])}
                    href="https://www.instagram.com/notoriousbrandfr"
                  >
                    notoriousbrandfr
                  </a>
                </li>
                <li>
                  <FontAwesomeIcon
                    className="mr-2"
                    opacity={0.9}
                    size="xs"
                    color={LINK_COLOR}
                    icon={faExternalLinkAlt}
                  />
                  <a
                    aria-label={getLabel([
                      'seulsNosCorps',
                      'saatoProjectLinkAria',
                    ])}
                    href="https://www.projetsaato.com/"
                  >
                    projetsaato.com
                  </a>
                </li>
              </ul>
            </div>
          </TechnicalDetails>
        </Infos>
        <h3>{getLabel(['seulsNosCorps', 'processTitle'])}</h3>
        <TextBlock>{getLabel(['seulsNosCorps', 'processText'])}</TextBlock>
        <YouTubeVideo id="-Pb8JjXK15Y" /> {/* oeuvre 3 */}
        <YouTubeVideo id="nqLDE6CHMy4" /> {/* oeuvre 9 */}
        <YouTubeVideo id="qOIYNd2BhHE" /> {/* oeuvre 10 */}
        <YouTubeVideo id="LnijsoKnfro" /> {/* oeuvre 12 */}
        <YouTubeVideo id="ZJ1RyvCxdgM" /> {/* oeuvre 13 */}
        <YouTubeVideo id="gK5frVZ1Xzw" /> {/* oeuvre 14 */}
        <YouTubeVideo id="ryXDhpMe9Kw" /> {/* oeuvre 15 */}
        <YouTubeVideo id="t9qhla2KOw0" /> {/* oeuvre 16 */}
      </CoreContent>
    </SubContainer>
  )
}

export default SeulsNosCorps
