'use client'

import styled from 'styled-components'
import { TextBlock, Text } from '@/frontend/_style/style.main'
import { FONT_SIZE, LINK_COLOR } from 'src/app/_style/style.constants'

export const SubContainer = styled.div`
  max-width: 1600px;
  width: 100%;
  flex-direction: column;
  margin: 0.8em auto 0;

  & h2 {
    margin: 1.5em 0.5em 0.2em;
    font-size: ${FONT_SIZE.h2};
    text-align: center;
  }
  & h3 {
    margin: 1em 1em 0.8em;
    font-size: ${FONT_SIZE.h3};
    padding-left: 0.5em;
    text-align: center;
  }
`

export const IntroWrap = styled.div`
  display: flex;
  flex-wrap: wrap;
  line-height: 1.3rem;

  & p {
    text-align: left;
  }
`

export const Order = styled(TextBlock)`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  font-size: ${FONT_SIZE.md};
  line-height: 1.3rem;
`
export const OrderLine2 = styled.div`
  text-align: start;
`

export const Infos = styled(Text)`
  display: flex;
  flex-direction: column;
  align-items: center;
  margin: 0.7em 0 0.5em;
`

export const TechnicalDetails = styled(TextBlock)`
  display: flex;
  flex-direction: column;
  justify-content: center;
  padding: 1em 1.2em;
  max-width: 720px;
  font-size: ${FONT_SIZE.md};
  text-align: left;

  & > div {
    margin: 0.3em 0.3em;
  }

  & b {
    font-size: ${FONT_SIZE.sm};
    font-weight: bold;
  }

  & a {
    color: ${LINK_COLOR};
  }

  & ul {
    margin: 0.2em;
    padding-left: 1em;
    list-style: none;
  }
  & li {
    margin: 0.2em 0;
  }
`
