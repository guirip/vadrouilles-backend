'use client'

import { faInstagram, faYoutube } from '@fortawesome/free-brands-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { useMemo } from 'react'

import config from '@/frontend/_config'
import type { ContentCategory } from 'src/models/common/ContentCategory'
import { Anchor, SocialName } from './common.style'
import { Social } from './social.type'
import { useLangContext } from '../../_providers/LangContextProvider'

const configKey = {
  [Social.INSTAGRAM]: 'MY_INSTAGRAM',
  [Social.YOUTUBE]: 'MY_YOUTUBE',
}

const icon = {
  [Social.INSTAGRAM]: faInstagram,
  [Social.YOUTUBE]: faYoutube,
}

const labelKey = {
  [Social.INSTAGRAM]: 'goToInstagram',
  [Social.YOUTUBE]: 'goToYoutube',
}

interface IProps {
  type: Social
  category: ContentCategory
  hideLabelOnSmallScreen?: boolean
}

export function SocialLink({
  type,
  category,
  hideLabelOnSmallScreen = false,
}: IProps) {
  const { getLabel } = useLangContext()

  const url = useMemo(
    () => config[configKey[type]][category].url,
    [category, type]
  )

  const name = useMemo(
    () => config[configKey[type]][category].name,
    [category, type]
  )

  return url ? (
    <Anchor
      className="mx-2 md:mx-3"
      href={url}
      aria-label={getLabel(labelKey[type])}
      title={getLabel(labelKey[type])}
    >
      <FontAwesomeIcon icon={icon[type]} size="lg" />
      <SocialName
        className={`text-lg md:text-xl ${hideLabelOnSmallScreen ? 'hidden md:block' : ''}`}
      >
        {name}
      </SocialName>
    </Anchor>
  ) : null
}
