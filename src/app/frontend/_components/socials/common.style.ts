'use client'

import styled from 'styled-components'

import { TEXT_COLOR } from 'src/app/_style/style.constants'
import { StyledHeader } from '../header/styles'

export const Anchor = styled.a`
  display: flex;
  padding: 0.5em 0;
  align-items: center;
  color: ${TEXT_COLOR};
  text-decoration: none;
  transition: opacity 0.4s;

  ${StyledHeader} & {
    opacity: 0.6;
    &:hover {
      opacity: 1;
    }
  }
`

export const SocialName = styled.span`
  margin-left: 8px;
  font-weight: 300;
`
