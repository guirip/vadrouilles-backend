'use client'

import { useLangContext } from '../../_providers/LangContextProvider'
import ErrorMessage from '../ErrorMessage'

export default function () {
  const { getLabel } = useLangContext()
  return <ErrorMessage text={getLabel('pageNotFound')} />
}
