'use client'

import { createRef, useCallback, useEffect, useMemo } from 'react'

import { Footer } from '@/frontend/_components/footer'
import SeulsNosCorps from '@/frontend/_components/seuls-nos-corps/SeulsNosCorps'
import Text from '@/frontend/_components/text'
import { ScrollContext } from '@/frontend/_providers/ScrollContext'
import { PageContent, ScrollableContent } from '@/frontend/_style/style.main'
import { HelmetProvider } from 'react-helmet-async'
import type { ICatalogData } from 'src/models/Catalog.type'
import type { ITextChapterPopulatedFront } from 'src/models/text/TextChapter.type'
import type {
  ITextPageFront,
  ITextPageMinimalPopulatedFront,
} from 'src/models/text/TextPage.type'
import { CatalogProvider } from '../../_providers/CatalogProvider'
import Catalog from '../catalog'
import PageNotFound from './PageNotFound'
import { SpecialPage, isSpecialPage } from './constants'

function getSpecialPage(
  page: ITextPageMinimalPopulatedFront,
  chapter: ITextChapterPopulatedFront,
  previewMode: boolean
) {
  switch (page.slug) {
    case SpecialPage.SEULS_NOS_CORPS:
      return <SeulsNosCorps currentTextPage={page} previewMode={previewMode} />

    case SpecialPage.CATALOG:
      return (
        <Catalog
          currentTextPage={page}
          chapter={chapter}
          previewMode={previewMode}
        />
      )

    default:
      return <PageNotFound />
  }
}

interface IProps {
  textPages: ITextPageFront[]
  currentTextPage: ITextPageMinimalPopulatedFront
  chapter: ITextChapterPopulatedFront
  catalogData: ICatalogData | null
  previewMode: boolean
}

export function GenericPageContent({
  textPages,
  currentTextPage,
  chapter,
  previewMode,
  catalogData,
}: IProps) {
  const contentRef = createRef<HTMLDivElement>()
  const scrollable = createRef<HTMLDivElement>()

  const resetScroll = useCallback(
    function () {
      if (scrollable.current) {
        // Reset scroll on tab change
        scrollable.current.scrollTop = 0
      }
    },
    [scrollable]
  )

  useEffect(() => {
    resetScroll()
  }, [scrollable, resetScroll])

  useEffect(() => {
    if (contentRef.current) {
      contentRef.current.scrollTop = 0
    }
  })

  const pageContent = useMemo(() => {
    return isSpecialPage(currentTextPage.slug) ? (
      getSpecialPage(currentTextPage, chapter, previewMode)
    ) : (
      <Text
        resetScroll={resetScroll}
        previewMode={previewMode}
        textPages={textPages}
        currentTextPage={currentTextPage}
        chapter={chapter}
      />
    )
  }, [chapter, currentTextPage, previewMode, resetScroll, textPages])

  return (
    <HelmetProvider>
      <CatalogProvider data={catalogData}>
        <ScrollableContent ref={scrollable}>
          <ScrollContext.Provider value={{ ref: scrollable }}>
            <PageContent ref={contentRef}>{pageContent}</PageContent>
            <Footer category={currentTextPage.category} />
          </ScrollContext.Provider>
        </ScrollableContent>
      </CatalogProvider>
    </HelmetProvider>
  )
}
