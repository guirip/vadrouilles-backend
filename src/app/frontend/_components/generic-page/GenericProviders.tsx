'use client'

import type { PropsWithChildren } from 'react'

import type { Lang } from 'src/models/common/Lang'
import { CodifContextProvider } from '@/frontend/_providers/CodifContextProvider'
import { LangContextProvider } from '@/frontend/_providers/LangContextProvider'
import { DeviceContextProvider } from 'src/app/_components/DeviceContextProvider'

interface IProps {
  lang: Lang
  codifs: {
    staticUrl: string | null
    catalogNotice: string | null
    catalogDiscount: number | null
  }
}

export function GenericProviders(props: PropsWithChildren<IProps>) {
  return (
    <DeviceContextProvider>
      <LangContextProvider lang={props.lang}>
        <CodifContextProvider codifs={props.codifs}>
          {props.children}
        </CodifContextProvider>
      </LangContextProvider>
    </DeviceContextProvider>
  )
}
