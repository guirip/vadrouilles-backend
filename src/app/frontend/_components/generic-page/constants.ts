// Special TextPage slugs
export enum SpecialPage {
  SEULS_NOS_CORPS = 'seuls-nos-corps',
  CATALOG = 'catalog',
}

const specialPageValues = Object.values(SpecialPage).map((v) => String(v))

export const isSpecialPage = (pageSlug: string): pageSlug is SpecialPage =>
  specialPageValues.includes(pageSlug)
