'use client'

import Link from 'next/link'
import styled from 'styled-components'

import { FontFamily } from 'src/app/_assets/fonts'
import { FONT_SIZE, TEXT_COLOR } from 'src/app/_style/style.constants'
import type { ContentCategory } from 'src/models/common/ContentCategory'
import { ACCENT, Mode, getModeStyle } from 'src/app/frontend/_style/mode.style'

export const StyledHeader = styled.div`
  flex-shrink: 0;
  display: flex;
  align-items: center;
  margin: 0 0 3px;
  padding: 6px;
  @media (max-width: 600px) {
    margin: 0 0 1px;
    padding: 2px;
  }
  width: 100%;
  background-color: rgba(0, 0, 0, 0.5);
`

interface ISideButtonContainerProps {
  $left: boolean
}
export const SideButtonContainer = styled.div<ISideButtonContainerProps>`
  display: flex;
  align-items: center;
  padding: ${({ $left }) => ($left ? '0' : '0.25em')} 0.4em 0;
  flex: 1 1 160px;
  justify-content: ${({ $left }) => ($left ? 'flex-start' : 'flex-end')};
  flex-wrap: ${({ $left }) => ($left ? 'nowrap' : 'wrap')};
`

interface IHeaderTitleProps {
  $category: ContentCategory
}

export const HeaderTitle = styled.h2<IHeaderTitleProps>`
  display: flex;
  flex-grow: 2;
  justify-content: center;
  margin: 2px 0.5em 0;
  line-height: 22px;
  font-family: ${FontFamily.LOBSTER};
  font-style: normal;
  font-weight: normal;
  text-align: center;
  letter-spacing: 4px;
  color: ${ACCENT};
`

export const StyledLink = styled(Link)`
  display: flex;
  justify-content: center;
  align-items: center;
  font-size: ${FONT_SIZE.h3};
  background-color: ${getModeStyle(false, Mode.DARK).background};
  border-radius: 5px;
  border: 1px solid black;
  cursor: pointer;
  text-decoration: none;
  color: ${TEXT_COLOR};
  transition: opacity 0.4s;
  opacity: 0.7;

  &:hover {
    opacity: 1;
  }

  & span {
    margin: 0.1rem 0.2rem 0 0.4rem;
    white-space: nowrap;
  }
`
