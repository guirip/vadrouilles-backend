'use client'

import { useMemo } from 'react'
import styled from 'styled-components'

import LangSwitch from '@/frontend/_components/lang-switch'
import { useLangContext } from '@/frontend/_providers/LangContextProvider'
import {
  faPaintBrush,
  faSmileBeam,
  faTree,
} from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { ContentCategory } from 'src/models/common/ContentCategory'
import { getFrontCompletePath } from 'src/utils/util'
import { SocialLink } from '../socials/SocialLink'
import { Social } from '../socials/social.type'
import {
  HeaderTitle,
  SideButtonContainer,
  StyledHeader,
  StyledLink,
} from './styles'
import Link from 'next/link'

const CategoryName = styled.span`
  font-weight: 300;
`

const SHOW_CATEGORY_NAME = true

interface IProps {
  isPreview: boolean
  category: ContentCategory
}

export function Header({ isPreview, category }: IProps) {
  const { getLabel, getAppTitle, lang } = useLangContext()

  const label = useMemo(
    () => getAppTitle(isPreview, category),
    [getAppTitle, isPreview, category]
  )

  return (
    <StyledHeader>
      <SideButtonContainer $left>
        <LangSwitch />
        <SocialLink
          type={Social.INSTAGRAM}
          category={category}
          hideLabelOnSmallScreen
        />
        <SocialLink
          type={Social.YOUTUBE}
          category={category}
          hideLabelOnSmallScreen
        />
      </SideButtonContainer>

      <Link
        className="no-underline"
        href={getFrontCompletePath(`/${category}`)}
      >
        <HeaderTitle
          className="text-3xl sm:text-4xl md:text-5xl m-0 md:mt-1"
          $category={category}
          dangerouslySetInnerHTML={{
            __html: label ?? '',
          }}
        />
      </Link>

      <SideButtonContainer $left={false}>
        {category !== ContentCategory.GRAFFITI && (
          <StyledLink
            className="mb-1 md:ml-3 p-1 md:py-2 md:px-2"
            href={getFrontCompletePath(`/${ContentCategory.GRAFFITI}`, lang)}
            title={getLabel('goToGraffitiCategory')}
            aria-label={getLabel('goToGraffitiCategory')}
          >
            {SHOW_CATEGORY_NAME && (
              <CategoryName className="text-lg md:text-xl">
                {getLabel('graffiti_short')}
              </CategoryName>
            )}
            <FontAwesomeIcon
              size="2xs"
              className="ml-1 sm:ml-2"
              icon={faPaintBrush}
            />
          </StyledLink>
        )}

        {category !== ContentCategory.NATURE && (
          <StyledLink
            className="mb-1 ml-1 md:ml-2 p-1 md:py-2 md:px-2"
            href={getFrontCompletePath(`/${ContentCategory.NATURE}`, lang)}
            title={getLabel('goToNatureCategory')}
            aria-label={getLabel('goToNatureCategory')}
          >
            {SHOW_CATEGORY_NAME && (
              <CategoryName className="text-lg md:text-xl">
                {getLabel('nature_short')}
              </CategoryName>
            )}
            <FontAwesomeIcon
              size="2xs"
              className="ml-1 sm:ml-2"
              icon={faTree}
            />
          </StyledLink>
        )}

        {category !== ContentCategory.ABOUT && (
          <StyledLink
            className="mb-1 md:ml-3 p-1 md:py-2 md:px-2"
            href={getFrontCompletePath(`/${ContentCategory.ABOUT}`, lang)}
            title={getLabel('goToAboutCategory')}
            aria-label={getLabel('goToAboutCategory')}
          >
            {SHOW_CATEGORY_NAME && (
              <CategoryName className="text-lg md:text-xl">
                {getLabel('about_short')}
              </CategoryName>
            )}
            <FontAwesomeIcon
              size="2xs"
              className="ml-1 sm:ml-2"
              icon={faSmileBeam}
            />
          </StyledLink>
        )}
      </SideButtonContainer>
    </StyledHeader>
  )
}
