import type { CSSProperties } from 'react'

export const styles: CSSProperties[] = [
  {
    background: 'rgba(255,255,255,0.8)',
    border: 0,
    borderRadius: '3px',
    boxShadow: '0 0 1px 0 rgba(0,0,0,0.5),0 1px 10px 0 rgba(0,0,0,0.15)',
    margin: '1px',
    maxWidth: '540px',
    minWidth: '326px',
    padding: 0,
    width: 'calc(100% - 2px)',
  },
  {
    padding: '16px',
    background:
      'radial-gradient(circle at 50% 50%, #ffffff 0%, transparent 64%)',
  },
  {
    lineHeight: 0,
    padding: '0 0',
    textAlign: 'center',
    textDecoration: 'none',
    width: '100%',
  },
  { display: 'flex', flexDirection: 'row', alignItems: 'center' },
  {
    backgroundColor: '#F4F4F4',
    borderRadius: '50%',
    flexGrow: 0,
    height: '40px',
    marginRight: '14px',
    width: '40px',
  },
  {
    display: 'flex',
    flexDirection: 'column',
    flexGrow: 1,
    justifyContent: 'center',
  },
  {
    backgroundColor: '#F4F4F4',
    borderRadius: '4px',
    flexGrow: 0,
    height: '14px',
    marginBottom: '6px',
    width: '100px',
  },
  {
    backgroundColor: '#F4F4F4',
    borderRadius: '4px',
    flexGrow: 0,
    height: '14px',
    width: '60px',
  },
  { padding: '19% 0' },
  { display: 'block', height: '50px', margin: '0 auto 12px', width: '50px' },
  { paddingTop: '8px' },
  {
    color: '#3897f0',
    fontFamily: 'Arial,sansSerif',
    fontSize: '14px',
    fontStyle: 'normal',
    fontWeight: 550,
    lineHeight: '18px',
  },
  { padding: '22% 0' },
]
