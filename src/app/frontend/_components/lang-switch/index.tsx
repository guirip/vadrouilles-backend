'use client'

import { Lang } from 'src/models/common/Lang'
import { useLangContext } from '@/frontend/_providers/LangContextProvider'
import Link from 'next/link'
import type { ReadonlyURLSearchParams } from 'next/navigation'
import { useSearchParams } from 'next/navigation'
import { useMemo } from 'react'
import flagEn from 'src/app/_assets/flag-en.webp'
import flagFr from 'src/app/_assets/flag-fr.webp'
import { getFrontCompletePath } from 'src/utils/util'
import styled from 'styled-components'

const flags = {
  [Lang.fr]: flagFr,
  [Lang.en]: flagEn,
}

const Flag = styled.div<{ $lang: Lang }>`
  flex-shrink: 0;
  width: 22px;
  height: 22px;
  margin-right: 2vw;
  opacity: 0.7;
  background-size: contain;
  cursor: pointer;
  background-image: ${({ $lang }) => `url(${flags[$lang].src})`};
  -webkit-tap-highlight-color: transparent;
  transition: opacity 0.3s;

  &:hover {
    opacity: 0.9;
  }
`

const getUrl = (sp: ReadonlyURLSearchParams | null, targetLang: Lang) => {
  const rQp = sp && sp.has('r') ? sp.get('r') : ''
  return getFrontCompletePath(rQp ? rQp : '', targetLang)
}

function LangSwitch() {
  const { lang, updateCurrentLang } = useLangContext()
  const searchParams = useSearchParams()

  const [nextLang, label, url] = useMemo(() => {
    if (lang === Lang.fr) {
      return [
        Lang.en,
        'Switch to english language',
        getUrl(searchParams, Lang.en),
      ]
    }
    return [
      Lang.fr,
      'Passer en langue française',
      getUrl(searchParams, Lang.fr),
    ]
  }, [lang, searchParams])

  return (
    <Link
      href={url}
      title={label}
      aria-label={label}
      onClick={() => updateCurrentLang(nextLang)}
    >
      <Flag $lang={nextLang} />
    </Link>
  )
}

export default LangSwitch
