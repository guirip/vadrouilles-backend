'use client'

import { useState, useEffect } from 'react'
import styled from 'styled-components'
import { Anchor } from '../socials/common.style'
import type { ContentCategory } from 'src/models/common/ContentCategory'
import { SocialLink } from '../socials/SocialLink'
import { Social } from '../socials/social.type'

interface IStyledFooterProps {
  $visible: boolean
}

export const StyledFooter = styled.div<IStyledFooterProps>`
  display: flex;
  justify-content: center;
  align-items: center;
  margin: 1.7em 0 0;
  padding: 0.2rem;
  background-color: rgba(0, 0, 0, 0.5);
  width: 100%;
  opacity: ${({ $visible }) => ($visible ? 1 : 0)};
  transition: opacity 0.5s;

  & ${Anchor} {
    margin: 0.3rem 2rem;
    opacity: 0.75;

    &:hover {
      opacity: 1;
    }
  }

  &:empty {
    visibility: hidden;
  }
`

interface IProps {
  category: ContentCategory
}

export function Footer({ category }: IProps) {
  const [isVisible, setVisibility] = useState(false)

  useEffect(() => {
    const timer = setTimeout(setVisibility, 1500, true)

    return function () {
      clearTimeout(timer)
    }
  }, [isVisible])

  return (
    <StyledFooter $visible={isVisible}>
      <SocialLink type={Social.INSTAGRAM} category={category} />
      <SocialLink type={Social.YOUTUBE} category={category} />
    </StyledFooter>
  )
}
