'use client'

import type { PropsWithChildren } from 'react'

// see https://primeflex.org/animationduration
export type FadeDuration =
  | 100
  | 150
  | 200
  | 300
  | 400
  | 500
  | 1000
  | 2000
  | 3000

interface IProps {
  duration?: FadeDuration
}

const DEFAULT_DURATION: FadeDuration = 1000

export const Fade = ({ children, duration }: PropsWithChildren<IProps>) => (
  <div
    className={`w-full h-full fadein animation-duration-${
      duration ?? DEFAULT_DURATION
    }`}
  >
    {children}
  </div>
)
