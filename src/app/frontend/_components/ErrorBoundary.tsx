'use client'

import type { PropsWithChildren } from 'react'
import { Component } from 'react'
import styled from 'styled-components'

import { LangContext } from '@/frontend/_providers/LangContextProvider'
import { FONT_SIZE } from 'src/app/_style/style.constants'

const Message = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  min-height: 4em;
  color: white;
  font-size: ${FONT_SIZE.h2};
`

class ErrorBoundary extends Component {
  state = {
    hasError: false,
  }

  static getDerivedStateFromError() {
    return {
      hasError: true,
    }
  }

  componentDidCatch(error: unknown, errorInfo: unknown) {
    console.error('did catch error: ', error, errorInfo)
  }

  render() {
    if (this.state.hasError) {
      return (
        <LangContext.Consumer>
          {(context) => (
            <Message>{context ? context.getLabel('error') : 'Error'}</Message>
          )}
        </LangContext.Consumer>
      )
    }

    return (this.props as PropsWithChildren).children
  }
}

export default ErrorBoundary
