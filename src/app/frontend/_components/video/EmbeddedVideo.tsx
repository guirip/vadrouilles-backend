'use client'

import { useEffect, useState, type ReactEventHandler } from 'react'
import styled from 'styled-components'

import {
  TextWrapper,
  TextWrapperVariant,
} from '../text/text-components/TextWrapper'
import { STANDARD_MARGIN, useSize } from './sizeHook'

export const VideoContainer = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  flex-direction: column;
  margin: 2em 0;

  & iframe {
    border: 0;
  }
`

const textVariants = [TextWrapperVariant.ITALIC]

interface IProps {
  description: string | undefined
  inputWidth: number | undefined
  inputHeight: number | undefined
  url: string
  allow: string
  title: string
  onLoad?: ReactEventHandler
}

function EmbeddedVideo({
  description,
  inputWidth,
  inputHeight,
  url,
  allow,
  title,
  onLoad,
}: IProps) {
  const { width, height } = useSize(inputWidth, inputHeight)
  const [visible, setVisible] = useState(false)

  useEffect(() => {
    setVisible(true)
  }, [])

  return (
    visible && (
      <VideoContainer>
        <iframe
          width={
            width ?? document.documentElement.clientWidth - STANDARD_MARGIN
          }
          height={
            height ?? document.documentElement.clientHeight - STANDARD_MARGIN
          }
          src={url}
          allow={allow}
          allowFullScreen
          title={title}
          onLoad={onLoad}
        />
        {description && (
          <TextWrapper text={description} variants={textVariants} />
        )}
      </VideoContainer>
    )
  )
}

export default EmbeddedVideo
