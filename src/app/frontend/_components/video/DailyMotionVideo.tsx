import type { ReactEventHandler } from 'react'
import { lazy } from 'react'

const EmbeddedVideo = lazy(() => import('./EmbeddedVideo'))

interface IProps {
  id: string
  description?: string
  width?: number | null
  height?: number | null
  onLoad?: ReactEventHandler
  autoplay?: boolean
}

function DailyMotionVideo({ id, description, width, height, onLoad }: IProps) {
  return (
    <EmbeddedVideo
      url={`https://geo.dailymotion.com/player.html?video=${id}`}
      allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
      description={description ?? undefined}
      inputWidth={width ?? undefined}
      inputHeight={height ?? undefined}
      title={`embedded youtube video ${id}`}
      onLoad={onLoad}
    />
  )
}

export default DailyMotionVideo
