'use client'

import { useState } from 'react'
import type { IVideo } from 'src/models/common/Video'
import { VideoProxy } from './VideoProxy'

interface IProps {
  videos: IVideo[] | null
}

export function VideoList({ videos }: IProps) {
  const [visibleCount, setVisibleCount] = useState(1)

  function loadNext() {
    setTimeout(setVisibleCount, 0, visibleCount + 1)
  }
  return (
    <>
      {(videos || []).map((video, index) =>
        index < visibleCount ? (
          <VideoProxy key={video.value} data={video} onLoad={loadNext} />
        ) : null
      )}
    </>
  )
}
