'use client'

import { debounce } from 'lodash'
import { useCallback, useEffect, useMemo, useState } from 'react'
import { simpleSortAsc } from 'src/utils/sortUtil'

/** margin needed for youtube embed to detect proper resolution... */
export const STANDARD_MARGIN = 20

const thresholds = {
  1920: 1080,
  1280: 720,
  854: 480,
  640: 360,
  426: 240,
}

const widthsDsc = new Float64Array(
  Object.keys(thresholds).map((w) => parseInt(w, 10))
).sort((a, b) => b - a)

const heightsDsc = Object.values(thresholds).sort((a, b) => b - a)

const getWidthFromHeight = (h: number) => {
  const match = Object.keys(thresholds).find((w) => thresholds[w] === h)
  if (typeof match === 'string') {
    return parseInt(match, 10)
  }
  return null
}

function getBestWidth() {
  const clientWidth = document.documentElement.clientWidth
  for (const width of widthsDsc) {
    if (clientWidth > width) {
      return width
    }
  }
  return null
}

function getBestHeight() {
  const clientHeight = document.documentElement.clientHeight
  for (const height of heightsDsc) {
    if (clientHeight > height) {
      return height
    }
  }
  return null
}

function compareBests(bestWidth: number, bestHeight: number) {
  const widths = [bestWidth, getWidthFromHeight(bestHeight)].filter(
    (w) => w
  ) as number[]
  return widths.sort(simpleSortAsc)[0]
}

function computeBasedOnScreen() {
  const availableWidth = document.documentElement.clientWidth - 30
  const availableHeight = document.documentElement.clientHeight - 30

  const scale169 = 16 / 9
  const scaleScreen = availableWidth / availableHeight
  if (scaleScreen < scale169) {
    // scale height
    return {
      width: availableWidth,
      height: Math.floor((availableWidth * 9) / 16),
    }
  } else {
    // scale width
    return {
      width: Math.floor((availableHeight * 16) / 9),
      height: availableHeight,
    }
  }
}

// Exported only for unit-testing purpose
export const computeSize = () => {
  const bestWidth = getBestWidth()
  const bestHeight = getBestHeight()
  if (bestWidth === null || bestHeight === null) {
    // below standards, e.g mobile phone
    return computeBasedOnScreen()
  }
  // Desktop
  const targetWidth = compareBests(bestWidth, bestHeight)
  return {
    width: targetWidth + STANDARD_MARGIN,
    height: thresholds[targetWidth] + STANDARD_MARGIN,
  }
}

export const useSize = (
  inputWidth: number | undefined,
  inputHeight: number | undefined
) => {
  const [width, setWidth] = useState(inputWidth)
  const [height, setHeight] = useState(inputHeight)

  const updateSize = useCallback(() => {
    const { width, height } = computeSize()
    // console.log(`width: ${width} / height: ${height}`)
    setWidth(width)
    setHeight(height)
  }, [])

  const debouncedUpdateSize = useMemo(
    () => debounce(updateSize, 500),
    [updateSize]
  )

  useEffect(() => {
    addEventListener('resize', debouncedUpdateSize)
    updateSize()

    return function cleanup() {
      removeEventListener('resize', debouncedUpdateSize)
    }
  }, [debouncedUpdateSize, inputHeight, inputWidth, updateSize])

  return { width, height }
}
