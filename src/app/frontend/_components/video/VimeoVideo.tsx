import type { ReactEventHandler } from 'react'
import { lazy } from 'react'

const EmbeddedVideo = lazy(() => import('./EmbeddedVideo'))

interface IProps {
  id: string
  description?: string
  width: number | null
  height: number | null
  onLoad?: ReactEventHandler
  autoplay?: boolean
}

function VimeoVideo({
  id,
  description,
  width,
  height,
  onLoad,
  autoplay = false,
}: IProps) {
  return (
    <EmbeddedVideo
      url={`https://player.vimeo.com/video/${id}?autoplay=${autoplay}`}
      allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
      description={description ?? undefined}
      inputWidth={width ?? undefined}
      inputHeight={height ?? undefined}
      title={`embedded vimeo video ${id}`}
      onLoad={onLoad}
    />
  )
}

export default VimeoVideo
