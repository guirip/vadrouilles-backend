'use client'

import { useRef } from 'react'
import styled from 'styled-components'

import { useLangContext } from '@/frontend/_providers/LangContextProvider'
import { FONT_SIZE } from 'src/app/_style/style.constants'
import type { ILocalizedTextFront } from 'src/models/text/LocalizedText.type'
import { LocalizedTextWrapper } from './LocalizedTextWrapper'
import { Block } from './text-components/Block'
import { TextWrapper } from './text-components/TextWrapper'
import { prepareBlocks } from './text.util'

const NoContent = styled.div`
  font-size: ${FONT_SIZE.md};
  text-align: center;
`
const LangNotAvailable = styled.div`
  font-size: ${FONT_SIZE.md};
  text-align: center;
`
interface IProps {
  item: ILocalizedTextFront | null
}

export function LocalizedText({ item }: IProps) {
  const { getLabel } = useLangContext()
  const containerRef = useRef<HTMLDivElement>(null)

  const hasBlocks =
    item &&
    item.content &&
    Array.isArray(item.content.blocks) &&
    item.content.blocks.length > 0

  if (!hasBlocks) {
    return (
      <NoContent className="px-2 py-8">
        <TextWrapper text={getLabel('noContent') as string} />
      </NoContent>
    )
  }
  const preparedBlocks = prepareBlocks(item.content.blocks)

  return (
    <LocalizedTextWrapper ref={containerRef} className="px-2 py-5">
      {(item.lang as string) !== getLabel('lang') && (
        <LangNotAvailable>{getLabel('langNotAvailable')}</LangNotAvailable>
      )}
      {preparedBlocks.map((block, index) => (
        <Block
          key={block.id ?? index}
          block={block}
          containerRef={containerRef}
        />
      ))}
    </LocalizedTextWrapper>
  )
}
