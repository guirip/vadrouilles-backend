'use client'

import { faChevronRight } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { Button } from 'primereact/button'
import { useEffect, useMemo, useRef, useState, useTransition } from 'react'
import { Helmet } from 'react-helmet-async'
import styled from 'styled-components'

import { CoreContent } from '@/frontend/_components/core-content'
import Tabs, { type ITabsHandle } from '@/frontend/_components/tabs'
import { Size } from '@/frontend/_components/tabs/size.styles'
import type { ITabData } from '@/frontend/_components/tabs/tab.type'
import { useLangContext } from '@/frontend/_providers/LangContextProvider'
import {
  findLocalizedTextMinimalByLang,
  generatePageMetadata,
  getChapterIndex,
  getLocalizedTextByLang,
} from '@/frontend/_services/text-service'
import { Mode, getModeStyle } from 'src/app/frontend/_style/mode.style'
import { getNavigateData } from '@/frontend/_types/navigation.types'
import Link from 'next/link'
import type { Lang } from 'src/models/common/Lang'
import type {
  IBaseTextChapterFront,
  ITextChapterMinimalPopulatedFront,
  ITextChapterPopulatedFront,
} from 'src/models/text/TextChapter.type'
import type {
  ITextPageFront,
  ITextPageMinimalPopulatedFront,
} from 'src/models/text/TextPage.type'
import { useCodifContext } from '../../_providers/CodifContextProvider'
import { getRoute } from '../../_services/navigation-service'
import { FONT_SIZE } from 'src/app/_style/style.constants'
import { LocalizedText } from './LocalizedText'

interface INextChapterData {
  _id: IBaseTextChapterFront['_id']
  slug: IBaseTextChapterFront['slug']
  title: string
}

const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
  width: 100%;
`
const NextChapterButton = styled(Button)`
  font-size: ${FONT_SIZE.xs};
  background-color: ${getModeStyle(true, Mode.LIGHT).background};
`

const getLocalizedTextTitleByLang = (
  chapter: ITextChapterMinimalPopulatedFront,
  lang: Lang,
  fallback: string
) =>
  findLocalizedTextMinimalByLang(chapter.localizedTexts, lang)?.title ||
  fallback

interface IProps {
  previewMode: boolean
  textPages: ITextPageFront[]
  currentTextPage: ITextPageMinimalPopulatedFront
  chapter: ITextChapterPopulatedFront
  resetScroll: () => void
}

function Text({ resetScroll, previewMode, currentTextPage, chapter }: IProps) {
  const [, startTransition] = useTransition()
  const tabRef = useRef<ITabsHandle>(null)
  const { lang, getLabel, getAppTitle } = useLangContext()
  const [nextChapterData, setNextChapterData] =
    useState<INextChapterData | null>(null)
  // const [commentBlockVisible, setCommentBlockVisible] = useState(false)
  const { staticUrl } = useCodifContext()

  const localizedText = useMemo(
    () => getLocalizedTextByLang(chapter.localizedTexts, lang),
    [chapter.localizedTexts, lang]
  )

  const [currentChapterIndex, setCurrentChapterIndex] = useState(
    getChapterIndex(currentTextPage, chapter.slug)
  )
  useEffect(() => {
    setCurrentChapterIndex(getChapterIndex(currentTextPage, chapter.slug))
  }, [setCurrentChapterIndex, currentTextPage, chapter.slug])

  useEffect(() => {
    tabRef.current?.resetScroll()
  }, [currentTextPage._id])

  useEffect(() => {
    startTransition(() => {
      if (
        currentChapterIndex !== -1 &&
        currentChapterIndex < currentTextPage.chapters.length - 1
      ) {
        const nextChapter = currentTextPage.chapters[currentChapterIndex + 1]
        setNextChapterData({
          _id: nextChapter._id,
          slug: nextChapter.slug,
          title: getLocalizedTextTitleByLang(
            nextChapter,
            lang,
            getLabel(['text', 'untitled'])
          ),
        })
      } else {
        setNextChapterData(null)
      }
    })
  }, [
    currentChapterIndex,
    currentTextPage.chapters,
    lang,
    getLabel,
    currentTextPage,
    previewMode,
  ])

  useEffect(() => {
    resetScroll()
  }, [currentChapterIndex, resetScroll])

  const tabData: ITabData[] = useMemo(
    function getTabData() {
      if (!currentTextPage || currentTextPage.chapters.length < 2) {
        return []
      }
      return currentTextPage.chapters.map(function (c): ITabData {
        const tabLabel = getLocalizedTextTitleByLang(
          c,
          lang,
          getLabel(['text', 'untitled'])
        )
        return {
          key: c.slug,
          content: {
            navigate: getNavigateData(currentTextPage, c),
            title: tabLabel,
          },
        }
      })
    },
    [currentTextPage, lang, getLabel]
  )

  const { title, description } = generatePageMetadata(
    currentTextPage,
    chapter,
    lang,
    staticUrl ?? '',
    getAppTitle(previewMode, currentTextPage.category)
  )

  return (
    <Wrapper>
      <>
        <Helmet>
          {typeof title === 'string' && <title>{title}</title>}
          {description && <meta name="description" content={description} />}
          {/* TODO should set other meta properties */}
        </Helmet>

        <Tabs
          previewMode={false}
          ref={tabRef}
          mode={Mode.LIGHT}
          size={Size.SMALLER}
          data={tabData}
          currentTabIndex={currentChapterIndex}
          onTabIndexChange={(index: number) => setCurrentChapterIndex(index)}
        />

        {localizedText && (
          <CoreContent>
            <LocalizedText item={localizedText} />
          </CoreContent>
        )}

        {nextChapterData && (
          <div className="flex justify-content-end">
            <Link
              href={getRoute(
                previewMode,
                currentTextPage.category,
                currentTextPage.slug,
                nextChapterData.slug,
                lang
              )}
            >
              <NextChapterButton className="p-button-sm mt-2 mx-5">
                <span className="mr-1">{nextChapterData.title}</span>
                <FontAwesomeIcon icon={faChevronRight} />
              </NextChapterButton>
            </Link>
          </div>
        )}
        {/*
        <CommentsBlock
          visible={commentBlockVisible}
          setBlockVisible={() => setCommentBlockVisible(true)}
          identifier={text.slug+'@'+getCurrentChapter().slug}
          title={tabData[currentChapterIndex].label}
        />
        */}
      </>
    </Wrapper>
  )
}

export default Text
