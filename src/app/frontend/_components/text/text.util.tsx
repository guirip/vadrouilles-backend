import type { IBlock, IGalleryBlock } from 'src/models/text/TextBlock.type'
import {
  BlockType,
  isGalleryBlock,
  isImageBlock,
  isVideoBlock,
} from 'src/models/text/TextBlock.type'
import type { ITextPageFront } from 'src/models/text/TextPage.type'

export const getDefaultPage = (textPages: ITextPageFront[]) => textPages[0]

export function getTextPage(
  slug: string | undefined,
  textPages: ITextPageFront[]
) {
  if (textPages.length === 0) {
    return null
  }
  if (!slug) {
    return getDefaultPage(textPages)
  }
  return textPages.find((textPage) => textPage.slug === slug)
}

export function prepareBlocks(blocks: IBlock[]) {
  const parsed: IBlock[] = []
  let galleryBlock: IGalleryBlock | null = null

  blocks.forEach((block, index) => {
    if (isImageBlock(block) || isVideoBlock(block)) {
      if (galleryBlock) {
        if (!galleryBlock.data.visuals) {
          throw new Error(`'visuals' array should be initialized`)
        }
        galleryBlock.data.visuals.push(block)
      } else {
        parsed.push(block)
      }
      return
    }
    if (galleryBlock) {
      parsed.push(galleryBlock)
      galleryBlock = null
    }
    if (isGalleryBlock(block)) {
      galleryBlock = {
        type: BlockType.Gallery,
        id: `gallery-${index}`,
        data: {
          ...block.data,
          visuals: [],
        },
      }
    } else {
      parsed.push(block)
    }
  })
  if (galleryBlock) {
    parsed.push(galleryBlock)
  }
  return parsed
}
