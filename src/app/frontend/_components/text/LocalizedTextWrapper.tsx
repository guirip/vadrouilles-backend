'use client'

import {
  BLOCK_MARGIN_TOP,
  BLOCK_MARGIN_BOTTOM,
} from 'src/app/_style/text/constants'
import styled from 'styled-components'

export const LocalizedTextWrapper = styled.div`
  line-height: 1.7rem;
  text-align: justify;

  & > * {
    margin: ${BLOCK_MARGIN_TOP} auto ${BLOCK_MARGIN_BOTTOM};
  }

  &:after {
    content: '';
    display: inline-block;
    width: 100%;
  }
`
