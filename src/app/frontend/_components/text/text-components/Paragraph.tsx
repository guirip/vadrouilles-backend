'use client'

import styled, { css } from 'styled-components'

import { FONT_SIZE } from 'src/app/_style/style.constants'
import {
  callOutStyle,
  citationStyle,
  detailsStyle,
  paragraphStyle,
} from 'src/app/_style/text/paragraph'
import type {
  AlignmentTune,
  IParagraphBlock,
} from 'src/models/text/TextBlock.type'
import { TextVariantTune, Tunes } from 'src/models/text/TextBlock.type'
import { TextWrapper } from './TextWrapper'

interface IBlockProps {
  $variant?: TextVariantTune
  $alignment?: AlignmentTune
}

const Block = styled.p<IBlockProps>`
  ${paragraphStyle}
  ${({ $alignment }) =>
    $alignment &&
    css`
      text-align: ${$alignment};
    `}

  ${({ $variant }) => {
    switch ($variant) {
      case TextVariantTune.CALL_OUT:
        return css`
          ${callOutStyle}
        `

      case TextVariantTune.CITATION:
        return css`
          ${citationStyle}
        `

      case TextVariantTune.DETAILS:
        return css`
          ${detailsStyle}
        `

      default:
        return
    }
  }}
`

interface IProps {
  data: IParagraphBlock['data']
  tunes: IParagraphBlock['tunes']
  fontSize?: string
}

export const Paragraph = ({ data, tunes, fontSize = FONT_SIZE.md }: IProps) => (
  <Block
    $variant={tunes?.[Tunes.TextVariant]}
    $alignment={tunes?.alignmentTune?.alignment}
  >
    <TextWrapper text={data.text} fontSize={fontSize} />
  </Block>
)
