import type { IHeaderBlock } from 'src/models/text/TextBlock.type'

export const getHeaderId = (blockId: IHeaderBlock['id']) => `header-${blockId}`
