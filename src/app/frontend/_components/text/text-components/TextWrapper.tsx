'use client'

import styled, { css } from 'styled-components'

import { FontFamily } from 'src/app/_assets/fonts'
import { TEXT_COLOR } from 'src/app/_style/style.constants'

export enum TextWrapperVariant {
  ITALIC = 'italic',
  CENTERED = 'centered',
  WEIGHT_LIGHT = 'weight-light',
  LETTER_SPACING = 'letter-spacing',
}

interface IStyledTextWrapperProps {
  $variants: TextWrapperVariant[]
  $fontSize?: string
}

const StyledTextWrapper = styled.span<IStyledTextWrapperProps>`
  display: inline-block;
  font-family: ${FontFamily.LEAGUESPARTAN}, sans-serif;
  ${({ $fontSize }) =>
    $fontSize &&
    css`
      font-size: ${$fontSize};
    `}

  ${({ $variants }) =>
    $variants.includes(TextWrapperVariant.ITALIC) &&
    css`
      font-style: italic;
    `}

  ${({ $variants }) =>
    $variants.includes(TextWrapperVariant.CENTERED) &&
    css`
      text-align: center;
    `}

  ${({ $variants }) =>
    $variants.includes(TextWrapperVariant.WEIGHT_LIGHT) &&
    css`
      font-weight: 100;
    `}

    ${({ $variants }) =>
    $variants.includes(TextWrapperVariant.LETTER_SPACING) &&
    css`
      letter-spacing: 1px;
    `}

  & a {
    color: #56ffff;
  }
  & a:visited {
    color: #abffff;
  }
  & u {
    text-decoration: underline;
  }
  & mark {
    background-color: ${TEXT_COLOR};
  }
`

interface IProps {
  text: string
  variants?: TextWrapperVariant[]
  fontSize?: string
}

export const TextWrapper = ({ text, variants = [], fontSize }: IProps) => (
  <StyledTextWrapper
    $variants={variants}
    $fontSize={fontSize}
    dangerouslySetInnerHTML={{ __html: text }}
  />
)
