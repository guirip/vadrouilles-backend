'use client'

import styled from 'styled-components'

import { getTableStyle } from 'src/app/_style/text/table'
import type { ITableBlock } from 'src/models/text/TextBlock.type'
import { TextWrapper } from './TextWrapper'

const StyledTable = styled.table`
  margin: 2rem auto;
  ${getTableStyle()}
`

type RowData = string[]

const TableHeader = ({ headerData }: { headerData: RowData }) => (
  <thead>
    <tr>
      {headerData.map((heading, index) => (
        <th key={index}>
          <TextWrapper text={heading} />
        </th>
      ))}
    </tr>
  </thead>
)

const TableRow = ({ rowData }: { rowData: RowData }) => (
  <tr>
    {rowData.map((cellData, index) => (
      <td key={index}>
        <TextWrapper text={cellData} />
      </td>
    ))}
  </tr>
)

const TableContent = ({ rows }: { rows: RowData[] }) => (
  <tbody>
    {rows.map((rowData, index) => (
      <TableRow key={index} rowData={rowData} />
    ))}
  </tbody>
)

export const Table = ({
  data: { content, withHeadings },
}: {
  data: ITableBlock['data']
}) => (
  <StyledTable>
    {withHeadings ? (
      <>
        <TableHeader headerData={content[0]} />
        <TableContent rows={content.slice(1)} />
      </>
    ) : (
      <TableContent rows={content} />
    )}
  </StyledTable>
)
