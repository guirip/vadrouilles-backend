'use client'

import styled from 'styled-components'

import type {
  IListBlock,
  INestedListItem,
} from 'src/models/text/TextBlock.type'
import { TextWrapper } from './TextWrapper'
import { listStyle, olStyle, ulStyle, liStyle } from 'src/app/_style/text/list'

const Wrapper = styled.div`
  ${listStyle}
`
const StyledOl = styled.ol`
  ${olStyle}
`
const StyledUl = styled.ul`
  ${ulStyle}
`
const StyledLi = styled.li`
  ${liStyle}
`

interface IListProps {
  style: IListBlock['data']['style']
  items: INestedListItem[]
}

const ListElements = ({ style, items }: IListProps) => (
  <>
    {items.map((item: INestedListItem, index: number) => (
      <StyledLi key={index}>
        <TextWrapper text={item.content} />
        {Array.isArray(item.items) && item.items.length > 0 && (
          <Group style={style} items={item.items} />
        )}
      </StyledLi>
    ))}
  </>
)

function getGroupComponent(style: string) {
  switch (style) {
    case 'ordered':
      return StyledOl

    case 'unordered':
    default:
      return StyledUl
  }
}

function Group({ style, items }: IListProps) {
  const Element = getGroupComponent(style)
  return (
    <Element>
      <ListElements items={items} style={style} />
    </Element>
  )
}

export const List = ({
  data: { style, items },
}: {
  data: IListBlock['data']
}) => (
  <Wrapper>
    <Group style={style} items={items} />
  </Wrapper>
)
