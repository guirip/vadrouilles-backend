import { GallerySize } from '@/frontend/_components/gallery/types'
import { VideoProxy } from '@/frontend/_components/video/VideoProxy'
import dynamic from 'next/dynamic'
import type {
  IBlock,
  IImageBlock,
  IVideoBlock,
} from 'src/models/text/TextBlock.type'
import {
  BlockType,
  isButtonBlock,
  isDelimiterBlock,
  isGalleryBlock,
  isHeaderBlock,
  isImageBlock,
  isListBlock,
  isParagraphBlock,
  isSocialBlock,
  isSpacerBlock,
  isSummaryBlock,
  isTableBlock,
  isTextReferenceBlock,
  isVideoBlock,
} from 'src/models/text/TextBlock.type'
import { isArray } from 'src/utils/tsUtil'
import { InstagramPost } from '../../instagram/InstagramPost'
import { Delimiter } from './Delimiter'
import { Header } from './Header'
import { Image } from './Image'
import { List } from './List'
import { Paragraph } from './Paragraph'
import { Spacer } from './Spacer'
import { Summary } from './Summary'
import { Table } from './Table'
import { TextRef } from './TextRef'
import { TextButton } from 'src/app/_components/text/TextButton'

const Gallery = dynamic(() => import('@/frontend/_components/gallery'))

interface IBlockProps {
  block: IBlock
  containerRef: React.RefObject<HTMLDivElement | null>
}

function getGalleryContent(block: IImageBlock | IVideoBlock) {
  switch (block.type) {
    case BlockType.Image:
      return block.data
    case BlockType.Video:
      return block.data
  }
}

export function Block({ block, containerRef }: IBlockProps) {
  if (isParagraphBlock(block)) {
    return <Paragraph data={block.data} tunes={block.tunes} />
  }
  if (isButtonBlock(block)) {
    return (
      <div className="flex justify-content-center">
        <TextButton data={block.data} />
      </div>
    )
  }
  if (isHeaderBlock(block)) {
    return <Header id={block.id} data={block.data} tunes={block.tunes} />
  }
  if (isImageBlock(block)) {
    return <Image data={block.data} />
  }
  if (isVideoBlock(block)) {
    return <VideoProxy data={block.data} />
  }
  if (isTableBlock(block)) {
    return <Table data={block.data} />
  }
  if (isDelimiterBlock(block)) {
    return <Delimiter />
  }
  if (isGalleryBlock(block) && isArray(block.data.visuals)) {
    return (
      <Gallery
        visuals={block.data.visuals.map((block) => getGalleryContent(block))}
        thumbSize={GallerySize.SMALL}
        showGalleryThumbnails={block.data.showThumbnails === true}
      />
    )
  }
  if (isListBlock(block)) {
    return <List data={block.data} />
  }
  if (isSocialBlock(block)) {
    if (block.data.type === 'instagram') {
      return <InstagramPost url={block.data.url} />
    }
    console.error('unmanaged social block type: ' + block.data.type)
    return null
  }
  if (isSpacerBlock(block)) {
    return <Spacer data={block.data} />
  }
  if (isSummaryBlock(block)) {
    return <Summary data={block.data} containerRef={containerRef} />
  }
  if (isTextReferenceBlock(block)) {
    return <TextRef data={block.data} />
  }

  console.error('unmanaged block type: ' + block.type)
  return null
}
