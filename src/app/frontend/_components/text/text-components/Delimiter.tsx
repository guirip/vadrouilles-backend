'use client'

import { delimiterStyle } from 'src/app/_style/text/delimiter'
import styled from 'styled-components'

const DelimiterWrapper = styled.div`
  ${delimiterStyle}
`

export const Delimiter = () => (
  <DelimiterWrapper>
    <div></div>
  </DelimiterWrapper>
)
