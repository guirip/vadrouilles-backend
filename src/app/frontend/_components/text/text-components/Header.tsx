'use client'

import styled, { css } from 'styled-components'

import { headerStyle } from 'src/app/_style/text/header'
import type {
  AlignmentTune,
  IHeaderBlock,
} from 'src/models/text/TextBlock.type'
import { TextWrapper } from './TextWrapper'
import { getHeaderId } from './helpers'
import { MAX_WIDTH } from 'src/app/_style/text/constants'

const Wrapper = styled.div<{ $alignment?: AlignmentTune }>`
  max-width: ${MAX_WIDTH}px;
  ${({ $alignment }) =>
    $alignment &&
    css`
      text-align: ${$alignment};
    `}

  ${headerStyle}
`

const wrapContent = (text: string) => <TextWrapper text={text} />

function getHeader(
  id: IHeaderBlock['id'],
  level: IHeaderBlock['data']['level'],
  text: IHeaderBlock['data']['text']
) {
  switch (level) {
    case 1:
      return <h1 id={getHeaderId(id)}>{wrapContent(text)}</h1>
    case 3:
      return <h3 id={getHeaderId(id)}>{wrapContent(text)}</h3>
    case 4:
      return <h4 id={getHeaderId(id)}>{wrapContent(text)}</h4>
    case 5:
      return <h5 id={getHeaderId(id)}>{wrapContent(text)}</h5>
    case 6:
      return <h6 id={getHeaderId(id)}>{wrapContent(text)}</h6>
    case 2:
    default:
      return <h2 id={getHeaderId(id)}>{wrapContent(text)}</h2>
  }
}

interface IProps {
  id: IHeaderBlock['id']
  data: IHeaderBlock['data']
  tunes: IHeaderBlock['tunes']
}

export const Header = ({ id, data: { text, level }, tunes }: IProps) => (
  <Wrapper $alignment={tunes?.alignmentTune?.alignment}>
    {getHeader(id, level, text)}
  </Wrapper>
)
