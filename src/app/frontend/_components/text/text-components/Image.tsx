'use client'

import styled from 'styled-components'

import { useCodifContext } from '@/frontend/_providers/CodifContextProvider'
import type { IImageBlock } from 'src/models/text/TextBlock.type'
import { TextWrapper, TextWrapperVariant } from './TextWrapper'
import { useLangContext } from 'src/app/frontend/_providers/LangContextProvider'
import { FONT_SIZE } from 'src/app/_style/style.constants'

const textVariants = [
  TextWrapperVariant.CENTERED,
  TextWrapperVariant.WEIGHT_LIGHT,
  // TextWrapperVariant.LETTER_SPACING,
]

export const ImageWrapper = styled.div`
  margin-top: 1.5rem;
  margin-bottom: 1.5rem;
  & span {
    padding: 0 1rem;
  }
`
const StyledAnchor = styled.a`
  display: flex;
  justify-content: center;
  min-height: 0;
`
const Photo = styled.img`
  max-width: 72vw;
  max-height: 84vh;
  cursor: pointer;

  @media (max-width: 760px) {
    max-width: 98vw;
    max-height: 78vh;
  }
`
const Caption = styled.div`
  max-width: 1024px;
  padding: 0.7rem 1rem 0.5rem;
  font-size: ${FONT_SIZE.sm};
  line-height: 1.4em;
`

export function Image({ data }: { data: IImageBlock['data'] }) {
  const { staticUrl } = useCodifContext()
  const { getLabel } = useLangContext()

  return !staticUrl ? null : (
    <ImageWrapper className="flex justify-content-center align-items-center flex-column">
      <StyledAnchor
        aria-label={getLabel('enlargePicture')}
        href={staticUrl + '/' + data.file.mid}
      >
        <Photo
          className="align-items-center mt-1 mb-2"
          src={staticUrl + '/' + data.file.mid}
          alt={data.caption}
        />
      </StyledAnchor>
      {data.caption && (
        <Caption>
          <TextWrapper text={data.caption} variants={textVariants} />
        </Caption>
      )}
    </ImageWrapper>
  )
}
