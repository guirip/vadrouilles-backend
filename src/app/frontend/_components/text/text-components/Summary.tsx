'use client'

import {
  faChevronCircleRight,
  faXmark,
} from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { useCallback, useMemo, useState } from 'react'
import styled, { css } from 'styled-components'

import { useLangContext } from '@/frontend/_providers/LangContextProvider'
import { useScrollContext } from '@/frontend/_providers/ScrollContext'
import { FONT_SIZE, TEXT_COLOR } from 'src/app/_style/style.constants'
import type {
  IHeaderBlock,
  ISummaryBlock,
} from 'src/models/text/TextBlock.type'
import { TextWrapper } from './TextWrapper'
import { getHeaderId } from './helpers'

const INDENT = 15

const StyledSummaryRow = styled.div<{ $level: number; $startLevel: number }>`
  display: flex;
  align-items: baseline;
  flex-wrap: nowrap;
  width: fit-content;
  max-width: 600px;
  margin-left: ${(props) => (props.$level - props.$startLevel) * INDENT}px;
  padding: 2px 3px;
  font-size: ${FONT_SIZE.sm};
  text-decoration: none;
  color: ${TEXT_COLOR};
  opacity: 0.9;
  cursor: pointer;
  -webkit-tap-highlight-color: rgba(0, 0, 0, 0.5);

  &:hover {
    opacity: 1;
  }
`

interface ISummaryRowProps {
  header: IHeaderBlock
  startLevel: number
  scrollTo: (id: string) => void
}

const SummaryRow = ({ header, startLevel, scrollTo }: ISummaryRowProps) => (
  <StyledSummaryRow
    $level={header.data.level}
    $startLevel={startLevel}
    onClick={() => scrollTo(header.id as string)}
  >
    <FontAwesomeIcon
      className="mr-2"
      icon={faChevronCircleRight}
      size="xs"
      opacity={0.3 - header.data.level * 0.05}
    />
    <TextWrapper text={header.data.text} />
  </StyledSummaryRow>
)

const StyledSummary = styled.div<{ $folded: boolean }>`
  display: flex;
  flex-direction: column;
  width: fit-content;
  ${({ $folded }) =>
    $folded
      ? ''
      : css`
          min-width: 250px;
        `}
  margin: 0 auto 1.7rem;
  background-color: rgba(0, 0, 0, 0.55);
`
const SummaryHeader = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: space-between;
  align-self: center;
  padding: 0.2rem 0.4rem;
  width: 100%;
  font-size: ${FONT_SIZE.xxs};
  font-weight: bold;
  opacity: 0.65;
  -webkit-tap-highlight-color: transparent;

  & span {
    flex-grow: 1;
    padding: 0 1.4rem;
    text-align: center;
    cursor: pointer;
  }
  & svg {
    padding-right: 0.2rem;
    opacity: 0.5;
    cursor: pointer;
  }
`
const SummaryContent = styled.div`
  margin: 0.4rem auto 0.7rem;
  padding: 0 1.8rem;
`

interface IProps {
  data: ISummaryBlock['data']
  containerRef: React.RefObject<HTMLDivElement | null>
}

export function Summary({ data, containerRef }: IProps) {
  const { getLabel } = useLangContext()
  const { ref: scrollable } = useScrollContext()
  const [folded, setFolded] = useState(false)

  const startLevel = useMemo(
    () => Math.min(...data.headers.map((h) => h.data.level)),
    [data]
  )

  const scrollTo = useCallback(
    (elId: string) => {
      const el = containerRef.current?.querySelector(`#${getHeaderId(elId)}`)

      if (scrollable?.current && el instanceof HTMLHeadingElement) {
        const scrollableOffsetTop = scrollable.current.offsetTop
        const elOffsetTop = el.offsetTop
        const translateY = elOffsetTop - scrollableOffsetTop - 20
        scrollable.current.scrollBy({
          left: 0,
          top: translateY,
          behavior: 'smooth',
        })
      }
    },
    [containerRef, scrollable]
  )

  return !Array.isArray(data.headers) || data.headers.length === 0 ? null : (
    <StyledSummary $folded={folded}>
      <SummaryHeader>
        <span onClick={() => setFolded(!folded)}>
          {getLabel(['text', 'summary'])}
        </span>
        {!folded && (
          <FontAwesomeIcon
            size="xl"
            icon={faXmark}
            onClick={() => setFolded(!folded)}
          />
        )}
      </SummaryHeader>

      {folded === false && (
        <SummaryContent>
          {data.headers.map((header) => (
            <SummaryRow
              key={header.id}
              header={header}
              startLevel={startLevel}
              scrollTo={scrollTo}
            />
          ))}
        </SummaryContent>
      )}
    </StyledSummary>
  )
}
