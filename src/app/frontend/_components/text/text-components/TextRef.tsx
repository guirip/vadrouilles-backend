'use client'

import { useRouter } from 'next/navigation'
import { useCallback, useEffect, useState } from 'react'
import { PagePreview } from 'src/app/_components/text/PagePreview'
import { useLangContext } from 'src/app/frontend/_providers/LangContextProvider'
import { getTextRefMetadata } from 'src/app/_server-actions/metadata.actions'
import { getRoute } from 'src/app/frontend/_services/navigation-service'
import type { ITextReferenceBlock } from 'src/models/text/TextBlock.type'
import styled from 'styled-components'

const TextRefWrapper = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  margin-bottom: 0.8rem;

  a {
    flex-grow: 1;
  }
`

interface IProps {
  data: ITextReferenceBlock['data']
}

export const TextRef = ({ data }: IProps) => {
  const router = useRouter()
  const { lang } = useLangContext()
  const [metadata, setMetadata] = useState<
    null | ITextReferenceBlock['data']['metadata']
  >(null)
  const [url, setUrl] = useState('')

  useEffect(() => {
    ;(async () => {
      const result = await getTextRefMetadata(data, lang)
      setMetadata(result?.metadata ?? null)

      if (result?.category && result.textPageSlug) {
        setUrl(
          getRoute(
            false,
            result.category,
            result.textPageSlug,
            result.chapterSlug ?? null,
            lang
          )
        )
      }
    })()
  }, [data, lang])

  const navigate = useCallback(() => {
    router.push(url)
  }, [router, url])

  return !metadata ? null : (
    <TextRefWrapper className="fadein animation-duration-1000">
      <PagePreview
        focus
        metadata={metadata}
        showPageTitle={data.showPageTitle}
        showPageSubtitle={data.showPageSubtitle}
        coverPosition={data.coverPosition}
        onClick={navigate}
      />
    </TextRefWrapper>
  )
}
