'use client'

import styled from 'styled-components'

import type { ISpacerBlock } from 'src/models/text/TextBlock.type'

interface IBlockProps {
  $value: ISpacerBlock['data']['value']
}

const Block = styled.p<IBlockProps>`
  margin: 0;
  height: ${({ $value }) => $value * 2}rem;
`

interface IProps {
  data: ISpacerBlock['data']
}

export const Spacer = ({ data }: IProps) => <Block $value={data.value} />
