'use client'

import type { ContentCategory } from 'src/models/common/ContentCategory'
import { createGlobalStyle } from 'styled-components'
import {
  BG_BY_CATEGORY,
  DEFAULT_ROOT_FONT_SIZE,
  getHtmlFontSizeStyle,
} from 'src/app/_style/style.constants'

export default createGlobalStyle<{ category: ContentCategory }>`
html {
  background-image: url('${({ category }) => BG_BY_CATEGORY[category].src}') !important;
}
body {
  margin: 0;
}
${getHtmlFontSizeStyle(DEFAULT_ROOT_FONT_SIZE, false)}
`
