import type { IFile } from 'src/models/common/File.type'

export const getImageLowUrl = (file: IFile, staticUrl: string) =>
  `${staticUrl}/${file.low}`

export const getImageMidUrl = (file: IFile, staticUrl: string) =>
  `${staticUrl}/${file.mid}`
