import type { IVideo } from 'src/models/common/Video'
import type { IImageBlock } from 'src/models/text/TextBlock.type'

export enum GallerySize {
  SMALL = 'small',
  MEDIUM = 'medium',
  BIG = 'big',
}

export type IVisual = IImageBlock['data'] | IVideo
