import { faVideo } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import styled from 'styled-components'

import { isVideo } from 'src/models/common/Video'
import { useCodifContext } from '@/frontend/_providers/CodifContextProvider'
import { getImageLowUrl } from './helpers'
import type { IVisual } from './types'
import { GallerySize } from './types'
import { stopEventPropagation } from '../../_utils/Utils'

const THUMB_MARGINS = {
  [GallerySize.SMALL]: '3px',
  [GallerySize.MEDIUM]: '6px',
  [GallerySize.BIG]: '8px',
}

const getThumbSizeStyle = (size: GallerySize) => {
  switch (size) {
    case GallerySize.SMALL:
      return `
        width: 60px;
        height: 60px;
      `
    case GallerySize.MEDIUM:
      return `
        width: 80px;
        height: 80px;
      `
    case GallerySize.BIG:
      return `
        width: 140px;
        height: 140px;
      `
  }
}

interface IThumbProps {
  $size: GallerySize
  $src: string
}

const Thumb = styled.div<IThumbProps>`
  ${({ $size }) => getThumbSizeStyle($size)}
  flex-shrink: 0;
  margin: ${({ $size }) => THUMB_MARGINS[$size]};
  background-image: url('${({ $src }) => $src}');
  background-size: cover;
  background-position: 50% 50%;
  cursor: pointer;
  -webkit-tap-highlight-color: transparent;
`

const getVideoThumbSizeStyle = (size: GallerySize) => {
  switch (size) {
    case GallerySize.SMALL:
      return `
        margin: 2px;
        font-size: 2rem;
      `

    case GallerySize.MEDIUM:
      return `
        margin: 4px;
        font-size: 2.3rem;
      `

    case GallerySize.BIG:
      return `
        margin: 2px;
        font-size: 3rem;
      `
  }
}

const VideoThumb = styled.span<{ $size: GallerySize }>`
  display: flex;
  justify-content: center;
  align-items: center;
  flex-shrink: 0;
  ${({ $size }) => getThumbSizeStyle($size)}
  margin: ${({ $size }) => THUMB_MARGINS[$size]};
  opacity: 0.7;
  cursor: pointer;
  background-color: rgba(0, 0, 0, 0.7);

  & svg {
    ${({ $size }) => getVideoThumbSizeStyle($size)}
  }
`

interface IProps {
  visual: IVisual
  size: GallerySize
  onClick?: () => void
}

export const Thumbnail = ({ visual, size, onClick }: IProps) => {
  const { staticUrl } = useCodifContext()

  return isVideo(visual) ? (
    <VideoThumb $size={size} onClick={onClick}>
      <FontAwesomeIcon icon={faVideo} />
    </VideoThumb>
  ) : (
    <Thumb
      $size={size}
      $src={getImageLowUrl(visual.file, staticUrl ?? '')}
      onClick={onClick}
      onTouchEnd={stopEventPropagation} // Import Galleria component bugfix
    />
  )
}
