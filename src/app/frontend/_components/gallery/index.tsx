'use client'

import type { Galleria } from 'primereact/galleria'
import { useCallback, useRef, useState } from 'react'

import './override.css'

import { useCodifContext } from '@/frontend/_providers/CodifContextProvider'
import { useDeviceContext } from 'src/app/_components/DeviceContextProvider'
import { isVideo } from 'src/models/common/Video'
import { Image } from '../text/text-components/Image'
import { VideoProxy } from '../video/VideoProxy'
import { Thumbnail } from './Thumbnail'
import { ThumbnailsRow } from './ThumbnailsRow'
import { getImageMidUrl } from './helpers'
import {
  GalleryImage,
  MainPicture,
  PicturesContainer,
  RESPONSIVE_OPTIONS,
  StyledGalleria,
} from './styles'
import type { IVisual } from './types'
import { GallerySize } from './types'
import { useVisuals } from './useVisuals.hook'

interface IGalleryProps {
  visuals?: IVisual[]
  thumbSize: GallerySize
  showGalleryThumbnails: boolean
}

export default function Gallery({
  visuals,
  thumbSize,
  showGalleryThumbnails,
}: IGalleryProps) {
  const { staticUrl } = useCodifContext()
  const { isMobile, isSafari } = useDeviceContext()

  const galleryRef = useRef<Galleria>(null)
  const [galleryActiveIndex, setGalleryActiveIndex] = useState(0)

  const onClickOnVisual = useCallback(function (index: number) {
    setGalleryActiveIndex(index)
    if (!galleryRef.current) {
      console.error('Gallery not intialized yet')
    } else {
      galleryRef.current.show()
    }
  }, [])

  const onGalleryItemChange = useCallback(function (e: { index: number }) {
    setGalleryActiveIndex(e.index)
  }, [])

  const itemTemplate = useCallback(
    (visual: IVisual) => (
      <GalleryImage $isMobile={isMobile} $isSafari={isSafari}>
        {isVideo(visual) ? (
          <VideoProxy data={{ ...visual, width: 200, height: 300 }} autoplay />
        ) : (
          <Image data={visual} />
        )}
      </GalleryImage>
    ),
    [isMobile, isSafari]
  )

  const thumbnailTemplate = useCallback(
    (visual: IVisual) => <Thumbnail visual={visual} size={GallerySize.SMALL} />,
    []
  )

  const { mainVisual, visualsExceptMain, allVisuals } = useVisuals(
    visuals ?? []
  )
  if (allVisuals.length === 0) {
    return null
  }

  return (
    <>
      <StyledGalleria
        ref={galleryRef}
        value={allVisuals}
        activeIndex={galleryActiveIndex}
        onItemChange={onGalleryItemChange}
        fullScreen
        showItemNavigators
        showIndicatorsOnItem
        showThumbnailNavigators={false}
        item={itemTemplate}
        thumbnail={showGalleryThumbnails ? thumbnailTemplate : undefined}
        responsiveOptions={RESPONSIVE_OPTIONS}
        $showThumbnails={showGalleryThumbnails}
        $isMobile={isMobile}
        $isSafari={isSafari}
      />
      <PicturesContainer>
        {mainVisual && isVideo(mainVisual) && <VideoProxy data={mainVisual} />}
        {mainVisual && !isVideo(mainVisual) && (
          <MainPicture
            src={getImageMidUrl(mainVisual.file, staticUrl ?? '')}
            onClick={() => onClickOnVisual(0)}
          />
        )}
        <ThumbnailsRow
          visuals={visualsExceptMain}
          size={thumbSize}
          onClickOnThumb={onClickOnVisual}
        />
      </PicturesContainer>
    </>
  )
}
