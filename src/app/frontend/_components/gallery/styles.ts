'use client'

import { Galleria } from 'primereact/galleria'
import styled, { css } from 'styled-components'
import { VideoContainer } from '../video/EmbeddedVideo'
import { ImageWrapper } from '../text/text-components/Image'

export const RESPONSIVE_OPTIONS = [
  {
    breakpoint: '10000px',
    numVisible: 14,
  },
  {
    breakpoint: '1280px',
    numVisible: 10,
  },
  {
    breakpoint: '768px',
    numVisible: 8,
  },
  {
    breakpoint: '560px',
    numVisible: 5,
  },
]

export const PicturesContainer = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  width: 100%;
`

export const MainPicture = styled.img`
  max-width: 96vw;
  cursor: pointer;
  -webkit-tap-highlight-color: transparent;
`

export const GalleryImage = styled.div<{
  $isMobile: boolean
  $isSafari: boolean
}>`
  flex-grow: 1;
  display: flex;
  padding-top: 5px;
  ${({ $isMobile, $isSafari }) =>
    // avoid bottom truncation on android mobile and safari mobile
    $isMobile &&
    css`
      padding-bottom: ${$isSafari ? '20px' : '40px'};
    `}
  max-width: 98vw;
  max-height: 84vh;

  & ${ImageWrapper} {
    margin: 0;
  }
`

export const StyledGalleria = styled(Galleria)<{
  $showThumbnails: boolean
  $isSafari: boolean
  $isMobile: boolean
}>`
  max-width: 100vw;
  height: 100%;

  & ${VideoContainer} {
    margin: 10px 0;
  }

  ${({ $showThumbnails, $isMobile, $isSafari }) =>
    !$showThumbnails &&
    css`
      ${GalleryImage} {
        max-height: ${$isSafari && $isMobile ? '90vh' : '98vh'};
      }
    `}

  & img {
    max-width: 100%;
  }

  ${({ $showThumbnails }) =>
    !$showThumbnails &&
    css`
      & .p-galleria-thumbnail-wrapper {
        display: none;
      }

      & img {
        max-height: 100%;
      }
    `}
`
