import { useMemo } from 'react'
import { isVideo } from 'src/models/common/Video'
import type { IVisual } from './types'

export function useVisuals(visuals: IVisual[]) {
  const { mainVisual, visualsExceptMain, allVisuals } = useMemo(
    function () {
      let mainVisual: IVisual | undefined
      const visualsExceptMain: IVisual[] = []

      visuals.forEach((visual) => {
        if (!isVideo(visual) && visual.file.isMain) {
          mainVisual = visual
        } else {
          visualsExceptMain.push(visual)
        }
      })
      if (!mainVisual && visualsExceptMain.length > 0) {
        mainVisual = visualsExceptMain.shift()
      }

      return {
        mainVisual,
        visualsExceptMain,
        allVisuals: mainVisual
          ? [mainVisual, ...visualsExceptMain]
          : visualsExceptMain,
      }
    },
    [visuals]
  )

  return {
    mainVisual,
    visualsExceptMain,
    allVisuals,
  }
}
