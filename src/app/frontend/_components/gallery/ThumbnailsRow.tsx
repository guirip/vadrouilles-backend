import styled from 'styled-components'
import type { GallerySize, IVisual } from './types'
import { Thumbnail } from './Thumbnail'

const Container = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: center;
  margin: 1rem auto;

  // max-width: 90vw;
  width: 800px;
  @media screen and (max-width: 800px) {
    width: 500px;
  }
  @media screen and (max-width: 512px) {
    width: 410px;
  }
  @media screen and (max-width: 414px) {
    width: 390px;
  }
  @media screen and (max-width: 360px) {
    width: 340px;
  }

  & img:last-child {
    padding-right: 0;
  }
`
const SubContainer = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
  padding-bottom: 8px;
  overflow-x: auto;
`

interface IProps {
  visuals: IVisual[]
  size: GallerySize
  onClickOnThumb: (index: number) => void
}

export const ThumbnailsRow = ({ visuals, size, onClickOnThumb }: IProps) => (
  <Container>
    <SubContainer>
      {visuals.map((visual, index) => (
        <Thumbnail
          key={index}
          visual={visual}
          size={size}
          onClick={() => onClickOnThumb(index + 1)}
        />
      ))}
    </SubContainer>
  </Container>
)
