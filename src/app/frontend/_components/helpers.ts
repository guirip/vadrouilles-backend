import type { Toast, ToastMessage } from 'primereact/toast'

export function showNotification(
  toast: Toast | null,
  summary: string,
  detail: string,
  level: ToastMessage['severity']
) {
  if (!toast) {
    throw new Error('toast ref should be defined')
  }
  toast.show({
    severity: level,
    summary,
    detail,
    life: 4000,
  })
}
