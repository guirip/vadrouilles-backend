'use client'

import { faDownLong, faFont, faUpLong } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { useEffect, useState } from 'react'
import styled, { createGlobalStyle, css } from 'styled-components'
import { useLangContext } from '../_providers/LangContextProvider'
import {
  DEFAULT_ROOT_FONT_SIZE,
  getHtmlFontSizeStyle,
} from 'src/app/_style/style.constants'

const Wrapper = styled.div`
  position: absolute;
  bottom: 2px;
  left: 2px;
  display: flex;
  flex-direction: column;
  background: black;
  padding: 4px;
  margin: 4px;
`
const Button = styled.div`
  display: flex;
  align-items: end;
  margin: 3px;
  padding: 4px 6px;
  background: rgba(255, 255, 255, 0.1);
  border-radius: 4px;
  cursor: pointer;

  &:active {
    background: rgba(255, 255, 255, 0.3);
  }

  & .mr-05 {
    margin-right: 2px;
  }
`

const GlobalStyle = createGlobalStyle<{ $rootFontSize: number }>`
  ${({ $rootFontSize }) => css`
    ${getHtmlFontSizeStyle($rootFontSize, true)}
  `}
`

const AppFontSize = () => {
  const [fontSize, setFontSize] = useLocalStorage(
    'font-size',
    DEFAULT_ROOT_FONT_SIZE
  )
  const { getLabel } = useLangContext()

  const [ready, setReady] = useState(false)

  useEffect(() => {
    setReady(true)

    return () => {
      setReady(false)
    }
  }, [])

  return (
    <>
      {ready && !!fontSize && <GlobalStyle $rootFontSize={fontSize} />}

      <div className="flex">
        <Wrapper>
          <Button
            id="increase-font"
            aria-label={getLabel('access_raiseFontSize')}
            title={getLabel('access_raiseFontSize')}
            onClick={() =>
              setFontSize(
                Math.min((fontSize ?? DEFAULT_ROOT_FONT_SIZE) + 1, 18)
              )
            }
          >
            <FontAwesomeIcon icon={faFont} className="mr-05" />
            <FontAwesomeIcon icon={faUpLong} size="lg" />
          </Button>
          <Button
            id="decrease-font"
            aria-label={getLabel('access_reduceFontSize')}
            title={getLabel('access_reduceFontSize')}
            onClick={() =>
              setFontSize(Math.max((fontSize ?? DEFAULT_ROOT_FONT_SIZE) - 1, 7))
            }
          >
            <FontAwesomeIcon icon={faFont} className="mr-1" />
            <FontAwesomeIcon icon={faDownLong} size="xs" />
          </Button>
        </Wrapper>
      </div>
    </>
  )
}

function parseStorageValue(key: string, initial: number) {
  if (global.localStorage) {
    const storedValue = localStorage.getItem(key)
    if (storedValue) {
      const parsed = parseInt(storedValue, 10)
      if (typeof parsed === 'number') {
        return parsed
      }
    }
  }
  return initial
}

export function useLocalStorage(
  key: string,
  initialValue: number
): [number, (value: number) => void] {
  const [fontSize, setFontSize] = useState<number>(
    parseStorageValue(key, initialValue)
  )

  useEffect(() => {
    localStorage.setItem(key, fontSize.toString())
  }, [key, fontSize])

  return [fontSize, setFontSize]
}

export default AppFontSize
