import config from '@/frontend/_config'
import { faInstagram } from '@fortawesome/free-brands-svg-icons'
import { faEnvelope } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { Fieldset } from 'primereact/fieldset'
import { useCodifContext } from '../../_providers/CodifContextProvider'
import { useLangContext } from '../../_providers/LangContextProvider'
import { getNoticeContent } from './catalog.util'
import {
  Notice,
  UsageWrapped,
  UsageSteps,
  UsageLastLine,
  Usage,
  Links,
} from './CatalogInfo.style'

export function CatalogInfo() {
  const { getLabel, lang } = useLangContext()

  const { catalogNotice } = useCodifContext()
  const noticeContent = getNoticeContent(catalogNotice, lang)

  return (
    <>
      {noticeContent && (
        <div className="mx-2">
          <Notice dangerouslySetInnerHTML={{ __html: noticeContent }} />
        </div>
      )}

      <div className="mx-2 mt-3 mb-3">
        <Fieldset legend={getLabel('catalog.usageTitle')} toggleable collapsed>
          <UsageWrapped>
            {/* <div><b>{getLabel('catalog.usageTitle}</b></div> */}
            {getLabel('catalog.usageFirstLine') && (
              <div>{getLabel('catalog.usageFirstLine')}</div>
            )}
            <UsageSteps
              dangerouslySetInnerHTML={{
                __html: getLabel('catalog.usageSteps'),
              }}
            />
            {getLabel('catalog.usageLastLine') && (
              <UsageLastLine>{getLabel('catalog.usageLastLine')}</UsageLastLine>
            )}
          </UsageWrapped>
        </Fieldset>

        <Usage>
          {getLabel('catalog.contactLine') && (
            <div>{getLabel('catalog.contactLine')}</div>
          )}
          <Links>
            <div className="px-2 pt-1 mx-3 font-semibold">
              <a
                aria-label={getLabel('sendMeAnEmail')}
                title={getLabel('sendMeAnEmail')}
                href={`mailto:${config.MY_EMAIL}`}
              >
                <FontAwesomeIcon
                  icon={faEnvelope}
                  className="mr-2"
                  color="white"
                />
                {config.MY_EMAIL}
              </a>
            </div>
            <div className="px-2 pt-1 mx-3 font-semibold">
              <a
                aria-label={getLabel('goToInstagram')}
                title={getLabel('goToInstagram')}
                href={config.MY_INSTAGRAM.graffiti.url}
              >
                <FontAwesomeIcon
                  icon={faInstagram}
                  className="mr-2"
                  color="white"
                />
                {config.MY_INSTAGRAM.graffiti.name}
              </a>
            </div>
          </Links>
        </Usage>
      </div>
    </>
  )
}
