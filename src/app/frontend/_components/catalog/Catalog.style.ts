'use client'

import { FONT_SIZE } from 'src/app/_style/style.constants'
import styled from 'styled-components'

export const StyledCatalog = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: center;
  align-items: flex-start;
  flex-wrap: wrap;
  margin: 0 0.5rem 1rem;

  @media (max-width: 760px) {
    flex-direction: column;
    align-items: center;
  }
`

export const ItemsCount = styled.div`
  text-align: center;
  font-style: normal;
  font-size: ${FONT_SIZE.md};
`

export const TabContent = styled.h4`
  color: black;
  font-weight: bold;
  padding: 0.3rem 0.8rem;
`
