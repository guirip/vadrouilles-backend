import type { Lang } from 'src/models/common/Lang'
import type { ICatalogItemCategoryFront } from 'src/models/CatalogItemCategory.type'
import type { IStringCodif } from 'src/models/Codif.type'

export function getNoticeContent(
  noticeValue: IStringCodif['value'] | null,
  lang: Lang
) {
  if (!noticeValue) {
    return null
  }
  try {
    // Test if notice containes i18n properties (such as 'fr', 'en')
    const noticeContent = JSON.parse(noticeValue) as Record<Lang, string>
    if (noticeContent[lang]) {
      return noticeContent[lang]
    }
    if (typeof noticeContent !== 'string') {
      return null
    }
  } catch (e) {
    // Case where value is a simple string
    return noticeValue
  }
}

export const findCategoryBySlug = (
  catSlug: ICatalogItemCategoryFront['slug'],
  categories: ICatalogItemCategoryFront[]
) => categories.find((cat) => cat.slug === catSlug)
