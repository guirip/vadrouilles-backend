import { FONT_SIZE, LINK_COLOR } from 'src/app/_style/style.constants'
import styled from 'styled-components'

export const Notice = styled.div`
  margin: 1rem auto 0.5rem;
  padding: 0.4rem 1rem;
  width: fit-content;
  font-style: normal;
  font-size: ${FONT_SIZE.md};
  text-align: center;
  font-weight: bold;
  line-height: 1.5em;
  background-color: rgba(255, 255, 255, 0.1);
`

export const UsageWrapped = styled(Notice)`
  font-size: ${FONT_SIZE.md};
  font-weight: normal;
  text-align: left;
  background-color: transparent;
`

export const Usage = styled(UsageWrapped)`
  display: flex;
  flex-direction: column;
  align-items: center;
  padding: 1rem 1.5rem;
  background-color: rgba(0, 0, 0, 0.45);

  b {
    display: block;
    font-size: ${FONT_SIZE.md};
    margin-bottom: 0.5rem;
    text-align: center;
  }
`

export const UsageSteps = styled.ul`
  margin: auto;
  width: fit-content;

  & li {
    margin: 0.5rem 0;
  }

  @media (max-width: 576px) {
    & {
      padding-left: 20px;
    }
  }
`

export const UsageLastLine = styled.div`
  line-height: 1.8em;
`

export const Links = styled.div`
  display: flex;
  justify-content: center;
  margin-top: 0.5rem;
  flex-wrap: wrap;
  font-size: ${FONT_SIZE.h6};
  letter-spacing: 1px;

  & > div {
    border-radius: 4px;
    background: transparent;
    transition: 0.3s background;
    &:hover {
      background-color: #ffffff22;
    }
  }

  & a {
    color: ${LINK_COLOR};
  }
`
