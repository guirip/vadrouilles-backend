'use client'

import { useMemo } from 'react'
import { Helmet } from 'react-helmet-async'

import { useCatalogContext } from '@/frontend/_providers/CatalogProvider'
import { useLangContext } from '@/frontend/_providers/LangContextProvider'

import CatalogItem from '@/frontend/_components/catalog-item'
import { CoreContent } from '@/frontend/_components/core-content'
import Tabs from '@/frontend/_components/tabs'
import type { ITabData } from '../tabs/tab.type'

import { Mode } from 'src/app/frontend/_style/mode.style'
import type { ITextChapterPopulatedFront } from 'src/models/text/TextChapter.type'
import type { ITextPageMinimalPopulatedFront } from 'src/models/text/TextPage.type'
import { Size } from '../tabs/size.styles'
import './Catalog.css'
import { StyledCatalog } from './Catalog.style'
import { CatalogInfo } from './CatalogInfo'

interface IProps {
  currentTextPage: ITextPageMinimalPopulatedFront
  chapter: ITextChapterPopulatedFront
  previewMode: boolean
}

function Catalog({ currentTextPage, previewMode }: IProps) {
  const { getLabel, lang } = useLangContext()

  const {
    categories,
    itemsPerCategory,
    currentCategory,
    currentTabIndex,
    setCurrentTabIndex,
  } = useCatalogContext()

  const tabData = useMemo(() => {
    const tabData: ITabData[] = []

    for (const cat of categories) {
      tabData.push({
        key: cat.slug,
        content: {
          title: cat.name[lang],
        },
      })
    }
    return tabData
  }, [categories, lang])

  return (
    <div>
      <Helmet>
        <title>{`${currentTextPage.title[lang]} - ${getLabel('catalog.pageTitle')}`}</title>
        <meta
          name="description"
          content={getLabel('catalog.pageDescription')}
        />
      </Helmet>

      <CoreContent>
        <>
          <CatalogInfo />

          {tabData.length > 1 && (
            <Tabs
              previewMode={previewMode}
              mode={Mode.LIGHT}
              size={Size.SMALLER}
              data={tabData}
              currentTabIndex={currentTabIndex}
              onTabIndexChange={(index) => {
                setCurrentTabIndex(index)
              }}
            />
          )}

          {currentCategory &&
            Array.isArray(itemsPerCategory[currentCategory._id]) &&
            itemsPerCategory[currentCategory._id].length > 0 && (
              <StyledCatalog>
                {itemsPerCategory[currentCategory._id].map((item) => (
                  <CatalogItem key={item._id} item={item} />
                ))}
              </StyledCatalog>
            )}
        </>
      </CoreContent>
    </div>
  )
}

export default Catalog
