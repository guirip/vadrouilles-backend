import { connect } from 'src/services/DbService'
import { generateSiteMap } from 'src/services/SiteMapService'

export default async function sitemap() {
  await connect()
  return await generateSiteMap(process.env.PUBLIC_URL ?? '')
}
