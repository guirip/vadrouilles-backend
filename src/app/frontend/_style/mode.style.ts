import { TEXT_COLOR } from 'src/app/_style/style.constants'

export enum Mode {
  DARK = 'dark',
  LIGHT = 'light',
}
const DEFAULT_MODE = Mode.DARK

export const revertMode = (mode: Mode) =>
  mode === Mode.DARK ? Mode.LIGHT : Mode.DARK

interface IModeStyle {
  color: string
  background: string
  boxShadow: string
  border: string
}

export const ACCENT = '#68bffa'

const MODE_STYLE: Record<Mode, { active: IModeStyle; inactive: IModeStyle }> = {
  [Mode.DARK]: {
    active: {
      color: TEXT_COLOR,
      background: '#120b54cc',
      boxShadow: '0 0 7px 0 rgb(0 0 0)',
      border: '1px solid #3f2de4',
    },
    inactive: {
      color: TEXT_COLOR,
      background: '#000000aa',
      boxShadow: 'none',
      border: '1px solid #2f2f2f',
    },
  },
  [Mode.LIGHT]: {
    active: {
      color: '#000000',
      background: `${ACCENT}e5`,
      boxShadow: 'none',
      border: 'none',
    },
    inactive: {
      color: '#ffffff',
      background: '#67676777',
      boxShadow: 'none',
      border: '1px solid rgb(102 106 113)',
    },
  },
}

export const getModeStyle = (selected: boolean, mode?: Mode) =>
  MODE_STYLE[mode || DEFAULT_MODE][selected ? 'active' : 'inactive']
