'use client'

import { FontFamily } from 'src/app/_assets/fonts'
import styled from 'styled-components'
import {
  TEXT_COLOR,
  FONT_SIZE,
  LINK_COLOR,
} from 'src/app/_style/style.constants'

export const Page = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  height: 100%;
  font-family: ${FontFamily.LEAGUESPARTAN}, sans-serif;
  font-weight: 300;
  color: ${TEXT_COLOR};
`

export const PageContent = styled.div`
  display: flex;
  justify-content: center;
  flex-shrink: 0;
  margin: auto;
`

export const ScrollableContent = styled.div`
  width: 100%;
  overflow-y: auto;
  -webkit-overflow-scrolling: touch;
`

export const Text = styled.div`
  padding: 0.5em;
  font-style: normal;
  font-size: ${FONT_SIZE.md};
`

export const TextBlock = styled(Text)`
  height: fit-content;
  margin: 0.4em 0.5em;
  padding: 0.8em 0.9em;
  text-align: center;
  background-color: rgba(0, 0, 0, 0.4);

  & a {
    color: ${LINK_COLOR};
  }

  & p {
    margin: 0 0 1em;

    &:last-child {
      margin: 0;
    }
  }
`
