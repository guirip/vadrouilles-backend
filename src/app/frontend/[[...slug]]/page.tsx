import { generatePageMetadata } from '@/frontend//_services/text-service'
import ErrorBoundary from '@/frontend/_components/ErrorBoundary'
import { SpecialPage } from '@/frontend/_components/generic-page/constants'
import { GenericPageContent } from '@/frontend/_components/generic-page/GenericPageContent'
import { GenericProviders } from '@/frontend/_components/generic-page/GenericProviders'
import PageNotFound from '@/frontend/_components/generic-page/PageNotFound'
import GlobalStyle from '@/frontend/_components/GlobalStyle'
import { Header } from '@/frontend/_components/header'
import Tabs from '@/frontend/_components/tabs'
import { getLabels } from '@/frontend/_lang'
import { getAppTitleLabelKey } from '@/frontend/_services/label-service'
import { generateTabsDataForPages } from '@/frontend/_services/navigation-service'
import { Page } from '@/frontend/_style/style.main'
import type { Metadata, ResolvingMetadata } from 'next'
import dynamic from 'next/dynamic'
import { redirect } from 'next/navigation'
import { ToastProvider } from 'src/app/_components/ToastProvider'
import { isLang, Lang } from 'src/models/common/Lang'
import { getCatalogData } from 'src/services/CatalogService'
import { connect } from 'src/services/DbService'
import {
  getPathParams,
  isCategoryPathParams,
} from 'src/services/SiteMapService'
import {
  parseCategorySlug,
  queryContentToDisplay,
} from 'src/services/TextService.page'
import { getFrontCompletePath, getNestedProperty } from 'src/utils/util'
import { getInitialLang } from '../_services/service.server'
import { getCodifStaticUrl, queryCodifs } from 'src/services/CodifService'

const AppFontSize = dynamic(() => import('../_components/AppFontSize'), {})

type IPageParams = { slug: string[] }
type ISearchParams = { r?: string; lang?: string }

export async function generateStaticParams() {
  await connect()
  const pathParams = await getPathParams()

  const staticParams = pathParams.map((pathParam) => {
    const param: IPageParams = { slug: [] }
    if (isCategoryPathParams(pathParam)) {
      param.slug.push(pathParam.categoryName)
    } else {
      param.slug.push(pathParam.textPage.category)
      param.slug.push(pathParam.textPage.slug)
      if (pathParam.chapter) {
        param.slug.push(pathParam.chapter.slug)
      }
    }
    return param
  })
  return staticParams
}

async function getContext(
  pPageParams: Promise<IPageParams>,
  pSearchParams: Promise<ISearchParams>
) {
  const [params, searchParams] = await Promise.all([pPageParams, pSearchParams])

  const slug = (() => {
    if (Array.isArray(params.slug)) {
      return params.slug
    }
    if (typeof searchParams?.r === 'string' && searchParams?.r.length > 0) {
      return searchParams.r.slice(1).split('/')
    }
    return []
  })()

  const [pCategory, textPageSlug, chapterSlug] = (slug ?? []).filter(
    (p) => p !== 'frontend'
  )
  const { category, previewMode } = parseCategorySlug(pCategory)

  const lang =
    searchParams.lang && isLang(searchParams.lang)
      ? searchParams.lang
      : await getInitialLang()

  return {
    category,
    textPageSlug,
    chapterSlug,
    previewMode,
    lang,
  }
}

export async function generateMetadata(
  props: { params: Promise<IPageParams>; searchParams: Promise<ISearchParams> },
  parent: ResolvingMetadata
): Promise<Metadata> {
  // optionally access and extend (rather than replace) parent metadata
  const previousImages = (await parent).openGraph?.images || []

  const { category, textPageSlug, chapterSlug, previewMode, lang } =
    await getContext(props.params, props.searchParams)

  await connect()

  const [staticUrlCodif, { textPage, chapter }] = await Promise.all([
    getCodifStaticUrl(),
    queryContentToDisplay(previewMode, category, textPageSlug, chapterSlug),
  ])
  const appTitleKey = getAppTitleLabelKey(previewMode, category)

  const metadata = generatePageMetadata(
    textPage,
    chapter,
    lang,
    typeof staticUrlCodif?.value === 'string' ? staticUrlCodif.value : '',
    (appTitleKey && getNestedProperty(appTitleKey, getLabels(lang))) || ''
  )
  metadata.openGraph = {
    ...metadata.openGraph,
    images: [
      ...(Array.isArray(metadata.openGraph?.images)
        ? metadata.openGraph.images
        : []),
      ...previousImages,
    ],
  }

  return {
    metadataBase: new URL(process.env.PUBLIC_URL ?? ''),
    ...metadata,
  }
}

export default async function GenericPage(props: {
  params: Promise<IPageParams>
  searchParams: Promise<{ r?: string }>
}) {
  // Redirect using new navigation url
  const params = await props.params
  if (Array.isArray(params.slug) && params.slug.length > 0) {
    redirect(getFrontCompletePath(`/${params.slug.join('/')}`))
  }

  const { category, textPageSlug, chapterSlug, previewMode, lang } =
    await getContext(props.params, props.searchParams)

  await connect()

  const { textPages, textPage, chapter } = await queryContentToDisplay(
    previewMode,
    category,
    textPageSlug,
    chapterSlug
  )
  const catalogData =
    !textPage || textPage.slug !== SpecialPage.CATALOG
      ? null
      : await getCatalogData()

  const tabData = [...generateTabsDataForPages(textPages, Lang.fr)]

  const codifs = await queryCodifs()

  const currentTabIndex =
    (textPage && textPages.findIndex(({ slug }) => slug === textPage.slug)) ?? 0

  return (
    <GenericProviders lang={lang} codifs={codifs}>
      <ErrorBoundary>
        <ToastProvider>
          <Page>
            <GlobalStyle category={category} />
            {AppFontSize && <AppFontSize />}
            <Header isPreview={previewMode} category={category} />

            {tabData.length > 1 && (
              <Tabs
                data={tabData}
                previewMode={previewMode}
                currentTabIndex={currentTabIndex}
              />
            )}

            {!textPage || !chapter ? (
              <PageNotFound />
            ) : (
              <GenericPageContent
                textPages={textPages}
                currentTextPage={textPage}
                chapter={chapter}
                previewMode={previewMode}
                catalogData={catalogData}
              />
            )}
          </Page>
        </ToastProvider>
      </ErrorBoundary>
    </GenericProviders>
  )
}
