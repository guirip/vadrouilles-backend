import { ContentCategory } from 'src/models/common/ContentCategory'

export interface ISocialConfig {
  name: string
  url: string
}

export interface IConfig {
  MY_EMAIL: string
  MY_INSTAGRAM: {
    [ContentCategory.NATURE]: ISocialConfig
    [ContentCategory.GRAFFITI]: ISocialConfig
    [ContentCategory.ABOUT]: ISocialConfig
  }
  MY_YOUTUBE: {
    [ContentCategory.NATURE]: ISocialConfig
    [ContentCategory.GRAFFITI]: ISocialConfig
    [ContentCategory.ABOUT]: ISocialConfig
  }
}

export default {
  MY_EMAIL: 'grigri@hacari.net',

  MY_INSTAGRAM: {
    [ContentCategory.NATURE]: {
      name: 'Vadrouilles.fr',
      url: 'https://www.instagram.com/vadrouilles.fr/',
    },
    [ContentCategory.GRAFFITI]: {
      name: 'Sulpeb',
      url: 'https://www.instagram.com/sulpeb/',
    },
    [ContentCategory.ABOUT]: {
      name: '',
      url: '',
    },
  },

  MY_YOUTUBE: {
    [ContentCategory.NATURE]: {
      name: 'Vadrouilles attentives',
      url: 'https://www.youtube.com/@vadrouillesattentives',
    },
    [ContentCategory.GRAFFITI]: {
      name: 'Beplus graffiti',
      url: 'https://www.youtube.com/@beplus.graffiti',
    },
    [ContentCategory.ABOUT]: {
      name: '',
      url: '',
    },
  },
} as IConfig
