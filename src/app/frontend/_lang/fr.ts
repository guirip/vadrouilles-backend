import config from '@/frontend/_config'
import { Lang } from 'src/models/common/Lang'

export default {
  lang: Lang.fr,
  isoCode: 'fr_FR',
  access_raiseFontSize: 'Augmenter la police de caractères',
  access_reduceFontSize: 'Diminuer la police de caractères',

  nature: 'Vadrouilles Attentives',
  nature_short: 'Vadrouilles',
  goToNatureCategory: 'Aller au contenu nature',

  graffiti: 'Beplus Graffiti',
  graffiti_short: 'Graffiti',
  goToGraffitiCategory: 'Aller au contenu graffiti',

  about: 'À propos',
  about_short: 'À propos',
  goToAboutCategory: 'En savoir plus',

  pickCategory: "Qu'est-ce qui t'intéresse ?",
  for: 'pour',
  loading: 'Chargement...',
  error: 'Flûte, une erreur est survenue 😨',
  pageNotFound: 'Page non trouvée 🤷',
  noContent: 'Aucun contenu 🤷',
  langNotAvailable: "🇫🇷 Désolé, il n'y a pas de version française 🤷",
  commentsToggle: 'Afficher les commentaires',
  enlargePicture: "Agrandir l'image",
  sendMeAnEmail: "M'écrire un email",
  goToInstagram: 'Mon compte Instagram',
  goToYoutube: 'Ma chaine Youtube',

  text: {
    untitled: 'Sans titre',
    nextChapter: 'Chapitre suivant',
    fetchError: 'Cette page ne peut être affichée. 😅',
    preview: 'Preview',
    summary: 'Sommaire',
  },

  natureVideos: 'Vidéos nature 🌳 Vadrouilles attentives',
  natureVideosDescription:
    "Vidéos d'immersions en pleine nature, de quelques heures à quelques semaines. Randonnées à la journée en France, treks en France et à l'étranger.",
  natureVideosError:
    "Oups, erreur lors de l'affichage de la liste des vidéos 😨",

  graffitiVideos: 'Vidéos graffiti 🎨',
  graffitiVideosDescription:
    'Vidéos montrant le processus / making-of de la réalisation de graffiti sur murs et autres supports',
  graffitiVideosError:
    "Oups, erreur lors de l'affichage de la liste des vidéos 😨",

  catalog: {
    pageTitle: "Catalogue d'oeuvres",
    pageDescription:
      "Voici mes oeuvres disponibles à l'achat, j'espère qu'elle vous plairont !",
    tabTitle: 'Oeuvres disponibles',
    fetchError: 'Le contenu du catalogue ne peut être affiché 🤷',
    status: {
      available: 'Disponible',
      booked: 'Réservée',
      sold: 'Vendue',
    },
    noItems: `Le catalogue est vide actuellement, revenez plus tard 🤷🙂`,
    noAvailableItem: "Il n'y a actuellement plus d'oeuvre disponible. 🤷🙂",
    oneAvailableItem: 'Il y a actuellement une seule oeuvre encore disponible.',
    severalAvailableItems: {
      start: 'Il y a actuellement ',
      end: ' oeuvres disponibles.',
    },
    price: '', // 'Prix :',
    dimensions: 'Dimensions',
    technique: 'Technique',
    weight: 'Poids',
    usageTitle: 'Comment acheter une œuvre',
    usageFirstLine: 'Une œuvre vous plaît ? 😍',
    usageSteps: `
  <li>Contactez moi, en me fournissant votre adresse postale et le nom de l'œuvre.</li>
  <li>Je vous réponds en vous procurant les informations pour payer soit par virement bancaire <i class="fas fa-credit-card"></i> soit par paypal <i class="fab fa-paypal"></i>. Note : si vous habitez en Ile-de-France, une remise en main propre avec un paiement en espèces sont possibles</li>
  <li>Lorsque le paiement est effectué, rapidement je vous envoie l'œuvre soigneusement emballée, avec son certificat d'authenticité et une référence pour le suivi postal.</li>
`,
    usageLastLine:
      "Les frais d'envoi pour la France sont inclus. Les oeuvres sont vendues non encadrées.",
    contactLine: "⬇ Une question ? Pour me joindre c'est juste ici ⬇",
  },

  seulsNosCorps: {
    title: 'Seuls nos corps sont confinés',
    intro1:
      "Au cours du premier confinement dû à l'épidémie de COVID-19, j'ai été sollicité par le <b>Projet Saato</b> qui souhaitait vendre des œuvres au profit du fonds d'urgence de l'APHP (CHU d'Ile-de-France, comprenant 39 hôpitaux).",
    intro2:
      "Peindre avec une finalité pareille a été un catalyseur ! Même si le télé-travail limitait mon temps libre j'ai réalisé cette série avec beaucoup de plaisir.",
    intro3:
      'Par la suite la visibilité offerte par le Projet Saato a amené des commandes qui sont venues compléter la série.',
    supportLabel: 'Support :',
    support:
      "pages issues d'un atlas routier de la France trouvé dans un lieu abandonné (édition 1977, échelle 1/500 000), sauf précision contraire",
    artwork: 'Œuvre',
    sizeLabel: 'Dimensions :',
    size: '22x28cm',
    techniqueLabel: 'Technique :',
    technique: 'bic, feutre, pastel, spray, acrylique, posca',
    saatoProject: 'Projet SAATO',
    notoriousInstagramLinkAria: 'Aller au compte instagram Notorious',
    saatoProjectLinkAria: 'Aller au site web du SAATO project',
    orderPossibleLine1: `Si vous verriez bien une peinture de ce style entre vos mains, je suis ouvert aux <b>commandes</b>.`,
    orderPossibleLine2: `<div class="flex align-items-center justify-content-center flex-wrap mt-3">
      <div>Contactez-moi pour en discuter <span class="emoji">🙂</span></div>
      <div class="mx-3">
        <div>→ par <a aria-label="M'écrire un email" href="mailto:${config.MY_EMAIL}"><b>email</b></a></div>
        <div>→ via instagram <a aria-label="Aller sur mon compte Instagram" href="${config.MY_INSTAGRAM.graffiti.url}"><b>@${config.MY_INSTAGRAM.graffiti.name}</b></a></div>
      </div></div>`,
    galleryTitle: 'Galerie',
    processTitle: 'Procédé',
    processText: 'Ci-dessous quelques vidéos type making-of de cette série.',
    status: {
      available: '<b>Disponible</b>',
      seeCatalog: '<b>Disponible (voir catalogue)</b>',
      unavailable: '<b>Non disponible</b>',
      booked: '<b>Réservée</b>',
      sold: '<b>Vendue</b>',
      soldForAPHP: "<b>Vendue au profit de l'APHP</b> 🏥",
      command: '<b>Commande</b>',
    },
    oldParisMapA4: '<br>Ancienne carte de Paris - A4',
    oldParisTarideMap1973: '<br>Ancienne carte Taride de 1973 - A4',
    oldMichelinMap1958: '<br>Ancienne carte Michelin de 1958 - A4',
  },
  month: {
    jan: 'Janvier',
    feb: 'Février',
    mar: 'Mars',
    apr: 'Avril',
    may: 'Mai',
    jun: 'Juin',
    jul: 'Juillet',
    aug: 'Août',
    sep: 'Septembre',
    oct: 'Octobre',
    nov: 'Novembre',
    dev: 'Décembre',
  },
}
