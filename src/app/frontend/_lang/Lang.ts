import type { Lang } from 'src/models/common/Lang'

export interface IAppLabels {
  lang: Lang
  isoCode: string
  [key: string]: unknown
}
