import frLabels from './fr'
import enLabels from './en'
import { isLang, Lang } from 'src/models/common/Lang'

const LANG_LS_KEY = 'lang'

const DEFAULT_LANG = Lang.fr

const labelsPerLang = {
  [Lang.fr]: frLabels,
  [Lang.en]: enLabels,
}

export function getLabels(lang: Lang) {
  return labelsPerLang[lang]
}

export function persistLang(lang: Lang) {
  localStorage.setItem(LANG_LS_KEY, lang)
}

export const getInitialLangValue = (headerLang: string | null) =>
  headerLang && isLang(headerLang) ? headerLang : DEFAULT_LANG

/**
 * Client side only
 */
export function getPersistedLangValue() {
  const paramLang: string | Lang =
    localStorage.getItem(LANG_LS_KEY) || navigator.language.slice(0, 2)
  if (isLang(paramLang)) {
    return paramLang
  }
  return null
}
