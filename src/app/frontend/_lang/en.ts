import config from '@/frontend/_config'
import { Lang } from 'src/models/common/Lang'

export default {
  lang: Lang.en,
  isoCode: 'en_GB',
  access_raiseFontSize: 'Raise font size',
  access_reduceFontSize: 'Reduce font size',

  nature: 'Vadrouilles Attentives',
  nature_short: 'Vadrouilles',
  goToNatureCategory: 'Go to nature content',

  graffiti: 'Beplus graffiti',
  graffiti_short: 'Graffiti',
  goToGraffitiCategory: 'Go to graffiti content',

  about: 'About',
  about_short: 'About',
  goToAboutCategory: 'Fond out more about me',

  pickCategory: 'What are you interested in?',
  for: 'for',
  loading: 'Loading...',
  error: 'Oops, an error occured 😨',
  pageNotFound: 'Page not found 🤷',
  noContent: 'No content 🤷',
  langNotAvailable: '🇬🇧 Sorry, no english version available 🤷',
  commentsToggle: 'Display comments',
  enlargePicture: 'Enlarge picture',
  sendMeAnEmail: 'Send me an email',
  goToInstagram: 'Go to my Instagram page',
  goToYoutube: 'Go to my Youtube channel',

  text: {
    untitled: 'Untitled',
    nextChapter: 'Next chapter',
    fetchError: 'This page cannot be displayed. 😅',
    preview: 'Preview',
    summary: 'Quick access',
  },

  natureVideos: 'Nature videos 🌳',
  natureVideosDescription:
    'Immersive videos of wanders in nature, from day hikes in France to treks abroad.',
  natureVideosError: 'Failed to list nature videos 😨',

  graffitiVideos: 'Graffiti videos 🎨',
  graffitiVideosDescription:
    'Process videos of graffiti performed on walls and on other media also.',
  graffitiVideosError: 'Failed to list graffiti videos 😨',

  catalog: {
    pageTitle: 'Artworks Catalog',
    pageDescription:
      'Here are my available artworks, I hope you will enjoy them!',
    tabTitle: 'Available Artworks',
    fetchError: 'Catalog content is currently unavailable 🤷',
    status: {
      available: 'Available',
      booked: 'Booked',
      sold: 'Sold',
    },
    noItems: 'The catalog is currently empty, please come back later. 🤷🙂',
    noAvailableItem: 'There are no available artworks left. 🤷🙂',
    oneAvailableItem: 'Currently there is only one available artwork left.',
    severalAvailableItems: {
      start: 'There are currently ',
      end: ' available artworks.',
    },
    price: '', // 'Price:',
    dimensions: 'Dimensions',
    technique: 'Technique',
    weight: 'Weight',
    usageTitle: 'How it works',
    usageFirstLine: 'You fell in love with an artwork? 😍',
    usageSteps: `
  <li>Contact me, supplying your postal address and the name of the artwork.</li>
  <li>I provide you the needed information so you can pay using bank transfer <i class="fas fa-credit-card"></i> or paypal <i class="fab fa-paypal"></i>.</li>
  <li>Once the payment is effective, I send you the piece of art with a certificate of authenticity and a reference to track the parcel.</li>
`,
    usageLastLine:
      'Shipping cost outside France are not included. Artworks are sold without frame.',
    contactLine: '⬇ Any question? Please contact me using the links below ⬇',
  },

  seulsNosCorps: {
    title: 'Confinement series',
    intro1:
      "During the first COVID confinement in France, I've been contacted by SAATO Project which was organizing art sales in <b>support of the 39 hospitals</b> around Paris.",
    intro2:
      "To have the opportunity to paint with such a goal was quite a catalyst! Despite remote working I've been able to find enough time to create this series, to my great pleasure.",
    intro3:
      'Subsequently, the visibility offered by the Saato Project brought orders which completed the series.',
    supportLabel: 'Support :',
    support:
      'pages from an old atlas of France found in an abandoned place (1977 edition, scale: 1/500 000), with some exceptions',
    artwork: 'Artwork',
    sizeLabel: 'Size :',
    size: '22x28cm',
    techniqueLabel: 'Technique :',
    technique:
      'bic pen, felt-tip pen, pastel, spray paint, acrylic paint, posca',
    saatoProject: 'SAATO Project',
    notoriousInstagramLinkAria: 'Go to Notorious instagram page',
    saatoProjectLinkAria: 'Go to SAATO project website',
    orderPossibleLine1: `Feel free to contact me on Instagram <a aria-label="Go to my Instagram page" href="${config.MY_INSTAGRAM.graffiti.url}"><b>@${config.MY_INSTAGRAM.graffiti.name}</b></a> or via <a aria-label="Send me an email" href="mailto:${config.MY_EMAIL}"><b>email</b></a> if you are interested in buying or <b>commissioning an artwork</b> <span class="emoji">🙂</span>.`,
    galleryTitle: 'Gallery',
    processTitle: 'Process',
    processText:
      "If you'd like to see how these artworks were created, take a look below. 😉",
    status: {
      available: '<b>Available</b>',
      seeCatalog: '<b>Available (see available artworks)</b>',
      unavailable: '<b>Unavailable</b>',
      booked: '<b>Reserved</b>',
      sold: '<b>Sold</b>',
      soldForAPHP: '<b>Sold to support hospitals</b> 🏥',
      command: '<b>Commissioned</b>',
    },
    oldParisMapA4: '<br>Old Paris map - A4',
    oldParisTarideMap1973: '<br>Old Taride map from 1973 - A4',
    oldMichelinMap1958: '<br>Old Michelin map from 1958 - A4',
  },
  month: {
    jan: 'January',
    feb: 'February',
    mar: 'March',
    apr: 'April',
    may: 'May',
    jun: 'June',
    jul: 'July',
    aug: 'August',
    sep: 'September',
    oct: 'October',
    nov: 'November',
    dev: 'December',
  },
}
