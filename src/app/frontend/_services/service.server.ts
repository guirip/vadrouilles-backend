import { headers } from 'next/headers'
import { getInitialLangValue } from '../_lang'

export async function getInitialLang() {
  const headersList = await headers()
  const headerLang = headersList.get('accept-language')
  return getInitialLangValue(headerLang)
}
