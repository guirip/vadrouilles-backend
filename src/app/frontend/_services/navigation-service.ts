import type { ITabData } from '@/frontend/_components/tabs/tab.type'
import { getNavigateData } from '@/frontend/_types/navigation.types'
import { PREVIEW_ROOT } from 'src/constants'
import type { ContentCategory } from 'src/models/common/ContentCategory'
import type { Lang } from 'src/models/common/Lang'
import type {
  ITextChapterMinimalPopulatedFront,
  ITextChapterPopulatedFront,
} from 'src/models/text/TextChapter.type'
import type {
  ITextPageFront,
  ITextPageMinimalPopulatedFront,
} from 'src/models/text/TextPage.type'
import { getFrontCompletePath } from 'src/utils/util'

const getPageI18nValue = (
  field: 'title' | 'subtitle',
  textPage: ITextPageFront,
  lang: Lang
) => textPage[field]?.[lang] ?? ''

export const generateTabsDataForPages = (
  textPages: ITextPageFront[],
  lang: Lang
) =>
  textPages.map(
    (textPage): ITabData => ({
      key: textPage.slug,
      content: {
        navigate: getNavigateData(textPage),
        title: getPageI18nValue('title', textPage, lang),
        subtitle: getPageI18nValue('subtitle', textPage, lang),
      },
    })
  )

export interface INavigationHistoryState {
  textPageId: string
  chapterId?: string
}

export const isHistoryState = (
  data: PopStateEvent['state']
): data is INavigationHistoryState => typeof data.textPageId === 'string'

export const getRoute = (
  previewMode: boolean,
  category: ContentCategory,
  textPage: ITextPageMinimalPopulatedFront['slug'],
  chapter: ITextChapterPopulatedFront['slug'] | null,
  lang: Lang
) =>
  getFrontCompletePath(
    `/${previewMode ? PREVIEW_ROOT : category}/${textPage}${chapter ? `/${chapter}` : ''}`,
    lang
  )

export const getUrlForPage = (
  previewMode: boolean,
  textPage: ITextPageMinimalPopulatedFront | ITextPageFront,
  chapter:
    | ITextChapterPopulatedFront
    | ITextChapterMinimalPopulatedFront
    | null,
  lang: Lang
) =>
  location.origin +
  getRoute(
    previewMode,
    textPage.category,
    textPage.slug,
    textPage.chapters.length > 1 && chapter ? chapter.slug : null,
    lang
  )
