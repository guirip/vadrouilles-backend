import { ContentCategory } from 'src/models/common/ContentCategory'

export function getAppTitleLabelKey(
  isPreview: boolean,
  category: ContentCategory
) {
  if (isPreview) {
    return 'text.preview'
  }
  if (category === ContentCategory.NATURE) {
    return 'nature'
  }
  if (category === ContentCategory.GRAFFITI) {
    return 'graffiti'
  }
  if (category === ContentCategory.ABOUT) {
    return 'about'
  }
  return ''
}
