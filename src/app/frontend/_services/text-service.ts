import type { Lang } from 'src/models/common/Lang'
import type {
  ILocalizedTextFront,
  ILocalizedTextMinimalFront,
} from 'src/models/text/LocalizedText.type'
import type {
  IBaseTextChapterFront,
  ITextChapterPopulatedFront,
} from 'src/models/text/TextChapter.type'
import type { ITextPageMinimalPopulatedFront } from 'src/models/text/TextPage.type'
import { generateTextMetadata } from 'src/services/TextService.metadata'

type NewType = IBaseTextChapterFront['slug']

export function getChapterIndex(
  text: ITextPageMinimalPopulatedFront,
  chapterSlug?: NewType
) {
  if (!text) {
    return -1
  }
  if (chapterSlug) {
    const chapterIndex = text?.chapters?.findIndex(
      (c) => c.slug === chapterSlug
    )
    if (chapterIndex !== -1) {
      return chapterIndex
    }
  }
  return 0
}

export const matchLang = (ltLang: Lang, lang: Lang) =>
  (ltLang as string) === lang

export const getLocalizedTextByLang = (
  texts: ILocalizedTextFront[],
  lang: Lang
) => texts.find((t) => matchLang(t.lang, lang))

export const findLocalizedTextMinimalByLang = (
  texts: ILocalizedTextMinimalFront[],
  lang: Lang
) => texts.find((t) => matchLang(t.lang, lang))

export const generatePageMetadata = (
  textPage: ITextPageMinimalPopulatedFront | null,
  chapter: ITextChapterPopulatedFront | null,
  lang: Lang,
  staticUrl = '',
  appTitle = ''
) => generateTextMetadata(textPage, chapter, lang, staticUrl, appTitle)
