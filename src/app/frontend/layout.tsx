import { Suspense, type PropsWithChildren } from 'react'

import StyledComponentsRegistry from 'src/app/styled-components-registry'

import 'primeflex/primeflex.css'
import 'primereact/resources/primereact.min.css'
import 'primereact/resources/themes/mdc-dark-indigo/theme.css'

// see https://fontawesome.com/v6/docs/web/use-with/react/use-with#getting-font-awesome-css-to-work
import { config } from '@fortawesome/fontawesome-svg-core'
import '@fortawesome/fontawesome-svg-core/styles.css'
config.autoAddCss = false

import { ContentCategory } from 'src/models/common/ContentCategory'
import { getInitialLang } from './_services/service.server'
import { BG_BY_CATEGORY } from 'src/app/_style/style.constants'
import './_style/style.css'

export default async function RootLayout({
  children,
}: PropsWithChildren<{ params: Promise<unknown> }>) {
  const lang = await getInitialLang()

  return (
    <html
      lang={lang}
      style={{
        backgroundImage: `url('${BG_BY_CATEGORY[ContentCategory.NATURE].src}')`,
        backgroundSize: 'cover',
      }}
    >
      <body>
        <StyledComponentsRegistry>
          <Suspense fallback={<div>...</div>}>{children}</Suspense>
        </StyledComponentsRegistry>
      </body>
    </html>
  )
}
