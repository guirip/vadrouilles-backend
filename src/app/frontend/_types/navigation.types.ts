import type { ContentCategory } from 'src/models/common/ContentCategory'
import type { IBaseTextChapterFront } from 'src/models/text/TextChapter.type'
import type { ITextPageFront } from 'src/models/text/TextPage.type'

export interface INavigationData {
  category: ContentCategory
  textPageId: ITextPageFront['_id']
  textPageSlug: ITextPageFront['slug']
  chapterId?: IBaseTextChapterFront['_id']
  chapterSlug?: IBaseTextChapterFront['slug']
}

export const getNavigateData = (
  textPage: Pick<ITextPageFront, '_id' | 'slug' | 'category'>,
  chapter?: Pick<IBaseTextChapterFront, '_id' | 'slug'>
): INavigationData => ({
  category: textPage.category,
  textPageId: textPage._id,
  textPageSlug: textPage.slug,
  chapterId: chapter?._id,
  chapterSlug: chapter?.slug,
})
