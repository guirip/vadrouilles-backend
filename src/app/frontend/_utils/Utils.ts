export const isMobileDevice = () => 'ontouchstart' in document.documentElement

let isSafariBrowser: null | boolean = null

/**
 * Client side only
 */
export const isSafari = () => {
  if (typeof isSafariBrowser !== 'boolean') {
    isSafariBrowser =
      navigator.userAgent.indexOf('Chrome') === -1 &&
      navigator.userAgent.indexOf('Safari') > -1
  }
  return isSafariBrowser
}

const PX_REGEXP = /^(\d*)px$/

export function getStyleAsInteger(el: HTMLElement, props: string[]) {
  const values: Record<string, number> = {}
  const computed = getComputedStyle(el)
  for (const prop of props) {
    const result = PX_REGEXP.exec(computed.getPropertyValue(prop))
    values[prop] = !result || !result[1] ? 0 : parseInt(result[1], 10)
  }
  return values
}

export function stopEventPropagation(e: React.UIEvent) {
  e.stopPropagation()
}

export function getErrorMessage(error: unknown) {
  const errorMessage =
    error && typeof error === 'object' && 'message' in error
      ? error.message
      : null
  return errorMessage || error
}
