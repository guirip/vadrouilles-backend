'use client'

import type { PropsWithChildren } from 'react'
import { createContext, useContext, useMemo, useState } from 'react'

interface ICodifContext {
  staticUrl: string | null
  catalogNotice: string | null
  catalogDiscount: number | null
}

const CodifContext = createContext<ICodifContext | null>(null)

export function useCodifContext() {
  const context = useContext(CodifContext)
  if (!context) {
    throw new Error('Lang context is not initialized yet')
  }
  return context
}

interface IProps {
  codifs: ICodifContext
}

export const CodifContextProvider = ({
  codifs,
  children,
}: PropsWithChildren<IProps>) => {
  const [staticUrl] = useState(codifs.staticUrl)
  const [catalogNotice] = useState(codifs.catalogNotice)
  const [catalogDiscount] = useState(codifs.catalogDiscount)

  const contextValue = useMemo(
    () => ({
      staticUrl,
      catalogNotice,
      catalogDiscount,
    }),
    [staticUrl, catalogNotice, catalogDiscount]
  )

  return (
    <CodifContext.Provider value={contextValue}>
      {children}
    </CodifContext.Provider>
  )
}
