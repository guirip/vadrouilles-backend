'use client'

import type { PropsWithChildren } from 'react'
import { createContext, useContext, useEffect, useMemo, useState } from 'react'
import type { ICatalogData } from 'src/models/Catalog.type'
import type { IItemsPerCategoryFront } from 'src/models/CatalogItem.type'
import type { ICatalogItemCategoryLean } from 'src/models/CatalogItemCategory'
import type { ICatalogItemCategoryFront } from 'src/models/CatalogItemCategory.type'

interface ICatalogContext {
  categories: ICatalogItemCategoryFront[]
  itemsPerCategory: IItemsPerCategoryFront
  currentTabIndex: number
  setCurrentTabIndex: (index: number) => void
  currentCategory: ICatalogItemCategoryFront | null
}

const CatalogContext = createContext<ICatalogContext | null>(null)

export function useCatalogContext() {
  const context = useContext(CatalogContext)
  if (!context) {
    throw new Error('Catalog context is not initialized yet')
  }
  return context
}

function getCategoryByIndex(
  index: number,
  categories: ICatalogItemCategoryLean[]
) {
  if (Array.isArray(categories) && categories.length + 1 >= index) {
    return categories[index]
  }
  return null
}

const DEFAULT_CAT_INDEX = 0

interface IProps {
  data: ICatalogData | null
}

export function CatalogProvider({ data, children }: PropsWithChildren<IProps>) {
  const [categories, itemsPerCategory] = useMemo(() => {
    const cats = data?.categories ?? []
    const itemsPerCategory = data?.itemsPerCategory ?? {}
    return [
      cats.filter(
        (c) =>
          Array.isArray(itemsPerCategory[c._id]) &&
          itemsPerCategory[c._id].length > 0
      ),
      itemsPerCategory,
    ]
  }, [data])

  const [currentTabIndex, setCurrentTabIndex] = useState(DEFAULT_CAT_INDEX)
  const [currentCategory, setCurrentCategory] =
    useState<ICatalogItemCategoryFront | null>(null)

  useEffect(() => {
    setCurrentCategory(getCategoryByIndex(currentTabIndex, categories))
  }, [categories, currentTabIndex])

  const contextValue = useMemo(
    () => ({
      categories,
      itemsPerCategory,
      currentTabIndex,
      setCurrentTabIndex,
      currentCategory,
    }),
    [categories, currentCategory, currentTabIndex, itemsPerCategory]
  )

  return (
    <CatalogContext.Provider value={contextValue}>
      {children}
    </CatalogContext.Provider>
  )
}
