'use client'

import { getLabels, persistLang } from '@/frontend/_lang'
import { isLang, type Lang } from 'src/models/common/Lang'
import type { PropsWithChildren } from 'react'
import {
  createContext,
  useCallback,
  useContext,
  useMemo,
  useState,
} from 'react'
import type { ContentCategory } from 'src/models/common/ContentCategory'
import { getNestedProperty } from 'src/utils/util'
import { getAppTitleLabelKey } from '../_services/label-service'

export interface ILangContext {
  lang: Lang
  updateCurrentLang: (lang: Lang) => void
  getLabel: (key: string | string[]) => string
  getAppTitle: (isPreview: boolean, category: ContentCategory) => string
}

export const LangContext = createContext<ILangContext | null>(null)

export function useLangContext() {
  const context = useContext(LangContext)
  if (!context) {
    throw new Error('Lang context is not initialized yet')
  }
  return context
}

interface IProps {
  lang: Lang
}

export const LangContextProvider = (props: PropsWithChildren<IProps>) => {
  const [currentLang, setCurrentLang] = useState(props.lang)
  const [labels, setLabels] = useState(getLabels(props.lang))

  const updateCurrentLang = useCallback(function (lang: Lang) {
    if (isLang(lang) !== true) {
      console.error(`Unsupported lang: ${lang}`)
      return
    }
    persistLang(lang)
    setCurrentLang(lang)
    setLabels(getLabels(lang))
  }, [])

  const getLabel = useCallback(
    (key: string | string[]) => getNestedProperty(key, labels) ?? '',
    [labels]
  )

  const getAppTitle = useCallback(
    (isPreview: boolean, category: ContentCategory) => {
      const key = getAppTitleLabelKey(isPreview, category)
      if (!key) {
        return ''
      }
      return getLabel(key)
    },
    [getLabel]
  )

  const contextValue = useMemo(
    () => ({
      lang: currentLang,
      getLabel,
      updateCurrentLang,
      getAppTitle,
    }),
    [currentLang, getLabel, updateCurrentLang, getAppTitle]
  )

  return (
    <LangContext.Provider value={contextValue}>
      {props.children}
    </LangContext.Provider>
  )
}
