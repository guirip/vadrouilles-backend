'use client'

import { createContext, useContext } from 'react'

interface IScrollContext {
  ref?: React.RefObject<HTMLDivElement | null>
}

export const ScrollContext = createContext<IScrollContext>({})

export const useScrollContext = () => useContext(ScrollContext)
