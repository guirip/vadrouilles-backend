import './lobster-font.css'
import './leaguespartan-font.css'

export enum FontFamily {
  LOBSTER = 'Lobster',
  LEAGUESPARTAN = 'Leaguespartan',
}
