import formidable from 'formidable'

const parseMultipartForm = (handler) => (req, res) =>
  new Promise((resolve) => {
    formidable({ multiples: true }).parse(req, async (err, fields, files) => {
      if (err) {
        console.error('Upload error: ', err)
        throw err
      }
      await handler(req, res, fields, files)
      resolve(null)
    })
  })

export default parseMultipartForm
