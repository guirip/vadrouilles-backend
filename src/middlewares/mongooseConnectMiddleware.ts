import { connect } from 'src/services/DbService'

const mongooseConnectMiddleware = (handler) => async (req, res) => {
  await connect()
  await handler(req, res)
}

export default mongooseConnectMiddleware
