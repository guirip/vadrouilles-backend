import { isApiAuthOk } from 'src/services/AuthService'

const authMiddleware = (handler) => async (req, res) => {
  const result = await isApiAuthOk(req, res)
  if (!result) {
    res.status(401).send('')
  } else {
    await handler(req, res)
  }
}

export default authMiddleware
