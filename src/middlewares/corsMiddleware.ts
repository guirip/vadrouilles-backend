import cors from 'cors'

const middleware = cors({
  methods: ['GET'], // only read access
})

const corsMiddleware = (handler) => (req, res) =>
  new Promise((resolve) => {
    middleware(req, res, async () => {
      await handler(req, res)
      resolve(null)
    })
  })

export default corsMiddleware
