import type { Model, Types } from 'mongoose'
import mongoose from 'mongoose'

import config from 'src/config'
import type { IDbItem } from 'src/models/common/DbItem'
import type { IDbStringId } from 'src/models/common/DbItemLean'
import { NotFoundError } from 'src/utils/error'

export async function connect() {
  if (mongoose.connections[0].readyState) {
    // Already connected
    return
  }
  const { HOST, PORT, INSTANCE } = config.DB

  if (process.env.NODE_ENV !== 'test') {
    console.info(`Opening mongoose connection (${INSTANCE})`)
  }

  mongoose.set('strictQuery', true)
  const uri = `mongodb://${HOST}:${PORT}/${INSTANCE}`
  return await mongoose.connect(uri)
}

/**
 * Close the default connection to mongodb
 * @public by pattern
 */
export async function disconnect() {
  if (process.env.NODE_ENV !== 'test') {
    console.info(`Closing mongoose connection`)
  }
  await mongoose.disconnect()
}

export function toSerializable<T>(doc): T {
  const obj = JSON.parse(JSON.stringify(doc))
  delete obj.__v
  return obj
}

export const objectIdsToStrings = (objectIds: Types.ObjectId[]) =>
  (objectIds || []).map((_id) => _id.toString())

/**
 * Simple (not very useful) wrapper, which always run validators
 * @param  {object} model (mongoose model)
 * @param  {object} data
 * @return {object} updated document
 */
export async function updateById<T>(
  model: Model<T>,
  { _id, ...data }: IDbItem | IDbStringId
): Promise<mongoose.HydratedDocument<T>> {
  if (!_id) {
    throw new Error("Can't update: missing document _id")
  }
  // @ts-expect-error see https://github.com/Automattic/mongoose/issues/14459
  const updatedDoc = (await model.findOneAndUpdate({ _id }, data, {
    new: true, // return the object how it is AFTER update
    runValidators: true,
  })) as Promise<mongoose.HydratedDocument<T>>
  if (!updatedDoc) {
    throw new NotFoundError()
  }
  return updatedDoc
}
