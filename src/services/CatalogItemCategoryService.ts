import type { FilterQuery } from 'mongoose'

import { CatalogItemModel } from 'src/models/CatalogItem'
import type { ICatalogItemCategory } from 'src/models/CatalogItemCategory'
import { CatalogItemCategoryModel } from 'src/models/CatalogItemCategory'
import type { StringOrObjectId } from 'src/models/common/DbItem'
import type { IIdOrderMap } from 'src/models/common/IdOrderMap'
import { NotFoundError } from 'src/utils/error'
import { updateById } from './DbService'

export async function checkSlug(
  slug: ICatalogItemCategory['slug'],
  exceptId?: StringOrObjectId
) {
  if (!slug) {
    throw new Error('Slug is mandatory')
  }

  const existingCatWithSameSlug = await CatalogItemCategoryModel.findOne({
    slug,
  })
  if (
    existingCatWithSameSlug &&
    (!exceptId ||
      exceptId.toString() !== existingCatWithSameSlug._id.toString())
  ) {
    throw new Error(`Slug ${slug} already exists`)
  }
}

export function getCategories(query: FilterQuery<ICatalogItemCategory>) {
  return CatalogItemCategoryModel.find(query)
    .sort({ order: 1 })
    .lean<ICatalogItemCategory[]>()
}

export async function createItemCategory(data: Partial<ICatalogItemCategory>) {
  if (!data.slug) {
    throw new Error('Slug is mandatory')
  }
  await checkSlug(data.slug)

  if (typeof data.order === 'number') {
    // Shift order of the existing catalog item categories
    const categories = await CatalogItemCategoryModel.find({
      order: { $gte: data.order },
    })
    await Promise.all(
      categories.map((category) => {
        category.order = category.order + 1
        return category.save()
      })
    )
  }

  return await CatalogItemCategoryModel.create(data)
}

export async function updateItemCategory(
  data: Partial<ICatalogItemCategory> & { _id: StringOrObjectId }
) {
  if (data.slug) {
    await checkSlug(data.slug, data._id)
  }

  const category = await CatalogItemCategoryModel.findById(data._id)
  if (!category) {
    throw new NotFoundError()
  }

  return await updateById<ICatalogItemCategory>(CatalogItemCategoryModel, data)
}

export async function updateOrder(idOrderMap: IIdOrderMap) {
  const categories = await CatalogItemCategoryModel.find({
    _id: {
      $in: Object.keys(idOrderMap),
    },
  })
  await Promise.all(
    categories.map((category) => {
      category.order = idOrderMap[category._id.toString()]
      return category.save()
    })
  )
}

export async function deleteItemCategory(_id: StringOrObjectId) {
  const category = await CatalogItemCategoryModel.findById(_id)
  if (!category) {
    throw new NotFoundError()
  }

  // Unset category for the related catalog items
  await CatalogItemModel.updateMany(
    { category: _id },
    { $unset: { category: '' } }
  )

  await category.deleteOne()
}
