import type { FilterQuery, HydratedDocument } from 'mongoose'

import config from 'src/config'
import type { ICatalogItem, ICatalogItemLean } from 'src/models/CatalogItem'
import { CatalogItemModel } from 'src/models/CatalogItem'
import type { StringOrObjectId } from 'src/models/common/DbItem'
import type { IFile } from 'src/models/common/File.type'
import { FILE_FIELDS } from 'src/models/common/File.type'
import type { IIdOrderMap } from 'src/models/common/IdOrderMap'
import { NotFoundError } from 'src/utils/error'
import { toSerializable, updateById } from './DbService'
import { deleteFile } from './FileService'

export function getItems(query: FilterQuery<ICatalogItem>) {
  return CatalogItemModel.find(query)
    .sort({ order: 1 })
    .lean<ICatalogItemLean[]>()
}

export async function createItem(itemFields: Partial<ICatalogItem>) {
  if (typeof itemFields.order !== 'number') {
    itemFields.order = 0
  }

  // Increment `order` for every existing item
  const items = await CatalogItemModel.find({
    order: { $gte: itemFields.order },
  })
  const savePromises = items.map((item) => {
    item.order = item.order + 1
    return item.save()
  })
  await Promise.all(savePromises)

  const itemToCreate = new CatalogItemModel({
    ...itemFields,
    createDate: new Date(),
  })

  return await itemToCreate.save()
}

export async function updateItem(
  data: Partial<ICatalogItem> & { _id: StringOrObjectId }
) {
  const item = await CatalogItemModel.findById(data._id)
  if (!item) {
    throw new NotFoundError()
  }
  data.updateDate = new Date()

  return await updateById(CatalogItemModel, data)
}

export async function updateOrder(idOrderMap: IIdOrderMap) {
  const items = await CatalogItemModel.find({
    _id: {
      $in: Object.keys(idOrderMap),
    },
  })
  await Promise.all(
    items.map((item) => {
      item.order = idOrderMap[item._id.toString()]
      return item.save()
    })
  )
}

export async function deleteItem(id: StringOrObjectId) {
  const itemToDelete = await CatalogItemModel.findById(id)
  if (!itemToDelete) {
    throw new NotFoundError()
  }

  // Delete uploaded files
  if (Array.isArray(itemToDelete.files)) {
    for (const file of itemToDelete.files) {
      deleteFile(`${config.UPLOAD_DEST}/${file.orig}`)
      deleteFile(`${config.UPLOAD_DEST}/${file.mid}`)
      deleteFile(`${config.UPLOAD_DEST}/${file.low}`)
    }
  }

  return await itemToDelete.deleteOne()
}

interface IItemFileIndex {
  item: HydratedDocument<ICatalogItem>
  files: IFile[]
  fileIndex: number
}

/**
 * Common logic to find and locate a specific file belonging to a catalog item
 */
async function retrieveItemAndFileIndex(
  id: StringOrObjectId,
  lowName: string
): Promise<IItemFileIndex> {
  const item = await CatalogItemModel.findById(id)
  if (!item) {
    throw new NotFoundError()
  }

  const files = item.files?.map((f) => toSerializable<IFile>(f)) || []

  const fileIndex = files.findIndex((file) => file.low === lowName)
  if (fileIndex === -1) {
    throw new NotFoundError(`file not found ${lowName}`)
  }

  return {
    item,
    files,
    fileIndex,
  }
}

export async function setMainFile(
  catalogItemId: StringOrObjectId,
  lowFileName: string
) {
  const { item, files } = await retrieveItemAndFileIndex(
    catalogItemId,
    lowFileName
  )

  item.set({
    files: files.map((file) => ({
      ...file,
      isMain: file.low === lowFileName,
    })),
  })
  return await item.save()
}

export async function deleteItemFile(
  catalogItemId: StringOrObjectId,
  lowFileName: string
) {
  const { item, files, fileIndex } = await retrieveItemAndFileIndex(
    catalogItemId,
    lowFileName
  )

  // Delete from file system
  const fileToDelete = files[fileIndex]
  for (const fileField of FILE_FIELDS) {
    const fileName = fileToDelete[fileField]
    deleteFile(`${config.UPLOAD_DEST}/${fileName}`)
  }

  // Update db
  item.set({
    files: [...files.slice(0, fileIndex), ...files.slice(fileIndex + 1)],
  })
  return await item.save()
}
