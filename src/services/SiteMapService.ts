import type { MetadataRoute } from 'next'
import { ContentCategory } from 'src/models/common/ContentCategory'
import type {
  ITextChapterMinimalPopulatedFront,
  ITextChapterPopulatedFront,
} from 'src/models/text/TextChapter.type'
import { TextPageModel } from 'src/models/text/TextPage'
import type { ITextPageMinimalPopulatedFront } from 'src/models/text/TextPage.type'
import { getRouteOld } from 'src/utils/util'

const DEBUG = false

type SiteMapItem = MetadataRoute.Sitemap[number]

type ICategoryPathParams = Omit<SiteMapItem, 'url'> & {
  categoryName: string
}
type IPagePathParams = Omit<SiteMapItem, 'url'> & {
  textPage: ITextPageMinimalPopulatedFront
  chapter?:
    | ITextChapterPopulatedFront
    | ITextChapterMinimalPopulatedFront
    | null
}

type IPathParams = ICategoryPathParams | IPagePathParams

export const isCategoryPathParams = (
  pathParam: IPathParams
): pathParam is ICategoryPathParams =>
  pathParam['categoryName'] && !pathParam['textPage']

const logPathParams = (pathParams: IPathParams[]) => {
  for (const param of pathParams) {
    if (isCategoryPathParams(param)) {
      console.log(`${param.categoryName}`)
    } else {
      console.log(
        `${param.textPage.category}.${param.textPage.slug}.${param.chapter?.slug ?? 'root'}`
      )
    }
  }
}

interface ICategoryData {
  name: string
  lastModified: number
}

export async function getPathParams() {
  const pathParams: IPathParams[] = []

  const categories: Record<string, ICategoryData> = Object.values(
    ContentCategory
  ).reduce((acc, cat) => {
    acc[cat] = {
      name: String(cat),
      lastModified: 0,
    }
    return acc
  }, {})

  const pages = await TextPageModel.find({
    visible: true,
    'chapters.0': { $exists: true },
  })
    .sort({ order: 1 })
    .populate({
      path: 'chapters',
      match: {
        visible: true,
        'localizedTexts.0': { $exists: true },
      },
      populate: {
        path: 'localizedTexts',
        select: '-content.blocks',
      },
    })
    .lean<ITextPageMinimalPopulatedFront[]>()

  pages.forEach((page) => {
    const cat = String(page.category)
    let nonEmptyChapterCount = 0

    page.chapters.forEach((chapter) => {
      const lastModified = Math.max(
        ...page.chapters[0].localizedTexts
          .filter((lt) => lt.content?.time)
          .map((lt) => lt.content.time)
      )
      if (lastModified < 0) {
        return
      }
      nonEmptyChapterCount++

      // Add each chapter route
      pathParams.push({
        textPage: page,
        chapter,
        lastModified: new Date(lastModified),
        // changeFrequency: 'monthly',
      })
      if (categories[cat].lastModified < lastModified) {
        categories[cat].lastModified = lastModified
      }
    })

    if (nonEmptyChapterCount > 1) {
      // Add page root
      pathParams.push({
        textPage: page,
        lastModified: new Date(page.chapters[0].localizedTexts[0].content.time),
        // changeFrequency: 'monthly',
      })
    }
  })

  // Add categories root
  Object.values(categories).forEach((catData) => {
    if (catData.lastModified) {
      pathParams.push({
        categoryName: catData.name,
        lastModified: new Date(catData.lastModified),
        // changeFrequency: 'monthly', // weekly?
      })
    }
  })

  if (DEBUG) {
    logPathParams(pathParams)
  }

  return pathParams
}

export async function generateSiteMap(baseUrl: string) {
  const pathParams: IPathParams[] = await getPathParams()

  const sitemap: MetadataRoute.Sitemap = pathParams.map((pathParam) => {
    if (isCategoryPathParams(pathParam)) {
      const { categoryName, ...restParams } = pathParam
      return {
        url: `${baseUrl}/${categoryName}`,
        ...restParams,
      }
    }
    const { textPage, chapter, ...restParams } = pathParam
    return {
      url: `${baseUrl}${getRouteOld(textPage.category, textPage.slug, chapter?.slug ?? null)}`,
      ...restParams,
    }
  })

  return sitemap
}
