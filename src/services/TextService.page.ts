import type { FilterQuery, HydratedDocument, PopulateOptions } from 'mongoose'

import { PREVIEW_ROOT } from 'src/constants'
import {
  type ContentCategory,
  isValidCategory,
} from 'src/models/common/ContentCategory'
import type { StringOrObjectId } from 'src/models/common/DbItem'
import type { IIdOrderMap } from 'src/models/common/IdOrderMap'
import { Lang } from 'src/models/common/Lang'
import type { IImageBlock } from 'src/models/text/TextBlock.type'
import { isImageBlock } from 'src/models/text/TextBlock.type'
import type {
  ITextChapter,
  ITextChapterPopulated,
} from 'src/models/text/TextChapter'
import type { ITextPage } from 'src/models/text/TextPage'
import { TextPageModel } from 'src/models/text/TextPage'
import {
  DEFAULT_TEXT_CATEGORY,
  type ITextPageFront,
  type ITextPageMinimalPopulatedFront,
} from 'src/models/text/TextPage.type'
import { NotFoundError } from 'src/utils/error'
import { isArray } from 'src/utils/tsUtil'
import { toSerializable, updateById } from './DbService'
import { getFullChapter } from './TextService.chapter'

export async function getFullPage(
  pageCriteria: FilterQuery<ITextPage> = {},
  onlyVisible: boolean
) {
  if (!pageCriteria._id && !pageCriteria.slug) {
    return null
  }

  if (onlyVisible) {
    pageCriteria.visible = true
  }
  pageCriteria['chapters.0'] = { $exists: true }

  const chapterCriteria: PopulateOptions['match'] = {
    'localizedTexts.0': { $exists: true },
  }
  if (onlyVisible) {
    chapterCriteria.visible = true
  }

  const textPage = await TextPageModel.findOne(pageCriteria)
    .populate<{
      chapters: HydratedDocument<ITextChapterPopulated>[]
    }>({
      path: 'chapters',
      match: chapterCriteria,
      options: { sort: { order: 1 } },
      populate: {
        path: 'localizedTexts',
        select: '-content.blocks',
      },
    })
    .lean()

  return !textPage
    ? null
    : toSerializable<ITextPageMinimalPopulatedFront>(textPage)
}

export function getDefaultChapterCriteria(
  textPage: ITextPageMinimalPopulatedFront | null
) {
  if (textPage && isArray(textPage.chapters) && textPage.chapters.length > 0) {
    return { _id: textPage.chapters[0]._id } as FilterQuery<ITextChapter>
  }
  return undefined
}

export async function getPageAndPopulatedChapter(
  pageCriteria: FilterQuery<ITextPage> = {},
  chapterCriteria: FilterQuery<ITextChapter> | null,
  onlyVisible: boolean
) {
  const textPage = await getFullPage(pageCriteria, onlyVisible)

  const cCriteria = chapterCriteria ?? getDefaultChapterCriteria(textPage)
  if (cCriteria?.slug && !cCriteria._id) {
    cCriteria._id = textPage?.chapters.find(
      ({ slug }) => slug === cCriteria.slug
    )?._id
  }
  let chapter = await getFullChapter(cCriteria, onlyVisible)

  if (textPage && chapter && chapterCriteria) {
    const chapterSlug = chapter.slug
    // ensure chapter belongs to page
    if (!textPage.chapters.find((c) => c.slug === chapterSlug)) {
      chapter = null
    }
  }

  return { textPage, chapter }
}

export const getPages = async (
  criteria: FilterQuery<ITextPage> = {},
  onlyVisible: boolean = true
) => {
  if (onlyVisible) {
    criteria.visible = true
  }
  const pages = await TextPageModel.find(criteria)
    .sort({ order: 1 })
    .lean<ITextPage[]>()

  return pages.map((p) => toSerializable<ITextPageFront>(p))
}

export const createPage = (data: Partial<ITextPage>) =>
  TextPageModel.create(data)

export function updatePage(
  data: Partial<ITextPage> & { _id: StringOrObjectId }
) {
  return updateById(TextPageModel, data)
}

export async function updateOrder(idOrderMap: IIdOrderMap) {
  const pages = await TextPageModel.find({
    _id: {
      $in: Object.keys(idOrderMap),
    },
  })
  await Promise.all(
    pages.map((page) => {
      page.order = idOrderMap[page._id.toString()]
      return page.save()
    })
  )
}

export async function listImageBlocks(_id: string | ITextPage['_id']) {
  const textPage = await TextPageModel.findById(_id)
    .select(['title', 'chapters'])
    .populate({
      path: 'chapters',
      select: ['slug', 'localizedTexts', 'order'],
      populate: {
        path: 'localizedTexts',
        select: ['title', 'lang', 'content'],
      },
    })
    .lean()
  if (!textPage) {
    throw new Error(`No textpage found matching id ${_id}`)
  }

  return {
    _id: textPage._id.toString(),
    title: textPage.title,
    imagesByChapter: (
      textPage.chapters as unknown as ITextChapterPopulated[]
    ).reduce((acc, chapter) => {
      let title = ''
      const images: IImageBlock[] = []

      for (const localizedText of chapter.localizedTexts) {
        if (!title || localizedText.lang === Lang.en) {
          title = localizedText.title
        }
        if (Array.isArray(localizedText.content?.blocks)) {
          const blocks = images.concat(
            localizedText.content.blocks.filter(isImageBlock)
          )
          for (const block of blocks) {
            if (!images.find(({ id }) => id === block.id)) {
              images.push(block)
            }
          }
        }
      }
      acc[chapter._id.toString()] = {
        title: title ?? '?',
        slug: chapter.slug,
        order: chapter.order,
        images,
      }
      return acc
    }, {}),
  }
}

export async function deletePage(_id: string | ITextPage['_id']) {
  const page = await TextPageModel.findById(_id).populate<{
    chapters: HydratedDocument<ITextChapter>[]
  }>('chapters')

  if (!page) {
    throw new NotFoundError('Text page not found')
  }
  if (Array.isArray(page.chapters) && page.chapters.length > 0) {
    throw new Error('Cannot delete page with chapters attached')
  }
  return await TextPageModel.deleteOne({ _id })
}

export function parseCategorySlug(categoryParam: string | undefined) {
  const previewMode = categoryParam === PREVIEW_ROOT
  let category = DEFAULT_TEXT_CATEGORY

  if (categoryParam && isValidCategory(categoryParam)) {
    category = categoryParam
  }
  return {
    previewMode,
    category,
  }
}

function getPageCriteria(
  slug: string | undefined,
  textPages: ITextPageFront[]
) {
  if (slug) {
    return { slug }
  }
  if (textPages.length > 0) {
    return { _id: textPages[0]._id }
  }
  return undefined
}

export async function queryContentToDisplay(
  previewMode: boolean,
  category: ContentCategory,
  textPageSlug?: string,
  chapterSlug?: string
) {
  const onlyVisible = previewMode ? false : true

  const textPages = await getPages(
    {
      category,
    },
    onlyVisible
  )
  let { textPage, chapter } = await getPageAndPopulatedChapter(
    getPageCriteria(textPageSlug, textPages),
    chapterSlug ? { slug: chapterSlug } : null,
    onlyVisible
  )

  // Handle fallback
  if (!textPage && textPages.length > 0) {
    const fallback = await getPageAndPopulatedChapter(
      { _id: textPages[0]._id },
      null,
      onlyVisible
    )
    textPage = fallback.textPage
    chapter = fallback.chapter
  }
  if (!chapter && textPage && textPage.chapters.length > 0) {
    chapter = await getFullChapter(
      { _id: textPage.chapters[0]._id },
      onlyVisible
    )
  }

  return {
    textPages,
    textPage,
    chapter,
  }
}
