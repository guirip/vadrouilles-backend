import type { ILocalizedText } from 'src/models/text/LocalizedText'
import { LocalizedTextModel } from 'src/models/text/LocalizedText'
import type { Lang } from 'src/models/common/Lang'
import type {
  IBlock,
  IButtonBlock,
  IHeaderBlock,
  IImageBlock,
  IListBlock,
  INestedListItem,
  IParagraphBlock,
  ISummaryBlock,
  ITableBlock,
  IVideoBlock,
} from 'src/models/text/TextBlock.type'
import {
  BlockType,
  isButtonBlock,
  isHeaderBlock,
  isImageBlock,
  isListBlock,
  isParagraphBlock,
  isTableBlock,
  isVideoBlock,
} from 'src/models/text/TextBlock.type'
import { translate } from 'src/utils/translation'
import { prepareSummaryBlock } from './TextService.localizedText'

export async function translateParagraphBlock(
  block: IParagraphBlock,
  srcLang: Lang,
  targetLang: Lang
): Promise<IParagraphBlock> {
  const enBlock = structuredClone(block)
  const frValue = block.data.text

  if (frValue.trim()) {
    enBlock.data.text = (
      await translate(frValue, srcLang, targetLang)
    ).translations[0].text
  }
  return enBlock
}

export async function translateHeaderBlock(
  block: IHeaderBlock,
  srcLang: Lang,
  targetLang: Lang
): Promise<IHeaderBlock> {
  const enBlock = structuredClone(block)
  const frValue = block.data.text

  if (frValue.trim()) {
    enBlock.data.text = (
      await translate(frValue, srcLang, targetLang)
    ).translations[0].text
  }
  return enBlock
}

export async function translateButtonBlock(
  block: IButtonBlock,
  srcLang: Lang,
  targetLang: Lang
): Promise<IButtonBlock> {
  const enBlock = structuredClone(block)
  const frValue = block.data.label

  if (frValue.trim()) {
    enBlock.data.label = (
      await translate(frValue, srcLang, targetLang)
    ).translations[0].text
  }
  return enBlock
}

export async function translateImageBlock(
  block: IImageBlock,
  srcLang: Lang,
  targetLang: Lang
): Promise<IImageBlock> {
  const enBlock = structuredClone(block)
  const frValue = block.data.caption

  if (frValue.trim()) {
    enBlock.data.caption = (
      await translate(frValue, srcLang, targetLang)
    ).translations[0].text
  }
  return enBlock
}

export async function translateVideoBlock(
  block: IVideoBlock,
  srcLang: Lang,
  targetLang: Lang
): Promise<IVideoBlock> {
  const enBlock = structuredClone(block)
  const frValue = block.data.description

  if (frValue.trim()) {
    enBlock.data.description = (
      await translate(frValue, srcLang, targetLang)
    ).translations[0].text
  }
  return enBlock
}

const translateListItems = async (
  items: INestedListItem[],
  srcLang: Lang,
  targetLang: Lang
) =>
  await Promise.all(
    items.map((item) => translateListItem(item, srcLang, targetLang))
  )

const translateListItem = async (
  item: INestedListItem,
  srcLang: Lang,
  targetLang: Lang
) => ({
  content:
    typeof item.content !== 'string' || !item.content
      ? ''
      : (await translate(item.content, srcLang, targetLang)).translations[0]
          .text,

  items: await translateListItems(item.items, srcLang, targetLang),
})

export async function translateListBlock(
  block: IListBlock,
  srcLang: Lang,
  targetLang: Lang
): Promise<IListBlock> {
  const { data, ...restBlock } = block

  return {
    ...restBlock,
    data: {
      ...data,
      items: await translateListItems(data.items, srcLang, targetLang),
    },
  }
}

export async function translateTableBlock(
  block: ITableBlock,
  srcLang: Lang,
  targetLang: Lang
): Promise<ITableBlock> {
  const {
    data: { content, ...restData },
    ...restBlock
  } = block

  const translatedContent: ITableBlock['data']['content'] = []

  for (const row of content) {
    const translatedRow: string[] = []
    for (const cell of row) {
      const result = await translate(cell, srcLang, targetLang)
      translatedRow.push(
        result.translations?.length ? result.translations[0].text : ''
      )
    }
    translatedContent.push(translatedRow)
  }

  return {
    ...restBlock,
    data: {
      ...restData,
      content: translatedContent,
    },
  }
}

async function translateBlocks(
  blocks: IBlock[] = [],
  srcLang: Lang,
  targetLang: Lang
) {
  const translatedBlocks: IBlock[] = []

  for (const block of blocks) {
    if (isHeaderBlock(block)) {
      translatedBlocks.push(
        await translateHeaderBlock(block, srcLang, targetLang)
      )
    } else if (isImageBlock(block)) {
      translatedBlocks.push(
        await translateImageBlock(block, srcLang, targetLang)
      )
    } else if (isVideoBlock(block)) {
      translatedBlocks.push(
        await translateVideoBlock(block, srcLang, targetLang)
      )
    } else if (isListBlock(block)) {
      translatedBlocks.push(
        await translateListBlock(block, srcLang, targetLang)
      )
    } else if (isParagraphBlock(block)) {
      translatedBlocks.push(
        await translateParagraphBlock(block, srcLang, targetLang)
      )
    } else if (isTableBlock(block)) {
      translatedBlocks.push(
        await translateTableBlock(block, srcLang, targetLang)
      )
    } else if (isButtonBlock(block)) {
      translatedBlocks.push(
        await translateButtonBlock(block, srcLang, targetLang)
      )
    } else {
      // No translation for the other blocks
      translatedBlocks.push(block)
    }
  }
  return translatedBlocks
}

export async function createTranslatedText(
  srcLt: ILocalizedText,
  targetLang: Lang
) {
  // Translate title and description
  const [title, description] = (
    await Promise.all(
      [srcLt.title, srcLt.description].map((str) =>
        translate(str, srcLt.lang, targetLang)
      )
    )
  ).map(({ translations }) =>
    translations.length > 0 ? translations[0].text : ''
  )

  // Translate blocks
  const translatedBlocks = await translateBlocks(
    srcLt.content.blocks,
    srcLt.lang,
    targetLang
  )

  // Translate summary
  const summaryBlockIndex = translatedBlocks.findIndex(
    ({ type }) => type === BlockType.Summary
  )
  if (summaryBlockIndex > -1) {
    translatedBlocks[summaryBlockIndex] = prepareSummaryBlock(
      translatedBlocks[summaryBlockIndex] as ISummaryBlock,
      translatedBlocks
    )
  }

  return await LocalizedTextModel.create({
    lang: targetLang,
    title,
    description,
    content: {
      time: Date.now(),
      version: srcLt.content.version,
      blocks: translatedBlocks,
    },
  })
}
