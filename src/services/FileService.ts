import type formidable from 'formidable'
import fs from 'fs'
import sharp from 'sharp'
import util from 'util'

import appConfig from 'src/config'
import type { ICodif } from 'src/models/Codif'
import { CodifModel } from 'src/models/Codif'
import type { IFile } from 'src/models/common/File.type'
import { getUploadedImageUrl } from './TextService.localizedText'

// (Doesn't make a lot of sense)
const DEFAULT_LANDSCAPE_VALUE = false

const FILENAME_REGEXP = /^(.*)(\.[\w]*)$/

const {
  UPLOAD_DEST,
  MID_SIZE,
  LOW_SIZE,
  MID_QUALITY,
  LOW_QUALITY,
  DEST_EXTENSION,
} = appConfig

const stat = util.promisify(fs.stat)
const copyFile = util.promisify(fs.copyFile)

interface IProcessStatus {
  outputs: IFile[]
  errors: string[]
}

async function processFormidableFiles(files: formidable.File[], next) {
  const outputs: IFile[] = [],
    errors: string[] = []

  console.log('-------------------------')
  console.log('-------------------------')
  console.log('Files: ', files.map((f) => f.filepath).join(', '))

  const staticUrlCodif = await CodifModel.findOne({
    name: 'static-url',
  }).lean<ICodif>()
  if (!staticUrlCodif) {
    throw new Error('Could not find `static-url` codif')
  }

  for (const file of files) {
    try {
      const output = await processFormidableImage(
        file,
        staticUrlCodif.value as string
      )
      if (output) {
        outputs.push(output)
      }
      console.log('-------------------------')
    } catch (e) {
      console.error('Image processing failed', e)
      errors.push(e.message)
    }
  }

  next({ outputs, errors })
}

/**
 * Set up original, mid, and low versions for each image in the array
 * @param {array} files
 */
export const processImages = (
  files: formidable.File[]
): Promise<IProcessStatus> =>
  new Promise(function (resolve) {
    processFormidableFiles(files, resolve)
  })

function parseNamesAndPaths(fileName: string, staticUrl: string) {
  const result = FILENAME_REGEXP.exec(fileName)
  if (!result) {
    throw new Error(`Failed to parse file name for: ${fileName}`)
  }
  const [, filename] = result
  const basename = `${filename}-${new Date().getTime()}`

  const midName = `${basename}-mid.${DEST_EXTENSION}`
  const names: IFile = {
    orig: `${basename}-orig.${DEST_EXTENSION}`,
    mid: midName,
    low: `${basename}-low.${DEST_EXTENSION}`,
    url: getUploadedImageUrl(staticUrl, midName), // for editor.js but not meant to be persisted
  }

  const paths = {
    orig: `${UPLOAD_DEST}/${names.orig}`,
    mid: `${UPLOAD_DEST}/${names.mid}`,
    low: `${UPLOAD_DEST}/${names.low}`,
  }

  return {
    names,
    paths,
  }
}

async function saveImage(
  image: sharp.Sharp,
  size: number,
  quality: number,
  destPath: string
) {
  const metadata = await image.metadata()
  const isPng = metadata.format === 'png'

  // Compute resize
  const isLandscape =
    typeof metadata.width === 'number' && typeof metadata.height === 'number'
      ? metadata.width > metadata.height
      : DEFAULT_LANDSCAPE_VALUE

  image.rotate() // automatic and based on EXIF Orientation tag

  // Resize
  if (isLandscape) {
    if (metadata.width === undefined) {
      console.warn('sharp metadata.width is undefined')
    }
    if (typeof metadata.width !== 'number' || metadata.width > size) {
      image.resize({ width: size })
    }
  } else {
    // portrait
    if (metadata.height === undefined) {
      console.warn('sharp metadata.height is undefined')
    }
    if (typeof metadata.height !== 'number' || metadata.height > size) {
      image.resize({ height: size })
    }
  }

  if (isPng) {
    image.png({ quality })
  } else {
    image.jpeg({ quality })
  }
  await image.toFile(destPath)
}

/**
 * Set up original, mid, and low versions for an image
 * @param  {object} file
 */
export async function processImage(
  fileName: string,
  filePath: string,
  staticUrl: string
): Promise<null | IFile> {
  const { names, paths } = parseNamesAndPaths(fileName, staticUrl)

  console.log('orig path: ' + paths.orig)
  console.log('mid path: ' + paths.mid)
  console.log('low path: ' + paths.low)

  const inputStats = await stat(filePath)
  console.log(
    `before rename. input exists? (${filePath}) ${inputStats.isFile()}`
  )

  // Move
  await copyFile(filePath, paths.orig)

  const outputStats = await stat(paths.orig)
  console.log(
    `after rename. output exists? (${paths.orig}) ${outputStats.isFile()}`
  )
  const image = sharp(paths.orig)
  console.log(`Has opened file at orig path ${paths.orig}`)

  // Create mid quality image
  await saveImage(image, MID_SIZE, MID_QUALITY, paths.mid)
  console.log(`Has created mid size (${paths.mid})`)

  // Create low quality image
  await saveImage(image, LOW_SIZE, LOW_QUALITY, paths.low)
  console.log(`Has created low size (${paths.low})`)

  return names
}

/**
 * Set up original, mid, and low versions for an image
 * @param  {object} file
 */
async function processFormidableImage(
  file: formidable.File,
  staticUrl: string
): Promise<null | IFile> {
  return await processImage(
    file.originalFilename ?? '',
    file.filepath,
    staticUrl
  )
}

/**
 * Delete a file (no error/exception if the file does not exist)
 * @param {string} path
 */
export const deleteFile = (path: string) => {
  try {
    fs.unlinkSync(path)
  } catch (e) {
    if (e.code !== 'ENOENT') {
      console.warn(`Failed to remove file at ${path}`, e)
    }
  }
}
