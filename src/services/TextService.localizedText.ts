import type { FilterQuery, HydratedDocument } from 'mongoose'

import type { StringOrObjectId } from 'src/models/common/DbItem'
import type {
  ILocalizedText,
  ILocalizedTextMinimal,
} from 'src/models/text/LocalizedText'
import { LocalizedTextModel } from 'src/models/text/LocalizedText'
import type {
  ILocalizedTextFront,
  ILocalizedTextMinimalFront,
} from 'src/models/text/LocalizedText.type'
import { isTextContent } from 'src/models/text/LocalizedText.type'
import type { IBlock, ISummaryBlock } from 'src/models/text/TextBlock.type'
import {
  isExistingImageBlock,
  isHeaderBlock,
  isImageBlock,
  isSummaryBlock,
} from 'src/models/text/TextBlock.type'
import type {
  ITextChapter,
  ITextChapterPopulated,
} from 'src/models/text/TextChapter'
import { TextChapterModel } from 'src/models/text/TextChapter'
import { NotFoundError } from 'src/utils/error'
import { objectIdsToStrings, toSerializable, updateById } from './DbService'
import { createTranslatedText } from './TextTranslateService'
import type { Lang } from 'src/models/common/Lang'

export const getUploadedImageUrl = (staticUrl: string, imageName: string) =>
  `${staticUrl}/${imageName}`

export const getLocalizedTextByLang = (
  texts: ILocalizedText[] | ILocalizedTextFront[],
  lang: 'fr' | 'en'
) => texts.find((t) => String(t.lang) === String(lang))

const toMinimalLt = <T>({ _id, content, ...rest }) =>
  ({
    ...rest,
    _id: _id.toString(),
    blocksCount: (content?.blocks || []).length,
  }) as T

export const toMinimalLocalizedText = (lt) =>
  toMinimalLt<ILocalizedTextMinimal>(lt)

export const toMinimalLocalizedTextFront = (lt) =>
  toMinimalLt<ILocalizedTextMinimalFront>(lt)

export async function getLocalizedTextsByChapter(
  criteria: FilterQuery<ITextChapter>
  // skipContent?: boolean
) {
  const chapter = await TextChapterModel.findOne(criteria)
    .populate('localizedTexts')
    .lean<ITextChapterPopulated>()
    .exec()
  if (!chapter) {
    return []
  }
  let localizedTexts: ILocalizedText[] = []
  if (Array.isArray(chapter.localizedTexts)) {
    localizedTexts = chapter.localizedTexts.map((lt) => {
      const _lt = toSerializable<ILocalizedText>(lt)
      /*
      if (skipContent) {
        return toMinimalLocalizedText(_lt)
      }
      */
      return _lt
    })
  }
  return localizedTexts
}

export async function createLocalizedText(
  chapterId: StringOrObjectId,
  ltData: Partial<ILocalizedText>
) {
  const chapter = await TextChapterModel.findById(chapterId).populate<{
    localizedTexts: HydratedDocument<ILocalizedText>[]
  }>('localizedTexts')

  if (!chapter) {
    throw new Error('No chapter matches id ' + chapterId)
  }
  if (!ltData.lang) {
    throw new Error('Missing `lang` property')
  }
  if (getLocalizedTextByLang(chapter.localizedTexts, ltData.lang)) {
    throw new Error(
      'This chapter already has a localized text for lang ' + ltData.lang
    )
  }
  if (ltData.content && Object.keys(ltData.content).length === 0) {
    delete ltData.content
  }
  const createdLocalizedText = await LocalizedTextModel.create(ltData)

  chapter.localizedTexts.push(createdLocalizedText)
  await chapter.save()

  return createdLocalizedText
}

export async function duplicateLocalizedText(ltId: StringOrObjectId) {
  const lt = await LocalizedTextModel.findById(ltId).lean()
  if (!lt) {
    throw new Error(`Unable to find localized text to duplicate (id: ${ltId})`)
  }

  const ltData: Partial<ILocalizedText> = { ...lt }
  delete ltData._id
  ltData.title = (ltData.title ?? '') + ' - COPY'

  return await LocalizedTextModel.create(ltData)
}

export function prepareSummaryBlock(block: ISummaryBlock, allBlocks: IBlock[]) {
  return {
    ...block,
    data: {
      ...block.data,
      headers: allBlocks.filter(isHeaderBlock),
    },
  }
}

function prepareBlock(block: IBlock, allBlocks: IBlock[]) {
  if (isExistingImageBlock(block)) {
    return block.data
  }
  if (isSummaryBlock(block)) {
    return {
      ...block,
      data: {
        ...block.data,
        headers: allBlocks.filter(isHeaderBlock),
      },
    }
  }
  if (isImageBlock(block)) {
    delete block.data.file.url
  }
  return block
}

async function checkLangPresenceInChapter(ltId: StringOrObjectId, lang: Lang) {
  const chapter = await TextChapterModel.findOne({
    localizedTexts: { $in: [ltId] },
  })
    .populate<{
      localizedTexts: HydratedDocument<ILocalizedText>[]
    }>('localizedTexts')
    .lean()

  if (!chapter) {
    throw new Error('Could not find parent chapter')
  }
  if (
    chapter.localizedTexts.find(
      (lt) => lt.lang === lang && lt._id.toString() !== ltId.toString()
    )
  ) {
    throw new Error(
      'This chapter already has a localized text for lang ' + lang
    )
  }
}

export async function updateLocalizedText(
  ltData: Partial<ILocalizedText> & { _id: StringOrObjectId }
) {
  await checkLangPresenceInChapter(ltData._id, ltData.lang as Lang)

  if (isTextContent(ltData.content) && Array.isArray(ltData.content.blocks)) {
    const allBlocks = ltData.content.blocks
    ltData.content.blocks = allBlocks
      .map((block) => prepareBlock(block, allBlocks))
      .filter((b) => b) as IBlock[]
  }
  const updatedDoc = await updateById(LocalizedTextModel, ltData)
  return updatedDoc
}

export async function deleteLocalizedText(_id: StringOrObjectId) {
  const chapter = await TextChapterModel.findOne({
    localizedTexts: { $in: _id },
  })
  if (!chapter) {
    throw new NotFoundError(
      'Unable to find parent chapter for localizedText _id ' + _id
    )
  }

  const deletedLocalizedText = await LocalizedTextModel.deleteOne({ _id })

  const localizedTextIndex = objectIdsToStrings(chapter.localizedTexts).indexOf(
    _id.toString()
  )
  chapter.localizedTexts.splice(localizedTextIndex, 1)
  await chapter.save()

  return deletedLocalizedText
}

/*
export const localizedTextToFront = (lt: ILocalizedText): ILocalizedText => {
  const { content, ...restLocalizedText } = lt
  return {
    ...restLocalizedText,
    content: !content
      ? {
          time: Date.now(),
          version: '0.0.0',
          blocks: [],
        }
      : {
          ...content,
          blocks: !Array.isArray(content.blocks)
            ? []
            : blocksToFront(content.blocks),
        },
  }
}
*/

export function addUrlToImageBlocks(
  localizedText: ILocalizedText,
  staticUrl: string
) {
  if (!localizedText.content?.blocks) {
    return
  }
  localizedText.content.blocks = localizedText.content.blocks.map(
    function (block) {
      if (!isImageBlock(block)) {
        return block
      }
      block.data.file.url = getUploadedImageUrl(staticUrl, block.data.file.mid)
      return block
    }
  )
}

export async function createTranslation(
  localizedTextId: StringOrObjectId,
  targetLang: Lang
) {
  const srcLocalizedText = await LocalizedTextModel.findById(localizedTextId)
  if (!srcLocalizedText) {
    throw new Error(`Could not find localized text ${localizedTextId}`)
  }

  const chapter = await TextChapterModel.findOne({
    localizedTexts: { $in: localizedTextId },
  })
  if (!chapter) {
    throw new Error(
      `Could not find parent chapter of localized text ${localizedTextId}`
    )
  }

  const localizedTexts = await getLocalizedTextsByChapter({ _id: chapter._id })
  if (getLocalizedTextByLang(localizedTexts, targetLang)) {
    throw new Error(`A localized text already exists for lang ${targetLang}`)
  }

  const newLocalizedText = await createTranslatedText(
    srcLocalizedText,
    targetLang
  )

  await chapter.updateOne({ $push: { localizedTexts: newLocalizedText._id } })
}
