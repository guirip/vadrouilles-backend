import type { Metadata } from 'next'
import type { OpenGraph } from 'next/dist/lib/metadata/types/opengraph-types'
import type { ICodif } from 'src/models/Codif'
import { CodifModel } from 'src/models/Codif'
import { isStringCodif } from 'src/models/Codif.type'
import { ContentCategory } from 'src/models/common/ContentCategory'
import type { IFile } from 'src/models/common/File.type'
import type { Lang } from 'src/models/common/Lang'
import type { ILocalizedText } from 'src/models/text/LocalizedText'
import type { ILocalizedTextFront } from 'src/models/text/LocalizedText.type'
import type { ITextReferenceBlock } from 'src/models/text/TextBlock.type'
import {
  isGalleryBlock,
  isHeaderBlock,
  isImageBlock,
  isParagraphBlock,
} from 'src/models/text/TextBlock.type'
import type { ITextChapterPopulated } from 'src/models/text/TextChapter'
import type { ITextChapterPopulatedFront } from 'src/models/text/TextChapter.type'
import type { ITextPagePopulated } from 'src/models/text/TextPage'
import { TextPageModel } from 'src/models/text/TextPage'
import type {
  IBaseTextPage,
  ITextPageMinimalPopulatedFront,
} from 'src/models/text/TextPage.type'
import { removeHtml } from 'src/utils/stringUtil'
import { getLeanCodif } from './CodifService'
import { getFullPage } from './TextService.page'
import { getFullChapter } from './TextService.chapter'

export const METADATA_CHAPTER_SEPARATOR = ' - '
export const METADATA_SUBTITLE_SEPARATOR = ' ~ '
const MAX_DESCRIPTION_LENGTH = 300

export function generateTextMetadata(
  textPage: ITextPageMinimalPopulatedFront | ITextPagePopulated | null,
  chapter: ITextChapterPopulatedFront | ITextChapterPopulated | null,
  lang: Lang,
  staticUrl = '',
  appTitle = ''
): Metadata {
  let title = appTitle ? `${appTitle} - ` : ''
  let description = ''
  let category = ''
  let creator = ''
  let authors: Metadata['authors'] = null
  const keywords: string[] = []
  let images: OpenGraph['images'] = []

  if (textPage) {
    let lt: ILocalizedText | ILocalizedTextFront | undefined = undefined

    category = textPage.category
    creator =
      textPage.category === ContentCategory.GRAFFITI
        ? 'Beplus graffiti'
        : 'Guillaume @ Vadrouilles attentives'
    authors = { name: creator }

    if (!chapter) {
      if (textPage.cover) {
        images = [getMetadataImage(staticUrl, textPage.cover, '')]
      }
      description += removeHtml(textPage.longDescription?.[lang] ?? '')
    } else {
      lt = chapter?.localizedTexts
        ? (
            chapter.localizedTexts as ILocalizedText[] | ILocalizedTextFront[]
          ).find((lt) => String(lt.lang) === String(lang))
        : undefined

      if (lt) {
        // Description
        if (lt.description) {
          description += removeHtml(lt.description)
        } else {
          for (const block of lt.content?.blocks ?? []) {
            if (isHeaderBlock(block) && block.data.text) {
              description += `${block.data.text} - `
            }
            if (isParagraphBlock(block) && block.data.text) {
              description += `${removeHtml(block.data.text).trim()} `
              if (description.length > MAX_DESCRIPTION_LENGTH) {
                break
              }
            }
          }
        }

        // Image
        if (Array.isArray(lt.content?.blocks)) {
          let imageBlock = lt.content.blocks.find((block) =>
            isImageBlock(block)
          )

          // gallery fallback
          if (!imageBlock) {
            const galleryBlock = lt.content.blocks.find((block) =>
              isGalleryBlock(block)
            )

            if (
              Array.isArray(galleryBlock?.data.visuals) &&
              galleryBlock.data.visuals.length > 0
            ) {
              imageBlock = galleryBlock.data.visuals.find((visual) =>
                isImageBlock(visual)
              )
            }
          }
          if (imageBlock) {
            images = [
              getMetadataImage(
                staticUrl,
                imageBlock.data.file,
                imageBlock.data.caption
              ),
            ]
          }
        }
      }
    }
    title +=
      removeHtml(textPage.title[lang]) +
      (textPage.chapters.length > 1 && lt?.title
        ? `${METADATA_CHAPTER_SEPARATOR}${lt.title}`
        : '')

    if (textPage.description?.[lang]) {
      title += `${METADATA_SUBTITLE_SEPARATOR}${textPage.description[lang]}`
    }
  }

  return {
    title,
    category,
    creator,
    authors,
    keywords,
    description:
      description.length < MAX_DESCRIPTION_LENGTH
        ? description
        : `${description.slice(0, MAX_DESCRIPTION_LENGTH)}...`,
    openGraph: { images },
  }
}

const getMetadataImage = (
  staticUrl: string,
  file: IFile,
  caption?: string
) => ({
  url: `${staticUrl}/${file.low}`,
  alt: removeHtml(caption) ?? '',
})

export interface IChapterMetadata {
  chapterId: string
  visible: boolean
  metadata: Metadata
}

export interface ITextPageMetadata {
  textPageId: string
  title: string
  visible: boolean
  cover: IBaseTextPage['cover']
  description?: string
  metadata: Metadata
  chapters: IChapterMetadata[]
}

export type AllTextMetadata = Record<ContentCategory, ITextPageMetadata[]>

async function getPageMetadata(
  textPage: ITextPagePopulated,
  lang: Lang,
  staticUrl: string
): Promise<ITextPageMetadata> {
  return {
    textPageId: textPage._id.toString(),
    title: textPage.title[lang],
    description: textPage.description?.[lang],
    cover: textPage.cover,
    visible: textPage.visible,
    metadata: generateTextMetadata(textPage, null, lang, staticUrl),
    chapters: await Promise.all(
      textPage.chapters.map(
        (chapter): IChapterMetadata => ({
          chapterId: chapter._id.toString(),
          visible: chapter.visible,
          metadata: generateTextMetadata(textPage, chapter, lang, staticUrl),
        })
      )
    ),
  }
}

export async function getFullMetadata(lang: Lang) {
  const staticUrl = await getLeanCodif({
    name: 'static-url',
  })
  if (!staticUrl) {
    throw new Error('Missing codif static-url')
  }
  const allMetadata = {} as AllTextMetadata
  for (const category of Object.values(ContentCategory)) {
    allMetadata[category] = await getCategoryMetadata(
      category,
      lang,
      staticUrl.value as string
    )
  }
  return allMetadata
}

async function getCategoryMetadata(
  category: ContentCategory,
  lang: Lang,
  staticUrl: string
) {
  const pages = await TextPageModel.find(
    { category },
    {},
    { sort: { order: 1 } }
  )
    .populate({
      path: 'chapters',
      options: { sort: { order: 1 } },
      populate: {
        path: 'localizedTexts',
        match: {
          lang,
        },
      },
    })
    .lean()
    .exec()
  return await Promise.all(
    pages.map((page) =>
      getPageMetadata(page as unknown as ITextPagePopulated, lang, staticUrl)
    )
  )
}

export async function queryTextRefMetadata(
  data: ITextReferenceBlock['data'],
  lang: Lang
) {
  if (data) {
    const { textPageId, chapterId } = data
    if (textPageId) {
      const textPage = await getFullPage({ _id: textPageId }, true)
      const chapter = chapterId
        ? await getFullChapter({ _id: chapterId }, false)
        : null

      const staticUrlCodif = await CodifModel.findOne({
        name: 'static-url',
      }).lean<ICodif>()

      if (
        textPage &&
        (!chapter || chapter.visible) &&
        isStringCodif(staticUrlCodif)
      ) {
        return {
          category: textPage.category,
          textPageSlug: textPage.slug,
          chapterSlug: chapter?.slug ?? '',
          metadata: generateTextMetadata(
            textPage,
            chapter,
            lang,
            staticUrlCodif.value
          ),
        }
      }
    }
  }
  return null
}
