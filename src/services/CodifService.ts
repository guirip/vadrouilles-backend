import type { ICodifLean } from 'src/models/Codif'
import { CodifModel } from 'src/models/Codif'
import type { StringOrObjectId } from 'src/models/common/DbItem'
import { NotFoundError } from 'src/utils/error'
import { updateById } from './DbService'

import type {
  IBooleanCodif,
  INumberCodif,
  IStringCodif,
} from 'src/models/Codif.type'

enum CodifName {
  STATIC_URL = 'static-url',
  CATALOG_STATUS = 'catalog-status',
  CATALOG_NOTICE = 'catalog-notice',
  CATALOG_DISCOUNT = 'catalog-discount',
}

export const getCodifStaticUrl = () =>
  getLeanCodif({ name: CodifName.STATIC_URL }) as Promise<IStringCodif | null>

const getCodifCatalogStatus = () =>
  getLeanCodif({
    name: CodifName.CATALOG_STATUS,
  }) as Promise<IBooleanCodif | null>

const getCodifCatalogNotice = () =>
  getLeanCodif({
    name: CodifName.CATALOG_NOTICE,
  }) as Promise<IStringCodif | null>

const getCodifCatalogDiscount = () =>
  getLeanCodif({
    name: CodifName.CATALOG_DISCOUNT,
  }) as Promise<INumberCodif | null>

export async function queryCodifs() {
  const [
    staticUrlCodif,
    catalogStatusCodif,
    catalogNoticeCodif,
    catalogDiscountCodif,
  ] = await Promise.all([
    getCodifStaticUrl(),
    getCodifCatalogStatus(),
    getCodifCatalogNotice(),
    getCodifCatalogDiscount(),
  ])

  return {
    staticUrl: (staticUrlCodif as IStringCodif)?.value ?? null,
    catalogStatus: (catalogStatusCodif as IBooleanCodif)?.value ?? null,
    catalogNotice: (catalogNoticeCodif as IStringCodif)?.value ?? null,
    catalogDiscount: (catalogDiscountCodif as INumberCodif)?.value ?? null,
  }
}

export const getLeanCodif = async (data: Partial<ICodifLean>) =>
  CodifModel.findOne(data).lean<ICodifLean>().exec()

export async function updateCodif(
  data: Partial<ICodifLean> & { _id: StringOrObjectId }
) {
  return updateById(CodifModel, data)
}

export async function deleteCodif(_id: StringOrObjectId) {
  const codif = await CodifModel.findById(_id)
  if (!codif) {
    throw new NotFoundError()
  }
  await codif.deleteOne()
}
