import type {
  ICatalogItemLean,
  IItemsPerCategory,
} from 'src/models/CatalogItem'
import { getCategories } from './CatalogItemCategoryService'
import { getItems } from './CatalogItemService'
import type { ICatalogData } from 'src/models/Catalog.type'
import { toSerializable } from './DbService'
import type { ICatalogItemCategoryFront } from 'src/models/CatalogItemCategory.type'

function spreadItemsPerCategory(
  items: ICatalogItemLean[],
  defaultCatId?: ICatalogItemLean['_id']
) {
  const _itemsPerCat: IItemsPerCategory = {}

  for (const item of items) {
    const catId = item.category?.toString() ?? defaultCatId

    if (catId) {
      if (!_itemsPerCat[catId]) {
        _itemsPerCat[catId] = []
      }
      _itemsPerCat[catId].push(toSerializable(item))
    }
  }
  return _itemsPerCat
}

export async function getCatalogData(): Promise<ICatalogData> {
  const [items, categories] = await Promise.all([
    getItems({ visibility: true }),
    getCategories({ visibility: true }),
  ])

  const defaultCategory = categories.find((cat) => cat.isDefault)

  const itemsPerCategory: IItemsPerCategory = spreadItemsPerCategory(
    items,
    defaultCategory?._id.toString()
  )
  return {
    categories: categories.map((c) =>
      toSerializable<ICatalogItemCategoryFront>(c)
    ),
    itemsPerCategory,
  }
}
