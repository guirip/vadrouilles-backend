import type { NextApiRequest, NextApiResponse } from 'next'
import { type Session } from 'next-auth'
import { getServerSession } from 'next-auth/next'
import CredentialsProvider from 'next-auth/providers/credentials'

import config from 'src/config'
import type { ICodif } from 'src/models/Codif'
import { CodifModel } from 'src/models/Codif'

export const AUTH_OPTIONS = {
  providers: [
    CredentialsProvider({
      name: 'password',

      credentials: {
        password: { label: 'Password', type: 'password' },
      },

      async authorize(credentials) {
        const codif = await CodifModel.findOne({
          name: config.AUTH_CODIF_NAME,
        }).lean<ICodif>()

        if (!codif) {
          const msg = 'No codif found in base, so authentication is impossible'
          console.error(msg)
          throw new Error(msg)
        }
        if (!credentials?.password) {
          throw new Error('Missing password')
        }
        if (codif.value !== credentials.password) {
          throw new Error('Invalid password')
        }

        return { id: 'admin' } // no real user data managed
      },
    }),
  ],

  debug: process.env.NODE_ENV !== 'production',

  session: {
    jwt: true,
    maxAge: 1 * 24 * 60 * 60, // 24 hours
  },
}

export async function isAuthOk() {
  let session: Session | null = null
  try {
    session = await getServerSession(AUTH_OPTIONS)
  } catch (e) {
    console.error('Failed to check session', e)
  }
  return !!session
}

export async function isApiAuthOk(req: NextApiRequest, res: NextApiResponse) {
  const session = await getServerSession(req, res, AUTH_OPTIONS)
  return !!session
}
