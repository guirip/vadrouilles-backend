import type { FilterQuery, HydratedDocument, Types } from 'mongoose'

import type { StringOrObjectId } from 'src/models/common/DbItem'
import type { IIdOrderMap } from 'src/models/common/IdOrderMap'
import type { ILocalizedText } from 'src/models/text/LocalizedText'
import type {
  ITextChapter,
  ITextChapterPopulated,
} from 'src/models/text/TextChapter'
import { TextChapterModel } from 'src/models/text/TextChapter'
import type {
  IBaseTextChapter,
  ITextChapterMinimalPopulatedFront,
  ITextChapterPopulatedFront,
} from 'src/models/text/TextChapter.type'
import type { ITextPage } from 'src/models/text/TextPage'
import { TextPageModel } from 'src/models/text/TextPage'
import { NotFoundError } from 'src/utils/error'
import { simpleSortAsc } from 'src/utils/sortUtil'
import { objectIdsToStrings, toSerializable, updateById } from './DbService'
import {
  duplicateLocalizedText,
  toMinimalLocalizedTextFront,
} from './TextService.localizedText'

const serializeMinimalChapter = ({ _id, localizedTexts, ...chapterData }) =>
  ({
    ...chapterData,
    _id: _id.toString(),
    localizedTexts: localizedTexts.map((lt) => {
      return toMinimalLocalizedTextFront(lt)
    }),
  }) as ITextChapterMinimalPopulatedFront

const serializeFullChapter = ({ _id, localizedTexts, ...chapterData }) =>
  ({
    ...chapterData,
    _id: _id.toString(),
    localizedTexts: localizedTexts.map((lt) => {
      return toSerializable(lt)
    }),
  }) as ITextChapterPopulatedFront

export async function getFullChapter(
  chapterCriteria: FilterQuery<ITextChapter> = {},
  onlyVisible: boolean
) {
  if (!chapterCriteria._id && !chapterCriteria.slug) {
    return null
  }
  if (onlyVisible) {
    chapterCriteria.visible = true
  }
  const chapter = await TextChapterModel.findOne(chapterCriteria)
    .populate<{ localizedTexts: ILocalizedText[] }>({
      path: 'localizedTexts',
    })
    .lean()

  return !chapter ? null : serializeFullChapter(chapter)
}

async function queryChaptersMinimalForFront(
  chapterIds: string[] | Types.ObjectId[]
) {
  const chapters = await TextChapterModel.find({
    _id: { $in: chapterIds },
    undefined,
  })
    .populate<{ localizedTexts: ILocalizedText[] }>({
      path: 'localizedTexts',
    })
    .lean()

  const chaptersMinimal: ITextChapterMinimalPopulatedFront[] = chapters.map(
    (c) => serializeMinimalChapter(c)
  )
  return (chaptersMinimal || []).sort(sortChapters)
}

export async function queryChapterMinimalForFront(
  chapterId: string | Types.ObjectId
) {
  const chapters = await queryChaptersMinimalForFront([chapterId] as
    | string[]
    | Types.ObjectId[])
  return chapters.length > 0 ? chapters[0] : null
}

export async function getChaptersByPage(pageId: string | ITextPage['_id']) {
  const result = await TextPageModel.findById(pageId, 'chapters', {
    lean: true,
  })
  if (!result) {
    throw new Error(`Page ${pageId} not found`)
  }
  const { chapters: chapterIds } = result
  return await queryChaptersMinimalForFront(chapterIds)
}

export async function createChapter(
  textPageId: StringOrObjectId,
  data: Partial<ITextChapter> | Partial<ITextChapterPopulated>
) {
  const textPage = await TextPageModel.findById(textPageId)
  if (!textPage) {
    throw new Error('No text page found for id: ' + textPageId)
  }

  // update chapter's order
  const chapters = await TextChapterModel.find({
    _id: { $in: textPage.chapters },
  }).sort({ order: 1 })

  await Promise.all(
    chapters.map((chapter, index) => {
      chapter.order = index + (data.order === 0 ? 1 : 0)
      return chapter.save()
    })
  )
  if (data.order !== 0) {
    data.order = chapters.length
  }

  const createdChapter = await TextChapterModel.create(data)

  try {
    textPage.chapters = [...textPage.chapters, createdChapter._id]
    await textPage.save()
  } catch (e) {
    await createdChapter.deleteOne()
    throw e
  }

  return createdChapter
}

export async function updateOrder(idOrderMap: IIdOrderMap) {
  const chapters = await TextChapterModel.find({
    _id: {
      $in: Object.keys(idOrderMap),
    },
  })
  await Promise.all(
    chapters.map((chapter) => {
      chapter.order = idOrderMap[chapter._id.toString()]
      return chapter.save()
    })
  )
}

export function updateChapter(
  data: Partial<ITextChapter> & { _id: StringOrObjectId }
) {
  return updateById(TextChapterModel, data)
}

export async function deleteChapter(_id: StringOrObjectId) {
  // Find related TextPage
  const [textPage] = await TextPageModel.find({ chapters: { $in: _id } })
  if (!textPage) {
    throw new NotFoundError('Cannot find related text page')
  }

  const chapter = await TextChapterModel.findById(_id).populate<{
    localizedTexts: HydratedDocument<ILocalizedText>[]
  }>('localizedTexts')

  if (!chapter) {
    throw new NotFoundError('Chapter not found')
  }
  if (
    Array.isArray(chapter.localizedTexts) &&
    chapter.localizedTexts.length > 0
  ) {
    throw new Error('Cannot delete chapter with localizedTexts attached')
  }
  const deletedChapter = await TextChapterModel.deleteOne({ _id })

  const index = objectIdsToStrings(textPage.chapters).indexOf(_id.toString())
  if (index === -1) {
    console.error('Failed to find chapter to delete')
  } else {
    textPage.chapters.splice(index, 1)
    await textPage.save()
  }

  return deletedChapter
}

export const sortChapters = (c1: IBaseTextChapter, c2: IBaseTextChapter) =>
  simpleSortAsc(c1.order, c2.order)

export async function duplicateChapter(
  textPageId: StringOrObjectId,
  chapterId: StringOrObjectId
) {
  const chapterToDuplicate = await TextChapterModel.findById(chapterId).lean()
  if (!chapterToDuplicate) {
    throw new Error(`Could not find chapter to duplicate ${chapterId}`)
  }

  const textPage = await TextPageModel.findById(textPageId)
  if (!textPage) {
    throw new Error(`Could not find target text page ${textPageId}`)
  }

  const duplicatedChapter: Partial<ITextChapter> = {
    ...chapterToDuplicate,
  }
  delete duplicatedChapter._id
  duplicatedChapter.slug = (duplicatedChapter.slug ?? '') + '-copy'

  if (typeof duplicatedChapter.order === 'number') {
    duplicatedChapter.order += 1
  }

  // Duplicate chapter content (localized texts)
  duplicatedChapter.localizedTexts = (
    await Promise.all(
      (duplicatedChapter.localizedTexts ?? []).map((ltId) =>
        duplicateLocalizedText(ltId)
      )
    )
  ).map(({ _id }) => _id)

  return await createChapter(textPageId, duplicatedChapter)
}

export async function moveChapter(
  chapterId: StringOrObjectId,
  targetTextPageId: StringOrObjectId
) {
  const currentPage = await TextPageModel.findOne({
    chapters: {
      $in: chapterId,
    },
  })

  const targetPage = await TextPageModel.findById(targetTextPageId)
  if (!targetPage) {
    throw new Error(`Failed to find target page ${targetTextPageId}`)
  }

  if (!currentPage) {
    // Gracefully handle the case when the chapter is not attached to any page
  } else {
    await currentPage.updateOne({ $pull: { chapters: chapterId } })
  }

  // add chapterId to targetPage.chapters
  await targetPage.updateOne({ $push: { chapters: chapterId } })
}
