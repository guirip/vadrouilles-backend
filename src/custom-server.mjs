import fs from 'fs'
import { parse } from 'url'
import next from 'next'

const { PORT, KEY_PATH, CERT_PATH, NODE_ENV, NEXTAUTH_URL, DB_INSTANCE } =
  process.env

if (!PORT) {
  console.error('Missing env var PORT')
  process.exit(1)
}
if (!DB_INSTANCE) {
  console.error('Missing env var DB_INSTANCE')
  process.exit(1)
}

const dev = NODE_ENV !== 'production'

if (!dev) {
  let hasMissingEnvVar
  if (!KEY_PATH) {
    hasMissingEnvVar = true
    console.error('Missing env var KEY_PATH')
  }
  if (!CERT_PATH) {
    hasMissingEnvVar = true
    console.error('Missing env var CERT_PATH')
  }
  if (hasMissingEnvVar) {
    process.exit(1)
  }
}

;(async () => {
  console.log('Starting custom server')

  const app = next({
    dev,
    quiet: !dev,
  })
  const handle = app.getRequestHandler()

  let createServer
  let options

  if (dev) {
    createServer = (await import('http')).createServer
    options = {}
  } else {
    if (!KEY_PATH || !CERT_PATH) {
      throw new Error(
        `Incomplete configuration, check that KEY_PATH and CERT_PATH are properly defined.`
      )
    }
    createServer = (await import('https')).createServer
    try {
      options = {
        key: fs.readFileSync(KEY_PATH),
        cert: fs.readFileSync(CERT_PATH),
      }
    } catch (error) {
      console.error(
        'Failed to read certificate files. ' +
          (error
            ? `${error.code || ''} - ${error.name || ''} - ${
                error.message || ''
              }`
            : '')
      )
      process.exit(1)
    }
  }

  await app.prepare()

  const server = createServer(options, async (req, res) => {
    // Be sure to pass `true` as the second argument to `url.parse`.
    // This tells it to parse the query portion of the URL.
    const parsedUrl = parse(req.url, true)

    await handle(req, res, parsedUrl)
  })

  server.listen(PORT, (err) => {
    if (err) throw err
    console.log(`> Ready on http://localhost:${PORT}`)
    console.log(`> Ready on ${NEXTAUTH_URL}`)
  })
})()
