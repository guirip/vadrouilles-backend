import type { ContentCategory } from 'src/models/common/ContentCategory'
import type { IIdOrderMap } from 'src/models/common/IdOrderMap'
import type { ITextPageFront } from 'src/models/text/TextPage.type'
import executeFetch from 'src/utils/executeFetch'
import { BASE_API_URL } from './constants'

const TEXT_PAGE_WS_URL = BASE_API_URL + '/text/page'

export async function apiFetchPages(cat: ContentCategory) {
  const response = await executeFetch(
    `${TEXT_PAGE_WS_URL}?category=${cat}&visibility=all`
  )
  return (await response.json()) as ITextPageFront[]
}

export async function apiCreatePage(textPage: Partial<ITextPageFront>) {
  const response = await executeFetch(TEXT_PAGE_WS_URL, {
    method: 'POST',
    body: JSON.stringify(textPage),
  })
  return (await response.json()) as ITextPageFront
}

export async function apiUpdatePage(textPage: Partial<ITextPageFront>) {
  const response = await executeFetch(`${TEXT_PAGE_WS_URL}/${textPage._id}`, {
    method: 'PUT',
    body: JSON.stringify(textPage),
  })
  return (await response.json()) as ITextPageFront
}

export async function apiUpdateOrder(idOrderMap: IIdOrderMap) {
  await executeFetch(`${TEXT_PAGE_WS_URL}/order`, {
    method: 'PUT',
    body: JSON.stringify({ idOrderMap }),
  })
}

export async function apiDeletePage(textPageId: ITextPageFront['_id']) {
  await executeFetch(`${TEXT_PAGE_WS_URL}/${textPageId}`, {
    method: 'DELETE',
  })
}
