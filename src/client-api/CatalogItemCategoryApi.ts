import type { ICatalogItemCategoryFront } from 'src/models/CatalogItemCategory.type'
import type { IIdOrderMap } from 'src/models/common/IdOrderMap'
import executeFetch from 'src/utils/executeFetch'
import { BASE_API_URL } from './constants'

const CATALOG_ITEM_CATEGORY_API_URL = BASE_API_URL + '/catalog-item-category'

export async function apiFetchItemCategories() {
  const response = await executeFetch(CATALOG_ITEM_CATEGORY_API_URL)
  return (await response.json()) as ICatalogItemCategoryFront[]
}

export async function apiCreateItemCategory(
  itemCategoryData: Partial<ICatalogItemCategoryFront>
) {
  await executeFetch(CATALOG_ITEM_CATEGORY_API_URL, {
    method: 'POST',
    body: JSON.stringify(itemCategoryData),
  })
}

export async function apiUpdateItemCategory(
  itemCategory: ICatalogItemCategoryFront
) {
  await executeFetch(`${CATALOG_ITEM_CATEGORY_API_URL}/${itemCategory._id}`, {
    method: 'PUT',
    body: JSON.stringify(itemCategory),
  })
}

export async function apiUpdateOrder(idOrderMap: IIdOrderMap) {
  await executeFetch(`${CATALOG_ITEM_CATEGORY_API_URL}/order`, {
    method: 'PUT',
    body: JSON.stringify({ idOrderMap: idOrderMap }),
  })
}

export async function apiDeleteItemCategory(_id: string) {
  await executeFetch(`${CATALOG_ITEM_CATEGORY_API_URL}/${_id}`, {
    method: 'DELETE',
  })
}
