import type { ICodifFront } from 'src/models/Codif.type'
import executeFetch from 'src/utils/executeFetch'
import { BASE_API_URL } from './constants'

const CODIF_WS_URL = BASE_API_URL + '/codif'

export async function apiGetCodifs() {
  const response = await executeFetch(CODIF_WS_URL)
  return (await response.json()) as ICodifFront[]
}

export async function apiCreateCodif(codifData: Partial<ICodifFront>) {
  await executeFetch(CODIF_WS_URL, {
    method: 'POST',
    body: JSON.stringify(codifData),
  })
}

export async function apiUpdateCodif(codif: ICodifFront) {
  const response = await executeFetch(`${CODIF_WS_URL}/${codif._id}`, {
    method: 'PUT',
    body: JSON.stringify(codif),
  })
  return (await response.json()) as ICodifFront
}

export async function apiDeleteCodif(_id: string) {
  await executeFetch(`${CODIF_WS_URL}/${_id}`, {
    method: 'DELETE',
  })
}
