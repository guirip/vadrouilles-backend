import executeFetch from 'src/utils/executeFetch'
import { BASE_API_URL } from './constants'

const UPLOADER_URL = BASE_API_URL + '/upload'

export async function apiUploadFile(file: File) {
  const formData = new FormData()
  formData.append('itemPhotos', file)

  const response = await executeFetch(UPLOADER_URL, {
    method: 'POST',
    headers: {},
    body: formData,
  })
  const result = await response.json()

  const { errors, outputs } = result
  const [image] = outputs

  return {
    success: errors && errors.length ? 0 : 1,
    file: image,
  }
}
