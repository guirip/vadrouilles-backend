import type { ICatalogItemLean } from 'src/models/CatalogItem'
import type { ICatalogItemCategoryLean } from 'src/models/CatalogItemCategory'
import type { IFile } from 'src/models/common/File.type'
import type { IIdOrderMap } from 'src/models/common/IdOrderMap'
import executeFetch from 'src/utils/executeFetch'
import { BASE_API_URL } from './constants'

const CATALOG_ITEM_API_URL = BASE_API_URL + '/catalog-item'

export async function apiRefreshItems(): Promise<{
  items: ICatalogItemLean[]
  categories: ICatalogItemCategoryLean[]
}> {
  const response = await executeFetch(CATALOG_ITEM_API_URL)
  return await response.json()
}

export async function apiCreateCatalogItem(
  catalogItem: Partial<ICatalogItemLean>
) {
  // Safety net
  const { _id, ...catalogItemData } = catalogItem

  const response = await executeFetch(CATALOG_ITEM_API_URL, {
    method: 'POST',
    body: JSON.stringify(catalogItemData),
  })
  return (await response.json()) as ICatalogItemLean
}

export async function apiUpdateOrder(idOrderMap: IIdOrderMap) {
  await executeFetch(`${CATALOG_ITEM_API_URL}/order`, {
    method: 'PUT',
    body: JSON.stringify({ idOrderMap }),
  })
}

export async function apiUpdateCatalogItem(
  catalogItem: Partial<ICatalogItemLean>
) {
  const response = await executeFetch(
    `${CATALOG_ITEM_API_URL}/${catalogItem._id}`,
    {
      method: 'PUT',
      body: JSON.stringify(catalogItem),
    }
  )
  return (await response.json()) as ICatalogItemLean
}

export async function apiSetMainPicture(
  itemId: ICatalogItemLean['_id'],
  lowFileName: IFile['low']
) {
  const response = await executeFetch(
    `${CATALOG_ITEM_API_URL}/${itemId}/main/${lowFileName}`,
    {
      method: 'PUT',
    }
  )
  return (await response.json()) as IFile[]
}

export async function apiDeleteCatalogItem(_id: string) {
  await executeFetch(`${CATALOG_ITEM_API_URL}/${_id}`, {
    method: 'DELETE',
  })
}

export async function apiDeleteCatalogItemFile(
  itemId: ICatalogItemLean['_id'],
  lowFileName: IFile['low']
) {
  const response = await executeFetch(
    `${CATALOG_ITEM_API_URL}/${itemId}/file/${lowFileName}`,
    {
      method: 'DELETE',
    }
  )
  return (await response.json()) as IFile[]
}
