import type {
  ILocalizedTextFront,
  ILocalizedTextMinimalFront,
} from 'src/models/text/LocalizedText.type'
import type { ITextChapterMinimalPopulatedFront } from 'src/models/text/TextChapter.type'
import executeFetch from 'src/utils/executeFetch'
import { BASE_API_URL } from './constants'
import type { Lang } from 'src/models/common/Lang'

const TEXT_LOCALIZED_TEXT_WS_URL = BASE_API_URL + '/text/localized-text'

export async function apiCreateLocalizedText(
  chapterId: ITextChapterMinimalPopulatedFront['_id'],
  ltData: Partial<ILocalizedTextFront>
) {
  await executeFetch(TEXT_LOCALIZED_TEXT_WS_URL, {
    method: 'POST',
    body: JSON.stringify({ chapterId, ...ltData }),
  })
}

export async function apiUpdateLocalizedText(
  payload: Partial<ILocalizedTextFront>
) {
  await executeFetch(`${TEXT_LOCALIZED_TEXT_WS_URL}/${payload._id}`, {
    method: 'PUT',
    body: JSON.stringify(payload),
  })
}

export async function apiDeleteLocalizedText(_id: ILocalizedTextFront['_id']) {
  await executeFetch(`${TEXT_LOCALIZED_TEXT_WS_URL}/${_id}`, {
    method: 'DELETE',
  })
}

export async function apiTranslateLocalizedText(
  localizedTextId: ILocalizedTextMinimalFront['_id'],
  targetLang: Lang
) {
  await executeFetch(
    `${TEXT_LOCALIZED_TEXT_WS_URL}/${localizedTextId}/translate-to/${targetLang}`,
    {
      method: 'POST',
    },
    60000
  )
}
