import type { IIdOrderMap } from 'src/models/common/IdOrderMap'
import type { ITextChapterMinimalPopulatedFront } from 'src/models/text/TextChapter.type'
import type { ITextPageFront } from 'src/models/text/TextPage.type'
import executeFetch from 'src/utils/executeFetch'
import { BASE_API_URL } from './constants'

const TEXT_CHAPTER_WS_URL = BASE_API_URL + '/text/chapter'

export async function apiFetchChapters(textPageId: ITextPageFront['_id']) {
  const response = await executeFetch(
    `${TEXT_CHAPTER_WS_URL}?textPageId=${textPageId}`
  )
  return (await response.json()) as ITextChapterMinimalPopulatedFront[]
}

export async function apiCreateChapter(
  chapterData: Partial<ITextChapterMinimalPopulatedFront>,
  textPageId: ITextPageFront['_id']
) {
  await executeFetch(TEXT_CHAPTER_WS_URL, {
    method: 'POST',
    body: JSON.stringify({ textPageId, ...chapterData }),
  })
}

export async function apiUpdateChapter(
  chapterData: Partial<ITextChapterMinimalPopulatedFront>
) {
  await executeFetch(`${TEXT_CHAPTER_WS_URL}/${chapterData._id}`, {
    method: 'PUT',
    body: JSON.stringify(chapterData),
  })
}

export async function apiUpdateChaptersOrder(idOrderMap: IIdOrderMap) {
  await executeFetch(`${TEXT_CHAPTER_WS_URL}/order`, {
    method: 'PUT',
    body: JSON.stringify({ idOrderMap }),
  })
}

export async function apiDuplicateChapter(
  textPageId: ITextPageFront['_id'],
  chapterId: ITextChapterMinimalPopulatedFront['_id']
) {
  await executeFetch(
    `${TEXT_CHAPTER_WS_URL}/${chapterId}/duplicate/${textPageId}`,
    {
      method: 'POST',
    }
  )
}

export async function apiMoveChapter(
  targetTextPageId: ITextPageFront['_id'],
  chapterId: ITextChapterMinimalPopulatedFront['_id']
) {
  await executeFetch(
    `${TEXT_CHAPTER_WS_URL}/${chapterId}/move-to/${targetTextPageId}`,
    {
      method: 'PUT',
    }
  )
}

export async function apiDeleteChapter(
  chapterId: ITextChapterMinimalPopulatedFront['_id']
) {
  await executeFetch(`${TEXT_CHAPTER_WS_URL}/${chapterId}`, {
    method: 'DELETE',
  })
}
