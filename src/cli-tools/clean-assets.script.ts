import minimist from 'minimist'
import { mkdir, readdir, rename, stat } from 'node:fs/promises'
import { resolve } from 'node:path'
import config from 'src/config'
import type { ICatalogItem } from 'src/models/CatalogItem'
import { CatalogItemModel } from 'src/models/CatalogItem'
import type { IFile } from 'src/models/common/File.type'
import type { ILocalizedText } from 'src/models/text/LocalizedText'
import { LocalizedTextModel } from 'src/models/text/LocalizedText'
import { isImageBlock } from 'src/models/text/TextBlock.type'
import { TextPageModel } from 'src/models/text/TextPage'
import type { IBaseTextPage } from 'src/models/text/TextPage.type'
import { connect, disconnect } from 'src/services/DbService'

const SKIP = ['gpx']

const twoDigits = (number) => (number < 10 ? `0${number}` : number)

const getDateTimeLabel = () => {
  const d = new Date()
  return `${d.getFullYear()}_${twoDigits(d.getMonth() + 1)}_${twoDigits(d.getDate())}-${twoDigits(d.getHours())}h${twoDigits(d.getMinutes())}m${twoDigits(d.getSeconds())}`
}

function logFiles(pictures: string[]) {
  if (pictures.length > 0) {
    console.log(`\n   - ${pictures.join('\n   - ')}`)
  }
}

function pushPictures(file: IFile, array: string[]) {
  array.push(file.low)
  array.push(file.mid)
  array.push(file.orig)
}

function parseTextBlocksPictures(
  localizedTexts: Pick<ILocalizedText, 'content'>[]
) {
  const pictures: string[] = []

  for (const localizedText of localizedTexts) {
    if (
      !localizedText.content ||
      !Array.isArray(localizedText.content.blocks)
    ) {
      continue
    }
    for (const block of localizedText.content.blocks) {
      if (isImageBlock(block)) {
        pushPictures(block.data.file, pictures)
      }
    }
  }
  return pictures
}

function parseCatalogItemsPictures(items: Pick<ICatalogItem, 'files'>[]) {
  const pictures: string[] = []

  for (const item of items) {
    if (Array.isArray(item.files)) {
      for (const file of item.files) {
        pushPictures(file, pictures)
      }
    }
  }
  return pictures
}

function parseTextPagesPictures(textPages: Pick<IBaseTextPage, 'cover'>[]) {
  const pictures: string[] = []

  for (const page of textPages) {
    if (page.cover) {
      pushPictures(page.cover, pictures)
    }
  }
  return pictures
}

async function listPicturesInDb() {
  console.log('\n 💾 Loading localized texts, catalog items, text pages ...')
  const [localizedTexts, catalogItems, textPages] = await Promise.all([
    LocalizedTextModel.find().select('content').lean(),
    CatalogItemModel.find().select('files').lean(),
    TextPageModel.find().select('cover').lean(),
  ])

  console.log(' 💾 Listing all pictures referenced in database ...')
  return parseTextBlocksPictures(localizedTexts)
    .concat(parseCatalogItemsPictures(catalogItems))
    .concat(parseTextPagesPictures(textPages))
}

async function initFolder(dirPath: string) {
  try {
    await stat(dirPath)
  } catch (e) {
    await mkdir(dirPath)
  }
}

async function moveFiles(files: string[], destFolder: string) {
  for (const file of files) {
    const source = resolve(config.UPLOAD_DEST, file)
    const destination = resolve(destFolder, file)
    await rename(source, destination)
  }
}

async function run() {
  const simulation = minimist(process.argv.slice(2)).simulation !== 'false'
  console.log(` ℹ Simulation: ${simulation}\n`)

  await connect()

  try {
    const picturesInDb = await listPicturesInDb()
    console.log(
      ` 💾 Found ${picturesInDb.length} referenced pictures in database`
    )
    // logFiles(pictures)

    console.log(`\n 🖼  Listing all files in folder ${config.UPLOAD_DEST} ...`)
    const filesOnFs = await readdir(config.UPLOAD_DEST)
    console.log(` 🖼  Found ${filesOnFs.length} files on file system`)
    // logFiles(filesOnFs)

    console.log(`\n ✨ Determining unused files ...`)
    const unusedFiles = filesOnFs.filter((fileOnFs) => {
      if (SKIP.includes(fileOnFs)) {
        console.log(`    - Ignoring: ${fileOnFs}`)
        return false
      }
      return !picturesInDb.includes(fileOnFs)
    })

    if (unusedFiles.length === 0) {
      console.log(
        ` ✅ This is so tidy! Not a single unused asset has been found 👏`
      )
    } else {
      console.log(` 🔎 Found ${unusedFiles.length} unused files:`)
      logFiles(unusedFiles)

      const destFolder = `${resolve(config.UPLOAD_DEST)}_UNUSED-${getDateTimeLabel()}`
      if (simulation) {
        console.log(
          `\n 📦 Simulation enabled - Would move ${unusedFiles.length} file${unusedFiles.length > 1 ? 's' : ''} to ${destFolder}`
        )
      } else {
        await initFolder(destFolder)

        console.log(
          `\n 📦 Moving ${unusedFiles.length} file${unusedFiles.length > 1 ? 's' : ''} to ${destFolder} ...`
        )
        await moveFiles(unusedFiles, destFolder)
        console.log(`\n ✅ Done`)
      }
    }
  } catch (e) {
    console.error('Process failed: ' + e)
  }

  console.log('')
  await disconnect()
  process.exit(0)
}

;(async function () {
  await run()
})()
