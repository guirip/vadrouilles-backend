import minimist from 'minimist'
import type { HydratedDocument } from 'mongoose'
import { readdir } from 'node:fs/promises'
import { resolve } from 'node:path'
import type { ICodif } from 'src/models/Codif'
import { CodifModel } from 'src/models/Codif'
import type { ILocalizedText } from 'src/models/text/LocalizedText'
import type { IImageBlock } from 'src/models/text/TextBlock.type'
import { BlockType } from 'src/models/text/TextBlock.type'
import { TextChapterModel } from 'src/models/text/TextChapter'
import { connect, disconnect } from 'src/services/DbService'
import { processImage } from 'src/services/FileService'

const usage = () => {
  console.log(`
  usage:
    npm run add-images-to-chapter -- --input=directoryPath --chapterId=mongoId
`)
}

const fatal = (msg: string) => {
  console.error(msg)
  process.exit(1)
}

async function run() {
  const args = minimist(process.argv.slice(2))
  const simulation = args.simulation !== 'false'
  console.log(` ℹ Simulation: ${simulation}`)

  const { input } = args
  if (!input) {
    usage()
    fatal(
      'Missing `input` parameter (path of the directory containing the images)'
    )
  }
  console.log(` ℹ Input: ${input}`)

  const { chapterId } = args
  if (!chapterId) {
    usage()
    fatal(
      'Missing `chapterId` parameter (where the image blocks should be appended)'
    )
  }
  console.log(` ℹ Chapter id: ${chapterId}`)

  let localizedTexts: Array<HydratedDocument<ILocalizedText>> = []
  await connect()

  try {
    const staticUrlCodif = await CodifModel.findOne({
      name: 'static-url',
    }).lean<ICodif>()
    if (!staticUrlCodif) {
      throw new Error('Could not find static-url codif')
    }

    const chapter = await TextChapterModel.findById(chapterId).populate<{
      localizedTexts: HydratedDocument<ILocalizedText>[]
    }>('localizedTexts')
    if (!chapter) {
      throw new Error(`Could not find chapter ${chapterId}`)
    }

    localizedTexts = chapter.localizedTexts
    if (Array.isArray(localizedTexts) !== true || localizedTexts.length === 0) {
      throw new Error(`Chapter ${chapterId} has no localized text`)
    }

    for (const localizedText of localizedTexts) {
      if (Array.isArray(localizedText.content.blocks) !== true) {
        localizedText.content.blocks = []
      }
    }

    const imagesOnFs = await readdir(input)
    console.log(` 🖼  Found ${imagesOnFs.length} pictures on file system`)
    for (const image of imagesOnFs) {
      console.log(`\n ⏳  Processing ${image}`)
      const names = await processImage(
        image,
        resolve(input, image),
        staticUrlCodif.value as string
      )

      const imageBlock = {
        type: BlockType.Image,
        data: {
          caption: '',
          file: names,
          stretched: false,
          withBackground: false,
          withBorder: false,
        },
      } as IImageBlock

      for (const localizedText of localizedTexts) {
        localizedText.content.blocks.push(imageBlock)
        localizedText.markModified('content')
      }
    }
  } catch (e) {
    console.error('Process failed: ' + e)
  }
  if (Array.isArray(localizedTexts)) {
    await Promise.all(localizedTexts.map((lt) => lt.save()))
  }

  console.log('')
  await disconnect()
  process.exit(0)
}

;(async function () {
  await run()
})()
