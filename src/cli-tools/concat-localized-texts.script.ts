import { LocalizedTextModel } from 'src/models/text/LocalizedText'
import { connect, disconnect } from 'src/services/DbService'

/* To run script:

NODE_ENV=development node_modules/.bin/ts-node --project ./tsconfig.json -r tsconfig-paths/register src/cli-tools/script.ts

 */

const TO_LT_ID = '6390d74d36b4f121e9db8fa8'
const LTs_TO_CONCAT = [
  // order matters
  '6390d76736b4f121e9db8fb7',
  '6390d79036b4f121e9db8fc5',
]
const LT_IDs = [TO_LT_ID, ...LTs_TO_CONCAT]

;(async () => {
  await connect()

  try {
    const lts = await Promise.all(
      LT_IDs.map((ltId) => LocalizedTextModel.findOne({ _id: ltId }))
    )
    const ltTo = lts.shift()
    const ltsToConcat = lts.filter((lt) => lt)

    if (!ltTo || ltsToConcat.length !== LTs_TO_CONCAT.length) {
      throw new Error(
        `Failed to load localized texts from DB (${LT_IDs.map(
          (ltId, index) => `${ltId}: ${!!lts[index]}`
        ).join(', ')})`
      )
    }
    for (const lt of [ltTo, ...ltsToConcat]) {
      if (!lt) {
        throw new Error('localized text can only be defined at this stage')
      }
      if (!lt.content) {
        throw new Error(`Localized text ${lt._id} content is not populated`)
      }
      ltTo.content.blocks.push(...lt.content.blocks)
    }

    ltTo.markModified('content')
    ltTo.markModified('content.blocks')
    await ltTo.save()

    console.log('Done')
  } catch (e) {
    console.error(e)
  }
  await disconnect()
})()
