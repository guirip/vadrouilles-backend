interface IConfig {
  FETCH_TIMEOUT: number
  AUTH_CODIF_NAME: string
  PUBLIC_URL: string

  UPLOAD_DEST: string
  DEST_EXTENSION: string
  MID_SIZE: number
  LOW_SIZE: number
  MID_QUALITY: number
  LOW_QUALITY: number

  DB: {
    INSTANCE: string
    HOST: string
    PORT: number
  }
}

const config: IConfig = {
  FETCH_TIMEOUT: 8000,
  AUTH_CODIF_NAME: 'access-pwd',
  PUBLIC_URL: process.env.PUBLIC_URL ?? '',

  UPLOAD_DEST: process.env.UPLOAD_DEST ?? '',
  DEST_EXTENSION: 'jpg',
  MID_SIZE: 1024,
  LOW_SIZE: 512,
  MID_QUALITY: 92,
  LOW_QUALITY: 88,

  DB: {
    HOST: process.env.DB_HOST ?? '127.0.0.1',
    PORT: process.env.DB_PORT ? parseInt(process.env.DB_PORT, 10) : 27017,
    INSTANCE: process.env.DB_INSTANCE ?? '',
  },
}
export default config
