import { Types } from 'mongoose'
import type { NextApiRequest, NextApiResponse } from 'next'
import corsMiddleware from 'src/middlewares/corsMiddleware'
import mongooseConnectMiddleware from 'src/middlewares/mongooseConnectMiddleware'
import { isApiAuthOk } from 'src/services/AuthService'
import {
  deleteItem,
  deleteItemFile,
  setMainFile,
  updateItem,
  updateOrder,
} from 'src/services/CatalogItemService'
import { getCatalogData } from 'src/services/CatalogService'

import { getErrorMessage, getHttpStatus } from 'src/utils/error'
import { isArray } from 'src/utils/tsUtil'

/**
 * Manage sub routes /api/catalog-item/*
 */
export async function handler(req: NextApiRequest, res: NextApiResponse) {
  const { slug } = req.query
  if (isArray(slug) && slug.length > 0) {
    const subPath = slug[0]
    const slugLength = slug.length

    switch (req.method) {
      case 'GET': {
        // /catalog-item/visible
        if (subPath === 'visible') {
          await handleGetVisibleItems(req, res)
          return
        }
        break
      }

      case 'PUT': {
        // /catalog-item/order
        if (subPath === 'order') {
          await handleUpdateOrder(req, res)
          return
        }
        if (Types.ObjectId.isValid(subPath)) {
          // /catalog-item/:id
          if (slug.length === 1) {
            await handleUpdate(req, res)
            return
          }
          // /catalog-item/:id/main/:lowFileName
          if (slugLength === 3 && slug[1] === 'main') {
            await handleSetMainFile(subPath, slug[2], req, res)
            return
          }
        }
        break
      }

      case 'DELETE': {
        if (Types.ObjectId.isValid(subPath)) {
          // /catalog-item/:id
          if (slug.length === 1) {
            await handleDelete(subPath, req, res)
            return
          }
          // /catalog-item/:id/file/:lowFileName
          if (slugLength === 3 && slug[1] === 'file') {
            await handleDeleteItemFile(subPath, slug[2], req, res)
            return
          }
        }
        break
      }
    }
  }

  res.status(405).send('')
}

/**
 * Get public catalog items
 *
 * PUBLIC API, no session check
 */
async function handleGetVisibleItems(
  req: NextApiRequest,
  res: NextApiResponse
) {
  try {
    const visibleItems = await getCatalogData()
    res.status(200).json(visibleItems)
  } catch (e) {
    res
      .status(getHttpStatus(e))
      .send(`Failed to retrieve visible items: ${getErrorMessage(e)}`)
  }
}

/**
 * Update catalog items order
 */
async function handleUpdateOrder(req: NextApiRequest, res: NextApiResponse) {
  if (!(await isApiAuthOk(req, res))) {
    res.status(401).send('')
    return
  }

  const { idOrderMap } = req.body
  try {
    await updateOrder(idOrderMap)
    res.status(200).send('')
  } catch (e) {
    res
      .status(getHttpStatus(e))
      .send(`Failed to update order: ${getErrorMessage(e)}`)
  }
}

/**
 * Update an item
 */
async function handleUpdate(req: NextApiRequest, res: NextApiResponse) {
  if (!(await isApiAuthOk(req, res))) {
    res.status(401).send('')
    return
  }

  const { __v, ...data } = req.body
  try {
    const updatedItem = await updateItem(data)
    res.status(200).json(updatedItem)
  } catch (e) {
    res
      .status(getHttpStatus(e))
      .send(`Failed to update item: ${getErrorMessage(e)}`)
  }
}

/**
 * Delete an item
 */
async function handleDelete(
  _id: string,
  req: NextApiRequest,
  res: NextApiResponse
) {
  if (!(await isApiAuthOk(req, res))) {
    res.status(401).send('')
    return
  }

  try {
    await deleteItem(_id)
    res.status(200).send('')
  } catch (e) {
    res
      .status(getHttpStatus(e))
      .send(`Failed to delete item: ${getErrorMessage(e)}`)
  }
}

/**
 * Set the main picture attached to a catalog item
 */
async function handleSetMainFile(
  _id: string,
  lowName: string,
  req: NextApiRequest,
  res: NextApiResponse
) {
  if (!(await isApiAuthOk(req, res))) {
    res.status(401).send('')
    return
  }

  try {
    const updatedItem = await setMainFile(_id, lowName)
    res.status(200).send(updatedItem.files)
  } catch (e) {
    res
      .status(getHttpStatus(e))
      .send(`Failed to set main file: ${getErrorMessage(e)}`)
  }
}

/**
 * Delete a file (picture) attached to a catalog item
 */
async function handleDeleteItemFile(
  _id: string,
  lowName: string,
  req: NextApiRequest,
  res: NextApiResponse
) {
  if (!(await isApiAuthOk(req, res))) {
    res.status(401).send('')
    return
  }

  try {
    const updatedItem = await deleteItemFile(_id, lowName)
    res.status(200).send(updatedItem.files)
  } catch (e) {
    res
      .status(getHttpStatus(e))
      .send(`Failed to delete file: ${getErrorMessage(e)}`)
  }
}

export default corsMiddleware(mongooseConnectMiddleware(handler))
