import { Types } from 'mongoose'
import type { NextApiRequest, NextApiResponse } from 'next'
import authMiddleware from 'src/middlewares/authMiddleware'
import corsMiddleware from 'src/middlewares/corsMiddleware'
import mongooseConnectMiddleware from 'src/middlewares/mongooseConnectMiddleware'
import {
  deleteItemCategory,
  updateItemCategory,
  updateOrder,
} from 'src/services/CatalogItemCategoryService'
import { getErrorMessage, getHttpStatus } from 'src/utils/error'
import { isArray } from 'src/utils/tsUtil'

/**
 * Manage sub routes /api/catalog-item-category/*
 */
export async function handler(req: NextApiRequest, res: NextApiResponse) {
  if (isArray(req.query.slug) && req.query.slug.length > 0) {
    const subPath = req.query.slug[0]
    switch (req.method) {
      case 'PUT': {
        // /catalog-item-category/${id}
        if (Types.ObjectId.isValid(subPath)) {
          await handleUpdate(req, res)
          return
        }
        // /catalog-item-category/order
        if (subPath === 'order') {
          await handleUpdateOrder(req, res)
          return
        }
        break
      }

      case 'DELETE': {
        // /catalog-item-category/${id}
        await handleDelete(subPath, res)
        return
      }

      default:
    }
  }
  res.status(405).send('')
}

/**
 * Update catalog item categories order
 */
async function handleUpdateOrder(req: NextApiRequest, res: NextApiResponse) {
  const { idOrderMap } = req.body

  try {
    await updateOrder(idOrderMap)
    res.status(200).send('')
  } catch (e) {
    res
      .status(getHttpStatus(e))
      .send(`Failed to update order - ${getErrorMessage(e)}`)
  }
}

/**
 * Update a catalog item category
 */
async function handleUpdate(req: NextApiRequest, res: NextApiResponse) {
  const { __v, ...data } = req.body
  try {
    const updatedItem = await updateItemCategory(data)
    res.status(200).json(updatedItem)
  } catch (e) {
    res
      .status(getHttpStatus(e))
      .send(`Failed to delete catalog item category - ${getErrorMessage(e)}`)
  }
}

/**
 * Delete a catalog item category
 */
async function handleDelete(_id: string, res: NextApiResponse) {
  try {
    await deleteItemCategory(_id)
    res.status(200).send('')
  } catch (e) {
    res
      .status(getHttpStatus(e))
      .send(`Failed to delete catalog item category - ${getErrorMessage(e)}`)
  }
}

export default corsMiddleware(
  authMiddleware(mongooseConnectMiddleware(handler))
)
