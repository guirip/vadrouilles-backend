import type { NextApiRequest, NextApiResponse } from 'next'
import authMiddleware from 'src/middlewares/authMiddleware'
import corsMiddleware from 'src/middlewares/corsMiddleware'
import mongooseConnectMiddleware from 'src/middlewares/mongooseConnectMiddleware'

import { getCategories } from 'src/services/CatalogItemCategoryService'
import { createItem, getItems } from 'src/services/CatalogItemService'
import { getErrorMessage, getHttpStatus } from 'src/utils/error'

/**
 * Main function to route to dedicated handlers
 */
export async function handler(req: NextApiRequest, res: NextApiResponse) {
  switch (req.method) {
    case 'GET':
      await handleGet(req, res)
      return

    case 'POST':
      await handleCreate(req, res)
      return

    default:
  }
  res.status(405).send('')
}

/**
 * Get items and categories
 */
async function handleGet(req: NextApiRequest, res: NextApiResponse) {
  try {
    const [items, categories] = await Promise.all([
      getItems({}),
      getCategories({}),
    ])
    res.status(200).json({ items, categories })
  } catch (e) {
    res
      .status(getHttpStatus(e))
      .send(`Failed to get items: ${getErrorMessage(e)}`)
  }
}

/**
 * Create an item
 */
async function handleCreate(req: NextApiRequest, res: NextApiResponse) {
  // Safety net
  const { _id, createDate, updateDate, ...itemFields } = req.body

  try {
    const createdItem = await createItem(itemFields)
    res.status(200).json(createdItem)
  } catch (e) {
    if (e.name === 'ValidationError') {
      res
        .status(400)
        .send('Invalid value for field(s): ' + Object.keys(e.errors).join(', '))
      return
    }
    res
      .status(getHttpStatus(e))
      .send(`Failed to create item: ${getErrorMessage(e)}`)
  }
}

export default corsMiddleware(
  authMiddleware(mongooseConnectMiddleware(handler))
)
