import type { NextApiRequest, NextApiResponse } from 'next'

import corsMiddleware from 'src/middlewares/corsMiddleware'
import mongooseConnectMiddleware from 'src/middlewares/mongooseConnectMiddleware'
import type { ICodif, ICodifLean } from 'src/models/Codif'
import { CodifModel } from 'src/models/Codif'
import { NON_PUBLIC_CODIFS_NAME } from 'src/models/Codif.type'
import { isApiAuthOk } from 'src/services/AuthService'
import { getErrorMessage } from 'src/utils/error'

/**
 * Main function to route to dedicated handlers
 */
export async function handler(req: NextApiRequest, res: NextApiResponse) {
  switch (req.method) {
    case 'GET':
      await handleGet(req, res)
      break

    case 'POST':
      await handleCreate(req, res)
      break

    default:
      res.status(405).send('')
  }
}

/**
 * Get one or several codifications
 */
async function handleGet(req: NextApiRequest, res: NextApiResponse) {
  const { name: nameQP } = req.query

  if (!nameQP) {
    if (await isApiAuthOk(req, res)) {
      // Ability to retrieve all codifs
      const codif = await CodifModel.find({}).sort({ name: 1 }).lean<ICodif[]>()
      res.status(200).send(codif)
    } else {
      res
        .status(400)
        .send("Invalid request, missing 'name' query string parameter")
    }
    return
  }
  if (typeof nameQP !== 'string') {
    res
      .status(400)
      .send("Invalid request, 'name' query string parameter should be a string")
    return
  }

  const names = nameQP
    .split(',')
    .filter((codifName) => NON_PUBLIC_CODIFS_NAME.indexOf(codifName) === -1)

  if (names.length === 0) {
    res.status(200).json([])
    return
  }

  const codifs = await CodifModel.find({
    name: { $in: names },
  }).lean<ICodifLean[]>()

  if (codifs.length === 0) {
    res.status(400).send(`Can't read codifs: ${names.join(', ')}`)
    return
  }

  if (names.length === 1) {
    res.status(200).json(codifs[0])
  } else if (names.length > 1) {
    res.status(200).json(codifs)
  }
}

/**
 * Create a codification
 */
async function handleCreate(req: NextApiRequest, res: NextApiResponse) {
  if (!(await isApiAuthOk(req, res))) {
    res.status(401).send('')
    return
  }

  const { name, value, type } = req.body

  const existingCodif = await CodifModel.findOne({ name }).lean<ICodif>()
  if (existingCodif) {
    res.status(400).send(`A codif with name ${name} already exists`)
    return
  }

  try {
    const createdCodif = await CodifModel.create({
      name,
      value,
      type,
    })
    res.status(200).json(createdCodif)
  } catch (e) {
    res.status(400).send(`Failed to create codif: ${getErrorMessage(e)}`)
  }
}

export default corsMiddleware(mongooseConnectMiddleware(handler))
