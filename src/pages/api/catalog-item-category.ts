import type { NextApiRequest, NextApiResponse } from 'next'
import authMiddleware from 'src/middlewares/authMiddleware'
import corsMiddleware from 'src/middlewares/corsMiddleware'
import mongooseConnectMiddleware from 'src/middlewares/mongooseConnectMiddleware'
import {
  createItemCategory,
  getCategories,
} from 'src/services/CatalogItemCategoryService'
import { getErrorMessage, getHttpStatus } from 'src/utils/error'

/**
 * Main function to route to dedicated handlers
 */
export async function handler(req: NextApiRequest, res: NextApiResponse) {
  switch (req.method) {
    case 'GET':
      await handleGet(req, res)
      return

    case 'POST':
      await handleCreate(req, res)
      return

    default:
  }
  res.status(405).send('')
}

/**
 * Retrieve catalog item categories
 */
async function handleGet(req: NextApiRequest, res: NextApiResponse) {
  const filter = req.query
  try {
    const categories = await getCategories(filter)
    res.status(200).json(categories)
  } catch (e) {
    res
      .status(getHttpStatus(e))
      .send(`Failed to get categories - ${getErrorMessage(e)}`)
  }
}

/**
 * Create a catalog item category
 */
async function handleCreate(req: NextApiRequest, res: NextApiResponse) {
  const data = req.body
  try {
    const createdCategory = await createItemCategory(data)
    res.status(200).json(createdCategory)
  } catch (e) {
    res
      .status(getHttpStatus(e))
      .send(`Failed to create category - ${getErrorMessage(e)}`)
  }
}

export default corsMiddleware(
  authMiddleware(mongooseConnectMiddleware(handler))
)
