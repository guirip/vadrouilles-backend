import NextAuth from 'next-auth'
import mongooseConnectMiddleware from 'src/middlewares/mongooseConnectMiddleware'
import { AUTH_OPTIONS } from 'src/services/AuthService'

const handler = (req, res) => NextAuth(req, res, AUTH_OPTIONS)

export default mongooseConnectMiddleware(handler)
