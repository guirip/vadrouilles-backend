import { Types } from 'mongoose'
import type { NextApiRequest, NextApiResponse } from 'next'
import authMiddleware from 'src/middlewares/authMiddleware'
import corsMiddleware from 'src/middlewares/corsMiddleware'
import { CodifModel } from 'src/models/Codif'
import { updateCodif, deleteCodif } from 'src/services/CodifService'
import { getErrorMessage, getHttpStatus } from 'src/utils/error'
import { isArray } from 'src/utils/tsUtil'

/**
 * Main function to route to dedicated handlers
 */
export async function handler(req: NextApiRequest, res: NextApiResponse) {
  if (isArray(req.query.slug) && req.query.slug.length > 0) {
    const subPath = req.query.slug[0]
    if (subPath) {
      switch (req.method) {
        case 'PUT': {
          // /codif/${id}
          if (Types.ObjectId.isValid(subPath)) {
            await handleUpdate(subPath, req, res)
            return
          }
          break
        }

        case 'DELETE':
          // /codif/${id}
          if (Types.ObjectId.isValid(subPath)) {
            await handleDelete(subPath, res)
            return
          }
          break

        default:
      }
    }
  }
  res.status(405).send('')
}

/**
 * Update a codif
 */
async function handleUpdate(
  _id: string,
  req: NextApiRequest,
  res: NextApiResponse
) {
  const item = await CodifModel.findById(_id)
  if (!item) {
    res.status(404).send('Codif not found, id: ' + _id)
    return
  }

  const { __v, ...data } = req.body
  try {
    const updatedItem = await updateCodif(data)
    res.status(200).json(updatedItem)
  } catch (e) {
    if (e.name === 'ValidationError') {
      res
        .status(400)
        .send('Invalid value for field(s): ' + Object.keys(e.errors).join(', '))
      return
    }
    res.status(400).send(`Failed to update codif: ${getErrorMessage(e)}`)
  }
}

/**
 * Delete a codif
 */
async function handleDelete(_id: string, res: NextApiResponse) {
  try {
    await deleteCodif(_id)
    res.status(200).send('')
  } catch (e) {
    res
      .status(getHttpStatus(e))
      .send(`Failed to delete codif: ${getErrorMessage(e)}`)
  }
}

export default corsMiddleware(authMiddleware(handler))
