import type { NextApiRequest, NextApiResponse } from 'next'
import multipartMiddleware from 'src/middlewares/multipartMiddleware'
import authMiddleware from 'src/middlewares/authMiddleware'
import { processImages } from 'src/services/FileService'
import type { IUploadResponse } from 'src/models/Api'

export const config = {
  api: {
    bodyParser: false,
  },
}

/**
 * Process images after their upload
 */
async function handler(
  req: NextApiRequest,
  res: NextApiResponse,
  fields,
  files
) {
  let images = Array.isArray(files.itemPhotos)
    ? files.itemPhotos
    : [files.itemPhotos]

  // Filter empty
  images = images.filter((file) => file)

  if (images.length === 0) {
    res.status(200).json({ outputs: [] } as IUploadResponse)
  } else {
    const { outputs, errors } = await processImages(images)
    res.status(200).json({ outputs, errors } as IUploadResponse)
  }
}

export default authMiddleware(multipartMiddleware(handler))
