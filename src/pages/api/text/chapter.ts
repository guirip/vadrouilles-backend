import { Types } from 'mongoose'
import type { NextApiRequest, NextApiResponse } from 'next'
import authMiddleware from 'src/middlewares/authMiddleware'
import corsMiddleware from 'src/middlewares/corsMiddleware'
import {
  getChaptersByPage,
  createChapter,
} from 'src/services/TextService.chapter'
import { getErrorMessage } from 'src/utils/error'

/**
 * Main function to route to dedicated handlers
 */
export async function handler(req: NextApiRequest, res: NextApiResponse) {
  switch (req.method) {
    case 'GET': {
      await handleGet(req, res)
      break
    }

    case 'POST': {
      await handleCreate(req, res)
      break
    }

    default:
      res.status(405).send('')
  }
}

/**
 * Get chapters belonging to a given page
 */
async function handleGet(req: NextApiRequest, res: NextApiResponse) {
  const { textPageId } = req.query
  if (!textPageId) {
    res.status(400).send("Missing 'textPageId' query parameter")
    return
  }
  if (typeof textPageId !== 'string' || !Types.ObjectId.isValid(textPageId)) {
    res.status(400).send("'textPageId' query parameter should be a string")
    return
  }
  try {
    const chapters = await getChaptersByPage(textPageId)
    res.status(200).json(chapters)
  } catch (e) {
    res.status(400).send(`Failed to get chapter: ${getErrorMessage(e)}`)
  }
}

/**
 * Create a chapter
 */
async function handleCreate(req: NextApiRequest, res: NextApiResponse) {
  const { _id, textPageId, ...chapterData } = req.body

  if (!textPageId) {
    res.status(400).send('Missing textPageId body parameter')
    return
  }
  if (!Types.ObjectId.isValid(textPageId)) {
    res.status(400).send('Invalid textPageId body parameter')
    return
  }

  try {
    const createdItem = await createChapter(textPageId, chapterData)
    res.status(200).json(createdItem)
  } catch (e) {
    if (e.name === 'ValidationError') {
      res
        .status(400)
        .send('Invalid value for field(s): ' + Object.keys(e.errors).join(', '))
      return
    }
    res.status(400).send(`Failed to create chapter: ${getErrorMessage(e)}`)
  }
}
export default corsMiddleware(authMiddleware(handler))
