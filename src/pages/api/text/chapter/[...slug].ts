import { Types } from 'mongoose'
import type { NextApiRequest, NextApiResponse } from 'next'
import authMiddleware from 'src/middlewares/authMiddleware'
import corsMiddleware from 'src/middlewares/corsMiddleware'
import { TextChapterModel } from 'src/models/text/TextChapter'
import {
  deleteChapter,
  updateOrder,
  duplicateChapter,
  moveChapter,
  queryChapterMinimalForFront,
  updateChapter,
} from 'src/services/TextService.chapter'
import { getErrorMessage, getHttpStatus } from 'src/utils/error'
import { isArray } from 'src/utils/tsUtil'

/**
 * Main function to route to dedicated handlers
 */
export async function handler(req: NextApiRequest, res: NextApiResponse) {
  if (isArray(req.query.slug) && req.query.slug.length > 0) {
    const subPath = req.query.slug[0]
    const slugLength = req.query.slug.length
    if (subPath) {
      switch (req.method) {
        case 'GET': {
          // /text/chapter/${id}
          if (Types.ObjectId.isValid(subPath)) {
            await handleGetOne(subPath, res)
            return
          }
          break
        }

        case 'POST': {
          // /text/chapter/${id}/duplicate/${textPageId}
          if (
            Types.ObjectId.isValid(subPath) &&
            slugLength === 3 &&
            req.query.slug[1] === 'duplicate' &&
            Types.ObjectId.isValid(req.query.slug[2])
          ) {
            await handleDuplicate(subPath, req.query.slug[2], res)
            return
          }
          break
        }

        case 'PUT': {
          // /text/chapter/order
          if (subPath === 'order') {
            await handleUpdateOrder(req, res)
            return
          }
          if (Types.ObjectId.isValid(subPath)) {
            // /text/chapter/${id}
            if (slugLength === 1) {
              await handleUpdate(subPath, req, res)
              return
            }
            // /text/chapter/${id}/move-to/${textPageId}
            if (
              slugLength === 3 &&
              req.query.slug[1] === 'move-to' &&
              Types.ObjectId.isValid(req.query.slug[2])
            ) {
              await handleMove(subPath, req.query.slug[2], res)
              return
            }
          }
          break
        }

        case 'DELETE': {
          // /text/chapter/${id}
          if (Types.ObjectId.isValid(subPath)) {
            await handleDelete(subPath, res)
            return
          }
          break
        }

        default:
      }
    }
  }
  res.status(405).send('')
}

/**
 * Get a specific chapter
 */
async function handleGetOne(chapterId: string, res: NextApiResponse) {
  const chapter = await queryChapterMinimalForFront(chapterId)

  if (!chapter) {
    res.status(404).send('')
    return
  }
  res.status(200).json(chapter)
}

/**
 * Update chapters order
 */
async function handleUpdateOrder(req: NextApiRequest, res: NextApiResponse) {
  const { idOrderMap } = req.body

  try {
    await updateOrder(idOrderMap)
    res.status(200).send('')
  } catch (e) {
    res
      .status(400)
      .send(`Failed to update chapters order: ${getErrorMessage(e)}`)
  }
}

/**
 * Duplicate one chapter
 */
async function handleDuplicate(
  chapterId: string,
  textPageId: string,
  res: NextApiResponse
) {
  try {
    await duplicateChapter(textPageId, chapterId)
    res.status(200).send('')
  } catch (e) {
    res.status(400).send(`Failed to duplicate chapter: ${getErrorMessage(e)}`)
  }
}

/**
 * Move a chapter to another text page
 */
async function handleMove(
  chapterId: string,
  targetTextPageId: string,
  res: NextApiResponse
) {
  try {
    await moveChapter(chapterId, targetTextPageId)
    res.status(200).send('')
  } catch (e) {
    res.status(400).send(`Failed to move chapter: ${getErrorMessage(e)}`)
  }
}

/**
 * Update a chapter
 */
async function handleUpdate(
  chapterId: string,
  req: NextApiRequest,
  res: NextApiResponse
) {
  const item = await TextChapterModel.findById(chapterId)
  if (!item) {
    res.status(404).send('Chapter not found, id: ' + chapterId)
    return
  }

  const { __v, ...data } = req.body
  try {
    const updatedItem = await updateChapter(data)
    res.status(200).json(updatedItem)
  } catch (e) {
    if (e.name === 'ValidationError') {
      res
        .status(400)
        .send('Invalid value for field(s): ' + Object.keys(e.errors).join(', '))
      return
    }
    res.status(400).send(`Failed to update chapter: ${getErrorMessage(e)}`)
  }
}

/**
 * Delete a chapter
 */
async function handleDelete(chapterId: string, res: NextApiResponse) {
  try {
    await deleteChapter(chapterId)
    res.status(200).send('')
  } catch (e) {
    res
      .status(getHttpStatus(e))
      .send(`Failed to delete chapter: ${getErrorMessage(e)}`)
  }
}

export default corsMiddleware(authMiddleware(handler))
