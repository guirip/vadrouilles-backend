import { Types } from 'mongoose'
import type { NextApiRequest, NextApiResponse } from 'next'
import authMiddleware from 'src/middlewares/authMiddleware'
import corsMiddleware from 'src/middlewares/corsMiddleware'
import { TextPageModel } from 'src/models/text/TextPage'
import {
  deletePage,
  listImageBlocks,
  updateOrder,
  updatePage,
} from 'src/services/TextService.page'
import { getErrorMessage, getHttpStatus } from 'src/utils/error'
import { isArray } from 'src/utils/tsUtil'

/**
 * Main function to route to dedicated handlers
 */
export async function handler(req: NextApiRequest, res: NextApiResponse) {
  if (isArray(req.query.slug) && req.query.slug.length > 0) {
    const subPath = req.query.slug[0]
    const slugLength = req.query.slug.length
    if (subPath) {
      switch (req.method) {
        case 'GET': {
          // /text/page/${id}/images
          if (
            Types.ObjectId.isValid(subPath) &&
            slugLength === 2 &&
            req.query.slug[1] === 'images'
          ) {
            await handleGetImages(subPath, res)
            return
          }
          break
        }

        case 'PUT':
          // /text/page/order
          if (subPath === 'order') {
            await handleUpdateOrder(req, res)
            return
          }
          // /text/page/${id}
          if (Types.ObjectId.isValid(subPath)) {
            await handleUpdate(subPath, req, res)
            return
          }
          break

        case 'DELETE':
          // /text/page/${id}
          if (Types.ObjectId.isValid(subPath)) {
            await handleDelete(subPath, res)
            return
          }
          break

        default:
      }
    }
  }
  res.status(405).send('')
}

/**
 * Get textPage image blocks
 */
async function handleGetImages(textPageId: string, res) {
  try {
    const images = await listImageBlocks(textPageId)
    res.status(200).json(images)
  } catch (e) {
    res.status(400).send(`Failed to list images: ${getErrorMessage(e)}`)
  }
}

/**
 * Update pages order
 */
async function handleUpdateOrder(req: NextApiRequest, res: NextApiResponse) {
  const { idOrderMap } = req.body

  try {
    await updateOrder(idOrderMap)
    res.status(200).send('')
  } catch (e) {
    res.status(400).send(`Failed to update pages order: ${getErrorMessage(e)}`)
  }
}

/**
 * Update a page
 */
async function handleUpdate(
  _id: string,
  req: NextApiRequest,
  res: NextApiResponse
) {
  const item = await TextPageModel.findById(_id)
  if (!item) {
    res.status(404).send('Page not found, id: ' + _id)
    return
  }

  const { __v, ...data } = req.body
  try {
    const updatedItem = await updatePage(data)
    res.status(200).json(updatedItem)
  } catch (e) {
    if (e.name === 'ValidationError') {
      res
        .status(400)
        .send('Invalid value for field(s): ' + Object.keys(e.errors).join(', '))
      return
    }
    res.status(400).send(`Failed to update page: ${getErrorMessage(e)}`)
  }
}

/**
 * Delete a page
 */
async function handleDelete(_id: string, res: NextApiResponse) {
  try {
    await deletePage(_id)
    res.status(200).send('')
  } catch (e) {
    res
      .status(getHttpStatus(e))
      .send(`Failed to delete page: ${getErrorMessage(e)}`)
  }
}

export default corsMiddleware(authMiddleware(handler))
