import type { FilterQuery } from 'mongoose'
import type { NextApiRequest, NextApiResponse } from 'next'

import corsMiddleware from 'src/middlewares/corsMiddleware'
import type { ContentCategory } from 'src/models/common/ContentCategory'
import type { ITextPage } from 'src/models/text/TextPage'
import { isValidTextCategory } from 'src/models/text/TextPage.type'
import { isApiAuthOk } from 'src/services/AuthService'
import {
  createPage,
  getFullPage,
  getPages,
} from 'src/services/TextService.page'
import { getErrorMessage } from 'src/utils/error'

/**
 * Main function to route to dedicated handlers
 *
 * @param  {ClientRequest} req
 * @param  {ServerResponse} res
 */
export async function handler(req: NextApiRequest, res: NextApiResponse) {
  switch (req.method) {
    case 'GET':
      await handleGet(req, res)
      break

    case 'POST':
      await handleCreate(req, res)
      break

    default:
      res.status(405).send('')
  }
}

const getSingleValueFromArray = (value: string | string[] | undefined) =>
  Array.isArray(value) ? value[0] : value

/**
 * Get a page and its children hierarchy
 *
 * @param  {ClientRequest} req
 * @param  {ServerResponse} res
 */
async function handleGet(req: NextApiRequest, res: NextApiResponse) {
  const category = getSingleValueFromArray(req.query.category)
  if (category && !isValidTextCategory(category)) {
    res.status(200).json([])
    return
  }

  const { slug } = req.query
  const _id = getSingleValueFromArray(req.query._id)
  const _slug = getSingleValueFromArray(req.query._slug)
  const visibility = getSingleValueFromArray(req.query.visibility)

  const onlyVisible = !visibility || visibility !== 'all'

  const pageCriteria: FilterQuery<ITextPage> = {}

  // handling path such as /api/text/page/[pageId]
  if (Array.isArray(slug) && slug.length > 1) {
    pageCriteria._id = slug[1]

    // handling path such as /api/text/page?_id=...
  } else if (_id) {
    pageCriteria.slug = _id

    // handling path such as /api/text/page?_slug=...
  } else if (_slug) {
    pageCriteria.slug = _slug
  }

  // Expose fully populated content
  if (Object.keys(pageCriteria).length > 0) {
    const page = await getFullPage(pageCriteria, onlyVisible)

    if (!page || page.chapters.length === 0) {
      res.status(404).send('page not found')
      return
    }

    res.status(200).json(page)
    return
  }

  pageCriteria.category = category as ContentCategory

  const pages = await getPages(pageCriteria, onlyVisible)
  res.status(200).json(pages)
}

/**
 * Create a page
 *
 * @param  {ClientRequest} req
 * @param  {ServerResponse} res
 */
async function handleCreate(req: NextApiRequest, res: NextApiResponse) {
  if (!(await isApiAuthOk(req, res))) {
    res.status(401).send('')
    return
  }

  // Safety net
  const { _id, ...data } = req.body

  try {
    const createdItem = await createPage(data)
    res.status(200).json(createdItem)
  } catch (e) {
    if (e.name === 'ValidationError') {
      res
        .status(400)
        .send('Invalid value for field(s): ' + Object.keys(e.errors).join(', '))
      return
    }
    res.status(400).send(`Failed to create text page: ${getErrorMessage(e)}`)
  }
}

export default corsMiddleware(handler)
