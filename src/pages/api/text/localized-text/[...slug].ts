import { Types } from 'mongoose'
import type { NextApiRequest, NextApiResponse } from 'next'
import authMiddleware from 'src/middlewares/authMiddleware'
import corsMiddleware from 'src/middlewares/corsMiddleware'
import { isLang } from 'src/models/common/Lang'
import type { ILocalizedText } from 'src/models/text/LocalizedText'
import { LocalizedTextModel } from 'src/models/text/LocalizedText'
import { toSerializable } from 'src/services/DbService'
import {
  createTranslation,
  deleteLocalizedText,
  updateLocalizedText,
} from 'src/services/TextService.localizedText'
import {
  BadRequestError,
  getErrorMessage,
  getHttpStatus,
} from 'src/utils/error'
import { isArray } from 'src/utils/tsUtil'

/**
 * Main function to route to dedicated handlers
 */
export async function handler(req: NextApiRequest, res: NextApiResponse) {
  if (isArray(req.query.slug) && req.query.slug.length > 0) {
    const subPath = req.query.slug[0]
    const slugLength = req.query.slug.length
    if (subPath) {
      switch (req.method) {
        case 'GET': {
          // /text/localized-text/${id}
          if (Types.ObjectId.isValid(subPath)) {
            await handleGetOne(subPath, res)
            return
          }
          break
        }

        case 'POST': {
          // /localized-text/${id}/translate-to/${targetLang}
          if (
            Types.ObjectId.isValid(subPath) &&
            slugLength === 3 &&
            req.query.slug[1] === 'translate-to'
          ) {
            await translate(subPath, req.query.slug[2], res)
            return
          }
          break
        }

        case 'PUT': {
          // /text/localized-text/${id}
          if (Types.ObjectId.isValid(subPath)) {
            await handleUpdate(subPath, req, res)
            return
          }
          break
        }

        case 'DELETE':
          // /text/localized-text/${id}
          if (Types.ObjectId.isValid(subPath)) {
            await handleDelete(subPath, res)
            return
          }
          break

        default:
      }
    }
  }
  res.status(405).send('')
}

/**
 * Get a specific localizedText
 */
async function handleGetOne(localizedTextId: string, res: NextApiResponse) {
  const localizedText =
    await LocalizedTextModel.findById(localizedTextId).lean<ILocalizedText>()

  if (!localizedText) {
    res
      .status(404)
      .send(`No localized text found matching id ${localizedTextId}`)
    return
  }

  const _lt = toSerializable<ILocalizedText>(localizedText) // needed?

  res.status(200).json(_lt)
}

/**
 * Translate a localized text
 */
async function translate(
  localizedTextId: string,
  targetLang: string,
  res: NextApiResponse
) {
  try {
    if (!isLang(targetLang)) {
      throw new BadRequestError(`${targetLang} is not a valid target lang`)
    }
    await createTranslation(localizedTextId, targetLang)
    res.status(200).send('')
  } catch (e) {
    res
      .status(400)
      .send(`Failed to translate localized text: ${getErrorMessage(e)}`)
  }
}

/**
 * Update a localized text
 */
async function handleUpdate(
  ltId: string,
  req: NextApiRequest,
  res: NextApiResponse
) {
  const item = await LocalizedTextModel.findById(ltId)
  if (!item) {
    res.status(404).send('Localized text not found, id: ' + ltId)
    return
  }

  const { __v, ...data } = req.body
  try {
    const updatedItem = await updateLocalizedText(data)
    res.status(200).json(updatedItem)
  } catch (e) {
    if (e.name === 'ValidationError') {
      res
        .status(400)
        .send('Invalid value for field(s): ' + Object.keys(e.errors).join(', '))
      return
    }
    res
      .status(400)
      .send(`Failed to update localized text: ${getErrorMessage(e)}`)
  }
}

/**
 * Delete a localized text
 */
async function handleDelete(_id: string, res: NextApiResponse) {
  try {
    await deleteLocalizedText(_id)
    res.status(200).send('')
  } catch (e) {
    res
      .status(getHttpStatus(e))
      .send(`Failed to delete localized text: ${getErrorMessage(e)}`)
  }
}

export default corsMiddleware(authMiddleware(handler))
