import { decode } from 'html-entities'
import type { FilterQuery } from 'mongoose'
import { Types } from 'mongoose'
import type { NextApiRequest, NextApiResponse } from 'next'

import corsMiddleware from 'src/middlewares/corsMiddleware'
import { isParagraphBlock } from 'src/models/text/TextBlock.type'
import type { ITextChapter } from 'src/models/text/TextChapter'
import { isApiAuthOk } from 'src/services/AuthService'
import { toSerializable } from 'src/services/DbService'
import {
  createLocalizedText,
  getLocalizedTextsByChapter,
} from 'src/services/TextService.localizedText'
import { getErrorMessage } from 'src/utils/error'

/**
 * Main function to route to dedicated handlers
 *
 * @param  {ClientRequest} req
 * @param  {ServerResponse} res
 */
export async function handler(req: NextApiRequest, res: NextApiResponse) {
  switch (req.method) {
    case 'GET':
      await handleGet(req, res)
      break

    case 'POST':
      await handleCreate(req, res)
      break

    default:
      res.status(405).send('')
  }
}

/**
 * Get the localized text belonging to a given chapter
 *
 * @param  {ClientRequest} req
 * @param  {ServerResponse} res
 */
async function handleGet(req: NextApiRequest, res: NextApiResponse) {
  const { chapterId, chapterSlug, lang } = req.query

  if (!chapterId && !chapterSlug) {
    res
      .status(400)
      .send('chapterId or chapterSlug query parameter must be provided')
    return
  }
  if (
    chapterId &&
    (typeof chapterId !== 'string' || !Types.ObjectId.isValid(chapterId))
  ) {
    res
      .status(400)
      .send("'chapterId' query parameter (when provided) should be a valid id")
    return
  }
  if (chapterSlug && typeof chapterSlug !== 'string') {
    res
      .status(400)
      .send("'chapterSlug' query parameter (when provided) should be a string")
    return
  }

  const chapterCriteria: FilterQuery<ITextChapter> = {}
  if (chapterId) {
    chapterCriteria._id = chapterId
  } else if (chapterSlug) {
    chapterCriteria.slug = chapterSlug
  }
  const localizedTexts = await getLocalizedTextsByChapter(chapterCriteria)

  if (lang) {
    // Only one localizedText per lang is expected
    const localizedText = localizedTexts?.find((a) => a.lang === lang)

    if (localizedText) {
      // decode html entities
      if (Array.isArray(localizedText.content?.blocks)) {
        localizedText.content.blocks.forEach((b) => {
          if (isParagraphBlock(b)) {
            b.data.text = decode(b.data.text)
          }
        })
      }
      res.status(200).json(toSerializable(localizedText))
    } else {
      res
        .status(404)
        .send(
          `No localizedText found matching ${
            chapterId ? `id ${chapterId}` : ''
          }${chapterSlug ? `slug ${chapterSlug}` : ''} and lang ${lang}`
        )
    }
    return
  }

  res.status(200).json(localizedTexts)
}

/**
 * Create a localized text
 *
 * @param  {ClientRequest} req
 * @param  {ServerResponse} res
 */
async function handleCreate(req: NextApiRequest, res: NextApiResponse) {
  if (!(await isApiAuthOk(req, res))) {
    res.status(401).send('')
    return
  }

  // Safety net
  const { _id, chapterId, ...ltData } = req.body

  if (!chapterId) {
    res.status(400).send('Missing chapterId body parameter')
    return
  }

  try {
    const createdItem = await createLocalizedText(chapterId, ltData)
    res.status(200).json(createdItem)
  } catch (e) {
    if (e.name === 'ValidationError') {
      res
        .status(400)
        .send('Invalid value for field(s): ' + Object.keys(e.errors).join(', '))
      return
    }
    res
      .status(400)
      .send(`Failed to create localized text: ${getErrorMessage(e)}`)
  }
}

export default corsMiddleware(handler)
