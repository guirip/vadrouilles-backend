# ARTEFACTS

## BACKLOG

Have a look at Next's Youtube Embed :
 https://nextjs.org/docs/app/building-your-application/optimizing/third-party-libraries#youtube-embed

FIXME

Handle GalleryBlock for first image to use as metadata

Rename Pyrenees parts:
 - part 1 = Pays Basque
 - part 2 = Béarn ??
 - part 3 = Pyréneés centrales ??
 - part 4 = Portillon -> Andorre
 - part 5 = Pyrénées orientales

- instagram posts don't load after shallow navigation (only on initial page display)

- gallery image shrink = deformation on safari mobile

- catalog tab navigation (currently broken)

- purge unused CSS - tried but lib is uneffective (next 14 support issue?)

- replace `<PageNotFound />` by 404 redirect?
- `not-found.tsx` see [doc](https://nextjs.org/docs/app/api-reference/file-conventions/not-found)

- handle size parameters on video blocks
  - old vimeo videos are 800x600 and show white rectangles on the website

- Text feature:
  - text slug unique index: should be contextualized to parent (category for textpage, textpage for chapter)
  - text: auto save mecanism?

- CatalogItemForm refactor

- sorte de Link Tree ? pour lister toutes les vidéos depuis instagram par exemple

- seuls nos corps: open pictures in same page using `Gallery` component?

- migrate pages/api (unit testing api using Next's App router in currently PITA)

- i18n on catalog items (?)

- find another comment system ?

- might be able to get rid of animejs dependency (used in tabs/index.tsx) to reduce app size
  - see scrollBy usage in text-components/Summary.tsx

## INCREMENT

### FRONTEND INCREMENT

- switch between nature and graffiti content
- display a list of videos from youtube and/or vimeo
- switch between french and english languages
- easily deploy the app
- responsive design
- handle https
- update `<head>` attributes depending on context (page, tab), for SEO friendliness
- catalog of products (artworks)
- catalog route renders to another tab (videos) when feature is disabled
- tech: TypeScript support
- display texts and navigate through their content
- preview texts
- attach text to video
- every page now is generic is relies on a TextPage instance

### BACKEND INCREMENT

- login feature
- expose api
- responsive design
- server side rendering
- generate sitemap.xml

- CATALOG
  - create items
  - update items
  - delete items
  - upload pictures
  - remove pictures
  - define the main picture
  - drag/drop items to set their order of appearance
  - retrieve only visible items
  - can edit and provide a feature flag

- TEXT
  - create texts, chapters, and localized content
  - categorize texts
  - order chapters by drag and drop
  - duplicate chapters
  - preview content
  - translate content
  - reuse an uploaded image
  - add video blocks
  - display a gallery of pictures/video
  - add a table of contents
  - add nested list elements
  - add tables
  - drag and drop blocks (desktop only)
  - duplicate a chapter
  - move a chapter to another page from same category
  - change page category
  - reorder pages by drag and drop
