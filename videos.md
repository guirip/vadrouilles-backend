
## GRAFFITI

  qyy4ZJJtTAs                          // old 3
  VzTxqz_WkmI                          // old 2
  TY8ep0q2tbw                          // old 1 quickie
  CGLcVy6aQZw                          // old 1
  wvZEPe7-kJI                          // old, countryside
  VocBljDquhw                          // solo freestyle du visage pâle
  -Pb8JjXK15Y                          // peinture 1 APHP
  0YjWDXcCEEo                          // session meaux à 3
  bp9mFU71A2g                          // abstract graffiti drawing
  70kpeONHe5I                          // meaux abstract graffiti session
  B1wMie2OAZM                          // experimental peinture pinceau
  BKpA-K-BCjo                          // crépy abstract graffiti session
  dPLqHIgwhKg                          // crépy session avec valdi
  28894553" width="640" height="360    // mouarf video st cyr
  29155413" width="640" height="360    // changeons d'ère
  R93xWwJkLXY                          // orphelinat (youtube)
  // 18091341 width="640" height="360" // orphelinat (vimeo)
  15598609 width="640" height="480"    // abstract compilation
  11843244 width="640" height="424"    // no stress - session avec soso toto
  10549457 width="640" height="424"    // abandoned graffiti timelapse - session avec toto
  10561456 width="640" height="424"    // afternoon with the crew

## NATURE

  OlvhdPLCKBQ  // Morvan à Noël version courte - VAEP3
  fcf2gemBy1Q  // Morvan à Noël version longue - VAEP3
  8DznoRdihkQ  // Inspiration en Chartreuse
  NSptxLreHAY  // 2ème Journée en Vanoise
  9WrOr2vNn0s  // 1ère Journée en Vanoise
  j0CWvuuM3us  // Forêt d'Orient - VAEP2
  FDnZIemWwMU  // VARN2
  Amj02H0X_DI  // Morvan vidéo 1 - VAEP1
  Vd_cMb5Sxuo  // VARN1
  bx-PLx2oX1g  // aubetin

