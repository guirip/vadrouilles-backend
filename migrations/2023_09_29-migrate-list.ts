import { HydratedDocument } from 'mongoose'
import {
  ILocalizedText,
  LocalizedTextModel,
} from 'src/models/text/LocalizedText'
import { isListBlock } from 'src/models/text/TextBlock.type'
import { connect, disconnect } from 'src/services/DbService'

function checkListMigration(lt: HydratedDocument<ILocalizedText>) {
  let performed = false
  if (Array.isArray(lt.content.blocks)) {
    for (const block of lt.content.blocks) {
      if (isListBlock(block)) {
        block.data.items = block.data.items.map((item) => {
          if (typeof item === 'string') {
            performed = true
            return {
              content: item,
              items: [],
            }
          }
          return item
        })
      }
    }
  }
  return performed
}

;(async function () {
  await connect()

  const lts = await LocalizedTextModel.find()

  try {
    for (const lt of lts) {
      const hasMigrated = checkListMigration(lt)
      if (hasMigrated) {
        console.log(`Has Migrated localized text ${lt._id}`)
        lt.markModified('content')
        await lt.save()
      }
    }
  } catch (e) {
    console.error(e)
  }

  await disconnect()
})()
