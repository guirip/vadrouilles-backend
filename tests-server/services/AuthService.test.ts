import { getServerSession } from 'next-auth/next'
import { createRequest, createResponse } from 'node-mocks-http'
import type { NextApiRequest, NextApiResponse } from 'next/types'
import * as AuthService from 'src/services/AuthService'

jest.mock('next-auth/next')

describe('auth service', function () {
  it('should return true when there is a session object', async function () {
    // eslint-disable-next-line @typescript-eslint/ban-ts-comment
    // @ts-ignore
    getServerSession.mockReturnValueOnce({})

    const result = await AuthService.isApiAuthOk(
      createRequest<NextApiRequest>(),
      createResponse<NextApiResponse>()
    )
    expect(result).toBe(true)
  })

  it('should return false when there is a session object', async function () {
    // eslint-disable-next-line @typescript-eslint/ban-ts-comment
    // @ts-ignore
    getServerSession.mockReturnValueOnce(null)

    const result = await AuthService.isApiAuthOk(
      createRequest<NextApiRequest>(),
      createResponse<NextApiResponse>()
    )
    expect(result).toBe(false)
  })
})
