import { CatalogItemModel } from 'src/models/CatalogItem'
import { CatalogItemCategoryModel } from 'src/models/CatalogItemCategory'
import { getCatalogData } from 'src/services/CatalogService'
import { connect, disconnect } from 'src/services/DbService'
import { generateItems } from 'tests-server/catalog-helpers'
import { clearCollections } from 'tests-server/helpers'

describe('Catalog service', function () {
  beforeAll(async function () {
    await connect()
    await CatalogItemModel.deleteMany()
    await CatalogItemCategoryModel.deleteMany()
  })

  afterEach(async function () {
    await clearCollections()
  })

  afterAll(async function () {
    await disconnect()
  })

  describe('getCatalogData', function () {
    beforeEach(async function () {
      await CatalogItemCategoryModel.deleteMany()
      const [cat1, cat2, nonVisibleCat] = await CatalogItemCategoryModel.create(
        [
          {
            name: {
              fr: 'catégorie test 1',
              en: 'test category 1',
            },
            visibility: true,
            order: 0,
            slug: 'slug-1',
          },
          {
            name: {
              fr: 'catégorie test 2',
              en: 'test category 2',
            },
            visibility: true,
            order: 1,
            slug: 'slug-2',
          },
          {
            name: {
              fr: 'catégorie test 3',
              en: 'test category 3',
            },
            visibility: false,
            order: 2,
            slug: 'slug-3',
          },
        ]
      )
      await CatalogItemModel.deleteMany()

      await generateItems([cat1, cat2, nonVisibleCat])
    })

    it('should return categories and all items grouped by category', async function () {
      const { itemsPerCategory, categories } = await getCatalogData()

      expect(Array.isArray(categories)).toBeTruthy()
      expect(categories.length).toBe(2)

      categories.forEach((category) => {
        const catId = category._id.toString()
        const items = itemsPerCategory[catId]
        items.forEach((item) => {
          expect(item.category?.toString()).toBe(catId)
          expect(item.visibility).toBeTruthy()
        })
      })
    })
  })
})
