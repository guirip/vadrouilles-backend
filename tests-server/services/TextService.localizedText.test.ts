import type { ILocalizedText } from 'src/models/text/LocalizedText'
import { LocalizedTextModel } from 'src/models/text/LocalizedText'
import { Lang } from 'src/models/common/Lang'
import type { IHeaderBlock } from 'src/models/text/TextBlock.type'
import { BlockType } from 'src/models/text/TextBlock.type'
import { TextChapterModel } from 'src/models/text/TextChapter'
import { connect, disconnect } from 'src/services/DbService'
import {
  createLocalizedText,
  deleteLocalizedText,
  duplicateLocalizedText,
  getLocalizedTextsByChapter,
  updateLocalizedText,
} from 'src/services/TextService.localizedText'
import { clearCollections } from 'tests-server/helpers'
import {
  createDummyChapter,
  createNDummyLocalizedText,
} from 'tests-server/text-data-helpers'

describe('Text service', function () {
  beforeAll(async function () {
    await connect()
    await clearCollections()
  })

  afterEach(async function () {
    await clearCollections()
  })

  afterAll(disconnect)

  it('create localizedTexts then get them by chapter id', async function () {
    const chapter1 = await createDummyChapter()
    const chapter2 = await createDummyChapter()

    interface ITestInput {
      chapterId: string
      data: Omit<ILocalizedText, '_id'>
    }
    const LOCALIZED_TEXTS: ITestInput[] = [
      {
        chapterId: chapter1._id.toString(),
        data: {
          title: 'Hey localizedText 1',
          lang: Lang.en,
          description: 'desc 1',
          content: {
            time: Date.now(),
            blocks: [{ type: BlockType.Paragraph, data: 'par ici' }],
            version: '1.0.1',
          },
        },
      },
      {
        chapterId: chapter1._id.toString(),
        data: {
          title: 'Salut localizedText 1',
          lang: Lang.fr,
          description: 'desc 1 encore',
          content: {
            time: Date.now(),
            blocks: [{ type: BlockType.Header, data: 'par là' }],
            version: '1.0.2',
          },
        },
      },
      {
        chapterId: chapter2._id.toString(),
        data: {
          title: 'Hey localizedText 2',
          lang: Lang.en,
          description: 'desc 2',
          content: {
            time: Date.now(),
            blocks: [
              {
                type: BlockType.List,
                data: [
                  { content: 'un', items: [] },
                  { content: 'deux', items: [] },
                  { content: 'trois', items: [] },
                ],
              },
            ],
            version: '1.0.3',
          },
        },
      },
    ]
    const createdLocalizedTexts = await Promise.all(
      LOCALIZED_TEXTS.map((input) =>
        createLocalizedText(input.chapterId, input.data)
      )
    )
    const leantCreatedLocalizedTexts = createdLocalizedTexts.map((a) =>
      a.toObject()
    )
    expect(leantCreatedLocalizedTexts.length).toBe(LOCALIZED_TEXTS.length)

    const localizedTextsChapter1 = await getLocalizedTextsByChapter({
      _id: chapter1._id,
    })
    expect(Array.isArray(localizedTextsChapter1)).toBeTruthy()
    expect(localizedTextsChapter1.length).toBe(2)

    const localizedTextsChapter2 = await getLocalizedTextsByChapter({
      _id: chapter2._id,
    })
    expect(Array.isArray(localizedTextsChapter2)).toBeTruthy()
    expect(localizedTextsChapter2.length).toBe(1)

    function checkLocalizedText(inputLocalizedText) {
      const createdLocalizedText = LOCALIZED_TEXTS.find(
        (p) => p.data.title === inputLocalizedText.title
      )
      expect(createdLocalizedText?.data.lang).toBe(inputLocalizedText.lang)
      expect(createdLocalizedText?.data.content).toStrictEqual(
        inputLocalizedText.content
      )
    }
    leantCreatedLocalizedTexts.forEach(checkLocalizedText)
  })

  describe('updateLocalizedText', function () {
    it('should throw if chapter has a localized text with same lang', async function () {
      const chapter = await createDummyChapter()

      const [lt1, lt2] = await LocalizedTextModel.create([
        { title: 'lt fr', lang: Lang.fr },
        { title: 'lt en', lang: Lang.en },
      ])

      chapter.set({ localizedTexts: [lt1._id, lt2._id] })
      await chapter.save()

      let hasThrown = false
      try {
        await updateLocalizedText({
          ...lt2.toObject(),
          lang: Lang.fr,
        })
      } catch (e) {
        hasThrown = true
      }
      expect(hasThrown).toBeTruthy()
    })

    it('should update localized text', async function () {
      const chapter1 = await createDummyChapter()
      const chapter2 = await createDummyChapter()

      interface ITestInput {
        chapterId: string
        data: Omit<ILocalizedText, '_id'>
      }
      const LOCALIZED_TEXTS: ITestInput[] = [
        {
          chapterId: chapter1._id.toString(),
          data: {
            title: 'Hey localizedText 1',
            lang: Lang.en,
            description: 'desc 1',
            content: {
              time: Date.now(),
              blocks: [{ type: BlockType.Paragraph, data: 'par ici' }],
              version: '1.0.1',
            },
          },
        },
        {
          chapterId: chapter1._id.toString(),
          data: {
            title: 'Salut localizedText 1',
            lang: Lang.fr,
            description: 'desc 1 encore',
            content: {
              time: Date.now(),
              blocks: [{ type: BlockType.Header, data: 'par là' }],
              version: '1.0.2',
            },
          },
        },
        {
          chapterId: chapter2._id.toString(),
          data: {
            title: 'Hey localizedText 2',
            lang: Lang.en,
            description: 'desc 2',
            content: {
              time: Date.now(),
              blocks: [
                {
                  type: BlockType.List,
                  data: [
                    { content: 'un', items: [] },
                    { content: 'huit', items: [] },
                    { content: 'trois', items: [] },
                  ],
                },
              ],
              version: '1.0.3',
            },
          },
        },
        {
          chapterId: chapter2._id.toString(),
          data: {
            title: 'Hey localizedText TWO',
            lang: Lang.fr,
            description: 'as you know...',
            content: {
              time: Date.now(),
              blocks: [
                {
                  type: BlockType.List,
                  data: [
                    { content: 'quatre', items: [] },
                    { content: 'cinq', items: [] },
                    { content: 'sept', items: [] },
                  ],
                },
              ],
              version: '1.0.4',
            },
          },
        },
      ]
      await Promise.all(
        LOCALIZED_TEXTS.map((input) =>
          createLocalizedText(input.chapterId, input.data)
        )
      )

      const lt = await LocalizedTextModel.findOne({
        title: LOCALIZED_TEXTS[0].data.title,
      })
      if (!lt) {
        throw new Error('text should exist')
      }
      const { _id } = lt
      expect(_id).toBeDefined()

      const NEW_VALUES = {
        _id,
        title: 'Hola',
        content: {
          time: new Date().getTime(),
          blocks: [
            {
              type: BlockType.Header,
              data: { text: 'TITLE', level: 4 },
            },
          ],
          version: '4.0.3',
        },
        description: 'Qué tal?',
      }
      await updateLocalizedText(NEW_VALUES)

      const updatedLocalizedText = await LocalizedTextModel.findById(_id)
      if (!updatedLocalizedText) {
        throw new Error('updated text should exist')
      }

      expect(updatedLocalizedText.title).toBe(NEW_VALUES.title)
      expect(updatedLocalizedText.content.time).toStrictEqual(
        NEW_VALUES.content.time
      )
      expect(updatedLocalizedText.content.version).toStrictEqual(
        NEW_VALUES.content.version
      )
      expect(
        (updatedLocalizedText.content.blocks[0] as IHeaderBlock).data.text
      ).toStrictEqual((NEW_VALUES.content.blocks[0] as IHeaderBlock).data.text)
      expect(updatedLocalizedText.description).toStrictEqual(
        NEW_VALUES.description
      )
    })
  })

  it('delete localizedText', async function () {
    const chapterStep1 = await createDummyChapter()

    await createLocalizedText(chapterStep1._id, {
      title: 'truc1',
      lang: Lang.fr,
    })
    const { _id } = await createLocalizedText(chapterStep1._id, {
      title: 'Hey localizedText 2',
      lang: Lang.en,
      content: {
        time: Date.now(),
        blocks: [{ type: BlockType.Header, data: 'TITLE' }],
        version: '2.0.3',
      },
    })
    expect(await LocalizedTextModel.findById(_id)).toBeDefined()

    const chapterStep2 = await TextChapterModel.findById(chapterStep1._id)
    if (!chapterStep2) {
      throw new Error('chapterStep2 should be defined')
    }
    expect(chapterStep2.localizedTexts.length).toBe(2)

    await deleteLocalizedText(_id)

    expect(await LocalizedTextModel.findById(_id)).toBeFalsy()

    const chapterStep3 = await TextChapterModel.findById(chapterStep1._id)
    if (!chapterStep3) {
      throw new Error('chapterStep3 should be defined')
    }
    expect(chapterStep3.localizedTexts.length).toBe(1)
  })

  it('duplicate localized text', async function () {
    const dummyChapter = await createDummyChapter()
    const [sampleLt] = await createNDummyLocalizedText(dummyChapter._id, 1)

    const duplicatedLt = await duplicateLocalizedText(sampleLt._id)

    expect(duplicatedLt._id.toString()).toBeDefined()
    expect(duplicatedLt._id.toString()).not.toBe(sampleLt._id.toString())

    const duplicatedLtFromDb = await LocalizedTextModel.findById(
      duplicatedLt._id
    ).lean()
    if (!duplicatedLtFromDb) {
      throw new Error('Duplicated localized text should exist in DB')
    }
    expect(duplicatedLtFromDb.title.includes(sampleLt.title)).toBeTruthy()
    expect(duplicatedLtFromDb.lang).toBe(sampleLt.lang)
    expect(duplicatedLtFromDb.description).toBe(sampleLt.description)
    expect(duplicatedLtFromDb.content.time).toBe(sampleLt.content.time)
    expect(duplicatedLtFromDb.content.version).toBe(sampleLt.content.version)
    expect(duplicatedLtFromDb.content.blocks.length).toBe(
      sampleLt.content.blocks.length
    )
  })
})
