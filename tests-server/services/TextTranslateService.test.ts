import { config } from 'dotenv'

import { Lang } from 'src/models/common/Lang'
import type {
  IHeaderBlock,
  IImageBlock,
  IListBlock,
  IParagraphBlock,
  ITableBlock,
} from 'src/models/text/TextBlock.type'
import { BlockType, TextVariantTune } from 'src/models/text/TextBlock.type'
import {
  translateHeaderBlock,
  translateImageBlock,
  translateListBlock,
  translateParagraphBlock,
  translateTableBlock,
} from 'src/services/TextTranslateService'
import * as translation from 'src/utils/translation'

const mock = (text: string) =>
  jest.spyOn(translation, 'translate').mockImplementation(
    () =>
      new Promise((resolve) =>
        resolve({
          translations: [
            {
              detected_source_language: '',
              text,
            },
          ],
        })
      )
  )

describe('TextTranslateService', function () {
  beforeAll(() => {
    config()
  })

  afterEach(() => {
    jest.clearAllMocks()
  })

  it('should translate paragraph block', async function () {
    const frBlock: IParagraphBlock = {
      type: BlockType.Paragraph,
      data: {
        text: 'Un chat est sur une chaise.',
      },
      tunes: {
        textVariant: TextVariantTune.NONE,
      },
    }
    const spy = mock('hello')

    const enBlock = await translateParagraphBlock(frBlock, Lang.fr, Lang.en)
    expect(enBlock).toBeDefined()
    expect(enBlock.type).toBe(BlockType.Paragraph)
    expect(enBlock.data.text).toBe('hello')

    expect(spy).toHaveBeenCalledWith(frBlock.data.text, Lang.fr, Lang.en)
    expect(spy).toHaveBeenCalledTimes(1)
  })

  it('should translate header block', async function () {
    const frBlock: IHeaderBlock = {
      type: BlockType.Header,
      data: {
        text: "J'ai un parapluie.",
        level: 4,
      },
    }
    const spy = mock('hello')

    const enBlock = await translateHeaderBlock(frBlock, Lang.fr, Lang.en)
    expect(enBlock).toBeDefined()
    expect(enBlock.type).toBe(BlockType.Header)
    expect(enBlock.data.level).toBe(frBlock.data.level)
    expect(enBlock.data.text).toBe('hello')

    expect(spy).toHaveBeenCalledWith(frBlock.data.text, Lang.fr, Lang.en)
    expect(spy).toHaveBeenCalledTimes(1)
  })

  it('should translate image block', async function () {
    const frBlock: IImageBlock = {
      type: BlockType.Image,
      data: {
        caption: 'Un homme vert.',
        file: {
          orig: 'orig.jpg',
          mid: 'mid.jpg',
          low: 'low.jpg',
          url: 'bla/static/bla',
        },
        stretched: false,
        withBackground: false,
        withBorder: false,
      },
    }
    const spy = mock('hello')

    const enBlock = await translateImageBlock(frBlock, Lang.fr, Lang.en)
    expect(enBlock).toBeDefined()
    expect(enBlock.type).toBe(BlockType.Image)
    expect(enBlock.data.file.orig).toBe(frBlock.data.file.orig)
    expect(enBlock.data.file.mid).toBe(frBlock.data.file.mid)
    expect(enBlock.data.file.low).toBe(frBlock.data.file.low)
    expect(enBlock.data.stretched).toBe(frBlock.data.stretched)
    expect(enBlock.data.withBackground).toBe(frBlock.data.withBackground)
    expect(enBlock.data.withBorder).toBe(frBlock.data.withBorder)
    expect(enBlock.data.caption).toBe('hello')

    expect(spy).toHaveBeenCalledWith(frBlock.data.caption, Lang.fr, Lang.en)
    expect(spy).toHaveBeenCalledTimes(1)
  })

  it('should translate list block', async function () {
    const frBlock: IListBlock = {
      type: BlockType.List,
      data: {
        items: [
          { content: 'Salut toi !', items: [] },
          { content: '   ', items: [] },
          {
            content: 'Le ciel est bleu.',
            items: [
              {
                content: 'Le chat is petit.',
                items: [{ content: 'La souris est amicale.', items: [] }],
              },
            ],
          },
        ],
        style: 'ordered',
      },
    }
    const spy = mock('hello')

    const enBlock = await translateListBlock(frBlock, Lang.fr, Lang.en)
    expect(enBlock).toBeDefined()
    expect(enBlock.type).toBe(BlockType.List)
    expect(enBlock.data.style).toBe(frBlock.data.style)
    expect(enBlock.data.items).toHaveLength(frBlock.data.items.length)
    expect(enBlock.data.items[0].content).toBe('hello')
    expect(enBlock.data.items[1].content).toBe('hello')
    expect(enBlock.data.items[2].content).toBe('hello')
    expect(enBlock.data.items[2].items[0].content).toBe('hello')
    expect(enBlock.data.items[2].items[0].items[0].content).toBe('hello')
    expect(spy).toHaveBeenCalledTimes(5)
  })

  it('should translate table block', async function () {
    const frBlock: ITableBlock = {
      id: '1QaF-btKn8',
      type: BlockType.Table,
      data: {
        withHeadings: true,
        content: [
          ['Item', 'Poids', 'Description'],
          ['Sac à dos <b>Exped</b>', '1130 grammes', 'Une pépite'],
        ],
      },
    }
    const spy = mock('hello')
    const enBlock = await translateTableBlock(frBlock, Lang.fr, Lang.en)
    expect(enBlock).toBeDefined()
    expect(spy).toHaveBeenCalledTimes(6)
  })
})
