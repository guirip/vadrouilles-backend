import type { ICodifLean } from 'src/models/Codif'
import { CodifModel } from 'src/models/Codif'
import { connect, disconnect, toSerializable } from 'src/services/DbService'

function checkDoc(codif) {
  expect(typeof codif._id).toBe('string')
  expect(typeof codif.__v).toBe('undefined')
}

describe('codif service', function () {
  beforeAll(async function () {
    await connect()
    await new CodifModel({
      name: 'truc2',
      type: 'string',
      value: 'whatever',
    }).save()
  })

  afterAll(async function () {
    await CodifModel.deleteMany({})
    await disconnect()
  })

  it('toSerializable', async function () {
    const codifs = await CodifModel.find()
    codifs.forEach((c) => checkDoc(toSerializable<ICodifLean>(c)))
  })
})
