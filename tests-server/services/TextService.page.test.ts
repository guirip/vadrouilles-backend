import { Types } from 'mongoose'

import { LocalizedTextModel } from 'src/models/text/LocalizedText'
import { BlockType } from 'src/models/text/TextBlock.type'
import { TextChapterModel } from 'src/models/text/TextChapter'
import { TextPageModel } from 'src/models/text/TextPage'
import { ContentCategory } from 'src/models/common/ContentCategory'
import { connect, disconnect } from 'src/services/DbService'
import { createChapter } from 'src/services/TextService.chapter'
import {
  createPage,
  getPages,
  updatePage,
  deletePage,
  listImageBlocks,
  updateOrder,
} from 'src/services/TextService.page'

import { checkRefArrays, clearCollections } from 'tests-server/helpers'

describe('Text service - TextPage functions', function () {
  beforeAll(async function () {
    await connect()
    await clearCollections()
  })

  afterEach(async function () {
    await clearCollections()
  })

  afterAll(disconnect)

  it('create and get visible textpage', async function () {
    const PAGES = [
      {
        slug: 'page1',
        title: { fr: 'page1 fr', en: 'page1 en' },
        subtitle: { fr: 'page1 subtitle fr', en: 'page1 subtitle en' },
        visible: true,
        chapters: [new Types.ObjectId()],
        category: ContentCategory.NATURE,
      },
      {
        slug: 'page2',
        title: { fr: 'page2 fr', en: 'page2 en' },
        subtitle: { fr: '', en: '' },
        visible: false,
        chapters: [new Types.ObjectId(), new Types.ObjectId()],
        category: ContentCategory.NATURE,
      },
    ]
    await Promise.all(PAGES.map((page) => createPage(page)))

    const pages = await getPages()

    expect(Array.isArray(pages)).toBeTruthy()
    expect(pages.length).toBe(1)

    function checkPage(inputPage) {
      const createdPage = pages.find((p) => p.slug === inputPage.slug)
      if (!createdPage) {
        throw new Error(
          `Page should exist. No match for slug ${inputPage.slug}`
        )
      }

      expect(createdPage.title.fr).toBe(inputPage.title.fr)
      expect(createdPage.title.en).toBe(inputPage.title.en)
      expect(createdPage.subtitle.fr).toBe(inputPage.subtitle.fr)
      expect(createdPage.subtitle.en).toBe(inputPage.subtitle.en)
      expect(createdPage.visible).toBe(inputPage.visible)

      checkRefArrays(inputPage.chapters, createdPage.chapters)
    }
    checkPage(pages[0])
  })

  describe('updateOrder', function () {
    beforeAll(async () => {
      await clearCollections()
    })

    it('Should update catalog item categories order', async function () {
      const pageIds = (
        await TextPageModel.create([
          {
            slug: 'page1',
            order: 1,
            category: ContentCategory.NATURE,
            title: { fr: '', en: '' },
            subtitle: { fr: '', en: '' },
          },
          {
            slug: 'page2',
            order: 2,
            category: ContentCategory.NATURE,
            title: { fr: '', en: '' },
            subtitle: { fr: '', en: '' },
          },
          {
            slug: 'page3',
            order: 3,
            category: ContentCategory.NATURE,
            title: { fr: '', en: '' },
            subtitle: { fr: '', en: '' },
          },
          {
            slug: 'page4',
            order: 4,
            category: ContentCategory.NATURE,
            title: { fr: '', en: '' },
            subtitle: { fr: '', en: '' },
          },
          {
            slug: 'page5',
            order: 5,
            category: ContentCategory.NATURE,
            title: { fr: '', en: '' },
            subtitle: { fr: '', en: '' },
          },
        ])
      ).map((page) => page._id.toString())

      const newOrder = {
        [pageIds[0]]: 2,
        [pageIds[1]]: 5,
        [pageIds[2]]: 1,
        [pageIds[3]]: 4,
        [pageIds[4]]: 3,
      }
      await updateOrder(newOrder)

      const pagesAfter = await TextPageModel.find({}, null, {
        sort: { order: 1 },
        lean: true,
      })
      expect(pagesAfter).toHaveLength(pageIds.length)
      for (const page of pagesAfter) {
        expect(page.order).toBe(newOrder[page._id.toString()])
      }
    })
  })

  it('update textpage', async function () {
    const PAGES = [
      {
        slug: 'page1',
        title: { fr: 'page1 fr', en: 'page1 en' },
        subtitle: { fr: 'page1 subtitle fr', en: 'page1 subtitle en' },
        visible: false,
        chapters: [new Types.ObjectId()],
        category: ContentCategory.NATURE,
      },
      {
        slug: 'page2',
        title: { fr: 'page2 fr', en: 'page2 en' },
        subtitle: { fr: '', en: '' },
        visible: false,
        chapters: [new Types.ObjectId(), new Types.ObjectId()],
        category: ContentCategory.NATURE,
      },
    ]
    await Promise.all(PAGES.map((page) => createPage(page)))

    const textPage1 = await TextPageModel.findOne({ slug: PAGES[0].slug })
    if (!textPage1) {
      throw new Error('page should exist')
    }
    const { _id } = textPage1
    expect(_id).toBeDefined()

    const NEW_VALUES = {
      slug: 'page1-updated',
      title: {
        fr: 'page1 fr updated',
        en: 'page1 en updated',
      },
      subtitle: { fr: '', en: '' },
      visible: true,
      chapters: [new Types.ObjectId(), new Types.ObjectId()],
    }
    await updatePage({ _id, ...NEW_VALUES })

    const updatedPage = await TextPageModel.findById(_id).lean()
    if (!updatedPage) {
      throw new Error('updated page should exist')
    }
    expect(updatedPage.slug).toBe(NEW_VALUES.slug)
    expect(updatedPage.visible).toBe(NEW_VALUES.visible)
    expect(updatedPage.title).toStrictEqual(NEW_VALUES.title)
    expect(updatedPage.subtitle).toStrictEqual(NEW_VALUES.subtitle)
    checkRefArrays(
      updatedPage.chapters.map((_id) => _id.toString()),
      NEW_VALUES.chapters.map((_id) => _id.toString())
    )
  })

  it('delete textpage without chapters', async function () {
    const INPUT = {
      slug: 'page1',
      title: { fr: 'page1 fr', en: 'page1 en' },
      subtitle: { fr: '', en: '' },
      visible: false,
      category: ContentCategory.NATURE,
    }
    const { _id } = await createPage(INPUT)
    expect(await TextPageModel.findById(_id)).toBeDefined()

    await deletePage(_id)

    expect(await TextPageModel.findById(_id)).toBeFalsy()
  })

  it('attempt to delete textpage with existing chapters should throw', async function () {
    const INPUT = {
      slug: 'page1',
      title: { fr: 'page1 fr', en: 'page1 en' },
      subtitle: { fr: '', en: '' },
      visible: false,
      category: ContentCategory.NATURE,
    }
    const createdPage = await createPage(INPUT)

    const chapter = await createChapter(createdPage._id.toString(), {
      slug: 'chapter1',
      visible: false,
    })
    createdPage.chapters.push(chapter._id)

    expect(await TextPageModel.findById(createdPage._id)).toBeDefined()

    await expect(() => deletePage(createdPage._id.toString())).rejects.toThrow(
      Error
    )

    expect(await TextPageModel.findById(createdPage._id)).toBeDefined()
  })

  describe('listImageBlocks', function () {
    it('should list image blocks per chapters', async function () {
      let iBlock = 1

      const localizedTexts = await LocalizedTextModel.create([
        {
          title: `localized text ${iBlock}`,
          lang: 'fr',
          content: {
            blocks: [
              {
                id: `block-id-DUP`,
                type: BlockType.Image,
                data: {
                  caption: `caption DUP`,
                  file: {
                    orig: `file-orig-DUP`,
                    mid: `file-mid-DUP`,
                    low: `file-low-DUP`,
                  },
                  stretched: false,
                  withBackground: false,
                  withBorder: false,
                },
              },
              {
                id: `block-id-${iBlock++}`,
                type: BlockType.Spacer,
                data: {
                  value: 1.5,
                },
              },
              {
                id: `block-id-${iBlock++}`,
                type: BlockType.Paragraph,
                data: {
                  text: 'blabla',
                },
              },
              {
                id: `block-id-${iBlock++}`,
                type: BlockType.Delimiter,
              },
            ],
          },
        },
        {
          title: `localized text ${iBlock}`,
          lang: 'en',
          content: {
            blocks: [
              {
                id: `block-id-${iBlock++}`,
                type: BlockType.Paragraph,
                data: {
                  text: 'ohoh',
                },
              },
              {
                id: `block-id-DUP`,
                type: BlockType.Image,
                data: {
                  caption: `caption DUP`,
                  file: {
                    orig: `file-orig-DUP`,
                    mid: `file-mid-DUP`,
                    low: `file-low-DUP`,
                  },
                  stretched: false,
                  withBackground: false,
                  withBorder: false,
                },
              },
              {
                id: `block-id-${iBlock}`,
                type: BlockType.Image,
                data: {
                  caption: `caption ${iBlock}`,
                  file: {
                    orig: `file-orig-${iBlock}`,
                    mid: `file-mid-${iBlock}`,
                    low: `file-low-${iBlock++}`,
                  },
                  stretched: false,
                  withBackground: false,
                  withBorder: false,
                },
              },
            ],
          },
        },
        {
          title: `localized text ${iBlock}`,
          lang: 'fr',
          content: {
            blocks: [
              {
                id: `block-id-${iBlock++}`,
                type: BlockType.Header,
                data: {
                  text: 'title',
                  level: 5,
                },
              },
              {
                id: `block-id-${iBlock}`,
                type: BlockType.Image,
                data: {
                  caption: `caption ${iBlock}`,
                  file: {
                    orig: `file-orig-${iBlock}`,
                    mid: `file-mid-${iBlock}`,
                    low: `file-low-${iBlock++}`,
                  },
                  stretched: false,
                  withBackground: false,
                  withBorder: false,
                },
              },
              {
                id: `block-id-${iBlock}`,
                type: BlockType.Image,
                data: {
                  caption: `caption ${iBlock}`,
                  file: {
                    orig: `file-orig-${iBlock}`,
                    mid: `file-mid-${iBlock}`,
                    low: `file-low-${iBlock++}`,
                  },
                  stretched: false,
                  withBackground: false,
                  withBorder: false,
                },
              },
            ],
          },
        },
      ])

      const chapters = await TextChapterModel.create([
        {
          slug: 'chapter1',
          localizedTexts: [localizedTexts.shift(), localizedTexts.shift()],
        },
        {
          slug: 'chapter2',
          localizedTexts: [localizedTexts.shift()],
        },
      ])

      const textPage = await TextPageModel.create({
        slug: 'test-text',
        title: {
          fr: 'title fr',
          en: 'title en',
        },
        subtitle: { fr: '', en: '' },
        category: ContentCategory.NATURE,
        chapters,
      })

      const result = await listImageBlocks(textPage._id)

      expect(result._id.toString()).toBe(textPage._id.toString())

      expect(result.title.fr).toBe(textPage.title.fr)
      expect(result.title.en).toBe(textPage.title.en)

      const chapter1Id = chapters[0]._id.toString()
      expect(result.imagesByChapter[chapter1Id]).toBeDefined()
      expect(result.imagesByChapter[chapter1Id].slug).toBe(chapters[0].slug)
      expect(result.imagesByChapter[chapter1Id].images).toHaveLength(2)

      const chapter2Id = chapters[1]._id.toString()
      expect(result.imagesByChapter[chapter2Id]).toBeDefined()
      expect(result.imagesByChapter[chapter2Id].slug).toBe(chapters[1].slug)
      expect(result.imagesByChapter[chapter2Id].images).toHaveLength(2)
    })
  })
})
