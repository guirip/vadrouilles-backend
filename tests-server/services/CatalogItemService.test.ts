import type { HydratedDocument } from 'mongoose'

import config from 'src/config'
import type { ICatalogItem } from 'src/models/CatalogItem'
import { CatalogItemModel } from 'src/models/CatalogItem'
import { ITEM_DEFAULT_STATUS, ItemStatus } from 'src/models/CatalogItem.type'
import { FILE_FIELDS } from 'src/models/common/File.type'
import type { IIdOrderMap } from 'src/models/common/IdOrderMap'
import {
  createItem,
  deleteItem,
  deleteItemFile,
  getItems,
  setMainFile,
  updateItem,
  updateOrder,
} from 'src/services/CatalogItemService'
import { connect, disconnect } from 'src/services/DbService'
import * as FileService from 'src/services/FileService'
import { NotFoundError } from 'src/utils/error'
import { generateItems } from 'tests-server/catalog-helpers'
import {
  clearCollections,
  generateObjectId,
  shuffleArray,
} from 'tests-server/helpers'

describe('CatalogItemService', function () {
  beforeAll(async function () {
    await connect()
    await CatalogItemModel.deleteMany()
  })

  afterEach(async function () {
    await clearCollections()
  })

  afterAll(async function () {
    await disconnect()
  })

  describe('getItems', function () {
    it('should return items sorted by order', async function () {
      await generateItems([])

      const items = await getItems({})
      expect(items.length).toBeGreaterThan(0)
      items.forEach((item, index) => {
        if (index > 0) {
          expect(item.order).toBeGreaterThan(items[index - 1].order)
        }
      })
    })
  })

  describe('createItem', function () {
    let itemsBefore: ICatalogItem[]

    beforeEach(async function () {
      const data: Partial<ICatalogItem>[] = Array(5)
        .fill(null)
        .map((_, index) => ({
          visibility: true,
          price: 50 * index,
          order: Math.floor(Math.random() * 30),
          title: `item ${index}`,
        }))
      itemsBefore = await CatalogItemModel.create(data)
    })

    afterEach(async function () {
      await clearCollections()
    })

    it('Should create new item and update existing items order', async function () {
      const newItemData: Partial<ICatalogItem> = {
        visibility: true,
        price: 30,
        title: 'new item',
      }

      await createItem(newItemData)

      const itemsAfter = await CatalogItemModel.find()
      expect(itemsAfter).toHaveLength(itemsBefore.length + 1)

      for (const item of itemsAfter) {
        if (item.title === newItemData.title) {
          expect(item.order).toBe(0)
          expect(item.createDate).toBeDefined()
          expect(item.visibility).toBe(newItemData.visibility)
          expect(item.price).toBe(newItemData.price)
        } else {
          const stringId = item._id.toString()
          // Exiting items order should have been shited
          const itemBefore = itemsBefore.find(
            (_item) => _item._id.toString() === stringId
          )
          if (!itemBefore) {
            throw new Error('item should be found in DB')
          }
          expect(item.order).toBe(itemBefore.order + 1)
        }
      }
    })
  })

  describe('Updating an item', function () {
    let existingItem: HydratedDocument<ICatalogItem>

    beforeEach(async function () {
      await CatalogItemModel.deleteMany()

      const existingData: ICatalogItem = {
        _id: generateObjectId(),
        title: 'test item',
        status: ITEM_DEFAULT_STATUS,
        visibility: true,
        price: 1,
        order: 0,
      }
      existingItem = await CatalogItemModel.create(existingData)
    })

    afterEach(async function () {
      await clearCollections()
    })

    it('should throw if item is not found', async function () {
      let hasThrown = false
      try {
        await updateItem({ _id: generateObjectId() })
      } catch (e) {
        hasThrown = true
        expect(e).toBeInstanceOf(NotFoundError)
      }
      expect(hasThrown).toBeTruthy()
    })

    it('should update item', async function () {
      const newValues = {
        title: 'new title',
        status: ItemStatus.Booked,
        price: 100,
        description: 'new description',
        visibility: false,
        order: 0,
        technique: 'Mixed media',
        dimensions: '50x60cm',
        weight: '360gr',
      }
      const d = new Date().getTime()

      function check(item) {
        expect(item.title).toBe(newValues.title)
        expect(item.status).toBe(newValues.status)
        expect(item.price).toBe(newValues.price)
        expect(item.description).toBe(newValues.description)
        expect(item.visibility).toBe(newValues.visibility)
        expect(item.order).toBe(newValues.order)
        expect(item.technique).toBe(newValues.technique)
        expect(item.dimensions).toBe(newValues.dimensions)
        expect(item.weight).toBe(newValues.weight)
        expect(new Date(item.updateDate).getTime()).toBeGreaterThan(d)
      }

      await new Promise((resolve) => setTimeout(resolve, 1))

      const returnedItem = await updateItem({
        _id: existingItem._id,
        ...newValues,
      })
      check(returnedItem)

      const itemFromDb = await CatalogItemModel.findById(existingItem._id)
      check(itemFromDb)
    })
  })

  describe('updateOrder', function () {
    beforeAll(async () => {
      await clearCollections()
    })

    it('Should update catalog item categories order', async function () {
      const items = (await generateItems([])).map((item) => item.toObject())
      const itemIds = items.map((cat) => cat._id.toString())
      shuffleArray(itemIds)

      const newOrder = itemIds.reduce(
        (order: IIdOrderMap, itemId: string, index) => {
          order[itemId] = index
          return order
        },
        {}
      )

      await updateOrder(newOrder)

      const itemsAfter = await getItems({})
      for (const item of itemsAfter) {
        expect(item.order).toBe(newOrder[item._id.toString()])
      }
    })
  })

  describe('Deleting an item', function () {
    it('should throw if item is not found', async function () {
      let hasThrown = false
      try {
        await deleteItem(generateObjectId())
      } catch (e) {
        hasThrown = true
        expect(e).toBeInstanceOf(NotFoundError)
      }
      expect(hasThrown).toBeTruthy()
    })

    it('should delete item', async function () {
      const data: ICatalogItem = {
        _id: generateObjectId(),
        title: 'test delete ok',
        status: ITEM_DEFAULT_STATUS,
        price: 100,
        order: 1,
        visibility: true,
        files: [
          {
            orig: 'picture-orig.jpg',
            mid: 'picture-mid.jpg',
            low: 'picture-low.jpg',
            url: 'blabla/truc/chose',
          },
        ],
      }
      const item = await CatalogItemModel.create(data)

      const spy = jest
        .spyOn(FileService, 'deleteFile')
        .mockImplementation(() => {
          /* noop */
        })

      await deleteItem(item._id)

      if (!item.files) {
        throw new Error('item.files should be defined')
      }
      for (let i = 0; i < item.files.length; i++) {
        expect(spy).toHaveBeenNthCalledWith(
          i + 1,
          `${config.UPLOAD_DEST}/${item.files[i].orig}`
        )
      }
      spy.mockReset()
    })
  })

  describe('setMainFile', function () {
    it('(retrieveItemAndFileIndex) should throw if item is not found', async function () {
      let hasThrown = false
      try {
        await setMainFile(generateObjectId(), 'lowFileName.jpg')
      } catch (e) {
        hasThrown = true
        expect(e).toBeInstanceOf(NotFoundError)
      }
      expect(hasThrown).toBeTruthy()
    })

    it('(retrieveItemAndFileIndex) should throw if file is not found', async function () {
      const [item] = await generateItems([], 1)

      let hasThrown = false
      try {
        await setMainFile(item._id, 'lowFileName.jpg')
      } catch (e) {
        hasThrown = true
        expect(e).toBeInstanceOf(NotFoundError)
        expect(e.message).toContain('file not found')
      }
      expect(hasThrown).toBeTruthy()
    })

    it('should update main file', async function () {
      const [item] = await generateItems([], 1)
      item.set({
        files: [
          { orig: 'orig1', mid: 'mid1', low: 'low1', isMain: false },
          { orig: 'orig2', mid: 'mid2', low: 'low2', isMain: false },
          { orig: 'orig3', mid: 'mid3', low: 'low3', isMain: true },
          { orig: 'orig4', mid: 'mid4', low: 'low4', isMain: false },
        ],
      })
      await item.save()

      const newMainFile = 'low2'
      await setMainFile(item._id, newMainFile)

      const itemAfter = await CatalogItemModel.findById(item._id)
      if (!itemAfter) {
        throw new Error('Catalog item should exist')
      }
      const mainFiles = (itemAfter.files ?? []).filter((f) => f.isMain === true)
      expect(mainFiles).toHaveLength(1)
      expect(mainFiles[0].low).toBe(newMainFile)
    })
  })

  describe('deleteItemFile', function () {
    it('should delete file', async function () {
      const [item] = await generateItems([], 1)
      item.set({
        files: [
          { orig: 'orig1', mid: 'mid1', low: 'low1', isMain: false },
          { orig: 'orig2', mid: 'mid2', low: 'low2', isMain: false },
          { orig: 'orig3', mid: 'mid3', low: 'low3', isMain: true },
          { orig: 'orig4', mid: 'mid4', low: 'low4', isMain: false },
        ],
      })
      await item.save()

      const spy = jest
        .spyOn(FileService, 'deleteFile')
        .mockImplementation(() => {
          /* noop */
        })

      const fileToDelete = 'low3'
      await deleteItemFile(item._id, fileToDelete)

      const itemAfter = await CatalogItemModel.findById(item._id)
      if (!itemAfter || !itemAfter.files) {
        throw new Error('Catalog item should exist and have files')
      }
      if (!item.files) {
        throw new Error('Initial catalog item should have files')
      }
      expect(itemAfter.files).toHaveLength(item.files.length - 1)
      expect(
        itemAfter.files.find(({ low }) => low === fileToDelete)
      ).toBeUndefined()

      expect(spy).toHaveBeenCalledTimes(FILE_FIELDS.length)
      spy.mockReset()
    })
  })
})
