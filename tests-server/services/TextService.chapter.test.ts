import { type HydratedDocument, Types } from 'mongoose'

import { ContentCategory } from 'src/models/common/ContentCategory'
import type {
  ITextChapter,
  ITextChapterPopulated,
} from 'src/models/text/TextChapter'
import { TextChapterModel } from 'src/models/text/TextChapter'
import {
  type ITextPagePopulated,
  TextPageModel,
} from 'src/models/text/TextPage'
import { connect, disconnect } from 'src/services/DbService'
import {
  createChapter,
  deleteChapter,
  duplicateChapter,
  getChaptersByPage,
  moveChapter,
  updateChapter,
  updateOrder,
} from 'src/services/TextService.chapter'
import { createLocalizedText } from 'src/services/TextService.localizedText'
import { createPage } from 'src/services/TextService.page'

import { Lang } from 'src/models/common/Lang'
import {
  checkRefArrays,
  clearCollections,
  generateObjectId,
} from 'tests-server/helpers'
import {
  createDummyChapter,
  createDummyPage,
  createNDummyLocalizedText,
} from 'tests-server/text-data-helpers'

describe('Text service - TextChapter', function () {
  beforeAll(async function () {
    await connect()
    await clearCollections()
  })

  afterEach(async function () {
    await clearCollections()
  })

  afterAll(disconnect)

  describe('createChapter', function () {
    it('Saving two chapters with same slug should be possible within different textpage scope', async function () {
      const [textPage1, textPage2] = await Promise.all([
        createDummyPage(),
        createDummyPage(),
      ])

      const slug = 'day-one'
      await createChapter(textPage1._id, { slug })
      await createChapter(textPage2._id, { slug })

      const chapters = await TextChapterModel.find({ slug })
      expect(chapters.length).toBe(2)
      expect(chapters[0].slug).toBe(slug)
      expect(chapters[1].slug).toBe(slug)
    })

    it('Saving two chapters with same slug should not be possible within same textpage scope', async function () {
      const textPage = await createDummyPage()

      const slug = 'day-two'
      await createChapter(textPage._id, { slug, order: 0 })
      let hasThrown = false
      try {
        await createChapter(textPage._id, { slug, order: 1 })
      } catch (e) {
        hasThrown = true
      }
      expect(hasThrown).toBeTruthy()

      const chapters = await TextChapterModel.find({ slug })
      expect(chapters.length).toBe(1)

      const textPageAfter = (await TextPageModel.findById(
        textPage._id
      ).populate('chapters')) as unknown as HydratedDocument<ITextPagePopulated>
      expect(textPageAfter?.chapters.length).toBe(1)
      expect(textPageAfter?.chapters[0].order).toBe(0)
    })

    it('create chapters then get them by textpage id', async function () {
      const { _id: textPageId } = await createDummyPage()
      const PAGE_CHAPTERS_COUNT = 3

      const chaptersData: Partial<ITextChapter>[] = [
        {
          slug: 'chapter1',
          order: 1,
          visible: false,
          localizedTexts: [],
        },
        {
          slug: 'chapter2',
          order: 2,
          visible: true,
          localizedTexts: [],
        },
        {
          slug: 'chapter3',
          order: 3,
          visible: false,
          localizedTexts: [],
        },
        {
          slug: 'chapter4',
          order: 4,
          visible: false,
          localizedTexts: [],
        },
      ]

      let createdChapters: ITextChapterPopulated[] = []
      for (const input of chaptersData) {
        // create sequentially because order matters
        const { _id } = await createChapter(textPageId, input)
        await createNDummyLocalizedText(_id, Math.ceil(Math.random() * 2))

        const createdChapter = (await TextChapterModel.findOne({ _id })
          .populate('localizedTexts')
          .exec()) as HydratedDocument<ITextChapterPopulated>
        if (!createdChapter) {
          throw new Error('Created chapter should exist')
        }
        createdChapters.push(createdChapter.toObject())
      }
      createdChapters = createdChapters.slice(0, PAGE_CHAPTERS_COUNT)

      const PAGE_INPUT = {
        slug: 'page1',
        title: { fr: 'page1 fr', en: 'page1 en' },
        subtitle: { fr: '', en: '' },
        chapters: createdChapters.map((c) => c._id),
        category: ContentCategory.NATURE,
      }
      const { _id: pageId } = await createPage(PAGE_INPUT)

      const chaptersAfter = await getChaptersByPage(pageId)

      expect(Array.isArray(chaptersAfter)).toBeTruthy()
      expect(chaptersAfter.length).toBe(PAGE_CHAPTERS_COUNT)

      function checkChapter(inputChapter: ITextChapterPopulated, index) {
        const chapterAfter = chaptersAfter.find(
          (p) => p.slug === inputChapter.slug
        )
        if (!chapterAfter) {
          throw new Error('chapter should be returned by `getChaptersByPage`')
        }

        expect(chapterAfter.visible).toBe(inputChapter.visible)
        expect(typeof chapterAfter.order).toBe('number')
        if (index > 0) {
          // Check ASC order
          expect(chapterAfter.order).toBeGreaterThan(
            chaptersAfter[index - 1].order
          )
        }
        for (const lt of inputChapter.localizedTexts) {
          const ltAfter = chapterAfter.localizedTexts.find(
            ({ _id }) => _id.toString() === lt._id.toString()
          )
          if (!ltAfter) {
            throw new Error(
              `Failed to find localizedText returned by getChaptersByPage and matching _id ${lt._id}`
            )
          }
          expect(ltAfter).not.toHaveProperty('content')
          if (!lt.content) {
            expect(ltAfter?.blocksCount).toBe(0)
          } else {
            expect(ltAfter?.blocksCount).toBe(lt.content?.blocks.length)
          }
        }
      }
      createdChapters.forEach(checkChapter)
    })
  })

  describe('updateOrder', function () {
    beforeAll(async () => {
      await clearCollections()
    })

    it('Should update chapters order', async function () {
      const chapterIds = (
        await TextChapterModel.create([
          { slug: 'chapter1', order: 1 },
          { slug: 'chapter2', order: 2 },
          { slug: 'chapter3', order: 3 },
          { slug: 'chapter4', order: 4 },
          { slug: 'chapter5', order: 5 },
        ])
      ).map((chapter) => chapter._id.toString())

      const newOrder = {
        [chapterIds[0]]: 2,
        [chapterIds[1]]: 4,
        [chapterIds[2]]: 3,
        [chapterIds[3]]: 5,
        [chapterIds[4]]: 1,
      }
      await updateOrder(newOrder)

      const chaptersAfter = await TextChapterModel.find({}, null, {
        sort: { order: 1 },
        lean: true,
      })
      expect(chaptersAfter).toHaveLength(chapterIds.length)
      for (const chapter of chaptersAfter) {
        expect(chapter.order).toBe(newOrder[chapter._id.toString()])
      }
    })
  })

  it('updateChapter', async function () {
    const { _id: textPageId } = await createDummyPage()
    const CHAPTERS = [
      {
        slug: 'chapter1',
        visible: false,
        localizedTexts: [generateObjectId()],
      },
      {
        slug: 'chapter2',
        visible: true,
        localizedTexts: [generateObjectId(), generateObjectId()],
      },
    ]
    for (const chapter of CHAPTERS) {
      // create sequentially because order matters
      await createChapter(textPageId, chapter)
    }

    const textChapter1 = await TextChapterModel.findOne({
      slug: CHAPTERS[0].slug,
    })
    if (!textChapter1) {
      throw new Error('chapter should exist')
    }
    const { _id } = textChapter1
    expect(_id).toBeDefined()
    const NEW_VALUES = {
      slug: 'chapter1-updated',
      visible: true,
      localizedTexts: [new Types.ObjectId(), new Types.ObjectId()],
    }
    await updateChapter({ _id, ...NEW_VALUES })

    const updatedChapter = await TextChapterModel.findById(_id)
    if (!updatedChapter) {
      throw new Error('updated chapter should exist')
    }
    expect(updatedChapter.slug).toBe(NEW_VALUES.slug)
    expect(updatedChapter.visible).toBe(NEW_VALUES.visible)
    checkRefArrays(updatedChapter.localizedTexts, NEW_VALUES.localizedTexts)
  })

  describe('deleteChapter', function () {
    it('delete chapter with no localizedTexts', async function () {
      const { _id: textPageId } = await createDummyPage()
      const { _id } = await createChapter(textPageId, {
        slug: 'chapter1',
        visible: false,
      })

      expect(await TextChapterModel.findById(_id)).toBeDefined()
      expect((await TextPageModel.findById(textPageId))?.chapters.length).toBe(
        1
      )

      await deleteChapter(_id)

      expect(await TextChapterModel.findById(_id)).toBeFalsy()
      const textPage = await TextPageModel.findById(textPageId)
      if (!textPage) {
        throw new Error('textPage should be defined')
      }
      expect(textPage.chapters.length).toBe(0)
    })

    it('delete chapter with existing localizedTexts should throw', async function () {
      const { _id: textPageId } = await createDummyPage()

      const { _id: chapterId } = await createChapter(textPageId, {
        slug: 'chapter1',
        visible: false,
      })
      expect(await TextChapterModel.findById(chapterId)).toBeDefined()

      await createLocalizedText(chapterId, {
        title: 'Hey localizedText 10',
        lang: Lang.fr,
        content: {
          time: Date.now(),
          blocks: [],
          version: '1.0.0',
        },
      })
      await expect(() => deleteChapter(chapterId)).rejects.toThrow(Error)
      expect(await TextChapterModel.findById(chapterId)).toBeDefined()
    })
  })

  describe('duplicate chapter', function () {
    it('should duplicate', async function () {
      const {
        _id: sampleChapterId,
        slug: sampleChapterSlug,
        order: sampleChapterOrder,
        visible: sampleChapterVisible,
      } = await createDummyChapter()

      const LT_AMOUNT = 3
      const sampleChapterLts = await createNDummyLocalizedText(
        sampleChapterId,
        LT_AMOUNT
      )

      const text = await TextPageModel.findOne({
        chapters: { $in: sampleChapterId },
      }).lean()
      if (!text) {
        throw new Error('Text page should be found in DB')
      }

      const sampleChapter =
        await TextChapterModel.findById(sampleChapterId).lean()
      if (!sampleChapter) {
        throw new Error('Chapter should exist in DB')
      }
      expect(sampleChapter.localizedTexts).toHaveLength(LT_AMOUNT)

      const duplicatedChapter = await duplicateChapter(
        text._id,
        sampleChapter._id
      )

      expect(duplicatedChapter._id.toString()).toBeDefined()
      expect(duplicatedChapter._id.toString()).not.toBe(
        sampleChapter._id.toString()
      )

      const duplicatedChapterFromDb = await TextChapterModel.findById(
        duplicatedChapter._id
      ).lean()
      if (!duplicatedChapterFromDb) {
        throw new Error('Duplicated localized text should exist in DB')
      }

      expect(
        duplicatedChapterFromDb.slug.includes(sampleChapterSlug)
      ).toBeTruthy()
      expect(duplicatedChapterFromDb.order).toBe(sampleChapterOrder + 1)
      expect(duplicatedChapterFromDb.visible).toBe(sampleChapterVisible)
      expect(duplicatedChapterFromDb.localizedTexts).toHaveLength(
        sampleChapterLts.length
      )

      const stringLtIds = duplicatedChapterFromDb.localizedTexts.map((_id) =>
        _id.toString()
      )
      for (const sampleChapterLt of sampleChapterLts) {
        expect(stringLtIds.includes(sampleChapterLt._id.toString())).toBeFalsy()
      }
    })

    it('duplicating twice', async function () {
      const dummyTextPage = await createDummyPage()
      const dummyChapter = await createDummyChapter(dummyTextPage)

      await duplicateChapter(dummyTextPage._id, dummyChapter._id)

      let hasThrown = false
      try {
        await duplicateChapter(dummyTextPage._id, dummyChapter._id)
      } catch (e) {
        hasThrown = true
      }
      expect(hasThrown).toBeTruthy()
    })
  })

  describe('move chapter', function () {
    it('should gracefully handle when current page is not found', async function () {
      const anotherChapter = await TextChapterModel.create({
        slug: 'another-chapter',
      })
      const chapter = await TextChapterModel.create({ slug: 'chapter-to-move' })
      const targetTextPage = await createDummyPage({
        chapters: [anotherChapter._id],
      })

      expect(targetTextPage.chapters).toHaveLength(1)

      await moveChapter(chapter._id, targetTextPage._id)

      const targetTextPageAfter = await TextPageModel.findById(
        targetTextPage._id
      )
      if (!targetTextPageAfter) {
        throw new Error('Target text page should exist')
      }
      expect(targetTextPageAfter.chapters).toHaveLength(2)
      expect(
        targetTextPageAfter.chapters.map((_id) => _id.toString())
      ).toContain(chapter._id.toString())
    })

    it('should move chapter to new page, removing it from current page', async function () {
      const anotherChapter = await TextChapterModel.create({
        slug: 'another-chapter',
      })
      const chapter = await TextChapterModel.create({ slug: 'chapter-to-move' })
      const currentTextPage = await createDummyPage({
        chapters: [anotherChapter._id, chapter._id],
      })

      const targetTextPage = await createDummyPage()

      await moveChapter(chapter._id, targetTextPage._id)

      // Check previous text page
      const previousTextPageAfter = await TextPageModel.findById(
        currentTextPage._id
      )
      if (!previousTextPageAfter) {
        throw new Error('Previous text page should exist')
      }
      expect(
        previousTextPageAfter.chapters.map((_id) => _id.toString())
      ).not.toContain(chapter._id.toString())

      // Check target text page
      const targetTextPageAfter = await TextPageModel.findById(
        targetTextPage._id
      )
      if (!targetTextPageAfter) {
        throw new Error('Target text page should exist')
      }
      expect(
        targetTextPageAfter.chapters.map((_id) => _id.toString())
      ).toContain(chapter._id.toString())
    })

    it('should throw if target page is not found', async function () {
      const chapter = await createDummyChapter()

      let hasThrown = false
      try {
        await moveChapter(chapter._id, new Types.ObjectId())
      } catch (e: unknown) {
        if (e instanceof Error) {
          hasThrown = true
          expect(e.message).toContain('target page')
        }
      }
      expect(hasThrown).toBeTruthy()
    })
  })
})
