import { CatalogItemModel } from 'src/models/CatalogItem'
import type { ICatalogItemCategory } from 'src/models/CatalogItemCategory'
import { CatalogItemCategoryModel } from 'src/models/CatalogItemCategory'
import {
  checkSlug,
  createItemCategory,
  deleteItemCategory,
  getCategories,
  updateItemCategory,
  updateOrder,
} from 'src/services/CatalogItemCategoryService'
import { connect, disconnect } from 'src/services/DbService'
import { NotFoundError } from 'src/utils/error'
import {
  clearCollections,
  generateObjectId,
  mockAuthOk,
} from 'tests-server/helpers'

describe('CatalogItemCategory service', function () {
  beforeAll(async function () {
    await connect()
    mockAuthOk()
  })

  afterAll(async function () {
    await clearCollections()
    await disconnect()
  })

  describe('checkSlug', function () {
    let existingCat

    beforeAll(async () => {
      await clearCollections()
      existingCat = await CatalogItemCategoryModel.create({
        slug: 'the-slug',
      })
    })

    it('should throw when slug is missing', async function () {
      let hasThrown = false
      try {
        await checkSlug('')
      } catch (e) {
        hasThrown = true
      }
      expect(hasThrown).toBeTruthy()
    })

    it('should throw when slug is already declared', async function () {
      let hasThrown = false
      try {
        await checkSlug(existingCat.slug)
      } catch (e) {
        hasThrown = true
      }
      expect(hasThrown).toBeTruthy()
    })

    it('should not throw when slug is used by an expected category', async function () {
      let hasThrown = false
      try {
        await checkSlug(existingCat.slug, existingCat._id)
      } catch (e) {
        hasThrown = true
      }
      expect(hasThrown).toBeFalsy()
    })
  })

  describe('getCategories', function () {
    let initCategories: ICatalogItemCategory[]

    beforeAll(async function () {
      await clearCollections()
      initCategories = await CatalogItemCategoryModel.create([
        {
          name: {
            en: 'cat 1 en',
            fr: 'cat 1 fr',
          },
          visibility: true,
          order: 1,
          slug: 'cat-1',
        },
        {
          name: {
            en: 'cat 2 en',
            fr: 'cat 2 fr',
          },
          visibility: false,
          order: 2,
          slug: 'cat-2',
        },
        {
          name: {
            en: 'cat 3 en',
            fr: 'cat 3 fr',
          },
          visibility: true,
          order: 3,
          slug: 'cat-3',
        },
      ])
    })

    it('should get all categories', async function () {
      const categories = await getCategories({})

      expect(categories.length).toBe(initCategories.length)
      expect(categories[0].order).toBeLessThan(categories[1].order)
      expect(categories[1].order).toBeLessThan(categories[2].order)
    })

    it('should get filtered categories', async function () {
      const query = { visibility: true }

      const categories = await getCategories(query)

      const expectedCount = categories.filter(
        (cat) => cat.visibility === query.visibility
      ).length
      expect(categories.length).toBe(expectedCount)
      for (const cat of categories) {
        expect(cat.visibility).toBe(query.visibility)
      }
    })
  })

  describe('createItemCategory', function () {
    let existingCategories: ICatalogItemCategory[]

    beforeAll(async function () {
      await clearCollections()
      existingCategories = await CatalogItemCategoryModel.create([
        {
          name: {
            en: 'cat 1 en',
            fr: 'cat 1 fr',
          },
          order: 1,
          slug: 'slug-1',
        },
        {
          name: {
            en: 'cat 2 en',
            fr: 'cat 2 fr',
          },
          order: 2,
          slug: 'slug-2',
        },
        {
          name: {
            en: 'cat 3 en',
            fr: 'cat 3 fr',
          },
          order: 3,
          slug: 'slug-3',
        },
      ])
    })

    it('should throw when slug is already declared', async function () {
      let hasThrown = false
      try {
        await createItemCategory({
          name: {
            en: 'new cat en',
            fr: 'new cat fr',
          },
          slug: existingCategories[0].slug,
        })
      } catch (e) {
        hasThrown = true
      }

      expect(hasThrown).toBeTruthy()
    })

    it('should persist new catalog item category and shift existing categories order', async function () {
      const data = {
        name: {
          en: 'new cat en',
          fr: 'new cat fr',
        },
        visibility: true,
        order: 1,
        slug: 'random-slug',
      }

      await createItemCategory(data)

      const createdCategory = await CatalogItemCategoryModel.findOne({
        'name.en': data.name.en,
      })
      if (!createdCategory) {
        throw new Error('Category should be found in DB')
      }
      expect(createdCategory.name.fr).toBe(data.name.fr)
      expect(createdCategory.name.en).toBe(data.name.en)
      expect(createdCategory.visibility).toBe(data.visibility)
      expect(createdCategory.order).toBe(data.order)

      const otherCategories = await CatalogItemCategoryModel.find({
        _id: { $in: existingCategories.map((cat) => cat._id) },
      })
      expect(otherCategories.length).toBe(existingCategories.length)
      for (const cat of otherCategories) {
        const catBefore = existingCategories.find(
          (existingCat) => existingCat._id.toString() === cat._id.toString()
        )
        if (!catBefore) {
          throw new Error('Failed to find existing category')
        }
        expect(cat.order).toBe(catBefore.order + 1)
      }
    })
  })

  describe('updateItemCategory', function () {
    const initialFrName = 'cat name FR init'
    const initialEnName = 'cat name EN init'
    let existingCat

    beforeEach(async function () {
      await clearCollections()
      existingCat = await CatalogItemCategoryModel.create({
        name: {
          fr: initialFrName,
          en: initialEnName,
        },
        visibility: true,
        order: 5,
        slug: 'existing-cat',
      })
    })

    it('should throw when item category is not found', async function () {
      let hasThrown = false
      try {
        await updateItemCategory({
          _id: generateObjectId(),
          slug: 'new-slug-value',
        })
      } catch (e) {
        hasThrown = true
        expect(e).toBeInstanceOf(NotFoundError)
      }

      expect(hasThrown).toBeTruthy()
    })

    it('should update catalog item category', async function () {
      const data = {
        _id: existingCat._id,
        name: {
          fr: 'cat name FR updated',
          en: initialEnName,
        },
        slug: 'new-slug-value',
      }

      function checkCat(cat: ICatalogItemCategory) {
        expect(cat.name.fr).toBe(data.name.fr)
        expect(cat.visibility).toBe(existingCat.visibility)
        expect(cat.order).toBe(existingCat.order)
      }

      const returnedCat = await updateItemCategory(data)
      checkCat(returnedCat)
    })
  })

  describe('updateOrder', function () {
    beforeAll(async () => {
      await clearCollections()
    })

    it('Should update catalog item categories order', async function () {
      const catIds = (
        await CatalogItemCategoryModel.create([
          { order: 1, slug: 'slug-1' },
          { order: 2, slug: 'slug-2' },
          { order: 3, slug: 'slug-3' },
          { order: 4, slug: 'slug-4' },
          { order: 5, slug: 'slug-5' },
        ])
      ).map((cat) => cat._id.toString())

      const newOrder = {
        [catIds[0]]: 4,
        [catIds[1]]: 2,
        [catIds[2]]: 3,
        [catIds[3]]: 5,
        [catIds[4]]: 1,
      }
      await updateOrder(newOrder)

      const categories = await getCategories({})
      for (const cat of categories) {
        expect(cat.order).toBe(newOrder[cat._id.toString()])
      }
    })
  })

  describe('Deleting a catalog item category', function () {
    let existingCats

    beforeAll(async function () {
      await clearCollections()
      existingCats = await CatalogItemCategoryModel.create([
        { slug: 'cat-1' },
        {
          name: { fr: 'cat fr 2', en: 'cat en 2' },
          slug: 'cat-2',
        },
      ])
    })

    it('should throw if catalog item category is not found', async function () {
      let hasThrown = false
      try {
        await deleteItemCategory(generateObjectId())
      } catch (e) {
        hasThrown = true
        expect(e).toBeInstanceOf(NotFoundError)
      }

      expect(hasThrown).toBeTruthy()
    })

    it('should delete category and unset related items category', async function () {
      const catIdToDelete = existingCats[0]._id

      await CatalogItemModel.create([
        {
          title: 'item1',
          category: catIdToDelete,
          price: 10,
          visibility: true,
        },
        {
          title: 'item2',
          category: existingCats[1]._id,
          price: 12,
          visibility: true,
        },
      ])

      await deleteItemCategory(catIdToDelete)

      const deletedCat = await CatalogItemCategoryModel.findById(catIdToDelete)
      expect(deletedCat).toBeNull()

      const catsFromDb = await CatalogItemCategoryModel.find({})
      expect(catsFromDb.length).toBe(1)
      expect(catsFromDb[0].name.fr).toBe(existingCats[1].name.fr)

      const items = await CatalogItemModel.find()
      expect(items).toHaveLength(2)
      for (const item of items) {
        if (item.title === 'item1') {
          expect(item.category).not.toBeDefined()
        } else {
          expect(item.category).toBeDefined()
        }
      }
    })
  })
})
