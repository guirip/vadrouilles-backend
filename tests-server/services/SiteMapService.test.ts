import type { ITextPagePopulated } from 'src/models/text/TextPage'
import { TextPageModel } from 'src/models/text/TextPage'
import { connect, disconnect } from 'src/services/DbService'
import { generateSiteMap } from 'src/services/SiteMapService'
import { clearCollections } from 'tests-server/helpers'
import {
  createDummyPage,
  createPageWithAChapterNotHavingLocalizedTexts,
  createPageWithChaptersAndLocalizedTexts,
  createPageWithNoVisibleChapter,
} from 'tests-server/text-data-helpers'

describe('SiteMapService', function () {
  beforeAll(async function () {
    await connect()
    await clearCollections()
  })

  afterEach(async function () {
    await clearCollections()
  })

  afterAll(disconnect)

  it('should not list category containing 0 visible page', async function () {
    await createDummyPage({
      visible: false,
    })
    const sitemap = await generateSiteMap('')
    expect(sitemap).toHaveLength(0)
  })

  it('should not list page containing 0 chapter', async function () {
    await createDummyPage()
    const sitemap = await generateSiteMap('')
    expect(sitemap).toHaveLength(0)
  })

  it('should not list page containing 0 visible chapter', async function () {
    await createPageWithNoVisibleChapter()
    const sitemap = await generateSiteMap('')
    expect(sitemap).toHaveLength(0)
  })

  it('should not list chapter containing 0 localized text', async function () {
    await createPageWithAChapterNotHavingLocalizedTexts()
    const sitemap = await generateSiteMap('')
    expect(sitemap).toHaveLength(0)
  })

  it('should list category, page and chapters', async function () {
    await createPageWithChaptersAndLocalizedTexts()

    const page = await TextPageModel.findOne().populate<ITextPagePopulated>([
      'chapters',
      'chapters.localizedTexts',
    ])
    if (!page) {
      throw new Error('Page should exist in DB')
    }

    const sitemap = await generateSiteMap('')

    expect(
      sitemap.some((entry) => entry.url.endsWith(page.category))
    ).toBeTruthy()

    expect(
      sitemap.some((entry) =>
        entry.url.endsWith(`${page.category}/${page.slug}`)
      )
    ).toBeTruthy()

    page.chapters.forEach((chapter) => {
      expect(
        sitemap.some((entry) =>
          entry.url.endsWith(`${page.category}/${page.slug}/${chapter.slug}`)
        )
      ).toBeTruthy()
    })
  })
})
