import { isValidTextCategory } from 'src/models/text/TextPage.type'

describe('TextPage model', function () {
  describe('isValidTextCategory', function () {
    it('should detect valid category', function () {
      expect(isValidTextCategory('nature')).toBeTruthy()
    })
    it('should detect invalid category', function () {
      expect(isValidTextCategory('knfsp,mdle')).toBeFalsy()
    })
  })
})
