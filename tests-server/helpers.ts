import mongoose, { Types } from 'mongoose'
import * as AuthService from 'src/services/AuthService'

export function mockAuthOk() {
  mockAuth(true)
}

export function mockAuthKo() {
  mockAuth(false)
}

function mockAuth(value: boolean) {
  jest.spyOn(AuthService, 'isApiAuthOk').mockImplementation(
    () =>
      new Promise((resolve) => {
        resolve(value)
      })
  )
}

export async function clearCollections() {
  if (!mongoose.connection.db) {
    console.error(`[clearCollections] mongoose.connection.db is void`)
    return
  }
  const collections = await mongoose.connection.db.collections()
  await Promise.all(collections.map((collection) => collection.deleteMany({})))
}

export const getRandomNumber = () => Math.floor(Math.random() * 10000)

export const generateObjectId = () => new Types.ObjectId()

export function checkRefArrays(
  arr1: Types.ObjectId[] | string[],
  arr2: Types.ObjectId[] | string[]
) {
  expect(arr1.length).toBe(arr2.length)
  const arr1StringIds = arr1.map((_id) => _id.toString())
  const arr2StringIds = arr2.map((_id) => _id.toString())
  for (const _id2 of arr2StringIds) {
    expect(arr1StringIds.includes(_id2)).toBeTruthy()
  }
}

export function shuffleArray(arr: unknown[]) {
  arr.sort(() => Math.random() - 0.5)
}
