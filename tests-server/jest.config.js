export default {
  preset: 'ts-jest',
  testEnvironment: 'node',
  moduleNameMapper: {
    '^src/(.+)$': '<rootDir>/../src/$1',
    '^tests-server/(.+)$': '<rootDir>/../tests-server/$1',
  },
  setupFiles: ['<rootDir>/loadTestEnv.ts'],
}