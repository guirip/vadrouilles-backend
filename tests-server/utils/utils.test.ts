import { getNestedProperty } from 'src/utils/util'

describe('utils', function () {
  describe('getNestedProperty', function () {
    const sample = {
      name: 'john',
      occupation: {
        title: 'carpenter',
        certificate: {
          name: 'Technical school of carpentry',
          date: '1998-06-17',
        },
      },
    }

    it('should return nested property value using string path', function () {
      expect(getNestedProperty('name', sample)).toBe(sample.name)
      expect(getNestedProperty('occupation.title', sample)).toBe(
        sample.occupation.title
      )
      expect(getNestedProperty('occupation.certificate.date', sample)).toBe(
        sample.occupation.certificate.date
      )
    })

    it('should return nested property value using an array of strings', function () {
      expect(getNestedProperty(['name'], sample)).toBe(sample.name)
      expect(getNestedProperty(['occupation', 'title'], sample)).toBe(
        sample.occupation.title
      )
      expect(
        getNestedProperty(['occupation', 'certificate', 'date'], sample)
      ).toBe(sample.occupation.certificate.date)
    })

    it('should return undefined when property does not exist', function () {
      expect(getNestedProperty('age', sample)).toBeUndefined()
      expect(getNestedProperty('hobbies.sport.level', sample)).toBeUndefined()
    })
  })
})
