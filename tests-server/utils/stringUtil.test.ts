import { parseNumber, removeHtml } from 'src/utils/stringUtil'

describe('stringUtil', function () {
  describe('parseNumber', function () {
    it('should return undefined when value is empty', function () {
      expect(parseNumber(undefined)).toBe(null)
      expect(parseNumber('')).toBe(null)
      expect(parseNumber(null)).toBe(null)
    })
    it('should return the number if argument is a number', function () {
      expect(parseNumber(0)).toBe(0)
      expect(parseNumber(234)).toBe(234)
    })
    it('should parse a regular string', function () {
      expect(parseNumber('0')).toBe(0)
      expect(parseNumber('234')).toBe(234)
    })
    it('should remove characters from a string', function () {
      expect(parseNumber('ze0!$ùpSDFs')).toBe(0)
      expect(parseNumber('po2à"éj\'3sdmfp4^QD:;,')).toBe(234)
    })
    it('should return undefined when the string contains no number', function () {
      expect(parseNumber('zeds:fm£%§M!$ùpSDFs')).toBe(null)
    })
  })

  describe('removeHtml', function () {
    it('should remove html entites and tags', function () {
      expect(removeHtml('<br /><b>h&eacute;&nbsp;h&eacute;</b>')).toBe('hé hé')
    })
  })
})
