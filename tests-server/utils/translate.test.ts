import { translate } from 'src/utils/translation'

describe('translate', function () {
  it('should translate from FR to EN', async function () {
    const result = await translate('chat', 'fr', 'en')

    expect(result).toBeDefined()
    expect(Array.isArray(result.translations)).toBeTruthy()
    expect(result.translations[0].text).toBe('cat')
  })

  it('should translate from EN to FR', async function () {
    const result = await translate('I have an umbrella.', 'en', 'fr')

    expect(result).toBeDefined()
    expect(Array.isArray(result.translations)).toBeTruthy()
    expect(result.translations[0].text).toBe("J'ai un parapluie.")
  })
})
