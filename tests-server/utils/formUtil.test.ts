import { updateNestedProperty } from 'src/utils/formUtil'

describe('formUtil', function () {
  it('should update nested property', function () {
    const data = {
      bestFriend: 'John',
      hobbies: ['hike', 'read'],
      occupation: {
        title: 'Librarian',
        duration: 15,
      },
    }

    let updatedData = updateNestedProperty('occupation.duration', 16, data)
    updatedData = updateNestedProperty('bestFriend', 'Mike', updatedData)
    updatedData = updateNestedProperty('hobbies', ['swim'], updatedData)

    expect(updatedData.occupation.duration).toBe(16)
    expect(updatedData.occupation.title).toBe('Librarian')
    expect(updatedData.bestFriend).toBe('Mike')
    expect(updatedData.hobbies).toHaveLength(1)
    expect(updatedData.hobbies.includes('swim')).toBeTruthy()
  })
})
