import { simpleSortAsc, simpleSortDsc } from 'src/utils/sortUtil'

describe('sortUtil', function () {
  describe('simpleSortAsc', function () {
    it('should sort numbers in ascending order', function () {
      const vegetables = [14, -52, 1042, 313]
      vegetables.sort(simpleSortAsc)

      expect(vegetables[0]).toBe(-52)
      expect(vegetables[1]).toBe(14)
      expect(vegetables[2]).toBe(313)
      expect(vegetables[3]).toBe(1042)
    })
    it('should sort strings in ascending order', function () {
      const vegetables = ['tomato', 'avocado', 'cucumber']
      vegetables.sort(simpleSortAsc)

      expect(vegetables[0]).toBe('avocado')
      expect(vegetables[1]).toBe('cucumber')
      expect(vegetables[2]).toBe('tomato')
    })
  })

  describe('simpleSortDsc', function () {
    it('should sort numbers in descending order', function () {
      const vegetables = [14, -52, 2042, 313]
      vegetables.sort(simpleSortDsc)

      expect(vegetables[0]).toBe(2042)
      expect(vegetables[1]).toBe(313)
      expect(vegetables[2]).toBe(14)
      expect(vegetables[3]).toBe(-52)
    })
    it('should sort strings in descending order', function () {
      const vegetables = ['tomato', 'avocado', 'cucumber']
      vegetables.sort(simpleSortDsc)

      expect(vegetables[0]).toBe('tomato')
      expect(vegetables[1]).toBe('cucumber')
      expect(vegetables[2]).toBe('avocado')
    })
  })
})
