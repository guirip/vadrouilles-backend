import type { RequestMethod } from 'node-mocks-http'
import { createRequest, createResponse } from 'node-mocks-http'
import { handler } from 'src/pages/api/catalog-item'
import * as CatalogItemService from 'src/services/CatalogItemService'
import * as CatalogItemCategoryService from 'src/services/CatalogItemCategoryService'
import { mockAuthOk } from 'tests-server/helpers'
import type { NextApiRequest, NextApiResponse } from 'next/types'

describe('catalog-item api - authenticated', function () {
  beforeAll(async function () {
    mockAuthOk()
  })

  it('Calling API using unsupported HTTP methods should result in 405 status', async function () {
    const unsupportedHttpMethods: RequestMethod[] = ['OPTIONS', 'PATCH']
    for (const method of unsupportedHttpMethods) {
      const req = createRequest<NextApiRequest>({ method })
      const res = createResponse<NextApiResponse>()

      await handler(req, res)
      expect(res.statusCode).toBe(405)
    }
  })

  describe('Retrieving all items', function () {
    it('status should be 400 in case of service failure', async function () {
      const req = createRequest<NextApiRequest>({ method: 'GET' })
      const res = createResponse<NextApiResponse>()

      jest
        .spyOn(CatalogItemService, 'getItems')
        .mockRejectedValue(new Error(''))

      jest
        .spyOn(CatalogItemCategoryService, 'getCategories')
        .mockResolvedValue([])

      await handler(req, res)
      expect(res.statusCode).toBe(400)
    })
  })

  describe('Creating an item', function () {
    it('should return 200 status and created item on update success', async function () {
      const req = createRequest<NextApiRequest>({
        method: 'POST',
      })
      const res = createResponse<NextApiResponse>()

      jest
        .spyOn(CatalogItemService, 'createItem')
        .mockRejectedValue(new Error(''))

      await handler(req, res)
      expect(res.statusCode).toBe(400)
    })
  })
})
