import { createRequest, createResponse } from 'node-mocks-http'
import handler from 'src/pages/api/catalog-item'
import { generateObjectId, mockAuthKo } from 'tests-server/helpers'
import type { NextApiRequest, NextApiResponse } from 'next/types'

describe('catalog-item/[...slug] api - unauthenticated', function () {
  beforeAll(function () {
    mockAuthKo()
  })

  describe('Updating items order', function () {
    it('status should be 401 when unauthenticated', async function () {
      const req = createRequest<NextApiRequest>({
        method: 'PUT',
        query: {
          slug: ['order'],
        },
      })
      const res = createResponse<NextApiResponse>()

      await handler(req, res)
      expect(res.statusCode).toBe(401)
    })
  })

  describe('Updating an item', function () {
    it('status should be 401 when unauthenticated', async function () {
      const req = createRequest<NextApiRequest>({
        method: 'PUT',
        query: {
          slug: [generateObjectId()],
        },
      })
      const res = createResponse<NextApiResponse>()

      await handler(req, res)
      expect(res.statusCode).toBe(401)
    })
  })

  describe('Setting item main file', function () {
    it('status should be 401 when unauthenticated', async function () {
      const req = createRequest<NextApiRequest>({
        method: 'PUT',
        query: {
          slug: [generateObjectId(), 'main', 'randomFileName.jpg'],
        },
      })
      const res = createResponse<NextApiResponse>()

      await handler(req, res)
      expect(res.statusCode).toBe(401)
    })
  })

  describe('Deleting an item', function () {
    it('status should be 401 when unauthenticated', async function () {
      const req = createRequest<NextApiRequest>({
        method: 'DELETE',
        query: {
          slug: [generateObjectId()],
        },
      })
      const res = createResponse<NextApiResponse>()

      await handler(req, res)
      expect(res.statusCode).toBe(401)
    })
  })

  describe('Deleting an item file', function () {
    it('status should be 401 when unauthenticated', async function () {
      const req = createRequest<NextApiRequest>({
        method: 'DELETE',
        query: {
          slug: [generateObjectId(), 'file', 'randomFileName.jpg'],
        },
      })
      const res = createResponse<NextApiResponse>()

      await handler(req, res)
      expect(res.statusCode).toBe(401)
    })
  })
})
