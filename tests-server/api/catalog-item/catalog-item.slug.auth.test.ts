import type { RequestMethod } from 'node-mocks-http'
import { createRequest, createResponse } from 'node-mocks-http'
import { handler } from 'src/pages/api/catalog-item/[...slug]'
import * as CatalogItemService from 'src/services/CatalogItemService'
import * as CatalogService from 'src/services/CatalogService'
import { generateObjectId, mockAuthOk } from 'tests-server/helpers'
import type { NextApiRequest, NextApiResponse } from 'next/types'

describe('catalog-item/[...slug] api - authenticated', function () {
  beforeAll(async function () {
    mockAuthOk()
  })

  it('status should be 405 for unsupported HTTP methods ', async function () {
    const unsupportedHttpMethods: RequestMethod[] = ['OPTIONS', 'PATCH']
    for (const method of unsupportedHttpMethods) {
      const req = createRequest<NextApiRequest>({ method })
      const res = createResponse<NextApiResponse>()

      await handler(req, res)

      expect(res.statusCode).toBe(405)
    }
  })

  describe('Getting visible items', function () {
    it('status should be 400 in case of service failure', async function () {
      const req = createRequest<NextApiRequest>({
        method: 'GET',
        query: {
          slug: ['visible'],
        },
      })
      const res = createResponse<NextApiResponse>()

      jest
        .spyOn(CatalogService, 'getCatalogData')
        .mockRejectedValue(new Error())

      await handler(req, res)
      expect(res.statusCode).toBe(400)
    })
  })

  describe('Updating items order', function () {
    it('status should be 400 in case of service failure', async function () {
      const req = createRequest<NextApiRequest>({
        method: 'PUT',
        query: {
          slug: ['order'],
        },
      })
      const res = createResponse<NextApiResponse>()

      jest
        .spyOn(CatalogItemService, 'updateOrder')
        .mockRejectedValue(new Error())

      await handler(req, res)
      expect(res.statusCode).toBe(400)
    })
  })

  describe('Updating an item', function () {
    it('status should be 400 in case of service failure', async function () {
      const req = createRequest<NextApiRequest>({
        method: 'PUT',
        query: {
          slug: [generateObjectId()],
        },
      })
      const res = createResponse<NextApiResponse>()

      jest
        .spyOn(CatalogItemService, 'updateItem')
        .mockRejectedValue(new Error())

      await handler(req, res)
      expect(res.statusCode).toBe(400)
    })
  })

  describe('Setting item main file', function () {
    it('status should be 400 in case of service failure', async function () {
      const req = createRequest<NextApiRequest>({
        method: 'PUT',
        query: {
          slug: [generateObjectId(), 'main', 'randomFileName.jpg'],
        },
      })
      const res = createResponse<NextApiResponse>()

      jest
        .spyOn(CatalogItemService, 'setMainFile')
        .mockRejectedValue(new Error())

      await handler(req, res)
      expect(res.statusCode).toBe(400)
    })
  })

  describe('Deleting an item', function () {
    it('status should be 400 in case of service failure', async function () {
      const req = createRequest<NextApiRequest>({
        method: 'DELETE',
        query: {
          slug: [generateObjectId()],
        },
      })
      const res = createResponse<NextApiResponse>()

      jest
        .spyOn(CatalogItemService, 'deleteItem')
        .mockRejectedValue(new Error())

      await handler(req, res)
      expect(res.statusCode).toBe(400)
    })
  })

  describe('Deleting an item file', function () {
    it('status should be 400 in case of service failure', async function () {
      const req = createRequest<NextApiRequest>({
        method: 'DELETE',
        query: {
          slug: [generateObjectId(), 'file', 'randomFileName.jpg'],
        },
      })
      const res = createResponse<NextApiResponse>()

      jest
        .spyOn(CatalogItemService, 'deleteItemFile')
        .mockRejectedValue(new Error())

      await handler(req, res)
      expect(res.statusCode).toBe(400)
    })
  })
})
