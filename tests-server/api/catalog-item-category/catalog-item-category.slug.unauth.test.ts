import { createRequest, createResponse } from 'node-mocks-http'
import handler from 'src/pages/api/catalog-item-category'
import { mockAuthKo } from 'tests-server/helpers'
import type { NextApiRequest, NextApiResponse } from 'next/types'

describe('catalog-item-category/[...slug] api - unauthenticated', function () {
  beforeAll(function () {
    mockAuthKo()
  })

  it('PUT - should result in 401 status', async function () {
    const req = createRequest<NextApiRequest>({ method: 'PUT' })
    const res = createResponse<NextApiResponse>()

    await handler(req, res)
    expect(res.statusCode).toBe(401)
  })

  it('DELETE - should result in 401 status', async function () {
    const req = createRequest<NextApiRequest>({ method: 'DELETE' })
    const res = createResponse<NextApiResponse>()

    await handler(req, res)
    expect(res.statusCode).toBe(401)
  })
})
