import type { RequestMethod } from 'node-mocks-http'
import { createRequest, createResponse } from 'node-mocks-http'
import { handler } from 'src/pages/api/catalog-item-category/[...slug]'
import * as CatalogItemCategoryService from 'src/services/CatalogItemCategoryService'
import { generateObjectId, mockAuthOk } from 'tests-server/helpers'
import type { NextApiRequest, NextApiResponse } from 'next/types'

describe('catalog-item-category/[...slug] api - authenticated', function () {
  beforeAll(async function () {
    mockAuthOk()
  })

  it('Calling API using unsupported HTTP methods should result in 405 status', async function () {
    const unsupportedHttpMethods: RequestMethod[] = ['PATCH']
    for (const method of unsupportedHttpMethods) {
      const req = createRequest<NextApiRequest>({ method })
      const res = createResponse<NextApiResponse>()

      await handler(req, res)

      expect(res.statusCode).toBe(405)
    }
  })

  describe('Updating catalog item categories order', function () {
    it('Should update catalog item categories order', async function () {
      const req = createRequest<NextApiRequest>({
        method: 'PUT',
        query: {
          slug: ['order'],
        },
      })
      const res = createResponse<NextApiResponse>()

      jest
        .spyOn(CatalogItemCategoryService, 'updateOrder')
        .mockRejectedValue(new Error(''))

      await handler(req, res)
      expect(res.statusCode).toBe(400)
    })
  })

  describe('Updating a catalog item category', function () {
    it('should get 400 status when slug is missing', async function () {
      const req = createRequest<NextApiRequest>({
        method: 'PUT',
        query: {
          slug: [generateObjectId()],
        },
      })
      const res = createResponse<NextApiResponse>()

      jest
        .spyOn(CatalogItemCategoryService, 'updateItemCategory')
        .mockRejectedValue(new Error(''))

      await handler(req, res)
      expect(res.statusCode).toBe(400)
    })
  })

  describe('Deleting a catalog item category', function () {
    it('should result in 400 status if _id parameter is missing', async function () {
      const req = createRequest<NextApiRequest>({
        method: 'DELETE',
        query: {
          slug: [generateObjectId()],
        },
      })
      const res = createResponse<NextApiResponse>()

      jest
        .spyOn(CatalogItemCategoryService, 'deleteItemCategory')
        .mockRejectedValue(new Error(''))

      await handler(req, res)
      expect(res.statusCode).toBe(400)
    })
  })
})
