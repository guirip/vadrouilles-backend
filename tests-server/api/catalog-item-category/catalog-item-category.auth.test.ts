import type { RequestMethod } from 'node-mocks-http'
import { createRequest, createResponse } from 'node-mocks-http'
import { handler } from 'src/pages/api/catalog-item-category'
import * as CatalogItemCategoryService from 'src/services/CatalogItemCategoryService'
import { mockAuthOk } from 'tests-server/helpers'
import type { NextApiRequest, NextApiResponse } from 'next/types'

describe('catalog-item-category api - authenticated', function () {
  beforeAll(async function () {
    mockAuthOk()
  })

  it('Calling API using unsupported HTTP methods should result in 405 status', async function () {
    const unsupportedHttpMethods: RequestMethod[] = ['PATCH']
    for (const method of unsupportedHttpMethods) {
      const req = createRequest<NextApiRequest>({ method })
      const res = createResponse<NextApiResponse>()

      await handler(req, res)

      expect(res.statusCode).toBe(405)
    }
  })

  describe('Fetching catalog item categories', function () {
    it('status should be 400 in case of service failure', async function () {
      const req = createRequest<NextApiRequest>({
        method: 'GET',
      })
      const res = createResponse<NextApiResponse>()

      jest
        .spyOn(CatalogItemCategoryService, 'getCategories')
        .mockRejectedValue(new Error(''))

      await handler(req, res)
      expect(res.statusCode).toBe(400)
    })
  })

  describe('Creating a catalog item category', function () {
    it('status should be 400 in case of service failure', async function () {
      const req = createRequest<NextApiRequest>({
        method: 'POST',
      })
      const res = createResponse<NextApiResponse>()

      jest
        .spyOn(CatalogItemCategoryService, 'createItemCategory')
        .mockRejectedValue(new Error(''))

      await handler(req, res)
      expect(res.statusCode).toBe(400)
    })
  })
})
