import { createRequest, createResponse } from 'node-mocks-http'
import handler from 'src/pages/api/catalog-item-category'
import { mockAuthKo } from 'tests-server/helpers'
import type { NextApiRequest, NextApiResponse } from 'next/types'

describe('catalog-item-category api - unauthenticated', function () {
  beforeAll(function () {
    mockAuthKo()
  })

  it('GET - should result in 401 status', async function () {
    const req = createRequest<NextApiRequest>({ method: 'GET' })
    const res = createResponse<NextApiResponse>()

    await handler(req, res)
    expect(res.statusCode).toBe(401)
  })

  it('POST - should result in 401 status', async function () {
    const req = createRequest<NextApiRequest>({ method: 'POST' })
    const res = createResponse<NextApiResponse>()

    await handler(req, res)
    expect(res.statusCode).toBe(401)
  })
})
