import { createRequest, createResponse } from 'node-mocks-http'
import type { NextApiRequest, NextApiResponse } from 'next/types'
import { TextPageModel } from 'src/models/text/TextPage'
import { ContentCategory } from 'src/models/common/ContentCategory'
import { handler } from 'src/pages/api/text/page'
import { connect, disconnect } from 'src/services/DbService'
import { clearCollections, mockAuthOk } from 'tests-server/helpers'
import {
  createPageWithNoVisibleChapter,
  createPageWithAChapterNotHavingLocalizedTexts,
  createPageWithChaptersAndLocalizedTexts,
  createDummyPage,
} from 'tests-server/text-data-helpers'

function expect404(res) {
  expect(res.statusCode).toBe(404)

  const data = res._getData()
  expect(data).toBe('page not found')
}

describe('text/page api - authenticated', function () {
  let pageWithNoChapters
  let notVisiblePage
  let pageWithNoVisibleChapter
  let pageWithAChapterNotHavingLocalizedTexts
  let pageWithChaptersAndLocalizedTexts

  beforeAll(async function () {
    await connect()
    await clearCollections()
    mockAuthOk()

    pageWithNoChapters = await createDummyPage()
    notVisiblePage = await createDummyPage({ visible: false })
    pageWithNoVisibleChapter = await createPageWithNoVisibleChapter()
    pageWithAChapterNotHavingLocalizedTexts =
      await createPageWithAChapterNotHavingLocalizedTexts()
    pageWithChaptersAndLocalizedTexts =
      await createPageWithChaptersAndLocalizedTexts()
  })

  afterAll(async function () {
    await clearCollections()
    await disconnect()
  })

  describe('HTTP GET', function () {
    const method = 'GET'

    it('should get all pages', async function () {
      const req = createRequest<NextApiRequest>({
        method,
        query: { visibility: 'all', category: ContentCategory.NATURE },
      })
      const res = createResponse<NextApiResponse>()

      await handler(req, res)

      expect(res.statusCode).toBe(200)

      const pages = res._getJSONData()
      expect(Array.isArray(pages)).toBeTruthy()
      expect(pages.length).toBe(await TextPageModel.countDocuments())
    })

    it('should get only visible pages', async function () {
      const req = createRequest<NextApiRequest>({
        method,
        query: { category: ContentCategory.NATURE },
      })
      const res = createResponse<NextApiResponse>()

      await handler(req, res)

      expect(res.statusCode).toBe(200)

      const pages = res._getJSONData()
      expect(Array.isArray(pages)).toBeTruthy()
      expect(pages.length).toBe(4)
    })

    it('should get one page by slug', async function () {
      const req = createRequest<NextApiRequest>({
        method,
        query: {
          _slug: [pageWithChaptersAndLocalizedTexts.slug],
        },
      })
      const res = createResponse<NextApiResponse>()

      await handler(req, res)

      expect(res.statusCode).toBe(200)

      const page = res._getJSONData()
      expect(page).toBeTruthy()
      expect(page._id.toString()).toBe(
        pageWithChaptersAndLocalizedTexts._id.toString()
      )
      expect(page.slug).toBe(pageWithChaptersAndLocalizedTexts.slug)
      expect(Array.isArray(page.chapters)).toBeTruthy()
    })

    it('should not be able to get a non visible page', async function () {
      const req = createRequest<NextApiRequest>({
        method,
        query: {
          slug: ['page', notVisiblePage._id.toString()],
        },
      })
      const res = createResponse<NextApiResponse>()

      await handler(req, res)
      expect404(res)
    })

    it('should not be able to get a page not having any chapter', async function () {
      const req = createRequest<NextApiRequest>({
        method,
        query: {
          slug: ['page', pageWithNoChapters._id.toString()],
        },
      })
      const res = createResponse<NextApiResponse>()

      await handler(req, res)
      expect404(res)
    })

    it('should not be able to get a page having no visible chapter', async function () {
      const req = createRequest<NextApiRequest>({
        method,
        query: {
          slug: ['page', pageWithNoVisibleChapter._id.toString()],
        },
      })
      const res = createResponse<NextApiResponse>()

      await handler(req, res)
      expect404(res)
    })

    it('should not be able to get a page where chapters have no localizedTexts', async function () {
      const req = createRequest<NextApiRequest>({
        method,
        query: {
          slug: [
            'page',
            pageWithAChapterNotHavingLocalizedTexts._id.toString(),
          ],
        },
      })
      const res = createResponse<NextApiResponse>()

      await handler(req, res)
      expect404(res)
    })

    it('chapters and localizedTexts should be populated', async function () {
      const req = createRequest<NextApiRequest>({
        method,
        query: {
          _slug: pageWithChaptersAndLocalizedTexts.slug,
        },
      })
      const res = createResponse<NextApiResponse>()

      await handler(req, res)

      expect(res.statusCode).toBe(200)

      const page = res._getJSONData()

      expect(page).toBeTruthy()
      expect(page._id.toString()).toBe(
        pageWithChaptersAndLocalizedTexts._id.toString()
      )
      expect(page.slug).toBe(pageWithChaptersAndLocalizedTexts.slug)

      expect(Array.isArray(page.chapters)).toBeTruthy()
      expect(page.chapters.length).toBe(2)

      const [chapter] = page.chapters
      expect(typeof chapter).toBe('object')
      expect(Array.isArray(chapter.localizedTexts)).toBeTruthy()

      const [localizedText] = chapter.localizedTexts
      expect(typeof localizedText.title).toBe('string')
      expect(typeof localizedText.lang).toBe('string')
      expect(localizedText.content.blocks).not.toBeDefined()
    })
  })
})
