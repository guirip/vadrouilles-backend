import { createRequest, createResponse } from 'node-mocks-http'
import { handler } from 'src/pages/api/text/chapter/[...slug]'
import { connect, disconnect } from 'src/services/DbService'
import * as ChapterService from 'src/services/TextService.chapter'
import {
  clearCollections,
  generateObjectId,
  mockAuthOk,
} from 'tests-server/helpers'
import type { NextApiRequest, NextApiResponse } from 'next/types'

describe('text/chapter/[...slug] api - authenticated', function () {
  beforeAll(async function () {
    await connect()
    mockAuthOk()
  })

  afterAll(async function () {
    await clearCollections()
    await disconnect()
  })

  describe('HTTP GET /text/chapter/:id', function () {
    it('status should be 405 when chapterId is invalid', async function () {
      const req = createRequest<NextApiRequest>({
        query: {
          slug: ['random'],
        },
        method: 'GET',
      })
      const res = createResponse<NextApiResponse>()

      await handler(req, res)
      expect(res.statusCode).toBe(405)
    })

    it('status should be 404 when chapterId is not found', async function () {
      const req = createRequest<NextApiRequest>({
        query: {
          slug: [generateObjectId()],
        },
        method: 'GET',
      })
      const res = createResponse<NextApiResponse>()

      await handler(req, res)
      expect(res.statusCode).toBe(404)
    })
  })

  describe('HTTP POST /text/chapter/:id/duplicate/:textPageId', function () {
    it('Status should be 400 in case of service failure', async function () {
      const req = createRequest<NextApiRequest>({
        query: {
          slug: [generateObjectId(), 'duplicate', generateObjectId()],
        },
        method: 'POST',
      })
      const res = createResponse<NextApiResponse>()

      jest
        .spyOn(ChapterService, 'duplicateChapter')
        .mockRejectedValue(new Error(''))

      await handler(req, res)
      expect(res.statusCode).toBe(400)
    })
  })

  describe('HTTP PUT /text/chapter/order', function () {
    it('status should be 400 in case of service failure', async function () {
      const req = createRequest<NextApiRequest>({
        query: {
          slug: ['order'],
        },
        method: 'PUT',
      })
      const res = createResponse<NextApiResponse>()

      jest.spyOn(ChapterService, 'updateOrder').mockRejectedValue(new Error(''))

      await handler(req, res)
      expect(res.statusCode).toBe(400)
    })
  })

  describe('HTTP PUT /text/chapter/:id', function () {
    it('status should be 405 when chapterId is invalid', async function () {
      const req = createRequest<NextApiRequest>({
        query: {
          slug: ['random'],
        },
        method: 'PUT',
      })
      const res = createResponse<NextApiResponse>()

      await handler(req, res)
      expect(res.statusCode).toBe(405)
    })
  })

  describe('HTTP DELETE /text/chapter/:id', function () {
    it('status should be 405 when chapterId is invalid', async function () {
      const req = createRequest<NextApiRequest>({
        query: {
          slug: ['random'],
        },
        method: 'DELETE',
      })
      const res = createResponse<NextApiResponse>()

      await handler(req, res)
      expect(res.statusCode).toBe(405)
    })

    it('status should be 400 in case of service failure', async function () {
      const req = createRequest<NextApiRequest>({
        query: {
          slug: [generateObjectId()],
        },
        method: 'DELETE',
      })
      const res = createResponse<NextApiResponse>()

      jest
        .spyOn(ChapterService, 'deleteChapter')
        .mockRejectedValue(new Error(''))

      await handler(req, res)
      expect(res.statusCode).toBe(400)
      expect(res._getData()).toMatch(/^Failed/)
    })
  })
})
