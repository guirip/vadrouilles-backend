import { createRequest, createResponse } from 'node-mocks-http'
import type { NextApiRequest, NextApiResponse } from 'next/types'
import { handler } from 'src/pages/api/text/page/[...slug]'
import { connect, disconnect } from 'src/services/DbService'
import * as TextPageService from 'src/services/TextService.page'
import { NotFoundError } from 'src/utils/error'
import { createDummyPage } from 'tests-server/text-data-helpers'
import {
  clearCollections,
  generateObjectId,
  mockAuthOk,
} from 'tests-server/helpers'

describe('text/page/[...slug] api - authenticated', function () {
  beforeAll(async function () {
    await connect()
    mockAuthOk()
  })

  afterAll(async function () {
    await clearCollections()
    await disconnect()
  })

  describe('HTTP GET /text/page/:id/images', function () {
    it('status should be 400 in case of service failure', async function () {
      const req = createRequest<NextApiRequest>({
        query: {
          slug: [generateObjectId(), 'images'],
        },
        method: 'GET',
      })
      const res = createResponse<NextApiResponse>()

      jest
        .spyOn(TextPageService, 'listImageBlocks')
        .mockRejectedValue(new Error(''))

      await handler(req, res)
      expect(res.statusCode).toBe(400)
      expect(res._getData()).toMatch(/^Failed/)
    })
  })

  describe('HTTP PUT /text/page/order', function () {
    it('status should be 400 in case of service failure', async function () {
      const req = createRequest<NextApiRequest>({
        query: {
          slug: ['order'],
        },
        method: 'PUT',
      })
      const res = createResponse<NextApiResponse>()

      jest
        .spyOn(TextPageService, 'updateOrder')
        .mockRejectedValue(new Error(''))

      await handler(req, res)
      expect(res.statusCode).toBe(400)
      expect(res._getData()).toMatch(/^Failed/)
    })
  })

  describe('HTTP PUT /text/page/:id', function () {
    it('status should be 405 when page id is invalid', async function () {
      const req = createRequest<NextApiRequest>({
        query: {
          slug: ['random'],
        },
        method: 'PUT',
      })
      const res = createResponse<NextApiResponse>()

      await handler(req, res)
      expect(res.statusCode).toBe(405)
    })

    it('status should be 404 when page is not found', async function () {
      const req = createRequest<NextApiRequest>({
        query: {
          slug: [generateObjectId()],
        },
        method: 'PUT',
      })
      const res = createResponse<NextApiResponse>()

      await handler(req, res)
      expect(res.statusCode).toBe(404)
    })

    it('status should be 400 in case of service failure', async function () {
      const page = await createDummyPage()
      const req = createRequest<NextApiRequest>({
        query: {
          slug: [page._id.toString()],
        },
        method: 'PUT',
      })
      const res = createResponse<NextApiResponse>()

      jest.spyOn(TextPageService, 'updatePage').mockRejectedValue(new Error(''))

      await handler(req, res)
      expect(res.statusCode).toBe(400)
      expect(res._getData()).toMatch(/^Failed/)
    })
  })

  describe('HTTP DELETE /text/page/:id', function () {
    it('status should be 405 when id is invalid', async function () {
      const req = createRequest<NextApiRequest>({
        query: {
          slug: ['random'],
        },
        method: 'DELETE',
      })
      const res = createResponse<NextApiResponse>()

      await handler(req, res)
      expect(res.statusCode).toBe(405)
    })

    it('status should be 404 when page is not found', async function () {
      const req = createRequest<NextApiRequest>({
        query: {
          slug: [generateObjectId()],
        },
        method: 'DELETE',
      })
      const res = createResponse<NextApiResponse>()

      jest
        .spyOn(TextPageService, 'deletePage')
        .mockRejectedValue(new NotFoundError())

      await handler(req, res)
      expect(res.statusCode).toBe(404)
    })

    it('status should be 400 in case of service failure', async function () {
      const req = createRequest<NextApiRequest>({
        query: {
          slug: [generateObjectId()],
        },
        method: 'DELETE',
      })
      const res = createResponse<NextApiResponse>()

      jest.spyOn(TextPageService, 'deletePage').mockRejectedValue(new Error(''))

      await handler(req, res)
      expect(res.statusCode).toBe(400)
      expect(res._getData()).toMatch(/^Failed/)
    })
  })
})
