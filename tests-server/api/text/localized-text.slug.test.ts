import { createRequest, createResponse } from 'node-mocks-http'
import type { NextApiRequest, NextApiResponse } from 'next/types'
import { handler } from 'src/pages/api/text/localized-text/[...slug]'
import { connect, disconnect } from 'src/services/DbService'
import * as LocalizedTextService from 'src/services/TextService.localizedText'
import { NotFoundError } from 'src/utils/error'
import {
  createDummyChapter,
  createDummyLocalizedText,
} from 'tests-server/text-data-helpers'
import {
  clearCollections,
  generateObjectId,
  mockAuthOk,
} from 'tests-server/helpers'

describe('text/localized-text/[...slug] api - authenticated', function () {
  beforeAll(async function () {
    await connect()
    mockAuthOk()
  })

  afterAll(async function () {
    await clearCollections()
    await disconnect()
  })

  /*
  describe('HTTP GET', function () {
    it('should ...', async function () { })
  })
  */

  describe('HTTP POST /text/localized-text/:id/translate-to/:lang', function () {
    it('status should be 405 if id is not valid', async function () {
      const req = createRequest<NextApiRequest>({
        query: {
          slug: ['random'],
        },
        method: 'POST',
      })
      const res = createResponse<NextApiResponse>()

      await handler(req, res)
      expect(res.statusCode).toBe(405)
    })

    it('status should be 405 if path is uncomplete', async function () {
      const req = createRequest<NextApiRequest>({
        query: {
          slug: [generateObjectId()],
        },
        method: 'POST',
      })
      const res = createResponse<NextApiResponse>()

      await handler(req, res)
      expect(res.statusCode).toBe(405)

      if (Array.isArray(req.query.slug)) {
        req.query.slug.push('translate-to')
      }
      await handler(req, res)
      expect(res.statusCode).toBe(405)
    })

    it('status should be 400 if lang is invalid', async function () {
      const req = createRequest<NextApiRequest>({
        query: {
          slug: [generateObjectId(), 'translate-to', 'azerty'],
        },
        method: 'POST',
      })
      const res = createResponse<NextApiResponse>()

      await handler(req, res)
      expect(res.statusCode).toBe(400)
      expect(res._getData()).toMatch(/^Failed/)
    })

    it('status should be 400 in case of service failure', async function () {
      const req = createRequest<NextApiRequest>({
        query: {
          slug: [generateObjectId(), 'translate-to', 'fr'],
        },
        method: 'POST',
      })
      const res = createResponse<NextApiResponse>()

      jest
        .spyOn(LocalizedTextService, 'createTranslation')
        .mockRejectedValue(new Error(''))

      await handler(req, res)
      expect(res.statusCode).toBe(400)
      expect(res._getData()).toMatch(/^Failed/)
    })
  })

  describe('HTTP PUT', function () {
    it('status should be 405 when localized text id is invalid', async function () {
      const req = createRequest<NextApiRequest>({
        query: {
          slug: ['random'],
        },
        method: 'PUT',
      })
      const res = createResponse<NextApiResponse>()

      await handler(req, res)
      expect(res.statusCode).toBe(405)
    })

    it('status should be 404 when localized text is not found', async function () {
      const req = createRequest<NextApiRequest>({
        query: {
          slug: [generateObjectId()],
        },
        method: 'PUT',
      })
      const res = createResponse<NextApiResponse>()

      await handler(req, res)
      expect(res.statusCode).toBe(404)
    })

    it('status should be 400 in case of service failure', async function () {
      const { _id: chapterId } = await createDummyChapter()
      const localizedText = await createDummyLocalizedText(chapterId)
      const req = createRequest<NextApiRequest>({
        query: {
          slug: [localizedText._id.toString()],
        },
        method: 'PUT',
      })
      const res = createResponse<NextApiResponse>()

      jest
        .spyOn(LocalizedTextService, 'updateLocalizedText')
        .mockRejectedValue(new Error(''))

      await handler(req, res)
      expect(res.statusCode).toBe(400)
      expect(res._getData()).toMatch(/^Failed/)
    })
  })

  describe('HTTP DELETE', function () {
    it('status should be 405 when id is invalid', async function () {
      const req = createRequest<NextApiRequest>({
        query: {
          slug: ['random'],
        },
        method: 'DELETE',
      })
      const res = createResponse<NextApiResponse>()

      await handler(req, res)
      expect(res.statusCode).toBe(405)
    })

    it('status should be 404 when localized text is not found', async function () {
      const req = createRequest<NextApiRequest>({
        query: {
          slug: [generateObjectId()],
        },
        method: 'DELETE',
      })
      const res = createResponse<NextApiResponse>()

      jest
        .spyOn(LocalizedTextService, 'deleteLocalizedText')
        .mockRejectedValue(new NotFoundError())

      await handler(req, res)
      expect(res.statusCode).toBe(404)
    })

    it('status should be 400 in case of service failure', async function () {
      const req = createRequest<NextApiRequest>({
        query: {
          slug: [generateObjectId()],
        },
        method: 'DELETE',
      })
      const res = createResponse<NextApiResponse>()

      jest
        .spyOn(LocalizedTextService, 'deleteLocalizedText')
        .mockRejectedValue(new Error(''))

      await handler(req, res)
      expect(res.statusCode).toBe(400)
      expect(res._getData()).toMatch(/^Failed/)
    })
  })
})
