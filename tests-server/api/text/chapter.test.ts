import { createRequest, createResponse } from 'node-mocks-http'
import type { NextApiRequest, NextApiResponse } from 'next/types'
import { handler } from 'src/pages/api/text/chapter'
import { connect, disconnect } from 'src/services/DbService'
import * as ChapterService from 'src/services/TextService.chapter'
import {
  clearCollections,
  generateObjectId,
  mockAuthOk,
} from 'tests-server/helpers'

describe('text/chapter api - authenticated', function () {
  beforeAll(async function () {
    await connect()
    await clearCollections()
    mockAuthOk()
  })

  afterAll(async function () {
    await clearCollections()
    await disconnect()
  })

  describe('HTTP GET', function () {
    it('status should be 400 when textPageId query param is missing', async function () {
      const req = createRequest<NextApiRequest>({
        query: {},
        method: 'GET',
      })
      const res = createResponse<NextApiResponse>()

      await handler(req, res)
      expect(res.statusCode).toBe(400)
    })

    it('status should be 400 when textPageId query param is invalid', async function () {
      const req = createRequest<NextApiRequest>({
        query: {
          textPageId: 'random',
        },
        method: 'GET',
      })
      const res = createResponse<NextApiResponse>()

      await handler(req, res)
      expect(res.statusCode).toBe(400)
    })

    it('status should be 400 in case of service failure', async function () {
      const req = createRequest<NextApiRequest>({
        query: {
          textPageId: generateObjectId(),
        },
        method: 'GET',
      })
      const res = createResponse<NextApiResponse>()

      jest
        .spyOn(ChapterService, 'getChaptersByPage')
        .mockRejectedValue(new Error(''))

      await handler(req, res)
      expect(res.statusCode).toBe(400)
    })
  })

  describe('HTTP POST', function () {
    it('status should be 400 when textPageId body param is missing', async function () {
      const req = createRequest<NextApiRequest>({
        body: {},
        method: 'POST',
      })
      const res = createResponse<NextApiResponse>()

      await handler(req, res)
      expect(res.statusCode).toBe(400)
      expect(res._getData()).toMatch(/^Missing/)
    })

    it('status should be 400 textPageId body param is invalid', async function () {
      const req = createRequest<NextApiRequest>({
        body: { textPageId: 'random' },
        method: 'POST',
      })
      const res = createResponse<NextApiResponse>()

      await handler(req, res)
      expect(res.statusCode).toBe(400)
      expect(res._getData()).toMatch(/^Invalid/)
    })

    it('status should be 400 in case of service failure', async function () {
      const req = createRequest<NextApiRequest>({
        body: {},
        method: 'POST',
      })
      const res = createResponse<NextApiResponse>()

      jest
        .spyOn(ChapterService, 'createChapter')
        .mockRejectedValue(new Error(''))

      await handler(req, res)
      expect(res.statusCode).toBe(400)
    })
  })
})
