import { createRequest, createResponse } from 'node-mocks-http'
import type { NextApiRequest, NextApiResponse } from 'next/types'
import { handler } from 'src/pages/api/text/localized-text'
import { connect, disconnect } from 'src/services/DbService'
import * as LocalizedTextService from 'src/services/TextService.localizedText'
import { clearCollections, mockAuthOk } from 'tests-server/helpers'

describe('text/localized-text api - authenticated', function () {
  beforeAll(async function () {
    await connect()
    await clearCollections()
    mockAuthOk()
  })

  afterAll(async function () {
    await clearCollections()
    await disconnect()
  })

  describe('HTTP GET', function () {
    it('status should be 400 when both chapterId and chapterSlug query params are missing', async function () {
      const req = createRequest<NextApiRequest>({
        query: {},
        method: 'GET',
      })
      const res = createResponse<NextApiResponse>()

      await handler(req, res)
      expect(res.statusCode).toBe(400)
    })

    it('status should be 400 when chapterId query param is provided and not a valid id', async function () {
      const req = createRequest<NextApiRequest>({
        query: { chapterId: 'whatever' },
        method: 'GET',
      })
      const res = createResponse<NextApiResponse>()

      await handler(req, res)
      expect(res.statusCode).toBe(400)
    })

    it('status should be 404 when chapterSlug query param is provided and not a string', async function () {
      const req = createRequest<NextApiRequest>({
        query: {
          chapterSlug: [],
        },
        method: 'GET',
      })
      const res = createResponse<NextApiResponse>()

      await handler(req, res)
      expect(res.statusCode).toBe(400)
    })

    it('status should be 404 when no localized text is found for given lang', async function () {
      const req = createRequest<NextApiRequest>({
        query: {
          chapterSlug: 'random-slug',
          lang: 'fr',
        },
        method: 'GET',
      })
      const res = createResponse<NextApiResponse>()

      jest
        .spyOn(LocalizedTextService, 'getLocalizedTextsByChapter')
        .mockResolvedValue([])

      await handler(req, res)
      expect(res.statusCode).toBe(404)
    })
  })

  describe('HTTP POST', function () {
    it('status should be 400 when chapterId body param is missing', async function () {
      const req = createRequest<NextApiRequest>({
        body: {},
        method: 'POST',
      })
      const res = createResponse<NextApiResponse>()

      await handler(req, res)
      expect(res.statusCode).toBe(400)
      expect(res._getData()).toMatch(/^Missing/)
    })

    it('status should be 400 in case of service failure', async function () {
      const req = createRequest<NextApiRequest>({
        body: { chapterId: 'random' },
        method: 'POST',
      })
      const res = createResponse<NextApiResponse>()

      jest
        .spyOn(LocalizedTextService, 'createLocalizedText')
        .mockRejectedValue(new Error(''))

      await handler(req, res)
      expect(res.statusCode).toBe(400)
    })
  })
})
