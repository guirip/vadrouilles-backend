import { createRequest, createResponse } from 'node-mocks-http'
import type { NextApiRequest, NextApiResponse } from 'next/types'
import defaultHandler from 'src/pages/api/codif/[...slug]'
import { mockAuthKo, generateObjectId } from 'tests-server/helpers'

describe('codif api - without being authenticated', function () {
  beforeAll(async function () {
    mockAuthKo()
  })

  it('PUT - should result in 401 status', async function () {
    const req = createRequest<NextApiRequest>({
      method: 'PUT',
      query: {
        slug: [generateObjectId()],
      },
    })
    const res = createResponse<NextApiResponse>()

    await defaultHandler(req, res)
    expect(res.statusCode).toBe(401)
  })

  it('DELETE - should result in 401 status', async function () {
    const req = createRequest<NextApiRequest>({
      method: 'DELETE',
      query: {
        slug: [generateObjectId()],
      },
    })
    const res = createResponse<NextApiResponse>()

    await defaultHandler(req, res)
    expect(res.statusCode).toBe(401)
  })
})
