import type { RequestMethod } from 'node-mocks-http'
import { createRequest, createResponse } from 'node-mocks-http'
import { handler } from 'src/pages/api/codif'
import { CodifModel } from 'src/models/Codif'
import { connect, disconnect } from 'src/services/DbService'
import { mockAuthOk } from 'tests-server/helpers'
import type { NextApiRequest, NextApiResponse } from 'next/types'

describe('codif api - authenticated', function () {
  beforeAll(async function () {
    await connect()
    mockAuthOk()
  })

  afterAll(async function () {
    await disconnect()
  })

  it('Calling API using unsupported HTTP methods should result in 405 status', async function () {
    const unsupportedHttpMethods: RequestMethod[] = ['PATCH']
    for (const method of unsupportedHttpMethods) {
      const req = createRequest<NextApiRequest>({ method })
      const res = createResponse<NextApiResponse>()

      await handler(req, res)

      expect(res.statusCode).toBe(405)
    }
  })

  it('Trying to retrieve a codif which is missing should result in 400 status', async function () {
    const req = createRequest<NextApiRequest>({
      method: 'GET',
      query: { name: 'catalog-status' },
    })
    const res = createResponse<NextApiResponse>()

    await handler(req, res)
    expect(res.statusCode).toBe(400)
  })

  it('Trying to retrieve a codif which is not public should result in 400 status', async function () {
    const req = createRequest<NextApiRequest>({
      method: 'GET',
      query: { name: 'dsdfmlmkdskjSQDq' },
    })
    const res = createResponse<NextApiResponse>()

    await handler(req, res)
    expect(res.statusCode).toBe(400)
  })

  describe('Fetching one existing codif', function () {
    const codif = {
      name: 'catalog-status',
      value: 'random',
    }

    beforeEach(async function () {
      await CodifModel.deleteMany({})
      await CodifModel.create(codif)
    })
    afterEach(async function () {
      await CodifModel.deleteMany({})
    })

    it('should result in 200 status with codif returned as json', async function () {
      const req = createRequest<NextApiRequest>({
        method: 'GET',
        query: { name: codif.name },
      })
      const res = createResponse<NextApiResponse>()

      await handler(req, res)

      expect(res.statusCode).toBe(200)
      expect(res._getJSONData().value).toBe(codif.value)
    })
  })
})
