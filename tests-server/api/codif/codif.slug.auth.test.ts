import type { RequestMethod } from 'node-mocks-http'
import { createRequest, createResponse } from 'node-mocks-http'
import { CodifModel, type ICodifLean } from 'src/models/Codif'
import { handler } from 'src/pages/api/codif/[...slug]'
import { connect, disconnect } from 'src/services/DbService'
import { generateObjectId, mockAuthOk } from 'tests-server/helpers'
import type { NextApiRequest, NextApiResponse } from 'next/types'

describe('codif api - authenticated', function () {
  beforeAll(async function () {
    await connect()
    mockAuthOk()
  })

  afterAll(async function () {
    await disconnect()
  })

  it('Calling API using unsupported HTTP methods should result in 405 status', async function () {
    const unsupportedHttpMethods: RequestMethod[] = ['PATCH']
    for (const method of unsupportedHttpMethods) {
      const req = createRequest<NextApiRequest>({
        method,
        query: {
          slug: [generateObjectId()],
        },
      })
      const res = createResponse<NextApiResponse>()

      await handler(req, res)

      expect(res.statusCode).toBe(405)
    }
  })

  describe('Updating a codif', function () {
    const codifName = 'whatever'
    let existingCodif

    beforeEach(async function () {
      await CodifModel.deleteMany({})
      existingCodif = await CodifModel.create({
        name: codifName,
        value: 'oldvalue',
      })
    })

    afterEach(async function () {
      await CodifModel.deleteMany({})
    })

    it('should result in 404 status if codif is not found', async function () {
      const req = createRequest<NextApiRequest>({
        method: 'PUT',
        body: {},
        query: {
          slug: [generateObjectId()],
        },
      })
      const res = createResponse<NextApiResponse>()

      await handler(req, res)
      expect(res.statusCode).toBe(404)
    })

    it('should result in 200 status with updated codif returned as json', async function () {
      const newCodifValue = `I'm new, hey`
      const req = createRequest<NextApiRequest>({
        method: 'PUT',
        body: {
          _id: existingCodif._id,
          name: codifName,
          value: newCodifValue,
        },
        query: {
          slug: [existingCodif._id],
        },
      })
      const res = createResponse<NextApiResponse>()

      await handler(req, res)

      expect(res.statusCode).toBe(200)
      expect(res._getJSONData().value).toBe(newCodifValue)

      // Check DB too
      const codif = await CodifModel.findOne({
        name: codifName,
      }).lean<ICodifLean>()
      expect(codif).not.toBeNull()
      expect(codif?.value).toBe(newCodifValue)
    })
  })

  describe('Deleting a codif', function () {
    let existingCodifs

    beforeAll(async function () {
      await CodifModel.deleteMany({})
      existingCodifs = await CodifModel.create([
        {
          name: 'bidule1',
          value: 'truc1',
        },
        {
          name: 'bidule2',
          value: 'truc2',
        },
      ])
    })

    afterAll(async function () {
      await CodifModel.deleteMany({})
    })

    it('should result in 404 status if codif is not found', async function () {
      const req = createRequest<NextApiRequest>({
        method: 'DELETE',
        query: {
          slug: [generateObjectId()],
        },
      })
      const res = createResponse<NextApiResponse>()

      await handler(req, res)

      expect(res.statusCode).toBe(404)
    })

    it('should result in 200 status when codif is found then deleted', async function () {
      const req = createRequest<NextApiRequest>({
        method: 'DELETE',
        query: {
          slug: [existingCodifs[0]._id],
        },
      })
      const res = createResponse<NextApiResponse>()

      await handler(req, res)

      expect(res.statusCode).toBe(200)

      const codifsFromDb = await CodifModel.find({}).lean<ICodifLean[]>()
      expect(codifsFromDb.length).toBe(1)
      expect(codifsFromDb[0].name).toBe(existingCodifs[1].name)
    })
  })
})
