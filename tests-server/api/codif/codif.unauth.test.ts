import { Types } from 'mongoose'
import { createRequest, createResponse } from 'node-mocks-http'
import type { ICodif } from 'src/models/Codif'
import { CodifModel } from 'src/models/Codif'
import { CodifType, NON_PUBLIC_CODIFS_NAME } from 'src/models/Codif.type'
import { handler } from 'src/pages/api/codif'
import { connect, disconnect } from 'src/services/DbService'
import type { NextApiRequest, NextApiResponse } from 'next/types'
import { mockAuthKo } from 'tests-server/helpers'

describe('codif api - without being authenticated', function () {
  beforeAll(async function () {
    mockAuthKo()
  })

  describe('GET - existing codif(s) without being authenticated should be fine', function () {
    const catalogStatusCodif: ICodif = {
      _id: new Types.ObjectId(),
      type: CodifType.String,
      name: 'catalog-status',
      value: true,
    }
    const catalogNoticeCodif: ICodif = {
      _id: new Types.ObjectId(),
      type: CodifType.String,
      name: 'catalog-notice',
      value: '30% discount during december !',
    }
    const nonPublicCodif: ICodif = {
      _id: new Types.ObjectId(),
      type: CodifType.String,
      name: NON_PUBLIC_CODIFS_NAME[0],
      value: 'whatever',
    }
    const codifs: ICodif[] = [
      catalogStatusCodif,
      catalogNoticeCodif,
      nonPublicCodif,
    ]

    beforeAll(async function () {
      await connect()
      await CodifModel.deleteMany({})
      await CodifModel.create(codifs)
    })

    afterAll(async function () {
      await CodifModel.deleteMany({})
      await disconnect()
    })

    it('case of single codif', async function () {
      const req = createRequest<NextApiRequest>({
        method: 'GET',
        query: { name: codifs[0].name },
      })
      const res = createResponse<NextApiResponse>()

      await handler(req, res)
      expect(res.statusCode).toBe(200)
      expect(res._getJSONData().value).toBe(codifs[0].value)
    })

    it('case of multiple codifs', async function () {
      const req = createRequest<NextApiRequest>({
        method: 'GET',
        query: { name: codifs.map((c) => c.name).join(',') },
      })
      const res = createResponse<NextApiResponse>()

      await handler(req, res)
      expect(res.statusCode).toBe(200)
      const data = res._getJSONData()
      expect(Array.isArray(data)).toBeTruthy()
      expect(data.length).toBe(2) // NON_PUBLIC_CODIFS_NAME shall not be returned

      const findCodif = (name: ICodif['name']) =>
        codifs.find((c) => c.name === name)
      function checkCodif(codif: ICodif) {
        const returnedCodif = findCodif(codif.name)
        expect(returnedCodif).toBeDefined()
        expect(returnedCodif).toHaveProperty('value', codif.value)
      }
      checkCodif(catalogStatusCodif)
      checkCodif(catalogNoticeCodif)
    })
  })

  it('GET - without `name` query parameter and without being authenticated should result in 401 status', async function () {
    const req = createRequest<NextApiRequest>({
      method: 'GET',
      query: {},
    })
    const res = createResponse<NextApiResponse>()

    await handler(req, res)
    expect(res.statusCode).toBe(400)
  })

  it('POST - should result in 401 status', async function () {
    const req = createRequest<NextApiRequest>({ method: 'POST' })
    const res = createResponse<NextApiResponse>()

    await handler(req, res)
    expect(res.statusCode).toBe(401)
  })
})
