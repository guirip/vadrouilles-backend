import type { HydratedDocument, Types } from 'mongoose'

import { ContentCategory } from 'src/models/common/ContentCategory'
import { Lang } from 'src/models/common/Lang'
import type {
  IBlock,
  IDelimiterBlock,
  IHeaderBlock,
  IImageBlock,
  IListBlock,
  IParagraphBlock,
  ISpacerBlock,
} from 'src/models/text/TextBlock.type'
import { BlockType } from 'src/models/text/TextBlock.type'
import type { ITextPage } from 'src/models/text/TextPage'
import { createChapter } from 'src/services/TextService.chapter'
import { createLocalizedText } from 'src/services/TextService.localizedText'
import { createPage } from 'src/services/TextService.page'
import { getRandomNumber } from './helpers'

export const createDummyPage = (data: Partial<ITextPage> = {}) =>
  createPage({
    slug: data.slug ?? `page-${getRandomNumber()}`,
    title: data.title ?? { fr: 'title fr', en: 'title en' },
    subtitle: data.subtitle ?? { fr: '', en: '' },
    category: data.category ?? ContentCategory.NATURE,
    chapters: data.chapters ?? [],
    visible: data.visible ?? true,
  })

export async function createDummyChapter(page?: HydratedDocument<ITextPage>) {
  return createChapter((page ?? (await createDummyPage()))._id.toString(), {
    slug: `chapter-${getRandomNumber()}-${Date.now()}`,
  })
}

export async function createDummyLocalizedText(chapterId: Types.ObjectId) {
  return createLocalizedText(chapterId, {
    title: `random title ${getRandomNumber()}`,
    lang: Lang.fr,
  })
}

function generateRandomBlocks(amount) {
  const blocks: IBlock[] = []
  for (let i = 0; i < amount; i++) {
    const random = Math.floor(Math.random() * 7)
    switch (random) {
      case 0:
        blocks.push({
          type: BlockType.Image,
          data: {
            caption: 'whatever image text',
            file: {
              orig: 'orig.jpg',
              mid: 'mid.jpg',
              low: 'low.jpg',
            },
            stretched: false,
            withBackground: false,
            withBorder: false,
          },
        } as IImageBlock)
        break

      case 1:
        blocks.push({
          type: BlockType.Header,
          data: {
            text: 'whatever header',
            level: 3,
          },
        } as IHeaderBlock)
        break

      case 2:
        blocks.push({
          type: BlockType.List,
          data: {
            items: [
              { content: 'eggs', items: [] },
              { content: 'flour', items: [] },
              { content: 'milk', items: [] },
            ],
            style: 'ordered',
          },
        } as IListBlock)
        break

      case 3:
        blocks.push({
          type: BlockType.Delimiter,
        } as IDelimiterBlock)
        break

      case 4:
        blocks.push({
          type: BlockType.Spacer,
          data: {
            value: 1.2,
          },
        } as ISpacerBlock)
        break

      default:
        blocks.push({
          type: BlockType.Paragraph,
          data: {
            text: 'whatever paragraph content',
          },
        } as IParagraphBlock)
    }
  }
  return blocks
}

export async function createNDummyLocalizedText(
  chapterId: Types.ObjectId,
  amount: number
) {
  return await Promise.all(
    Array(amount)
      .fill(null)
      .map(() =>
        createLocalizedText(chapterId, {
          lang: Lang.fr,
          title: 'dummy localized content',
          content: {
            time: Date.now() - Math.floor(Math.random() * 10000),
            version: '1.0.0',
            blocks: generateRandomBlocks(Math.floor(Math.random() * 4)),
          },
        })
      )
  )
}

export async function createPageWithNoVisibleChapter() {
  const page = await createDummyPage()
  await createChapter(page._id, {
    slug: `chapter-${getRandomNumber()}`,
    visible: false,
  })
  return page
}

export async function createPageWithAChapterNotHavingLocalizedTexts() {
  const page = await createDummyPage()
  await createChapter(page._id, {
    slug: `chapter-${getRandomNumber()}`,
    visible: true,
  })
  return page
}

export async function createPageWithChaptersAndLocalizedTexts() {
  const page = await createDummyPage()

  const chapter1 = await createChapter(page._id, {
    slug: `chapter-${getRandomNumber()}`,
    visible: true,
  })
  await createNDummyLocalizedText(chapter1._id, 2)

  const chapter2 = await createChapter(page._id, {
    slug: `chapter-${getRandomNumber()}`,
    visible: true,
  })
  await createNDummyLocalizedText(chapter2._id, 1)

  return page
}
