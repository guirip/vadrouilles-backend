import type { ICatalogItem } from 'src/models/CatalogItem'
import { CatalogItemModel } from 'src/models/CatalogItem'
import { ITEM_DEFAULT_STATUS } from 'src/models/CatalogItem.type'
import type { ICatalogItemCategory } from 'src/models/CatalogItemCategory'
import { generateObjectId } from './helpers'

const MINIMUM_GENERATED_ITEMS_COUNT = 5

export async function generateItems(
  catIds: ICatalogItemCategory[],
  amount?: number
) {
  const count =
    amount ?? Math.floor(Math.random() * 10) + MINIMUM_GENERATED_ITEMS_COUNT
  const items: ICatalogItem[] = []
  for (let i = 0; i < count; i++) {
    items.push({
      _id: generateObjectId(),
      title: `item ${i}`,
      price: i * 100,
      status: ITEM_DEFAULT_STATUS,
      order: i,
      visibility: true,
      category: catIds.length > 0 ? catIds[i % 3]._id : undefined,
    })
  }
  return CatalogItemModel.create(items)
}
