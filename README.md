
### Init database

Mongo instance is defined in src/config-[env].js

Insert these initial codifications:

```javascript
db.codifs.save({ "name": "access-pwd", "value": " DEFINE PSSWD HERE " })
db.codifs.save({ "name": "static-url", "value": "/static-files" }) // prod et integration
db.codifs.save({ "name": "static-url", "value": "http://192.168.1.21:8080" }) // dev
db.codifs.save({ "name": "catalog-notice", "value": "" })
```

### Dev env

- `yarn dev` to start development server


### Deploy

- open firewall ports for:
  - integration next.js instance (3020)
  - production next.js instance (3024)
- git clone this repository
- install dependencies
- check configuration for the environment (src/config...)
- create or update `.env` file (at project root) such as:

```bash
# dev
KEY_PATH=
CERT_PATH=

PUBLIC_URL=https://localhost:3000
NEXTAUTH_URL=http://localhost:3000
NEXTAUTH_SECRET= generate one using `pwgen 24`

NODE_ENV=development
PORT=3000

NEXT_PUBLIC_FRONT_BASE_PATH=
NEXT_PUBLIC_BACK_BASE_PATH=/backend

DEEPL_AUTH_KEY= ...
DEEPL_API_DOMAIN= ...

UPLOAD_DEST=/home/guillaume/vadrouilles-backend-uploads
DB_INSTANCE=vadrouilles-backend-dev

# enables @next/bundle-analyzer (see ./next.config.mjs)
ANALYZE_BUNDLE=false
```

```bash
# integration
KEY_PATH=../certs/integration.vadrouilles.fr/privkey.pem
CERT_PATH=../certs/integration.vadrouilles.fr/fullchain.pem

PUBLIC_URL=https://integration.vadrouilles.fr
NEXTAUTH_URL=https://integration.vadrouilles.fr
NEXTAUTH_SECRET= generate one using `pwgen 24`

NODE_ENV=production
PORT=3020

NEXT_PUBLIC_FRONT_BASE_PATH=
NEXT_PUBLIC_BACK_BASE_PATH=/backend

DEEPL_AUTH_KEY= ...
DEEPL_API_DOMAIN= ...

UPLOAD_DEST=/var/www/integration.vadrouilles.fr/static-files
DB_INSTANCE=vadrouilles-backend-integration
```

```bash
# production
KEY_PATH=../certs/vadrouilles.fr/privkey.pem
CERT_PATH=../certs/vadrouilles.fr/fullchain.pem

PUBLIC_URL=https://www.vadrouilles.fr
NEXTAUTH_URL=https://www.vadrouilles.fr
NEXTAUTH_SECRET= generate one using `pwgen 24`

NODE_ENV=production
PORT=3024

NEXT_PUBLIC_FRONT_BASE_PATH=
NEXT_PUBLIC_BACK_BASE_PATH=/backend

DEEPL_AUTH_KEY= ...
DEEPL_API_DOMAIN= ...

UPLOAD_DEST=/var/www/vadrouilles.fr/static-files
DB_INSTANCE=vadrouilles-backend-prod
```

- build (`npm run build`)
- start

```bash
# integration
pm2 start npm --name="vadrouilles integration" -- start
```

```bash
# production
pm2 start npm --name="vadrouilles production" -- start
```

- save pm2 process list `pm2 save`
- NB: on save pm2 outputs a dump in `/home/www/.pm2/dump.pm2`, which can be reused with `pm2 resurrect` command

- configure VirtualHost (ProxyPass+ProxyPassReverse, url rewriting for SPA router, ...) - see sample below
- reload apache
- open your browser and navigate to the app to check if everything is fine


### Note on Node upgrade

- pm2 can struggle to switch on new node version
- pm2 must be reinstalled globally (because global packages are installed in /home/www/.nvm/versions/node/vX.Y.Z/)
- eventually drop apps in pm2 listing and recreate them using commandes mentioned in READMEs


### Apache virtual host

> NB: these apache mods must be enabled:
> - proxy
> - proxy_http


Copy/paste vhosts backups from doc/vhosts

or manually create them following the procedure below.

##### First create a simple http version, such as:

```apache
<VirtualHost *:80>
        ServerName integration.vadrouilles.fr
        DocumentRoot /var/www/integration.vadrouilles.fr

        <Directory /var/www/integration.vadrouilles.fr>
                Options FollowSymLinks
                Require all granted
        </Directory>

        ErrorLog ${APACHE_LOG_DIR}/integration.vadrouilles.fr-error.log
        CustomLog ${APACHE_LOG_DIR}/integration.vadrouilles.fr-access.log combined
</VirtualHost>
```

##### Enable it and test it

##### Generate SSL certificate with certbot

##### Disable the virtual host for http

##### Update the SSL virtual host

See Apache Virtual Hosts samples in [./doc/vhosts](./doc/vhosts)

##### Reload apache and test if everything works fine

### Log analytics

Install goaccess ([documentation](https://goaccess.io/get-started))
> apt-get install goaccess

#### Configure the 'reports' folder

Create the .htpasswd file following this [documentation](https://cwiki.apache.org/confluence/display/HTTPD/PasswordBasicAuth)

Add this to the virtual host managing the domain.

<Directory /var/www/html/reports>
        AuthType Basic
        AuthName "Authentication required"
        AuthUserFile "/etc/htpasswd/.htpasswd"
        Require valid-user

        Order allow,deny
        Allow from all
</Directory>

**important note**: create a `report` user to access the 'reports' folder through apache (passw is classik!REPORT)

#### Update log format in virtual hosts

Rotate when the log file size reaches 10 MB
> CustomLog "|/usr/bin/rotatelogs /var/log/access.log 10M" combined

Reload apache service

#### Setup a cron running this script

See: `scripts/make-vadrouilles-report.sh`

The commands will run every two hours:

```crontab
5 */2 * * * /opt/make-vadrouilles-report.sh >> /opt/goaccess-vadrouilles.log 2>/opt/goaccess-vadrouilles.error.log
```
